<?php
use skewer\base\section\models\ParamsAr;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\build\Design\Zones\Api;
use skewer\components\config\PatchPrototype;
use yii\helpers\StringHelper;
use skewer\build\Page;

/**
 * @class PatchInstall
 */
class PatchInstall extends PatchPrototype{

    public $sDescription = 'Параметры layout:detail';


    public function execute(){

        $aEntities = [
            Page\Articles\Module::getNameModule(),
            Page\News\Module::getNameModule(),
            Page\FAQ\Module::getNameModule(),
            Page\Gallery\Module::getNameModule(),
            Page\CatalogViewer\Module::getNameModule()
        ];

        // Массив id шаблонов
        $aTemplatesId = [];
        foreach ($aEntities as $sEntity) {
            if ( ($iTempId = self::getIdTemplateByModuleName($sEntity)) !== false )
                $aTemplatesId[] = $iTempId;
        }

        foreach ($aTemplatesId as $iTplId) {

            $aSections = Tree::getSubSectionsByTemplate( $iTplId );

            // Создание content:detail для шаблонов
            if ( $aLayouts = self::getLayoutsParamsBySectionId($iTplId, ['content']) ) {
                /** @var ParamsAr $oContentParam */
                $oContentParam = $aLayouts[0];
                self::createSpecLayoutFromDefault($oContentParam);
            }

            // Создание content:detail для конкретных разделов
            $aLayouts = self::getLayoutsParamsBySectionId($aSections, ['content']);
            foreach ($aLayouts as $item)
                self::createSpecLayoutFromDefault($item);


        }

    }

    /**
     * По имени модуля вернет id шаблона
     * @param $sModuleName
     * @return bool|int
     */
    public static function getIdTemplateByModuleName($sModuleName ){

        $aTemplatesId = Tree::getSubSections(\Yii::$app->sections->templates(), true, true);

        $sMainGroup = 'content';

        $aParam = Parameters::getList($aTemplatesId)
            ->group($sMainGroup)
            ->name(Parameters::object)
            ->value($sModuleName)
            ->asArray()
            ->get();


        return ($aParam)
            ? (int) $aParam[0]['parent']
            : false
            ;

    }


    /**
     * Получить layouts по id раздела
     * @param $mSectionId
     * @param array $aInputLayouts - конкретные layout
     * @return array
     */
    public static function getLayoutsParamsBySectionId($mSectionId, array $aInputLayouts = [] ){

        $aLayoutsParams = Parameters::getList($mSectionId)->group(Api::layoutGroupName);

        if (is_numeric($mSectionId))
            $aLayoutsParams->rec();

        $aLayoutsParams = $aLayoutsParams->get();

        $aOut = [];
        foreach ($aLayoutsParams as $item) {

            if ( ( mb_substr($item->name, 0, 1) === '.' ) || ( mb_substr($item->name, -4) === '_tpl' ) )
                continue;

            if (!empty($aInputLayouts) && !in_array($item->name, $aInputLayouts))
                continue;

            $aOut[] = $item;
        }

        return $aOut;
    }

    /**
     * Создать спец. layout
     * @param ParamsAr $oParam
     * @param string $sPreffix
     * @param array $aExcludedLabels
     * @return bool
     */
    public static function createSpecLayoutFromDefault(ParamsAr $oParam, $sPreffix = Api::DETAIL_LAYOUT, $aExcludedLabels = ['staticContent', 'staticContent2'] ){

        $oParam->name .= ':' . $sPreffix;

        // Если такой параметр есть - выходим
        if ( Parameters::getByName($oParam->parent, $oParam->group, $oParam->name) )
            return false;

        $aLabels = StringHelper::explode($oParam->show_val, ',', true, true);
        $oParam->title .= '_' . $sPreffix;
        foreach ($aExcludedLabels as $item) {

            if (in_array($item, $aLabels)) {
                // удалить параметр
                unset($aLabels[array_search($item, $aLabels)]);
                $oParam->show_val = implode(',', $aLabels);
            }

        }

        $oNewParam = Parameters::createParam($oParam->getAttributes(null, ['id']));
        return $oNewParam->save();
    }



}