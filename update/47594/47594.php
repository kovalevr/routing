<?php
use skewer\components\config\PatchPrototype;
use skewer\components\config\UpdateException;

/**
 *
 * @class PatchInstall
 *
 * @author user, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package Updates
 */
class PatchInstall extends PatchPrototype
{

    public $sDescription = 'Обновление до версии 27';

    public $sTargetBuildName    = 'canape';

    public $sCurrentBuildNumber  = '0026m2';

    public $sTargetBuildNumber  = '0027';
    /* -------------------------------- */

    public $sTargetBuildVersion = '';

    public $sTplRootPath = '';

    public $sReleasePath = '';

    public $bUpdateCache = false;

    /**
     * Базовый метод запуска обновления
     * @throws UpdateException
     * @return bool
     */
    public function execute()
    {

        /* Перезаписываем constants */
        if(!APPKEY)
            $this->fail('У площадки отсутствуют ключи!');

        if ( !USECLUSTERBUILD )
            $this->fail( 'Этим патчем нельзя обновить отцепленную площадку' );

        if ( BUILDNUMBER != $this->sCurrentBuildNumber )
            $this->fail( sprintf(
                "Обновить можно только площадку версии [%s], а текущая [%s]",
                $this->sCurrentBuildNumber,
                BUILDNUMBER
            ));

        /** добавляем новый реестр**/
        $this->executeSQLQuery( "INSERT INTO `registry_storage`(`name`, `data`) SELECT 'build_".$this->sTargetBuildName.$this->sTargetBuildNumber."', `data` FROM `registry_storage` WHERE `name`='build_".$this->sTargetBuildName.$this->sCurrentBuildNumber."'");

        /************************ Меняем версию сборки *****************************/

        $this->sTargetBuildVersion = $this->sTargetBuildName.$this->sTargetBuildNumber;

        /* Используем свою сборку либо кластерную */
        $this->sReleasePath = dirname(dirname(RELEASEPATH)).DIRECTORY_SEPARATOR.$this->sTargetBuildNumber.DIRECTORY_SEPARATOR.'skewer'.DIRECTORY_SEPARATOR;
        $this->sTplRootPath = $this->sReleasePath.'build/common/templates/';

        $aData['appKey']         = APPKEY;
        $aData['rootPath']       = ROOTPATH;
        $aData['buildName']      = $this->sTargetBuildName;
        $aData['buildNumber']    = $this->sTargetBuildNumber;
        $aData['releasePath']    = $this->sReleasePath;
        $aData['clusterGateway'] = CLUSTERGATEWAY;
        $aData['USECLUSTERBUILD']= USECLUSTERBUILD;
        
        $this->updateConstants($this->sTplRootPath, $aData);

        /*---------------*/
        $aData = array();

        $aData['buildName']       = $this->sTargetBuildName;
        $aData['inCluster']       = INCLUSTER;
        $aData['buildNumber']     = $this->sTargetBuildNumber;
        $aData['redirectItems']   = array();
        $aData['USECLUSTERBUILD'] = USECLUSTERBUILD;
        $aData['USECLUSTERBUILD'] = USECLUSTERBUILD;

        $this->updateHtaccess($this->sTplRootPath, $aData);

        return true;

   }

}