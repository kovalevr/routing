<?php
use skewer\components\config\PatchPrototype;
use skewer\components\seo;
use yii\helpers\StringHelper;

/**
 * @class PatchInstall
 */
class PatchInstall extends PatchPrototype{

    public $sDescription = 'Разделение поля таблицы seo_templates sid на два';

    public function execute(){

        Yii::$app->db->createCommand()->addColumn('seo_templates', 'alias', 'varchar(255) not null')->execute();
        Yii::$app->db->createCommand()->addColumn('seo_templates', 'extraalias', 'varchar(255) not null')->execute();


        $aTemplates = Yii::$app->db->createCommand('SELECT * FROM `seo_templates`')->queryAll();

        foreach ($aTemplates as $aTemplate) {

            $sAlias = $sExtraAlias = '';

            if ( strpos($aTemplate['sid'], ':') !== false ){
                list($sAlias, $sExtraAlias) = StringHelper::explode($aTemplate['sid'], ':', true, true);

            } else {
                $sAlias = $aTemplate['sid'];

            }

            Yii::$app->db->createCommand()->update('seo_templates',
                ['alias' => $sAlias, 'extraalias' => $sExtraAlias],
                ['id' => $aTemplate['id']]
            )->execute();

        }


        seo\Template::rebuildTable();

    }



}