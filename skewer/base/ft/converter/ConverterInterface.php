<?php

namespace skewer\base\ft\converter;

use skewer\base\ft;

/**
 * Интерфейс для классов преобразования в ft модель и из нее
 * @package skewer\base\ft\formater
 */
interface ConverterInterface {

    /**
     * Преобрзовывает данные в ft модель
     * @param string $sIn входные данные
     * @return ft\Model
     */
    function dataToFtModel( $sIn );

    /**
     * Преобрзовывает данные в ft модель
     * @param ft\Model $oModel модель данных для экспорта
     * @return string
     */
    function ftModelToData(ft\Model $oModel );

}