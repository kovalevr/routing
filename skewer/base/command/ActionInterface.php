<?php
/**
 * Интерфейс работы подчиненного элемента прототипа "Команда" (Command)
 */

namespace skewer\base\command;

interface ActionInterface {

    /**
     * Выполнение команды
     * @throws \Exception
     */
    function execute();

    /**
     * Откат команды
     */
    function rollback();

}
