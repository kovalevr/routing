<?php

namespace skewer\base\section;
use yii\base\ErrorException;

/**
 * Класс для работы с кэшем параметров
 * От же является оперативным хранилицем
 *
 * Сброс происходит при каждой модификации параметров и дерева разделов.
 * В худшем случае мы получим время, сопостовимое с работой без кэша.
 */
class ParamCache {

    /**
     * Массив запрошенных данных
     * Содержит трехуровневый массив
     * [
     *      278 => [ // id раздела
     *          0 => [ // набор параметров
     *              'id' => 2948
     *              'parent' => 5
     *              'group' => g1
     *              'name' => n1
     *              'value' => v51
     *              'title' => title
     *              'access_level' => 0
     *              'show_val' => test
     *          ],
     *          1 => [],
     *          ...
     *      ],
     *      789 => [...]
     * ]
     * @var array[]
     */
    private static $cache = [];

    /**
     * Флаг использования кэша параметров
     * @var bool
     */
    public static $useCache = true;

    /**
     * Отдает массив данных параметров по указанномк разделу
     * Все, с учеотом рекурсивного наследования
     * В случае отсутствия данных выкинет исключение, поэтому следует использовать с методом has
     * @param int $id
     * @return array|mixed
     * @throws ErrorException
     */
    public static function get( $id ) {

        $id = (int)$id;

        if ( !self::has($id) )
            throw new ErrorException('No cache data for ['.$id.'] section');

        return self::$cache[$id];

    }

    /**
     * Проверяет наличие данных по указанному id раздела
     * @param int $id
     * @return bool
     */
    public static function has($id) {
        return isset(self::$cache[$id]);
    }

    /**
     * Устанавливает данные в кэш
     * @param $id
     * @param $data
     */
    public static function set( $id, $data ) {
        self::$cache[$id] = $data;
    }

    /**
     * Сбрасывает кэш
     */
    public static function clear() {
        self::$cache = [];
    }

}