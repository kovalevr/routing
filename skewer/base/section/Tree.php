<?php

namespace skewer\base\section;

use skewer\base\orm\Query;
use skewer\build\Tool\LeftList\Group;
use skewer\components\auth\CurrentAdmin;
use skewer\components\auth\CurrentUser;
use skewer\components\auth\Policy;
use skewer\build\Page\Main;
use skewer\base\section\models\TreeSection;
use yii\helpers\ArrayHelper;
use skewer\components\seo;

class Tree {

    const typeSection = 0; /** тип - раздел */
    const typeDirectory = 1; /** тип - папка */

    const policyUser = 'user';
    const policyAdmin = 'admin';

    /** id для подстановки в набор шаблонов */
    const tplDirId = -1;

    protected static $AvailableSectionCache = [];
    protected static $copyes = [];

    /** @var bool Флаг очистки кэша разделов */
    private static $bClearCache = true;

    /**
     * Получение кешированного списка разделов
     * @param int $iId Id раздела
     * @param bool $bOnlyVisible Только видимый раздел.
     * @return array
     * todo доработать кеш через поле и оформить отдельным объектом
     * todo добавить выборку только разрешенных политикой разделов
     * использовать только для клиенской части
     */
    public static function getCachedSection($iId = 0, $bOnlyVisible = false) {
        static $aSections = null;

        if ($iId and $bOnlyVisible and !isset(self::getVisibleSections()[$iId]))
            return [];

        if ( !$aSections or self::$bClearCache ) {

            $sections = TreeSection::find();

            foreach ( $sections->each() as $section )
                $aSections[$section->id] = $section->getAttributes();

        }
        self::$bClearCache = false;

        // Если раздел ещё не проиндексирован, то проиндексировать
        if ($iId and !isset($aSections[$iId])) {
            $aSections[$iId] = self::getSection($iId, true) ?: [];
            // Обновить кэш видимых разделов
            $aSections[$iId] and self::getVisibleSections(true);
        }

        return $iId ?
            (( isset($aSections[$iId]) and (!$bOnlyVisible or isset(self::getVisibleSections()[$iId])) ) ? $aSections[$iId] : []) :
            ($bOnlyVisible ? array_intersect_key($aSections, self::getVisibleSections()) : $aSections);
    }

    /**
     * Возвращает массив id видимых и существующих разделов. Использует кэш.
     * @param bool $bUpdateCache Обновить кэш?
     * @return array
     */
    public static function &getVisibleSections($bUpdateCache = false) {
        static $aSectionsVisibleIds = null;

        if (($aSectionsVisibleIds === null) or $bUpdateCache)
            $aSectionsVisibleIds = TreeSection::find()
                ->where(['visible' => Visible::$aOpenByLink])
                ->andWhere("link LIKE ''")
                ->indexBy('id')
                ->asArray()
                ->column();

        return $aSectionsVisibleIds;
    }


    /**
     * Получение объекта раздела
     * @param int|string $mId Идентификатор (id/alias)
     * @param bool $bAsArray Флаг вывода результатов как массив, а не объект
     * @param bool $bOnlyVisible Только видимый раздел
     * @return null|TreeSection|array
     */
    public static function getSection( $mId, $bAsArray = false, $bOnlyVisible = false ) {

        if ( is_numeric($mId) )
            $oQuery = TreeSection::find()->where(['id' => $mId]);
        else
            $oQuery = TreeSection::find()->where(['alias' => $mId]);

        if ($bOnlyVisible)
            $oQuery->where(['visible' => Visible::$aOpenByLink, 'link' => '']);

        if ($bAsArray) $oQuery->asArray();

        return $oQuery->one();
    }


    /**
     * Получение набора объектов разделов
     * @param int[] $list Набор идентификаторов
     * @param bool $bAsArray Флаг вывода результатов как массив, а не объект
     * @param bool $bKeepOrder Использовать оригинальный порядок
     * @return array
     * fixme $bKeepOrder - wtf??
     * todo нет проверки на политики
     */
    public static function getSections( $list, $bAsArray = false, $bKeepOrder = false ) {

        $out = [];
        $sections = TreeSection::findAll(['id' => $list]);

        foreach ( $sections as $section )
            $out[$section->id] = $bAsArray ? $section->getAttributes() : $section;

        if ( $bKeepOrder ) {
            $realOut = [];
            foreach ( $list as $key )
                if ( isSet($out[$key]) )
                    $realOut[] = $out[$key];
            return $realOut;
        }

        return $out;
    }


    /**
     * @param $path
     * @param string $tail
     * @param array $denySections
     * @param $bMain - в качестве окончания условия поиска раздела разбор урла до "/'
     * @return int fixme Доработать функцию (переехало из tree->getIdByPath)
     * fixme Доработать функцию (переехало из tree->getIdByPath)
     */
    public static function getSectionByPath( $path, &$tail = '', $denySections = [], $bMain = true ) {

        if ( !$path )
            return 0;

        $curPath = $path;
        $id = 0;
        $i = 0;

        do {

            $oQuery = TreeSection::find()
                ->where([
                    'alias_path' => $curPath,
                    'visible' => [
                        Visible::HIDDEN_FROM_MENU,
                        Visible::VISIBLE,
                        Visible::HIDDEN_NO_INDEX,
                    ]]
            );

            if ($denySections)
                $oQuery->andWhere(['NOT IN', 'id', $denySections]);

            $row = $oQuery->one();

            if ( $row ) {
                $id = $row->id;
            } else {
                $curPath = ( strlen($curPath) >= 2 ) ? substr( $curPath, 0, strrpos($curPath,'/',-2)+1 ) : '/';
            }

            $bConditionExitComplete = false;
            if ( $bMain && ($curPath == '/') )
                break;

        } while ( !$id and $curPath and ++$i<10 );// fixme static::iLevelLimit == 10

        $tail = ($curPath===$path) ? '' : substr( $path, strlen($curPath) );

        return $id;

    }


    /**
     * @param $id
     * @return array
     * todo пересмотреть метод и его применение
     */
    public static function getSectionByParent( $id ) {

        $out = [];

        $list = TreeSection::findAll( ['parent' => $id] );

        if ( $list )
            foreach ( $list as $section )
                $out[] = $section->getAttributes();

        return $out;
    }


    public static function getSectionByAlias( $alias, $parent ) { //@todo смысл в parent, если alias уникален?
        $row = TreeSection::findOne( ['alias' => $alias, 'parent' => $parent] );
        return $row ? $row->id : null;
    }



    public static function getSectionAlias( $id ) {
        $row = TreeSection::findOne( ['id' => $id] );
        return $row ? $row->alias : '';
    }


    /**
     * Значение поля alias_path для раздела $id
     * @param int $id Идентификатор раздела
     * @param bool $bUseCache Флаг использования кешированных данных
     * @param bool $bUseLink Использовать разделы-ссылки
     * @param bool $bOnlyVisible Только видимый раздел
     * @return string
     */
    public static function getSectionAliasPath( $id, $bUseCache = false, $bUseLink = false, $bOnlyVisible = false ) {

        // todo При $id = 0 self::getCachedSection Возвращет список закешированных разделов
        if (!$id) return '';

        $aSection = $bUseCache ? self::getCachedSection($id, $bOnlyVisible) : self::getSection($id, true, $bOnlyVisible);
        if (!$aSection) return '';

        /** Массив для защиты от бесконечного цикла редиректов на разделы */
        $aRedirectIds = [];
        while ($bUseLink and $aSection['link']) {

            // Если редирект на раздел
            if ( ($iRedirectId = (int)trim($aSection['link'], '[]')) and
                 ( ($bUseCache and $aSectionRedirect = self::getCachedSection($iRedirectId, $bOnlyVisible)) or
                   (!$bUseCache and $aSectionRedirect = self::getSection($iRedirectId, true, $bOnlyVisible)) )
               ) {

                // Если возникло зацикливание
                if (isset($aRedirectIds[$iRedirectId]))
                    return $aSection['alias_path'];

                $aRedirectIds[$iRedirectId] = 1;
                $aSection = $aSectionRedirect;
            } else {
                // Иначе редирект на url
                break;
            }
        }

        return ($bUseLink and $aSection['link']) ? $aSection['link'] : ($aSection['alias_path'] ?: '');
    }


    /**
     * Получить заголовок для раздела $id
     * @param int $id Идентификатор раздела
     * @param bool $bUseCache Флаг использования кешированных данных
     * @return string
     */
    public static function getSectionTitle( $id, $bUseCache = false ) {

        if ( $bUseCache ) {
            $sections = self::getCachedSection();
            $val = ArrayHelper::getValue($sections, [$id, 'title']);
            if ( !is_null($val) )
                return $val;
        }

        $row = TreeSection::findOne( ['id' => $id] );
        return $row ? $row->title : '';
    }


    public static function getSectionParent( $id, $bUseCache = false ) {

        if ( $bUseCache ) {
            $sections = self::getCachedSection();
            $val = ArrayHelper::getValue($sections, [$id, 'parent']);
            if ( !is_null($val) )
                return $val;
        }

        $row = TreeSection::findOne( ['id' => $id] );
        return $row ? (int)$row->parent : 0;
    }

    /**
     * @param $iParent
     * @return array
     */
    public static function getAllSubsection($iParent){

        $aSections = [];
        $aParents = [$iParent];

        do {
            $aNewParents = [];
            $bWhile = false;
            foreach (self::getCachedSection() as $key => $value) {
                if (isset($value['parent']) && in_array($value['parent'], $aParents)) {
                    $bWhile = true;
                    $aSections[$key] = $key;
                    $aNewParents[] = $key;
                }
            }
            $aParents = $aNewParents;
        }
        while ($bWhile);

        return $aSections;

    }


    /**
     * @param $id
     * @param bool $bAsArray
     * @param bool $bOnlyKeys
     * @return array|TreeSection[]
     */
    public static function getSubSections( $id, $bAsArray = false, $bOnlyKeys = false ) {

        $list = TreeSection::find()->where(['parent' => $id])->orderBy( 'position' )->all();

        if ( !$bAsArray )
            return $list;

        $out = [];
        /** @var TreeSection $section */
        foreach ( $list as $section )
            if ( $bOnlyKeys )
                $out[] = $section->id;
            else
                $out[$section->id] = $section->getAttributes();

        return $out;
    }


    /**
     * Получение набора родительских разделов
     * @param int $id Идентификатор
     * @param int $stop Идентификатор стоп раздела
     * @param bool $bUseCache
     * @return array
     */
    public static function getSectionParents( $id, $stop = -1, $bUseCache = true ) {

        $out = [];

        $curId = $id; // текущий обрабатываемый раздел

        do {

            // запрос родительского раздела
            $curId = self::getSectionParent( $curId, $bUseCache ); // fixme избавиться от рекурсивного обращения

            // выходим если достигли стоп-вершины
            if ($curId == $stop) break;

            // дополнение выходного массива
            if ( $curId )
                $out[] = $curId;

        } while ( $curId );

        return $out;
    }


    /**
     * Получение заголовка или списка заголовков страниц
     * @param int|int[] $id Иденификатор или список идентификаторов
     * @param bool $bWithSubs Флаг вывода подразделов
     * @param bool $bOnlyVisible Только видимые разделы?
     * @return array|string
     */
    public static function getSectionsTitle( $id, $bWithSubs = false, $bOnlyVisible = false ) {

        if ( $bWithSubs ) {

            $out = [];
            $items = self::getSectionList( $id );

            foreach ( $items as $section )
                if ( $bOnlyVisible and (!in_array($section['visible'], Visible::$aOpenByLink) or $section['link']) )
                    continue;
                else
                    $out[$section['id']] = $section['title'];

            return $out;

        } else if ( is_array( $id ) ) {

            $out = [];
            $items = TreeSection::findAll( ['id' => $id] + ($bOnlyVisible ? ['visible' => Visible::$aOpenByLink, 'link' => ''] : []) );

            /** @var TreeSection $section */
            foreach ( $items as $section )
                $out[$section->id] = $section->title;

            return $out;

        } else {

            $section = self::getSection( $id );

            if ($bOnlyVisible and !$section->hasRealUrl())
                return '';

            return $section ? $section->title : '';
        }

    }


    /**
     * Получение подразделов в виде списка
     * @param int $id Идентификатор раздела корня дерева
     * @param bool|int|string $policy Политика доступа
     * @return array|bool
     * todo заменить $oTree->getSectionsInLine()
     */
    public static function getSectionList( $id, $policy = false ) {

        $fullSections = self::getAvailableSections( $policy, $id );

        if ( isSet( $fullSections['.'] ) ) {
            $out = [$fullSections['.']];
            $add = self::collect( $id, $fullSections, 0, false );
            if ( $add )
                $out = array_merge( $out, $add );
        } else
            $out = self::collect( $id, $fullSections, 0, false );

        return $out;
    }


    /**
     * Получение подразделов в виде дерева
     * @param int $id Идентификатор раздела корня дерева
     * @param bool|int|string $policy Политика доступа
     * @return array|bool
     * todo заменить $oTree->getAllSections()
     */
    public static function getSectionTree( $id, $policy = false ) {

        $fullSections = self::getAvailableSections( $policy );

        return self::collect( $id, $fullSections, 0 );
    }


    /**
     * Получение ветки дерева разделов для публичной части
     * @param int $root Корень ветки
     * @param int $current Текущий (выделенный) раздел
     * @param int $showLvl
     * @return array|bool
     */
    public static function getUserSectionTree( $root, $current = 0, $showLvl = 0 ) {

        $fullSections = self::getAvailableSections( self::policyUser, $current, true );

        $parents = [$current];
        if ( isSet($fullSections['#'][$current]) )
            while ( $current = $fullSections['#'][$current] ) {
                if( !isSet($fullSections['#'][$current]) )
                    break;
                $parents[] = $current;
            }

        $tree = self::collectMarkTree( $root, $fullSections, 0, $parents, $showLvl );

        return $tree;
    }


    /**
     * Получение всех разрешенных политикой разделов
     * ~~~
     * Отдает массив массивов
     *  1. ключ # - int[] - массив int - id разделов, доступных для вывода
     *  2. ключ . - [] - типовой массив с данными текущего раздела
     *  3. ключ int (тиких много) - [][] - массив типовых массивов с резделами,
     *          подчиненными для раздела с id = ключу
     *
     *  Типовой массив имеет структуру
     *     'id' => 278      // int id раздела
     *     'parent' => 277  // int id родительского раздела
     *     'title' => 'FAQ' // str название раздела
     *     'visible' => 1   // int id режима видимости (см класс Visible)
     *     'show' => true   // bool выводить на сайте или нет (true при visible == Visible::VISIBLE )
     *     'link' => ''     // str ссылка с раздела
     * ~~~
     * @param bool $policy Политика доступа
     * @param int $id Выбранный раздел
     * @param bool $bUseCached Кешировать пользовательские выборки
     * @return array
     * @throws \Exception
     */
    protected static function getAvailableSections( $policy = false, $id = 0, $bUseCached = false ) {

        if ( $bUseCached && self::$AvailableSectionCache && $policy == self::policyUser )
            return self::$AvailableSectionCache;

        $out = [];

        $query = TreeSection::find()->orderBy( 'position' );

        if ( $policy ) { // fixme поправить политики
            if ( $policy == self::policyAdmin )
                $sections = CurrentAdmin::getReadableSections();
            elseif ( $policy == self::policyUser )
                $sections = CurrentUser::getReadableSections();
            else
                $sections = ArrayHelper::getValue( Policy::getGroupPolicyData( $policy ), 'read_access', [] );

            $query->where( ['id' => $sections] );
        }


        /** @var TreeSection $section */
        foreach ( $query->each() as $section ) {
            $out[ $section->parent ][] = [
                'id' => $section->id,
                'parent' => $section->parent,
                'title' => $section->title,
                'visible' => $section->visible,
                'show' => in_array($section['visible'], Visible::$aShowInMenu),
                'link' => $section->link,
            ];
            $out['#'][$section->id] = $section->parent;
            if ( $id && $section->id == $id )
                $out[ '.' ] = [
                    'id' => $section->id,
                    'parent' => $section->parent,
                    'title' => $section->title,
                    'visible' => $section->visible,
                    'show' => in_array($section['visible'], Visible::$aShowInMenu),
                    'link' => $section->link,
                ];
        }

        if ( $bUseCached && $policy == self::policyUser )
            self::$AvailableSectionCache = $out;

        return $out;
    }

    /**
     * Сбрасывает текущий кэш
     */
    public static function dropCache() {
        self::$AvailableSectionCache = [];
    }

    public static function collect( $section, &$list, $lvl = 0, $bTree = true ) {

        if ( $lvl > 10 || !isSet( $list[$section] ) )
            return [];

        $out = [];

        foreach ( $list[$section] as $data ) {

            if ( !isSet( $list[$data['id']] ) ) {

                if ( !$bTree )
                    $data['title'] = str_repeat( '-', $lvl + 1 ) . $data['title'];
                else
                    $data['children'] = []; // fixme рудимент от старого формата

                $out[] = $data;

            } elseif ( $bTree ) {

                $data['children'] = self::collect( $data['id'], $list, $lvl + 1, $bTree );
                $out[] = $data;

            } else {

                $data['title'] = str_repeat( '-', $lvl + 1 ) . $data['title'];
                $out[] = $data;
                $out = array_merge( $out, self::collect( $data['id'], $list, $lvl + 1, $bTree ) );

            }

        }

        return $out;
    }


    protected static function collectMarkTree( $section, &$list, $lvl = 0, $selected = [], $showLvl = 0 ) {

        if ( $lvl > 5 || !isSet( $list[$section] ) )
            return false;

        $out = [];

        foreach ( $list[$section] as $data ) {

            $data['href'] = $data['link'] ?: '['.$data['id'].']';
            $data['items'] = [];

            if ( in_array( $data['id'], $selected ) )
                $data['selected'] = true;

            if ( !isSet( $list[$data['id']] ) ) {

                $out[] = $data;

            } else {

                if ( $showLvl-1 > $lvl || !$showLvl || isSet($data['selected']) )
                    $data['items'] = self::collectMarkTree( $data['id'], $list, $lvl + 1, $selected, $showLvl );

                $out[] = $data;
            }

        }

        return $out;
    }


    /**
     * Добавление нового раздела
     * @param $iParent
     * @param $sTitle
     * @param int $iTemplateId
     * @param string $sAlias
     * @param int $visible
     * @param string $sLink
     * @return bool|TreeSection
     */
    public static function addSection( $iParent, $sTitle, $iTemplateId = 0, $sAlias = '', $visible = Visible::VISIBLE, $sLink = '' ) {

        $section = new TreeSection();
        $section->parent = $iParent;
        $section->alias = $sAlias;
        $section->title = $sTitle;
        $section->visible = $visible;
        $section->link = $sLink;

        if ( !$section->save() ) {
            //var_dump( $section->errors );
            return false;
        }

        if ( !$iTemplateId )
            return $section;

        $section->setTemplate( $iTemplateId );

        return $section;
    }


    /**
     * Копирует раздел в другой.
     * @param TreeSection $oSection
     * @param $iParent
     * @param bool|false $bRec
     * @param array $filter
     * @return array
     */
    public static function copySection( TreeSection $oSection, $iParent, $bRec = false, $filter = [] ){

        self::$copyes = [];

        self::copy($oSection, $iParent, $bRec, $filter);

        return self::$copyes;

    }


    /**
     * @todo рефакторить!!!
     * Копирует раздел в другой.
     * @param TreeSection $oSection
     * @param $iParent
     * @param bool $bRec
     * @param array $filter
     * @return bool
     * @todo тесты на эту функцию
     * Добавить проверки на зацикливание
     */
    public static function copy( TreeSection $oSection, $iParent, $bRec = false, $filter = [] ){

        $sTitle = $oSection->title;

        $aServicesParam = \Yii::$app->sections->getByValue($oSection->id);
        if ($aServicesParam){
            $sParam = $aServicesParam[0];
            $sTitle = \Yii::t('data/app', 'section_' . $sParam, [], Parameters::getLanguage($iParent)); //@todo может как-то запоминать язык?
            if ($sTitle == 'section_' . $sParam)
                $sTitle = $oSection->title;
        }

        $oNewSection = self::addSection($iParent, $sTitle, $oSection->getTemplate(),
            $oSection->alias, $oSection->visible, ''
        );

        if (!$oNewSection) {
            return false;
        }

        /** Запомним - пригодится */
        self::$copyes[$oSection->id] = $oNewSection->id ;

        /**
         * Если копируется раздел, прописанный как сервисный раздел для чего-либо,
         * то нужно сделать новый такой же параметр для языка создаваемого раздела!
         */

        $lang = Parameters::getLanguage($oNewSection->id);

        if ($aServicesParam){
            self::$bClearCache = true; //хак @todo вспомнить зачем
            if ($lang){
                foreach( $aServicesParam as $sParam ){
                    $sTitle = \Yii::t('data/app', 'section_' . $sParam, [], Parameters::getLanguage($iParent));
                    if ($sTitle == 'section_' . $sParam)
                        $sTitle = $oSection->title;
                    \Yii::$app->sections->setSection($sParam, $sTitle, $oNewSection->id, $lang);

                    /** @todo хак на главные разделы*/
                    if ($sParam == 'main')
                        $oNewSection->save();
                }
            }
        }

        /**
         * Копирование параметров
         */
        $aAddParams = Parameters::getList( $oSection->id )
            ->level( params\ListSelector::alAll )
            ->get();

        // установить все параметры
        foreach ( $aAddParams as $oParam ) {

            /** Меняем подписи для диз режима */
            if ($oParam->group == \skewer\build\Design\Zones\Api::layoutGroupName && $oParam->name == \skewer\build\Design\Zones\Api::layoutTitleName)
                $oParam->value = $oNewSection->title . ' ('. $lang .')';

            Parameters::setParams(
                $oNewSection->id,
                $oParam->group,
                $oParam->name,
                $oParam->value,
                $oParam->show_val,
                $oParam->title,
                $oParam->access_level
            );
        }

        if ($bRec){
            $aSubSections = self::getSubSections($oSection->id);
            if ($aSubSections)
                foreach( $aSubSections as $oSubSection){
                    if (!$filter || array_search($oSubSection->id, $filter) !== false)
                        self::copy($oSubSection, $oNewSection->id, $bRec);
                }
        }

        /* Копирование SEO-параметров */
        $aDataSearch = seo\Api::get(Main\Seo::getGroup(), $oSection->id, 0, true);
        if ($aDataSearch) {
            unset($aDataSearch['id']);
            $aDataSearch['row_id'] = $oNewSection->id;
            seo\Api::set(Main\Seo::getGroup(), 0, 0, $aDataSearch);
        }

        /**
         * Обновление поиска должно быть после добавления параметров в раздел
         */
        $search = new \skewer\build\Adm\Tree\Search();
        $search->updateByObjectId( $oNewSection->id );

        return true;
    }

    /**
     * Удаляет ветву дерева разделов
     * @param int|string|TreeSection $id
     * @return bool
     * @throws \Exception
     */
    public static function removeSection( $id ) {

        $oSection = is_object($id) ? $id : self::getSection( $id );

        if ( !$oSection )
            return false;

        /* Удаление SEO-параметров */
        seo\Api::del(Main\Seo::getGroup(), $oSection->id);

        return (bool)$oSection->delete();

    }

    /**
     * @param int $section
     * @param int $level
     * @param string $path
     * @param int $defaultSection
     * перенес из TreePrototype::updateAliasPathsRec
     * todo без комментариев
     */
    private static function updateAliasPathsRec( $section=0, $level=0, $path='/', $defaultSection=0 ) {

        // рекурсивно перестроить информацию о маршрутах в таблице main.
        // принцип обхода:
        // вершины с level < 0 не обрабатываются (потомки также не обрабатываются)
        // вершины с visible < 0 или alias = *  - пропускаются в пути


        $aData['level']      = $level++;
        $aData['path']       = $path;
        $aData['section']    = $section;
        $sTableName = TreeSection::tableName();

        // обрабатываем нормальные вершины
        $sQuery = "UPDATE `$sTableName` SET `alias_path`=CONCAT(:path,`alias`,'/'), `level`=:level WHERE parent = :section AND visible IN (0,1) AND alias!='';";

        Query::SQL( $sQuery, $aData );

        // обрабатываем вершины без алиасов
        $sQuery = "UPDATE `$sTableName` SET `alias_path`=CONCAT(:path,`id`,'/'), `level`=:level WHERE parent = :section AND visible IN (0,1) AND alias='' ;";

        Query::SQL( $sQuery, $aData );

        // обрабатываем вершины с пропусками
        $sQuery = "UPDATE `$sTableName` SET `alias_path`=:path, `level`=:level WHERE parent = :section AND visible=2;";

        Query::SQL( $sQuery, $aData );

        // обходим дочерние вершины
        $sQuery = "SELECT id, alias_path, parent FROM `$sTableName` WHERE parent = ? AND level > -1;";

        $oResult = Query::SQL( $sQuery, $aData['section'] );

        while ($item = $oResult->fetchArray() ) {

            $double_slash = strpos($item['alias_path'], '//');

            // исправляем пути если алиас со слешем
            if ($double_slash !== false) {

                $item['alias_path'] = substr($item['alias_path'], $double_slash + 1); // было +2

                $sQuery = "UPDATE `$sTableName` SET `alias_path`=? WHERE id=?;";

                Query::SQL( $sQuery, $item['alias_path'], $item['id'] );
            }


            //             исправляем пути если первый алиас со слешем
            //            отключено за ненадобностью
            //            if (strpos($item['alias_path'], '/') === 0) {
            //
            //                $item['alias_path'] = substr($item['alias_path'], 1);
            //                $sQuery = "
            //                  UPDATE
            //                    `[table_name:q]`
            //                  SET
            //                    `alias_path`=[alias_path:s]
            //                  WHERE
            //                    id=[id:i]";
            //            }
            /** >>>>>>>>>>>>>>>>>>>>>>>>> recursion */
            static::updateAliasPathsRec($item['id'], $level, $item['alias_path'], $defaultSection);

        }// while

        // сбрасываем пути вершин с пропусками, чтобы исключить их из адреса
        if ($level == 1) {

            $sQuery = "UPDATE `$sTableName` SET `alias_path`='' WHERE parent = ? AND visible=2;";

            Query::SQL( $sQuery, $aData['section'] ); // $aData['section'] => $section

            // обходим все пути и проверяем на совпадение
            $last_path = '';

            $sQuery = "SELECT id, alias, alias_path, parent FROM `$sTableName` WHERE alias_path!='' AND visible IN (0,1) AND level > -1 ORDER BY alias_path, level;";

            $oResult = Query::SQL( $sQuery );

            while ($item = $oResult->fetchArray() ){

                //$item['table_name'] = static::getTableName();

                if ($last_path == $item['alias_path']) {

                    $item['new_alias_path'] = substr($item['alias_path'], 0, -1) . '-' . $item['id'] . '/';
                    // todo возможно лишний селект тк new_alias_path имеет уникальный суффикс из id раздела

                    $sQuery = "SELECT COUNT(id) as `alias_duplicates` FROM `$sTableName` WHERE alias_path=? AND visible IN (0,1) AND level > -1;";

                    $oSubresult = Query::SQL( $sQuery, $item['new_alias_path'] );

                    $aAliasDuplicates = $oSubresult->fetchArray();

                    if ($aAliasDuplicates['alias_duplicates'] == 0) {

                        $sQuery = "UPDATE `$sTableName` SET `alias` = CONCAT(`alias`,'-',`id`), `alias_path` = ? WHERE id = ?;";

                        Query::SQL( $sQuery, $item['new_alias_path'], $item['id'] );

                    } // если нет повторений
                } // if

                $last_path = $item['alias_path'];
            } // while
            //
            // обходим вершины которые мы пропускали и восстанавливаем им путь (для ссылок)
            $sQuery = "SELECT id FROM `$sTableName` WHERE `visible` = 2 AND `level` > -1;";

            $oResult = Query::SQL( $sQuery );

            while ( $item = $oResult->fetchArray() ) {
                //                $item['new_alias_path'] = $this->get_first_subsection_path($item['id']);
                //
                //                if ($new_path) {
                //                    $q = "UPDATE " . MINC_DBPREFIX . "main SET `alias_path` ='$new_path'
                //                      WHERE id = " . $item['id'];
                //                    mysql_query($q);
                //                } // if
            } // while
        } // if

        // Убираем дубль главной страницы
        if($level==1 and $defaultSection){

            $sQuery = "UPDATE `$sTableName` SET `alias_path`='' WHERE id=?;";

            Query::SQL( $sQuery, $defaultSection );
        }
    }


    public static function shiftPositionByParent($iParentId, $iPositionId) {

        $sQuery = "UPDATE `tree_section` SET `position`=`position`+1 WHERE `parent` = :parent AND `position` >= :position";

        $oResult = Query::SQL( $sQuery, [
            'parent' => $iParentId,
            'position' => $iPositionId
        ]);

        return $oResult->affectedRows();

    }


    /**
     * Отдает набор шаблонов.
     * Для вывода в интерфейсе CMS при добавлении нового раздела.
     * Дополняет список фиктивными служебными
     * @param int $iTemplateId
     * @param int $iParentTpl
     * @return array
     */
    public static function getTplSection( &$iTemplateId, $iParentTpl ) {

        $aOut = [];

        if ( !($oSection = self::getSection( $iParentTpl )) )
            return $aOut;

        $list = $oSection->getSubSections();

        $bFlag = false;

        /** @var TreeSection $section */
        foreach ( $list as $section ) {
            if ( $section->visible == Visible::VISIBLE ) {

                $aOut[] = [
                    'id' => $section->id,
                    'title' => $section->title,
                ];

                if ( $section->id == $iTemplateId )
                    $bFlag = true;

            }
        }

        if ( !$bFlag ) {
            if ( $list ) {
                $section = reset( $list );
                $iTemplateId = $section->id;
            } else {
                array_unshift($aOut, [
                    'id' => (int)$iTemplateId,
                    'title' => (string)($iTemplateId?:'---')
                ]);
            }
        }

        return $aOut;
    }

    /**
     * По имени модуля находит первый шаблон для него
     * @param string $sModuleName
     * @return int id раздела или 0
     */
    public static function getTemplateIdForModule($sModuleName) {

        // запросить список id шаблонных разделов
        $aSectionList = self::getSectionByParent( \Yii::$app->sections->templates() );
        $aIds = ArrayHelper::getColumn($aSectionList, 'id');
        
        // запросить список объектов для этих страниц
        foreach ($aIds as $id) {
            // отдать первый попавшийся
            $oParam = Parameters::getByName($id, Group::CONTENT, 'object', true);
            if ( $oParam and $oParam->value===$sModuleName )
                return $id;
        }

        // если не нашли, то 0
        return 0;

    }


    /**
     * Вернет полную цепочку разделов, унаследованных от шаблона $mTpl
     * @param $aSections - массив разделов(для рекурсии)
     * @param $mTpl array | int - шаблон
     * @return array
     */
    public static function getSubSectionsByTemplate($mTpl, array &$aSections = []){
        //todo Удалить метод-клон из \skewer\build\Adm\ParamSettings\Prototype::getSectionsByTemplate
        $aChildSections = Parameters::getList()
            ->fields(['parent'])
            ->group(Parameters::settings)
            ->name(Parameters::template)
            ->value($mTpl)
            ->asArray()->get();

        if (!$aChildSections = ArrayHelper::getColumn($aChildSections, 'parent'))
            return $aSections;
        else
            return ArrayHelper::merge($aSections, self::getSubSectionsByTemplate($aChildSections,$aChildSections));
    }

    /**
     * Выборка разделов по их шаблону
     * @param $iTplId
     * @return array
     */
    public static function getSectionsByTplId($iTplId){

        $command = \Yii::$app
            ->db
            ->createCommand("SELECT `tree_section`.* FROM `tree_section`
                              INNER JOIN `parameters` ON `tree_section`.`id`=`parameters`.`parent`
                              WHERE `parameters`.`name`='template' AND `parameters`.`value`='$iTplId'");
        $rows = $command->queryAll();

       return $rows;
    }

}