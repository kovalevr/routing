<?php

namespace skewer\base\ui\state;

use skewer\base\ft;
use skewer\base\ui;

/**
 * Определение обязательных методов состояния пользовательского интерфейса
 * который может отображать данные
 * @package skewer\base\ui
 */
interface StateInterface extends BaseInterface {

    /**
     * Задает набор полей по FT модели
     * @param ft\Model $oModel описание модели
     * @param string $mColSet набор колонок для вывода
     * @return void
     */
    public function setFieldsByFtModel( ft\Model $oModel, $mColSet='' );

}