<?php

namespace skewer\base\ui;

use skewer\base\ui;

/**
 * Фабрика интерфейсов
 * Class Factory
 * @package skewer\base\ui
 */
interface FactoryInterface {

    /**
     * Отдает объект для построения спискового интерфейса
     * @return ui\state\ListInterface
     */
    function getList();

    /**
     * Отдает объект для построения интерфейса редактирования
     * @return ui\state\EditInterface
     */
    function getEdit();

    /**
     * Отдает объект для построения интерфейса отображения данных
     * @return ui\state\ShowInterface
     */
    function getShow();

}
