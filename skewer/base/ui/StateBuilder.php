<?php

namespace skewer\base\ui;

use skewer\base\ui\builder\ListBuilder;
use skewer\base\ui\builder\FormBuilder;
use skewer\base\ui\builder\Prototype;
use skewer\components\ext;
use skewer\build\Cms;

/**
 * Сборщик интерфейсных моделей
 */
class StateBuilder {

    /**
     * Отправляет в интерфейс команду на перестрение одной строки в списке
     * @param Cms\Tabs\ModulePrototype $oModule модуль в котором отстраивается интерфейс
     * @param array $aData данные
     * @param string|array $mSearchFields имя колонки (или набор) по которой будет произведено сравнение
     * @return ext\ListRows
     */
    public static function updRow(Cms\Tabs\ModulePrototype $oModule, $aData, $mSearchFields = 'id') {

        $oListVal = new ext\ListRows();
        $oListVal->setSearchField( $mSearchFields );
        $oListVal->addDataRow( $aData );
        $oListVal->setData( $oModule );
        return $oListVal;

    }

    /**
     * Создание нового списки
     * @param null|Prototype $oInterface
     * @return ListBuilder
     */
    public static function newList( $oInterface = null ) {
        return new ListBuilder($oInterface);
    }


    /**
     * Создание новой формы редактирования
     * @param null|Prototype $oInterface
     * @return FormBuilder
     * todo rename createForm + deprecated
     */
    public static function newEdit($oInterface = null) {
        return new FormBuilder($oInterface);
    }

}