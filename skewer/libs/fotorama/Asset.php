<?php
namespace skewer\libs\fotorama;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle{

    public $sourcePath = '@skewer/libs/fotorama/web/';

    public $css = [
        'fotorama.css'
    ];

    public $js = [
        'fotorama.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];


}