<?php
namespace skewer\libs\jquery;

use yii\web\AssetBundle;
use yii\web\View;

class JeegoocontextAsset extends  AssetBundle{

    public $sourcePath = '@skewer/libs/jquery/web/';
    public $css = [
    ];
    public $js = [
        'jquery.jeegoocontext.min.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\libs\jquery\Asset'
    ];
}