<?php
namespace skewer\libs\jquery;

use yii\web\AssetBundle;

/**
 * Библиотека адаптивной карусели для клиентской части сайта
 */
class SlickAsset extends AssetBundle{

    public $sourcePath = '@skewer/libs/jquery/web/';

    public $js = [
        'slick.js',
    ];

    public $css = [
        'slick.css',
        'slick-theme.css',
    ];

    public $depends = [
        'skewer\libs\jquery\Asset'
    ];

}