/**
 * Created by na on 23.12.2016.
 */
CKEDITOR.dialog.add( 'popupformDialog', function( editor ) {
    return {
        title: editor.lang.popupform.title,
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: editor.lang.popupform.title,

                // The tab content.
                elements: [
                    {
                        type: 'text',
                        id: 'text',
                        label: editor.lang.popupform.text_caption,
                        validate: CKEDITOR.dialog.validate.notEmpty( editor.lang.popupform.error_text ),
                        default: 'Callback form'
                    },
                    {
                        type: 'checkbox',
                        id: 'ajaxform',
                        default: 'checked',
                        label: editor.lang.popupform.ajaxform_caption,
                    },
                    {
                        type: 'text',
                        id: 'width',
                        label: editor.lang.popupform.width_caption+' (px)',
                        validate: CKEDITOR.dialog.validate.integer( editor.lang.popupform.error_width ),
                        default: 600,
                    },
                    {
                        type: 'text',
                        id: 'section',
                        label: editor.lang.popupform.section_caption,
                        default: '0',
                        validate: CKEDITOR.dialog.validate.integer( editor.lang.popupform.error_section )
                    }
                ]
            }
        ],
        onOk: function() {
            var dialog = this;

            var popupform = editor.document.createElement( 'popupform' );

            var text = dialog.getValueOf( 'tab-basic', 'text' );
            var ajaxform = dialog.getValueOf( 'tab-basic', 'ajaxform' );
            var width = dialog.getValueOf( 'tab-basic', 'width' );
            var section = dialog.getValueOf( 'tab-basic', 'section' );

            var content = '<a data-ajaxform="'+ajaxform+'" class="js-callback" href="#" data-js_max_width="'+width+'" data-section="'+section+'">'+text+'</a>'

            popupform.setHtml( content );

            editor.insertElement( popupform );
        }
    };
});