/**
 * Created by na on 31.10.2016.
 */
CKEDITOR.plugins.setLang( 'popupform', 'de', {
    text_caption: 'Текст ссылки',
    ajaxform_caption: 'Ajax',
    width_caption: 'Ширина формы',
    section_caption: 'ID раздела с формой',
    title:'Генератор форм',
    error_400:'Es ist ein Fehler 400 aufgetreten',
    not_200_error:'etwas anderes als 200 zurückgegeben wurde',
    error_section:'Incorrect section ID',
    error_width:'Width must be integer',
    error_text:'Incorrect link text'
} );
