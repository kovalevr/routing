<?php

$aLanguage = array();

$aLanguage ['ru']['Search.Cms.tab_name'] = 'Панель поиска';
$aLanguage ['ru']['searchSubText'] = 'Поиск работает при вводе 2 и более символов';

$aLanguage ['en']['Search.Cms.tab_name'] = 'Search panel';
$aLanguage ['en']['searchSubText'] = 'Live search requires a minimum of 2 characters';

return $aLanguage;