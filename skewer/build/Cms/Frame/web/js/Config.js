/**
 * Основные конфигурационные константы CMS
 */
Ext.define('Ext.Cms.Config', {

    // версия движка
    cmsVersion: '3.0',

    // название слоя
    layerName: layerName,
    rootPath: rootPath,

    // имя основного файла для запросов
    request_script: '/admin/index.php',

    request_dir: '/admin/',

    // путь для вызова файлового браузера
    files_path: '/admin/',

    // время ожидания ответа ajax запроса
    request_timeout: 300000, // 5 минут

    CKEditorLang: lang
});
