```css
/*------------------------*/
.b-picture {
    margin: 0 auto;
    overflow: hidden;
    position: relative;
    text-align: center;
    width: 100%;
}
.b-picture .picture__wrapper {
    margin-left: -640px;
    width: 1280px;
    position: absolute;
    top: 0;
    left: 50%;
}
.b-picture img {
    border: 0 none;
    vertical-align: top;
}
.b-picture .picture__item {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    text-align: left;
}
/* Basic jQuery Slider essential styles */

ul.bjqs{position:relative; list-style:none;padding:0;margin:0;overflow:hidden; display:none;}
li.bjqs-slide{position:absolute; display:none;}
ul.bjqs-controls{list-style:none;margin:0;padding:0;z-index:9999;}
ul.bjqs-controls.v-centered li a{position:absolute;}
ul.bjqs-controls.v-centered li.bjqs-next {margin-top:-18px; width:36px; height:36px; position:absolute; top:50%; right:30px; display: block; z-index: 50;}
ul.bjqs-controls.v-centered li.bjqs-prev {margin-top:-18px; width:36px; height:36px; position:absolute; top:50%; left:30px; display: block; z-index: 50;}
ul.bjqs-controls.v-centered li.bjqs-next a {position:relative;  z-index: 50;}
ul.bjqs-controls.v-centered li.bjqs-prev a {position:relative;  z-index: 50;}
ol.bjqs-markers{position: absolute; bottom: 20px; right: 150px; z-index: 15;}
ol.bjqs-markers.h-centered{text-align: center;}
ol.bjqs-markers li{margin-right: 1em;display:block;float:left;}
ol.bjqs-markers li a{padding: 0.3em 0.5em;display:block;background-color: #4e4746;}
p.bjqs-caption{display:block;width:96%;margin:0;padding:2%;position:absolute;bottom:0;}
```

```html
<div class="b-section">
    <div class="section__inner">
        <div class="section__content">
            <div class="b-picture" style="height: {{ tools.banner_h }}px;">
                <div class="picture__wrapper" style="width: {{ tools.banner_w }}px; margin-left: -{{ tools.banner_w // 2 }}px;">
                    <div class="js_landing_slider" mb_scroll="{{ tools.scroll }}" mb_bullet="{{ tools.bullet }}" animtype="{{ tools.animtype }}" animduration="{{ tools.animduration }}" animspeed="{{ tools.animspeed }}" hoverpause="{{ tools.hoverpause }}" banner_w="{{ tools.banner_w }}" banner_h="{{ tools.banner_h }}">
                        <ul class="bjqs">
                            {% for item in banner %}
                                <li>
                                    {% if item.slide_link %}<a href="{{ item.slide_link }}">{% endif %}
                                        <img src="{{ item.img }}">
                                        <div class="picture__item">
                                            <div class="picture__text1" style="position: absolute; width: 700px; {% if item.text1_h %}left: {{ item.text1_h }}px;{% endif %}{% if item.text1_v %}top: {{ item.text1_v }}px;{% endif %}">{{ item.text1 }}</div>
                                            <div class="picture__text2" style="position: absolute; width: 700px; {% if item.text2_h %}left: {{ item.text2_h }}px;{% endif %}{% if item.text2_v %}top: {{ item.text2_v }}px;{% endif %}">{{ item.text2 }}</div>
                                            <div class="picture__text3" style="position: absolute; width: 700px; {% if item.text3_h %}left: {{ item.text3_h }}px;{% endif %}{% if item.text3_v %}top: {{ item.text3_v }}px;{% endif %}">{{ item.text3 }}</div>
                                            <div class="picture__text4" style="position: absolute; width: 700px; {% if item.text4_h %}left: {{ item.text4_h }}px;{% endif %}{% if item.text4_v %}top: {{ item.text4_v }}px;{% endif %}">{{ item.text4 }}</div>
                                        </div>
                                        {% if item.slide_link %}</a>{% endif %}
                                </li>
                            {% endfor %}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
```