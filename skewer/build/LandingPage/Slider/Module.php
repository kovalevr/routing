<?php

namespace skewer\build\LandingPage\Slider;

use skewer\base\section\Parameters;
use skewer\components\lp;
use skewer\build\Adm\Slider;
use skewer\base\Twig;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Editor
 */
class Module extends lp\PagePrototype {


    /**
     * Отдает полностью сформированный текст блока
     * @return string
     */
    protected function getRenderedText() {

        $sContent = lp\Api::getViewForSection( $this->sectionId() );

        $iBannerId = Parameters::getValByName( $this->sectionId(), 'object', 'currentBanner', false );

        $aBanner = Slider\Banner::find( $iBannerId );

        if ($aBanner)
            \Yii::$app->router->setLastModifiedDate($aBanner->last_modified_date);

        $aSlides = Slider\Slide::getSlides4Banner( $iBannerId );

        if (!$aSlides) return false;

        $aBannerTools = Slider\Banner::getAllTools();
        if ( isSet( $aBanner->bullet ) && isSet( $aBanner->scroll ) ) {
            $aBannerTools['pager'] = (float)$aBanner->bullet;
            $aBannerTools['controls'] = (float)$aBanner->scroll;
        }

        $aData = array(
            'banner' => $aSlides,
            'tools' => $aBannerTools,
            'config' => json_encode($aBannerTools)
        );

        return Twig::renderSource( $sContent, $aData );
        //return psRendered;

    }

}