```css
.[block_class] .b-section {
    background: #ccc;
}
/*-__-*/
.image_carousel {
    /*padding: 15px 0 15px 40px;*/
    position: relative;
    padding: 0 50px;
}
.image_carousel li {
    border: 1px solid #ccc;
    background-color: white;
    padding: 9px;
    margin: 7px;
    display: inline-block;
    vertical-align: top;
}
a.prev, a.next {
    background: url(/skewer/build/Page/GalleryViewer/images/miscellaneous_sprite.png) no-repeat transparent;
    width: 45px;
    height: 50px;
    display: block;
    position: absolute;
    top: 85px;
}
a.prev {
    left: -22px;
    background-position: 0 0; }
a.prev:hover {		background-position: 0 -50px; }
a.prev.disabled {	background-position: 0 -100px !important;  }
a.next {
    right: -22px;
    background-position: -50px 0;
}
a.next:hover {		background-position: -50px -50px; }
a.next.disabled {	background-position: -50px -100px !important;  }
a.prev.disabled, a.next.disabled {
    cursor: default;
}

a.prev span, a.next span {
    display: none;
}
.pagination {
    text-align: center;
}
.pagination a {
    background: url(/skewer/build/Page/GalleryViewer/images/miscellaneous_sprite.png) 0 -300px no-repeat transparent;
    width: 15px;
    height: 15px;
    margin: 0 5px 0 0;
    display: inline-block;
}
.pagination a.selected {
    background-position: -25px -300px;
    cursor: default;
}
.pagination a span {
    display: none;
}
.clearfix {
    float: none;
    clear: both;
}
ul.gallery_slider {
    margin: 0;
    padding: 0;
    text-align: center;
}
```

```html
<div class="b-section">
    <div class="section__inner">
        <div class="section__content">
            <div class="b-slide-title">
                {% if titleOnMain or Design.modeIsActive() %}
                    <h2 sktag="editor.h2" skeditor="GalleryViewer/titleOnMain">{{ titleOnMain }}</h2>
                {% endif %}
            </div>
            <div class="image_carousel">
                <ul id="gallery_slider" class="gallery_slider">
                    {%  for aItem in list  %}
                        <li><a class="single_3" rel="carousel-group" href="{{ aItem.med_src }}"><img src="{{ aItem.min_src }}" alt="{{ aItem.alt_title }}" height="160px" /></a></li>
                    {% endfor %}
                </ul>
                <div class="clearfix"></div>
                <a class="prev" id="foo2_prev" href="#"><span>prev</span></a>
                <a class="next" id="foo2_next" href="#"><span>next</span></a>
                <div class="pagination" id="foo2_pag"></div>
            </div>
            <div class="g-clear"></div>
        </div>
    </div>
</div>
```