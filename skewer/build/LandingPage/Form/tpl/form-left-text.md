tpl

```html
<div class="b-section">
    <div class="section__inner">
        <div class="section__content">
            <h2 class="section__header">{Заголовок секции}</h2>
            <div class="b-cols b-cols_2">
                <div class="cols__col">
                    <div class="cols__colcontent">
                        <h2>{string|Заголовок блока}</h2>
                        <div class="b-editor">
                            {wyswyg|Текст}
                        </div>
                    </div>
                </div>
                <div class="cols__col">
                    {:form}
                </div>
            </div>
        </div>
    </div>
</div>
```
