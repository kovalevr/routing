<?php

namespace skewer\build\LandingPage\Form;

use skewer\base\section\Tree;
use skewer\base\section\Parameters;
use skewer\components\lp;
use skewer\build\Page\Forms as PageForms;
use skewer\components\forms;
use skewer\base\Twig;
use skewer\components\targets\models\Targets;
use skewer\components\targets\Yandex;

/**
 * Модуль вывода формы на странице LandingPage
 * Class Module
 * @package skewer\build\Page\LandingPage\Form
 */
class Module extends lp\PagePrototype {

    public $FormId = 0;

    /**
     * Отдает полностью сформированный текст блока
     * @todo большую часть ф-ционала перенести в компонент, т. к. дублируется в модуле форм Page слоя
     * @return string
     */
    protected function getRenderedText() {

        $cmd = $this->getStr('cmd');

        if ( $cmd == 'send' ) {

            $iFormId    = $this->getInt( 'form_id' );
            $sLabel     = $this->oContext->getLabel();
            if ( $sLabel != $this->getStr('label') )
                return $this->actionShowForm();

            try {

                /** @var forms\Entity $oForm */
                $oForm = forms\Table::build( $iFormId, $_POST );

                $formHash = $oForm->getHash( $this->sectionId(), $sLabel );

                if ( !$oForm->validate( $formHash ) )
                    throw new \Exception( $oForm->getError() );

                switch( $oForm->getHandlerType() ){

                    case 'toMail':

                        $bRes = $oForm->send2Mail( 'letter.twig', $this->getModuleDir() . $this->getTplDirectory() );
                        break;

                    case 'toMethod':

                        $bRes = $oForm->send2Method();
                        break;

                    case 'toBase':

                        $oForm->send2Base( $this->sectionId() );
                        $bRes = $oForm->send2Mail( 'letter.twig', $this->getModuleDir() . $this->getTplDirectory() );
                        break;

                    default:
                        throw new \Exception('handler_not_found');# обработчик неизвестен - выходим
                }

                // вывести сообщение об успешной / неуспешной отправке
                if ( $oForm->getFormRedirect() ) {
                    \Yii::$app->getResponse()->redirect($oForm->getFormRedirect(),'301');
                    return '';
                }

                // добавление обработчика ReachGoal
                if ( $bRes ) {
                    $sTarget = $oForm->getTarget();
                    if ( $sTarget and Yandex::isActive() ) {
                        $aData['yandexReachGoal'] = array(
                            'id' => rand(0, 1000),
                            'cnt' => Yandex::getCounter(),
                            'target' => $sTarget
                        );
                    }

                    $gTarget = $oForm->getTargetGoogle();

                    $aTargetAttrs = ($oTarget = Targets::findOne(['name'=>$gTarget])) ? $oTarget->getAttributes() : [];
                    if ( $gTarget  ) {
                        $this->setData('googleReachGoal', array(
                            'id' => rand(0, 1000),
                            'target' => $gTarget,
                            'category' => $aTargetAttrs['category']
                        ));
                    }
                }

                $sData = ($bRes)?'success':'error';

                $aData[$sData] = 1;
                $aData['form_section'] = $this->sectionId();

            }
            catch( \Exception $e ) {

                $aData[$e->getMessage()] = 1;
            }

            // текст шаблона
            $sTplText = lp\Tpl::getForSection( $this->sectionId() );

            // шаблон ответа
            $sAnsTpl = Parameters::getShowValByName( $this->sectionId(), lp\Api::groupMain, 'answer', true );

            // полный шаблон - вставляем форму в основной
            $aAllTpl = str_replace( '{:form}', $sAnsTpl, $sTplText );

            // парсим lp метки
            $sContent = lp\Parser::render(
                $aAllTpl,
                lp\Api::getDataRows( $this->sectionId() )
            );

            // парсим twig метки
            return Twig::renderSource( $sContent, $aData );

        }

        if ( $cmd != 'send' ) {

            return $this->actionShowForm();
        }

        return '';

    }

    /**
     * Отдает текст формы в виде html разметки
     * @return string
     * @throws \Exception
     */
    protected function actionShowForm() {

        if (!$this->FormId) return '';

        // текст шаблона
        $sTplText = lp\Tpl::getForSection( $this->sectionId() );

        // шаблон формы
        $sFormText = lp\Tpl::getForSection( $this->sectionId(), 'form' );

        // полный шаблон - вставляем форму в основной
        $aAllTpl = str_replace( '{:form}', $sFormText, $sTplText );

        // парсим lp метки
        $sContent = lp\Parser::render(
            $aAllTpl,
            lp\Api::getDataRows( $this->sectionId() )
        );

        $oForm = forms\Table::build($this->FormId);
        \Yii::$app->router->setLastModifiedDate($oForm->getFormParam('last_modified_date'));
        
        $sLabel = $this->oContext->getLabel();
        $aData = [
            'oForm' => $oForm,
            'iRandVal' => rand(0, 1000),
            'form_section' => '#' . Tree::getSectionAlias( $this->sectionId() ),
            'formHash' => $oForm->getHash($this->sectionId(), $sLabel),
            'label' => $sLabel,
            'moduleWebPath' => $this->getModuleWebDir()
        ];

        // парсим twig метки
        return Twig::renderSource( $sContent, $aData );
    }
}