```css
.[block_class] .b-section {
    color: #000;
}
/*-__-*/
.b-news {
    position: relative;
    padding: 0 0 1em;
}
.b-news dl {
    display: block;
}
.b-news .news-title,
.b-news .news-title:link,
.b-news .news-title:visited {
    color: #0080b4;
    font-size: 18px;
    font-weight: normal;
    text-decoration: underline;
}
.b-news .news-title:active,
.b-news .news-title:hover {
    text-decoration: none;
}
.b-news .news-title {
    text-decoration: none;
}
.b-news .news__date {
    color: #333;
    font-size: 12px;
    padding-bottom: 10px;
}
    /*-----------------------*/
    .b-news_galley {
        margin-right: -3%;
    }
    .b-news_galley dl {
        display: inline-block;
        vertical-align: top;
        width: 30%;
        padding-right: 3%;
    }
```

```html
<div class="b-section">
    <div class="section__inner">
        <div class="section__content">
            <h2 class="section__header">{string|Заголовок секции}</h2>
            {% if aNewsList is defined %}
                {% if titleOnMain or Design.modeIsActive() %}
                    <h2 sktag="editor.h2" skeditor="news/titleOnMain">{{ titleOnMain }}</h2>
                {% endif %}
                <div class="b-news b-news_galley" sktag="modules.news">
                    {% for iKey, aNews in aNewsList %}
                        <dl>
                            <dt>
                                {% if (aNews.full_text or aNews.hyperlink) and not aNews.without_href %}
                                    <a class="news-title" sktag="modules.news.title" href="{% if aNews.hyperlink  %}{{aNews.hyperlink}}{% else %}[{{aNews.parent_section}}][NewsPageModule?{% if aNews['news_alias'] %}news_alias={{aNews['news_alias']}}{% else %}news_id={{aNews.id}}{% endif %}]{% endif %}">{{aNews.title}}</a>
                                {% else %}
                                    <span class="news-title" sktag="modules.news.normal">{{aNews.title}}</span>
                                {% endif %}
                            </dt>
                            <dd>
                                <p class="news__date" sktag="modules.news.date">{{aNews.publication_date}}</p>
                                <div class="b-editor" sktag="editor">
                                    {{aNews.announce}}
                                    {% if (aNews.full_text or aNews.hyperlink) and not aNews.without_href %}
                                        <p class="news__linkback">
                                            <a href="{% if aNews.hyperlink  %}{{aNews.hyperlink}}{% else %}[{{aNews.parent_section}}][NewsPageModule?{% if aNews['news_alias'] %}news_alias={{aNews['news_alias']}}{% else %}news_id={{aNews.id}}{% endif %}]{% endif %}">{{ Lang.get('Main.readmore') }}</a>
                                        </p>
                                    {% endif %}
                                </div>
                                <div class="g-clear"></div>
                            </dd>
                        </dl>
                    {% endfor %}
                </div>
            {% endif %}
        </div>
    </div>
</div>
```