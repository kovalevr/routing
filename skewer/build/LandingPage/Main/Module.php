<?php

namespace skewer\build\LandingPage\Main;

use skewer\build\Page;
use skewer\base\section;
use skewer\base\section\Tree;
use skewer\base\section\Parameters;
use skewer\build\Page\Main\Seo as MainSeo;
use skewer\components\seo;
use skewer\components\lp;
use skewer\base\site\Layer;
use skewer\base\site_module;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;

/**
 * Модуль сборки набора блоков в LandingPage
 * Class Module
 * @package LandingPage
 */
class Module extends site_module\page\ModulePrototype {

    /** @var int id страницы */
    public $sectionId = 0;

    /**
     * Отдает id текущего раздела
     * @return mixed
     */
    public function sectionId() {
        return $this->sectionId;
    }

    /** @inheritdoc */
    public function getAllowedChildClass() {
        return 'skewer\components\lp\ModuleInterface';
    }

    /**
     * Конструктор
     * @param site_module\Context $oContext
     * @throws ServerErrorHttpException
     */
    public function __construct(site_module\Context $oContext) {

        \Yii::$app->router;

        parent::__construct( $oContext );

        $aSubSectionList = Tree::getSubSections( $this->sectionId() );

        foreach ( $aSubSectionList as $section ) {

            if ( !in_array( (int)$section->visible, array(
                section\Visible::HIDDEN_FROM_PATH,
                section\Visible::VISIBLE
            ) ) ) continue;

            // id подраздела
            $iSubSectionId = (int)$section->id;

            // параметр объекта
            $aObjParam =  Parameters::getByName( $iSubSectionId, Parameters::settings, Parameters::object, true );
            if ( !$aObjParam )
                throw new ServerErrorHttpException ( 'Не найден корневой модуль для подчиненного блока' );

            // набор параметров
            $aParams = Parameters::getList( $iSubSectionId )
                ->rec()
                ->asArray()
                ->group(Parameters::settings)
                ->index('name')
                ->get()
            ;

            $aParams = ArrayHelper::getColumn($aParams, 'value');

            $aParams['sectionId'] = $iSubSectionId;

            // имя модуля
            $sClassName = site_module\Module::getClassOrExcept( $aObjParam['value'], Layer::PAGE );

            // добавление подчиненного модуля
            $sLabelName = 'block_' . $iSubSectionId;
            $this->addChildProcess( new site_module\Context(
                $sLabelName,
                $sClassName,
                ctModule,
                $aParams
            ) );

        }

    }

    /**
     * Метод - исполнитель функционала
     */
    public function execute() {

        // если не все дети отработали - ждем
        foreach ( $this->getChildProcesses() as $oProcess ) {
            if ( !in_array( $oProcess->getStatus(), array( psComplete, psRendered, psWait ) ) )
                return psWait;
        }

        $aParamsList = Parameters::getList( $this->sectionId() )
            ->rec()
            ->asArray()
            ->groups()
            ->fields(['value', 'show_val'])
            ->get();

        /** Метки из . */
        foreach($aParamsList as $sGroupName => $aParams) {
            if ( $sGroupName === Parameters::settings ){
                foreach( $aParams as $aParamsSet){
                    $this->setData($aParamsSet['name'], ['value' => $aParamsSet['value'], 'text' => $aParamsSet['show_val']]);
                }
            }
        }

        $this->setSEO(new MainSeo($this->sectionId(), $this->sectionId(), Tree::getCachedSection($this->sectionId())));

        $aOut = array();

        // menu
        if ( Parameters::getValByName( $this->sectionId(), Parameters::settings, lp\Api::paramShowMenu, true ) ) {

            $list = Tree::getSubSections( $this->sectionId() );

            $aMenu = [];

            foreach ( $list as $section )
                if ( $section->visible == section\Visible::VISIBLE )
                    $aMenu[$section->id] = $section->getAttributes();

            $this->setData('menu', $aMenu);
        }

        //$this->addCSSFile( 'main.css' );

        // собираем со всех выходные данные
        foreach ( $this->getChildProcesses() as $oProcess ) {

            if ( $oProcess->getStatus() === psComplete )
                $oProcess->render();

            $oModule = $oProcess->getModule();
            if ( $oModule instanceof lp\PagePrototype )
                $aOut[] = $oProcess->getOuterText();

        }

        $this->setParser(parserPHP);
        $this->setTemplate( 'main.php' );

        $this->setData('blockList', $aOut);

        $this->setData('css', lp\Css::getParsedText( $this->sectionId() ));

        $this->oContext->oProcess->setStatus( psComplete );

        if (\Yii::$app->register->moduleExists(Page\Copyright\Module::getNameModule(), Layer::PAGE)){

            $this->addChildProcess( new site_module\Context(
                Page\Copyright\Module::GROUP_COPYRIGHT,
                Page\Copyright\Module::className(),
                ctModule,
                array( 'sectionId' => $this->sectionId() )
            ) );

            $oCopyRight = $this->getChildProcess(Page\Copyright\Module::GROUP_COPYRIGHT);
            $oCopyRight->execute();
            $this->setData('CopyRightModule', $oCopyRight->render());
        }

        $this->addChildProcess( new site_module\Context(
            'seo',
            'skewer\\build\\Page\\SEOMetatags\\Module',
            ctModule,
            array( 'sectionId' => $this->sectionId() )
        ) );

        $oSeoProcess = $this->getChildProcess('seo');
        $oSeoProcess->execute();
        $oSeoProcess->render();

        return psComplete;

    }

    public function setSEO(seo\SeoPrototype $oSeo){
        $this->setEnvParam(seo\Api::SEO_COMPONENT, $oSeo );

        $this->setEnvParam( seo\Api::OPENGRAPH,
            \Yii::$app->getView()->renderPhpFile(
                $this->getModuleDir() . DIRECTORY_SEPARATOR . $this->getTplDirectory() . DIRECTORY_SEPARATOR . 'OpenGraph.php',
                ['aTree' => Tree::getCachedSection($this->sectionId()), 'oSeoComponent' => $oSeo]
            )
        );

    }

}