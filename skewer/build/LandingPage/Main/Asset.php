<?php

namespace skewer\build\LandingPage\Main;


use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/LandingPage/Main/web';

    public $css = [
        'css/main.css'
    ];

    public $depends = [
        'skewer\build\Page\Main\BaseAsset',
        'skewer\components\content_generator\Asset'
    ];

}