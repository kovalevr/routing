<?php

namespace skewer\build\Design\Layout;

use skewer\build\Cms;
use skewer\base\site_module\Context;


/**
 * Модуль вывода основного интерфейсного контейнера
 * Class Module
 * @package skewer\build\Design\Layout
 */
class Module extends Cms\Frame\ModulePrototype {

    public function execute() {

        $this->addChildProcess(new Context('head','skewer\build\Design\Header\Module',ctModule,array()));
        $this->addChildProcess(new Context('tabs','skewer\build\Design\Tabs\Module',ctModule,array()));

        return psComplete;
    }

}
