<?php

namespace skewer\build\Design\ParamPanel;

use skewer\build\Cms;
use skewer\base\site_module\Context;


/**
 * Модуль для вывода панели с редакторм параметров
 * Class Module
 * @package skewer\build\Design\ParamPanel
 */
class Module extends Cms\Frame\ModulePrototype {

    public function execute() {

        $this->addChildProcess(new Context('tree','skewer\build\Design\ParamTree\Module',ctModule,array()));
        $this->addChildProcess(new Context('params','skewer\build\Design\ParamEditor\Module',ctModule,array()));

        return psComplete;
    }

}
