<?php

$aLanguage = array();

$aLanguage ['ru']['Zones.Design.tab_name'] = 'Зоны';
$aLanguage ['ru']['sections']  = 'Разделы';
$aLanguage ['ru']['templates'] = 'Шаблоны';
$aLanguage ['ru']['page_already_override'] = 'Страница уже отвязана';

$aLanguage ['en']['Zones.Design.tab_name'] = 'Zones';
$aLanguage ['en']['sections']  = 'Sections';
$aLanguage ['en']['templates'] = 'Templates';
$aLanguage ['en']['page_already_override'] = 'Page already overlapped';

return $aLanguage;