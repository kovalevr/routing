<?php

namespace skewer\build\Page\AdaptiveMode;

use skewer\base\site_module;
use skewer\base\site;

class Module extends site_module\page\ModulePrototype {
	
	public function execute() {

        /** Контент всех модулей адаптивного меню */
        $sAdaptiveMenuContent = '';

        // Дождаться выполнения всех модулей адаптивного меню и получить их контент
        if ( ($oPage = site\Page::getRootModule()) and
             ($aLayouts = $oPage->getData('.layout')) and
             isset($aLayouts[Api::ADP_MENU_LAYOUT_NAME]) and
             is_array($aLayouts[Api::ADP_MENU_LAYOUT_NAME]) ) {

            // Дождаться выполнения всех модулей адаптивного меню
            foreach ($aLayouts[ Api::ADP_MENU_LAYOUT_NAME ] as $mLabel) {
                if ( ($oModule = $this->getProcess("out.$mLabel", psAll)) and ($oModule instanceof site_module\Process) )
                    if (!$oModule->isComplete())
                        return psWait;
            }

            // Получить контент всех модулей адаптивного меню
            foreach ($aLayouts[ Api::ADP_MENU_LAYOUT_NAME ] as $mLabel) {

                $oModule = $this->getProcess("out.$mLabel", psAll);
                if ($oModule instanceof site_module\Process)
                    $sAdaptiveMenuContent .= $oModule->render();

            }
        }

        $this->setData('asset_path', $this->getAssetWebDir());
        $this->setData('adaptive_menu_content', $sAdaptiveMenuContent);

        $this->setTemplate('adaptive-menu_base.twig');

        return psComplete;
    }
}
