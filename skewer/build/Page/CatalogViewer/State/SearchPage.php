<?php

namespace skewer\build\Page\CatalogViewer\State;

use skewer\components\catalog;
use yii\web\NotFoundHttpException;


/**
 * Объект вывода списка товарных позиций для поисковой старницы
 * Class SearchPage
 * @package skewer\build\Page\CatalogViewer\State
 */
class SearchPage extends ListPage {


    /**
     * Получение списка товарных позиций для текущей страницы
     * @return bool
     * @throws NotFoundHttpException
     * @throws catalog\Exception
     */
    protected function getGoods() {

        if ($this->getModule()->getStr( 'goods-alias', '' ))
            throw new NotFoundHttpException();

        if ( !$this->iCount )
            return false;

        $oSelector = catalog\GoodsSelector::getList( $this->getModuleField('searchCard') );

        if ( !$oSelector ) return false;

        $oSelector->applyFilter();

        $this->list = $oSelector
            ->condition( 'active', 1 )
            ->sort( $this->sSortField, ($this->sSortWay == 'down' ? 'DESC' : 'ASC') )
            ->limit( $this->iCount, $this->iPageId, $this->iAllCount )
            ->parse()
        ;

        return true;
    }


    public function build() {

        $this->getModule()->setData( 'useMainSection', 1 );

        parent::build();
    }


}