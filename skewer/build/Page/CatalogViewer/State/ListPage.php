<?php

namespace skewer\build\Page\CatalogViewer\State;


use skewer\base\ft;
use skewer\base\section\Tree;
use skewer\base\site;
use skewer\components\catalog;
use skewer\helpers\Html;
use skewer\base\SysVar;
use yii\helpers\ArrayHelper;


/**
 * Объект вывода списка товарных позиций
 * Class ListPage
 * @package skewer\build\Page\CatalogViewer\State
 */
class ListPage extends Prototype {

    /** @var array Набор товарных позиций для вывода */
    protected $list = [];

    /** @var int Всего найдено товарных позиций */
    protected $iAllCount = 0;

    /** @var int Номер страницы */
    protected $iPageId = 1;

    /** @var int Кол-во позиций на страницу */
    protected $iCount = 12;

    /** @var int Название категории */
    private $iCategory = '';

    /** @var string Шаблон для вывода */
    protected $sTpl = '';

    /** @var string Поле для сортировки */
    protected $sSortField = '';

    /** @var string Напровление сортировки */
    protected $sSortWay = 'up';

    /** @var array Набор шаблонов для каталога */
    public static $aTemplates = [
        'list' => [
            'title' => 'Editor.type_catalog_list',
            'file'  => 'SimpleList.twig',
        ],

        'gallery' => [
            'title' => 'Editor.type_catalog_gallery',
            'file'  => 'GalleryList.twig',
        ],

        'table' => [
            'title' => 'Editor.type_catalog_table',
            'file'  => 'TableList.twig',
        ],
    ];

    /** @var bool Факт использования фильтрации */
    protected $bFilterUsed = false;


    /**
     * Набор шаблонов для вывода в пользовательской части
     * @return array
     */
    public static function getTemplates() {

        $aList = ['list', 'gallery', 'table'];

        $aOut = [];
        foreach ($aList as $sName)
            $aOut[$sName] = \Yii::t( 'catalog', 'tpl_'.$sName);

        return $aOut;

    }

    /**
     * Отдает список всех каталожных разделов
     * @param $iPageId
     * @return array
     */
    public static function getRelatedList(){

        $aSections = Tree::getSectionsByTplId(\Yii::$app->sections->getValue('catalog_tpl'));

        return ArrayHelper::map($aSections,'id','title');
    }

    public function init() {

        \Yii::$app->router->setLastModifiedDate(catalog\model\GoodsTable::getMaxLastModifyDate());
        
        // категория
        $this->iCategory = $this->getModuleField( 'viewCategory' );

        // постраничный
        $this->iPageId = $this->getModule()->getInt( 'page', 1 );
        $this->iCount = $this->getModuleField( 'onPage', $this->iCount );

        // сортировка
        $this->sSortField = $this->getModuleField( 'listSortField' );
        if ( $sSortField = $this->getModule()->get('sort') )
            $this->sSortField = $sSortField;

        if ( $sSortWay = $this->getModule()->get('way') )
            $this->sSortWay = ( $sSortWay=='down' ? 'down' : 'up' );

        // выбор шаблона для вывода
        $this->getTpl();

        // получение набора товаров
        $this->getGoods();
    }


    public function build() {

        $this->getModule()->iOutStatus = null;

        if ( empty($this->list) ) {

            if ( $this->bFilterUsed ) {
                $this->getModule()->setTemplate( 'NotFound.twig' );
                return;
            }

            $oPage = site\Page::getRootModule();
            $aStaticContent = $oPage->getData('staticContent');
            $sText = (is_array($aStaticContent) and isset($aStaticContent['text'])) ? $aStaticContent['text'] : '';
            if ( Html::hasContent($sText) || !SysVar::get('catalog.section_filling',0)) {
                return;
            }

            $this->getModule()->setTemplate( 'Empty.twig' );
            return;
        }

        /** @todo продвиженцы пока не определились, как выводить на постраничных */
        //$oPage->setData('canonical_url', $this->getCanonical());

        // Рейтинг
        $this->addRating($this->list);

        // парсинг
        $this->getModule()->setData( 'section', $this->getModule()->getEnvParam('sectionId') );
        $this->getModule()->setData( 'aObjectList', $this->list );
        $this->getModule()->setData( 'form_section', $this->getModuleField( 'buyFormSection' ) );
        $this->getModule()->setData( 'useCart', site\Type::isShop() );
        $this->getModule()->setData( 'hideBuy1lvlGoods', SysVar::get('catalog.hideBuy1lvlGoods') );

        // панель сортировки для списка
        if ( $this->getModuleField( 'showSort' ) and $this->list )
            $this->showSortPanel();

        // шаблон
        $this->getModule()->setTemplate( self::$aTemplates[$this->sTpl]['file'] );

        // постраничник
        $this->setPathLine();
    }


    /**
     * Получение списка товарных позиций для текущей страницы
     * @return bool
     */
    protected function getGoods() {

        $oSelector = catalog\GoodsSelector::getList4Section( $this->getSection() );

        if (!$oSelector) return false;

        if ( $this->getModuleField( 'showFilter' ) )
            $this->bFilterUsed = $oSelector->applyFilter();

        if ( !$this->iCount )
            return false;

        $this->list = $oSelector
            ->condition( 'active', 1 )
            ->sort( $this->sSortField, ($this->sSortWay == 'down' ? 'DESC' : 'ASC') )
            ->limit( $this->iCount, $this->iPageId, $this->iAllCount )
            ->withSeo($this->getSection())
            ->parse()
        ;

        return true;
    }


    /**
     * Вывод плашки для выбора сотрировки и типа отображения списка
     */
    protected function showSortPanel() {

        /** @var ft\model\Field[] $aFieldList */
        $aFieldList = catalog\cache\Api::get( 'Catalog.Fields' );

        $aSortFieldArr = array();
        foreach ( $aFieldList as $oField ) {

            if ( !$oField->getAttr( 'show_in_sortpanel' ) || !isset($this->list[0]['fields'][$oField->getName()]) )
                continue;

            $aSortFieldArr[] = array(
                'name' => $oField->getName(),
                'title' => $oField->getTitle(),
                'sel' => ( $this->sSortField == $oField->getName() ) ? $this->sSortWay : false
            );
        }
        if (!empty($aSortFieldArr)){
            $this->getModule()->setData('filter', array('aFields' => $aSortFieldArr) );
            $this->getModule()->setData('viewState', $this->sTpl );
            $this->getModule()->setData('sortState', $this->sSortField );
            $this->getModule()->setData('defSortState', $this->getModuleField( 'listSortField' ) );
            $this->getModule()->setData('sortWay', $this->sSortWay );
        }
    }


    /**
     * Установка шаблона для вывода
     */
    private function getTpl() {

        // берем из параметров раздела
        $this->sTpl = $this->getModuleField( 'listTemplate' );

        // проверяем перекрытие из GET
        if ( $sView = $this->getModule()->get('view') )
            $this->sTpl = $sView;

        // убеждаемся в наличии
        if ( !isSet( self::$aTemplates[$this->sTpl] ) )
            $this->sTpl = 'list';
    }


    /**
     * Вывод PathLine
     */
    protected function setPathLine() {

        // todo и все таки параметры должны браться из запросника после обработки
        $aURLParams = $_GET;
        unSet( $aURLParams['page'] );
        unSet( $aURLParams['url'] );

        foreach( $aURLParams as $sKey=>$mParam )
            if ( is_array( $mParam ) ) {

                foreach( $mParam as $sWKey=>$mValue )
                    $aURLParams[ $sKey . '[' .$sWKey . ']' ] = $mValue;

                unSet( $aURLParams[$sKey] );
            }

        if ( $this->sSortField )
            $aURLParams['sort'] = $this->sSortField;

        if ( $this->sSortWay )
            $aURLParams['way'] = $this->sSortWay;

        if ( $this->sTpl )
            $aURLParams['view'] = $this->sTpl;

        $this->getModule()->getPageLine(
            $this->iPageId,
            $this->iAllCount,
            $this->sectionId(),
            $aURLParams,
            array( 'onPage' => $this->iCount )
        );

    }

//    /**
//     * Канонический урл страницы
//     * @return string
//     */
//    private function getCanonical(){
//
//        return \Site::httpDomain().\Yii::$app->router->rewriteURL('[' . $this->getModule()->getEnvParam('sectionId') . ']');
//
//    }

}