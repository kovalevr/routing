<?php

/* main */
use skewer\base\site\Layer;

$aConfig['name']     = 'YiiController';
$aConfig['version']  = '1.0';
$aConfig['title']    = 'YiiController';
$aConfig['description']  = 'для вывода yii контроллеров как раодных модулей';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::PAGE;
$aConfig['useNamespace'] = true;

return $aConfig;
