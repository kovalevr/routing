<?
/**
 * Шаблон соответствует ветке дерева разделов
 * Рекурсивно парсится(аналог виджета)
 */

/** @var int $level */
/** @var array $aSections */
?>

<? if ($aSections): ?>

    <ul class="level-<?=$level?>">
        <li class="item-<?=$level?>">
            <? if ($aSections['bActiveLink']): ?>
                <a href="<?=$aSections['href']?>"><?=$aSections['title']?></a>
            <? else: ?>
                <?=$aSections['title']?>
            <? endif; ?>
            <? foreach ($aSections['children'] as $child):?>
                <?= Yii::$app->view->renderPhpFile(
                    __FILE__,
                    [
                        'aSections' => $child,
                        'level' => $level + 1
                    ]);
                ?>
            <? endforeach; ?>
        </li>
    </ul>
<? endif; ?>