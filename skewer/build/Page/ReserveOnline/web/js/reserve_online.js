$(function() {

    $('.js-datepicker_reserve_online').datepicker({
        dateFormat: 'dd.mm.yy',
        minDate: new Date(),
    });

    $('body').on('click', '.js-datepicker_trigger', function() {
        $( '.js-datepicker_reserve_online', $(this).parent('div') ).focus();
    });

});