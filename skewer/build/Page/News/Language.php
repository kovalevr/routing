<?php

$aLanguage = array();

$aLanguage['ru']['News.Page.tab_name'] = 'Новости';

$aLanguage['ru']['groups_news'] = 'Параметры вывода новостей';

$aLanguage['ru']['back']               = 'Назад';
$aLanguage['ru']['news_title']         = 'Новости';
$aLanguage['ru']['all']                = 'Все новости';
$aLanguage['ru']['modified']           = 'Дата последнего изменения';
$aLanguage['ru']['param_on_page']      = 'Новостей в разделе';
$aLanguage['ru']['section_all']        = 'Id раздела всех новостей';
$aLanguage['ru']['all_section_link']   = 'Все новости';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['News.Page.tab_name'] = 'News';

$aLanguage['en']['groups_news'] = 'Output Options News';

$aLanguage['en']['back']               = 'Back';
$aLanguage['en']['news_title']         = 'News';
$aLanguage['en']['all']                = 'All news';
$aLanguage['en']['modified']           = 'Last modified date';
$aLanguage['en']['param_on_page']      = 'News on section';
$aLanguage['en']['section_all']        = 'Id section with all the news';
$aLanguage['en']['all_section_link']   = 'All the news';

return $aLanguage;