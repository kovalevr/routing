<?php

namespace skewer\build\Page\News;
use skewer\components\config\InstallPrototype;


/**
 * Class Install
 * @package skewer\build\Page\News
 */
class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}// class
