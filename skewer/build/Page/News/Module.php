<?php

namespace skewer\build\Page\News;

use skewer\base\section\Page;
use skewer\base\SysVar;
use skewer\build\Adm\News\models\News;
use skewer\build\Design\Zones;
use skewer\components\seo;
use skewer\build\Adm\News as NewsAdm;
use skewer\base\site;
use skewer\base\site_module;
use yii\web\NotFoundHttpException;

/**
 * Публичный модуль вывода новостей
 * Class Module
 * @package skewer\build\Page\News
 */
class Module extends site_module\page\ModulePrototype {

    public $parentSections;
    public $onPage = 10;
    public $listTemplate = 'list.php';
    public $detailTemplate = 'detail_page.php';
    public $titleOnMain = 'Новости';
    public $showArchive;
    public $showFuture;
    public $showOnMain;  // отменяет фильтр по разделам
    public $allNews;
    public $sortNews;
    public $showList;
    public $onMainShowType = 'list';
    // public $usePageLine = 1;

    private static $showDetailLink = null;

    public $aParameterList = array(
        'order'    => 'DESC',
        'future'   => '',
        'byDate'   => '',
        'on_page'  => '',
        'on_main'  => '',
        'section'  => '',
        'archive'  => '',
        'all_news' => '',
        'show_list'=> '',
    );

    public $section_all = 0;

    protected function onCreate() {
        if ( $this->showList )
            $this->setUseRouting( false );
    }

    public function init() {

        $this->onPage = abs($this->onPage);

        $this->setParser(parserPHP);

        $this->aParameterList['on_page'] = $this->onPage;

        if ( $this->allNews ) $this->aParameterList['all_news'] = 1;
        if ( $this->showOnMain ) $this->aParameterList['all_news'] = 1;

        if ( $this->parentSections ) $this->aParameterList['section'] = $this->parentSections;
        else $this->aParameterList['section'] = $this->sectionId();

        if ( $this->sectionId() == \Yii::$app->sections->main() ) $this->aParameterList['on_main'] = 1;
        if ( $this->showArchive ) $this->aParameterList['archive'] = 1;
        if ( $this->showFuture ) $this->aParameterList['future'] = 1;
        if ( $this->sortNews ) $this->aParameterList['order'] = $this->sortNews;

        if ( $this->showList ) $this->aParameterList['show_list'] = 1;

        return true;

    }// func

    /**
     * Выводит новость по псевдониму
     * @param $news_alias
     * @return int
     * @throws NotFoundHttpException
     */
    public function actionView($news_alias) {
        return $this->showOne(News::getPublicNewsByAlias($news_alias));
    }

    /**
     * Выводит новость по id
     * @param $id
     * @return int
     * @internal param $news_alias
     */
    public function actionViewById($id) {
        return $this->showOne(News::getPublicNewsById($id));
    }

    /**
     * Выводит новость
     * @param News|null $news
     * @return int
     * @throws NotFoundHttpException
     */
    public function showOne( $news ) {

        $this->setStatePage( Zones\Api::DETAIL_LAYOUT );

        if ( !$news )
            throw new NotFoundHttpException();

        \Yii::$app->router->setLastModifiedDate($news->last_modified_date);

        //Метатеги для списка новостей
        $this->setSEO( new NewsAdm\Seo( 0, $this->sectionId(), $news->getAttributes()) );

        // меняем заголовок
        site\Page::setTitle( $news->title );

        // добавляем элемент в pathline
        site\Page::setAddPathItem( $news->title );

        $hideDate = Page::getVal( 'content', 'hideDate' );

        if ( $hideDate )
            $this->setData( 'hideDate', true );

        $this->setData( 'news', $news );
        $this->setTemplate($this->detailTemplate);

        return psComplete;


    }
    /**
     * Выводит список новостей
     * @param int $page номер страницы
     * @param string $date фильтр по дате
     * @return int
     */
    public function actionIndex( $page=1, $date = '' ) {

        if (!$this->onPage) return psComplete;

        $this->aParameterList['page'] = $page;

        if ( !empty($date) ) {
            $sDateFilter = date('Y-m-d', strtotime($date));
            $this->aParameterList['byDate'] = $sDateFilter;
        }

        $iAllSection = 0;
        if ($this->showOnMain){
            $iAllSection = $this->section_all;
        }

        if ($iAllSection){
            $this->aParameterList['all_news'] = 0;
            $this->aParameterList['section'] = $iAllSection;
        }

        // Получаем список новостей
        $dataProvider = News::getPublicList($this->aParameterList);

        \Yii::$app->router->setLastModifiedDate(News::getMaxLastModifyDate());

        $this->setData('dataProvider',$dataProvider);
        $this->setData( 'titleOnMain', $this->titleOnMain );

        if ($iAllSection){
            $this->setData('section_all', $iAllSection);
        }
        // todo исправить в базе
        $this->setTemplate($this->listTemplate);
        $this->setData('showDetailLink', self::hasShowDetailLink());
        $this->setData('onMainShowType', $this->onMainShowType);
        $this->setData('zone', $this->zone);
        $this->setData('date', $date);

        return psComplete;

    }


    public function setSEO( seo\SeoPrototype $oSeo ) {

        $this->setEnvParam(seo\Api::SEO_COMPONENT, $oSeo );

        $this->setEnvParam( seo\Api::OPENGRAPH,
            \Yii::$app->getView()->renderPhpFile(
                __DIR__ . DIRECTORY_SEPARATOR . $this->getTplDirectory() . DIRECTORY_SEPARATOR . 'OpenGraph.php',
                ['oNews' => new News($oSeo->getDataEntity()), 'oSeoComponent' => $oSeo]
            )
        );

        site\Page::reloadSEO();

    }


    /**
     * Флаг вывода ссылки "Подробнее"
     * @return bool
     */
    public static function hasShowDetailLink()
    {
        if (is_null(self::$showDetailLink))
            self::$showDetailLink = (bool)SysVar::get('News.showDetailLink');

        return self::$showDetailLink;
    }

} 