<?php

namespace skewer\build\Page\Cart;

use skewer\build\Adm\Order\ar;
use skewer\components\forms\field as FormFields;
use skewer\base\orm;
use skewer\build\Tool\Payments;
use skewer\build\Adm\Auth\ar\UsersRow;
use skewer\build\Adm\Auth\ar\Users;
use skewer\components\auth\CurrentUser;
use yii\helpers\ArrayHelper;


class OrderForm extends orm\FormRecord {

    public $person = '';
    public $postcode = '';
    public $address = '';
    public $phone = '';
    public $mail = '';
    public $tp_deliv = 1;
    public $tp_pay = 1;
    public $accepts_the_licen = 0;
    public $text = '';
    public $is_mobile = 0;

    /** @var bool Флаг на быстрый заказ через 1 клик */
    private $fast = false;

    public function rules() {
        return array(
            array( array('person','postcode','address','phone','mail','tp_deliv','tp_pay','accepts_the_licen'), 'required', 'msg'=> \Yii::t('forms', 'err_empty_field' ) ),
            array( array('mail'), 'email', 'msg'=>\Yii::t('order', 'incor_email' ) ),
        );
    }

    public function getLabels() {
        return array(
            'person' => \Yii::t('order', 'person' ),
            'postcode' => \Yii::t('order', 'postcode' ),
            'address' => \Yii::t('order', 'address' ),
            'phone' => \Yii::t('order', 'phone' ),
            'mail' => \Yii::t('order', 'mail' ),
            'tp_deliv' => \Yii::t('order', 'tp_deliv' ),
            'tp_pay' => \Yii::t('order', 'tp_pay' ),
            'text' => \Yii::t('order', 'text' ),
            'accepts_the_licen' => \Yii::t('order', 'accepts_the_licen' ),
        );
    }

    public function getEditors() {
        return array(
            'tp_deliv' => array( 'select', 'method'=>'getTypeDelivList' ),
            'tp_pay' => array( 'select', 'method'=>'getTypePayList' ),
            'accepts_the_licen' => 'checkbox',
        );
    }

    /**
     * Установка флага о быстром заказе
     * @param bool $fast
     */
    public function setFast($fast){
        $this->fast = $fast;
    }

    /**
     * Быстрый заказ
     * @return bool
     */
    public function getFast(){
        return $this->fast;
    }

    public function getTypeDelivList() {
        return ArrayHelper::map(ar\TypeDelivery::find()->asArray()->getAll(),'id','title');
    }

    public function getTypePayList() {
        return ArrayHelper::map(ar\TypePayment::find()->asArray()->getAll(),'id','title');
    }

    public function getPayList() {
        $aPayments = Payments\Api::getPaymentsList( true );
        foreach( $aPayments as &$aPayment){
            $aPayment['title'] = \Yii::t('payments', $aPayment['type']);
        }
        return $aPayments;
    }

    // js валидация
    public function getRules() {
        $aRules = array();
        $aRules['rules'] = array(
            'person' => array('required'=>true,"maxlength"=>255),
            'postcode' => array('required'=>true,"maxlength"=>255),
            'address' => array('required'=>true,"maxlength"=>255),
            'phone' => array('required'=>true,"maxlength"=>255),
            'mail' => array('required'=>true,"maxlength"=>255,"email"=>true),
            'tp_deliv' => array('required'=>true,"maxlength"=>255),
            'tp_pay' => array('required'=>true,"maxlength"=>255),
            'text' => array('required'=>false,"maxlength"=>255),
            'accepts_the_licen' => array('required'=>true,"maxlength"=>255),
        );
        return json_encode($aRules);
    }


    /**
     * Заполнение формы используя анкетные данные пользователя
     * @param null|int $id
     */
    public function setUserData( $id = null ) {

        if ( !is_null( $id ) ) {

            /** @var UsersRow $oUser */
            $oUser = Users::find( CurrentUser::getId() );
            $this->person = $oUser->name;
            $this->postcode = $oUser->postcode;
            $this->mail = $oUser->email;
            $this->address = $oUser->address;
            $this->phone = $oUser->phone;
        }
    }


}