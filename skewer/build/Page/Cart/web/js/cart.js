$(document).ready(function(){

    // Общая функция посылки команд для работы с корзиной и обновления изменённых параметров в заказе
    function sendParam(params, callback) {

        params.moduleName = 'Cart';
        params.language = $('#current_language').val();

        $.post('/ajax/ajax.php', params, function(jsonData) {

            var aData = jQuery.parseJSON(jsonData);

            // Обновить общую стоимость последней изменённой позиции в соответствии с данными от сервера
            if (params.id != undefined && aData.lastItem.id_goods == params.id) {

                var oItemRow = $('.cart__row[data-id=' + params.id + ']');

                oItemRow.find('.js_cart_amount').val(aData.lastItem.count);
                oItemRow.find('.item_total').text(aData.lastItem.total);
            }

            $('.total').text(aData.total);

            // Если указана ф-ция обратного вызова, то выполнить её
            if (typeof callback === 'function')
                callback(aData);

            // Обновить мини-корзину, если присутствует
            if (window.updateMiniCart !== undefined)
                updateMiniCart(jsonData);
        });
    }

    // Удаление каталожной позиции
    $('.js_cart_remove').click(function() {

        var me = this;
        var id_good = $(this).attr('data-id');

        sendParam({cmd: 'removeItem', id: id_good}, function(aData) {

            // Удалить строку каталожной позиции
            $(me).parents('.cart__row').remove();

            // Если больше нет заказанных позиций, то очистить корзину
            if (!aData.count) {
                $('.cart__content').hide();
                $('.cart__empty').removeClass('cart__empty-hidden');
            }
        });

        return false;
    });

    // Уменьшение количества каталожной позиции
    $(".js_cart_minus").click(
        function() {
            var me = this;
            var id_good = $(me).attr('data-id');

            var row = $('.js_cart_amount[data-id=' + id_good + ']');
            var count = parseInt(row.val());

            // если не число - поставить 1
            if ( isNaN(count) )
                count = 1;

            // меньше 1 быть не может
            if (count > 1)
                count--;
            else
                count = 1;

            sendParam({cmd: 'recountItem', id: id_good, count: count});
        }
    );

    // Увеличение количества каталожной позиции
    $(".js_cart_plus").click(
        function(){
            var me = this;
            var id_good = $(me).attr('data-id');

            var row = $('.js_cart_amount[data-id=' + id_good + ']');
            var count = parseInt(row.val());

            // проверка на валидность
            if ( isNaN(count) )
                count = 0;

            // увеличть на 1 (если меньше 0 - привести к 1)
            if ( count >= 0 )
                count++;
            else
                count = 1;

            sendParam({cmd: 'recountItem', id: id_good, count: count});
        }
    );

    // Ручное изменение количества товара
    $('.js_cart_amount')
        .keypress(function(e){
            if (e.keyCode==13){
                $(this).blur();
            }
        })
        .blur(function() {

            var me = this;
            var id_good = $(me).attr('data-id');

            var countValue = parseInt($(me).val());
            if ( isNaN(countValue) )
                countValue = 1;

            if (countValue > 0 && countValue == $(me).val())
                sendParam({cmd: 'recountItem', id: id_good, count: countValue});
            else
                alert($('#js_translate_msg_count_gt_zero').html());
        })
    ;

    // Очистка корзины
    $('.cart__reset').click(function(){

        if (!confirm($('#js_translate_msg_dell_all').html()))
            return false;

        sendParam({cmd: 'unsetAll'}, function(aData){

            $('.cart__content').hide();
            $('.cart__empty').removeClass('cart__empty-hidden');
        });

        return false;
    });

    $('.agreed_readmore').fancybox();

    $('body').on( 'click', '.fast_agreed_readmore', function(){

        var agreed_container = $('.js_agreed_readmore');

        if (agreed_container.hasClass('open')){
            agreed_container
                .removeClass('open')
                .hide()
            ;
        }else{
            agreed_container
                .addClass('open')
                .show()
            ;
        }

        return false;

    });
});