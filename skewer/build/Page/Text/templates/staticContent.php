<?php
    /**
     * @var $source array
     * @var $designLabel string
     */
use skewer\components\design\Design;

?>


<? if (!empty($source['show_val'])): ?>
    <div class="b-editor"<? if (Design::modeIsActive()): ?> sktag="editor" skeditor="./<?=$designLabel?>"<? endif; ?>>
        <?=$source['show_val']?>
    </div>
<? endif; ?>






