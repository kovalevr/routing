<?php

    /**
     * Шаблон блока "Контакты" для адаптивного меню
     */

    use \skewer\base\section\Parameters;
    use \skewer\base\section\Page;
?>

<? if ($sHeadText3 = Page::getShowVal(Parameters::settings, 'headtext3')): ?>
    <div class="sidebar__item sidebar__contacts">
      <?= $sHeadText3 ?>
    </div>
<? endif; ?>