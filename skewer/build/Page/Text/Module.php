<?php

namespace skewer\build\Page\Text;

use skewer\base\section\Page;
use skewer\base\site_module;


/**
 * Публичный модуль вывода новостей
 * Class Module
 * @package skewer\build\Page\Text
 */
class Module extends site_module\page\ModulePrototype {

    public $template = null;

    public function init() {

        $this->setParser(parserPHP);
        return true;

    }// func


    public function execute(){

        $aParameters = Page::getByGroup($this->getLabel());


        if (isset($aParameters['source']))
            $this->setData('source', $aParameters['source']);


        $this->setData('designLabel',$this->getLabel());


        $this->setTemplate($this->template);


        return psComplete;

    }



} 