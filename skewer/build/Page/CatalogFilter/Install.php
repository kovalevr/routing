<?php

namespace skewer\build\Page\CatalogFilter;

use skewer\base\section\params\Type;

use skewer\components\config\InstallPrototype;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {

        $this->setParameter(\Yii::$app->sections->tplNew(), '.title', 'CatalogFilter', 'Фильтр для каталога', '', '', 0);
        $this->setParameter(\Yii::$app->sections->tplNew(), 'layout', 'CatalogFilter', 'left', '', '', 0);
        $this->setParameter(\Yii::$app->sections->tplNew(), 'object', 'CatalogFilter', 'CatalogFilter', '', '', 0);
        $this->setParameter(\Yii::$app->sections->tplNew(), 'title', 'CatalogFilter', '', '', 'catalogFilter.BlockTitle', Type::paramString);
        $this->setParameter(\Yii::$app->sections->main(), 'title', 'CatalogFilter', '', '', 'catalogFilter.BlockTitle', Type::paramString);

        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}// class
