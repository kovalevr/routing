<?php

namespace skewer\build\Page\CatalogFilter;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Page/CatalogFilter/web/';

    public $js = [
        'js/filter.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];
}