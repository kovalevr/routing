<?php

namespace skewer\build\Page\Subscribe;

use skewer\base\site\Page;
use skewer\base\site\Site;
use skewer\base\site_module;
use skewer\base\SysVar;
use skewer\build\Page\Subscribe\ar\SubscribeUser;
use skewer\build\Tool\Subscribe\Api;
use skewer\build\Page\Subscribe\Api as PageApi;
use skewer\components\i18n\ModulesParams;
use skewer\helpers\Mailer;

class Module extends site_module\page\ModulePrototype implements site_module\Ajax {

    public $AnswersTemplate = 'answers.twig';
    public $sTpl = 'form.twig';
    public $sMiniTpl = 'mini.twig';

    public $mini = 0;
    public $enable = 0;

    public function execute() {

        if ($this->oContext->getLabel()!='content' && $this->sectionId()==\Yii::$app->sections->getValue('subscribe')){
            return psComplete;
        } else {

            $sCmd = $this->getStr( 'cmd' );

            $sActionName = 'action' . ucfirst( $sCmd );

            if ( method_exists( $this, $sActionName ) )
                $this->$sActionName();
            else
                $this->actionInit();

            return psComplete;
        }
    }

    public function actionInit(){

        $this->showForm();
    }

    public function actionConfirm(){

        $this->setTemplate( 'detail.twig' );

        $sCode = $this->getStr('confirm');

        $oUser = SubscribeUser::find()
            ->where('confirm',$sCode)
            ->getOne();

        if (!$oUser){
            //сообщение об ошибке
            $this->setData( 'msg', \skewer\build\Page\Subscribe\Api::tagsReplacement(ModulesParams::getByName('subscribe','mail.errorText'),$oUser));
            Page::setTitle( \skewer\build\Page\Subscribe\Api::tagsReplacement(ModulesParams::getByName('subscribe','mail.errorTitle'),$oUser) );
        } else {
            SubscribeUser::update()
                ->set('confirm',1)
                ->where('confirm',$sCode)
                ->get();

            $this->setData( 'msg', \skewer\build\Page\Subscribe\Api::tagsReplacement(ModulesParams::getByName('subscribe','mail.successText'),$oUser) );
            Page::setTitle(\skewer\build\Page\Subscribe\Api::tagsReplacement(ModulesParams::getByName('subscribe','successTitle'),$oUser));
        }

    }

    public function showForm(){

        $this->setTemplate( 'detail.twig' );

        $subscribeForm = new SubscribeForm();

        if ( $subscribeForm->load( $this->getPost() ) && $subscribeForm->isValid()) {

            if ((!is_null(SysVar::get('subscribe_mode'))) and (SysVar::get('subscribe_mode')==Api::withConfirm)){

                $iCountKeys = '1';
                while ($iCountKeys!='0'){
                    $sKey = Api::getRandKey(150);
                    $iCountKeys = SubscribeUser::find()
                        ->where('confirm',$sKey)
                        ->getCount()
                    ;
                }

                $subscribeForm->confirm = $sKey;

                if ($subscribeForm->save()){

                    Mailer::sendMail($subscribeForm->email,
                        \skewer\build\Page\Subscribe\Api::tagsReplacement(ModulesParams::getByName('subscribe','mail.mailTitle'),$subscribeForm),
                        \skewer\build\Page\Subscribe\Api::tagsReplacement(ModulesParams::getByName('subscribe','mail.mailText'),$subscribeForm),
                        [],
                        Site::getNoReplyEmail());

                    $this->setData( 'msg', \skewer\build\Page\Subscribe\Api::tagsReplacement(ModulesParams::getByName('subscribe','mail.resultText'),$subscribeForm) );
                    Page::setTitle( \skewer\build\Page\Subscribe\Api::tagsReplacement(ModulesParams::getByName('subscribe','mail.resultTitle'),$subscribeForm) );
                }

            } else {
                $subscribeForm->confirm = 1;
                if ($subscribeForm->save())
                    $this->setData( 'msg', \Yii::t('subscribe','success') );
            }

        } else {
            $aParams = array(
                'page' => $this->sectionId(),
                'url' => \Yii::$app->router->rewriteURL( '['.$this->sectionId().']' )
            ) ;

            if ($this->mini){

                $idSubscribe = \Yii::$app->sections->getValue('subscribe');

                $aParams['url'] = \Yii::$app->router->rewriteURL( "[$idSubscribe]" );

                /*Если пришли данные с миниформы, скроем ее*/
                if (($this->get('email')==''))
                    $this->setData( 'forms', $subscribeForm->getForm( 'miniForm.twig', __DIR__, $aParams ) );
            } else {
                $this->setData( 'forms', $subscribeForm->getForm( $this->sTpl, __DIR__, $aParams ) );
            }
        }
    }

    public function actionUnsubscribe(){

        $this->setTemplate($this->AnswersTemplate);

        $sEmail = $this->getStr('email');
        $sToken = $this->getStr('token');

        $bEmail = PageApi::checkEmail($sEmail);

        if ( md5('unsub'.$sEmail.'010') != $sToken || !$bEmail ){
            $this->setData('subscriber_not_delete', 1);
        }else{
            $this->setData('subscriber_delete', PageApi::delSubscriber($sEmail));
        }
    }

    public function actionUnsubscribe_ajax(){
        $sEmail = $this->getStr('email');

        $bEmail = \skewer\build\Page\Subscribe\Api::checkEmail($sEmail);

        if ( !$bEmail )
            $this->setData('out', 0);
        else
            $this->setData('out', \skewer\build\Page\Subscribe\Api::delSubscriber($sEmail));

        return psRendered;
    }

} //class