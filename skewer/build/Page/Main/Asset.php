<?php

namespace skewer\build\Page\Main;

use skewer\components\design\Design;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/Main/web/';

    public $css = [
        'css/param.css',
        'css/main.css',
        'css/superfast.css',
        //'css/shcart.css',
        'css/typo.css'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\build\Page\Main\PrintAsset',
        'skewer\libs\jquery\Asset',
        'skewer\libs\datepicker\Asset',
        'skewer\libs\fancybox\Asset',
        'skewer\build\Page\Main\BaseAsset',
        'skewer\build\Page\Forms\Asset',
        'skewer\components\content_generator\Asset'
    ];

    public function init()
    {

        if (Design::modeIsActive())
            array_unshift( $this->css, 'css/design.css' );

        parent::init();
        /** @noinspection PhpUnusedParameterInspection */
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            if (is_dir($from) && basename($from)=='images'){
                FileHelper::copyDirectory($from,WEBPATH.'images/');
            }
            return true;
        };
    }

}
