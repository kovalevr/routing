<?php

namespace skewer\build\Page\Main;

use skewer\base\section\Parameters;
use \skewer\build\Adm\ParamSettings\Prototype;


class ParamSettings extends Prototype {

    public static $aGroups = [
        'page' => 'page.groups_main'
    ];

    public static $iGroupSortIndex = 100;

    /** @inheritdoc */
    public function getList() {

        return [
            [
                'name'   => 'logo',
                'group'  => 'page',
                'label'  => 'content',
                'title'  => 'page.logo',
                'editor' => 'file',
                'section' => \Yii::$app->sections->getValue('main'),
                'value'=>$this->getLogo(),
                'settings' => [],
            ]
        ];
    }

    /** @inheritdoc */
    public function saveData() {

        $sLogo = Parameters::getValByName(\Yii::$app->sections->getValue('main'),'content','logo');

        \Yii::$app->db->createCommand("UPDATE `css_data_params` SET `value`='$sLogo' WHERE `name`='page.head.logo.logo'")->execute();

    }

    public function getInstallationParam(){
        return [];
    }

    private function getLogo(){

        $aLogo = \Yii::$app->db->createCommand("SELECT * FROM `css_data_params` WHERE `name`='page.head.logo.logo'")->queryAll();

        return $aLogo[0]['value'];
    }

}