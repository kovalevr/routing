<?php

namespace skewer\build\Page\FAQ;


use skewer\build\Adm\FAQ as AdmFAQ;

class Api {
    
    /**
     * Метод по выборке из базы конкретного вопроса по ID
     * @param $iFAQId
     * @return array|bool
     */
    public static function getFAQById($iFAQId){

        $aItem = AdmFAQ\ar\FAQ::find()->where('id', $iFAQId)->where('status', AdmFAQ\Api::statusApproved)->asArray()->getOne();
        
        if ( isset($aItem['date_time']) )
            $aItem['date_time'] = date("d.m.Y", strtotime($aItem['date_time']));
        
        return $aItem;
    }

    /**
     * Метод по выборке из базы конкретного вопроса по псевдониму
     * @param $sAlias
     * @return array|bool
     */
    public static function getFAQByAlias($sAlias){

        $aItem = AdmFAQ\ar\FAQ::find()->where('alias', $sAlias)->where('status', AdmFAQ\Api::statusApproved)->asArray()->getOne();

        if ( isset($aItem['date_time']) )
            $aItem['date_time'] = date("d.m.Y", strtotime($aItem['date_time']));

        return $aItem;
    }

    /**
     * Список из раздела по страницам
     * @param $iSectionId
     * @param $iPage
     * @param $iOnPage
     * @param $iCount
     * @return mixed
     */
    public static function getItems( $iSectionId, $iPage, $iOnPage, &$iCount){

        return AdmFAQ\ar\FAQ::find()->where('status', AdmFAQ\Api::statusApproved)
            ->where('parent', $iSectionId)
            ->limit($iOnPage, ($iPage-1)*$iOnPage)
            ->order('date_time', 'DESC')
            ->asArray()
            ->setCounterRef($iCount)
            ->getAll();
    }
}
