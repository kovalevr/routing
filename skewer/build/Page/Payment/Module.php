<?php

namespace skewer\build\Page\Payment;

use skewer\base\site_module;
use yii\web\NotFoundHttpException;

class Module extends site_module\page\ModulePrototype {

    /** @var string Тип уведомления */
    public $type = 'error';

    public function execute() {

        $orderId = $this->getInt('InvId');
        if (!$orderId) {
            $orderId = $this->getInt('MNT_TRANSACTION_ID');
        }
        if (!$orderId) {
            $orderId = $this->getInt('order_id');
            if ($orderId){
                /**
                 * @todo paypal ??? вот тут в первой версии был execute
                 */
            }
        }
        if (!$orderId) {
            $orderId = $this->getInt('orderNumber');
        }

        if (!$orderId) {
            $this->type = 'error';
        }

        switch ($this->type) {
            /**
             * @todo $this->lang
             */
            case 'success':
                $message = \Yii::t('payments', 'success_text');
                break;

            case 'fail':
                $message = \Yii::t('payments', 'fail_text', $orderId);
                break;

            case 'error':
            default:
                throw new NotFoundHttpException();
        }

        $this->setData('message', $message);
        $this->setTemplate('view.twig');
        return psComplete;
    }
} 