<?php

namespace skewer\build\Page\GuestBook;

/**
 * Класс, содержащий редактируемые для текущего модуля параметры, используемые в админском модуле "Настройка параметров"
 * (skewer\build\Adm\ParamSettings)
 * Class ParamSettings
 * @package skewer\build\Page\ParamSettings
 */

class ParamSettings extends \skewer\build\Adm\ParamSettings\Prototype {

    public static $aGroups = [
        'Review' => 'review.groups_Review',
    ];

    public static $iGroupSortIndex = 30;

    /** @inheritdoc */
    public function getList() {

        return [
            [
                'name'   => 'titleOnMain',
                'group'  => 'Review',
                'title'  => 'news.title_on_main',
            ],
            [
                'name'    => 'onPage',
                'group'   => 'Review',
                'title'   => 'review.show_on_page',
                'editor'  => 'int',
                'settings' => [
                    'minValue' => 0,
                    'allowDecimals' => false
                ],
            ],
            [
                'name'   => 'maxLen',
                'group'  => 'Review',
                'title'  => 'review.maxLen',
                'editor' => 'int',
                'settings' => [
                    'minValue' => 0,
                    'allowDecimals' => false
                ],
            ],
            [
                'name'   => 'rating',
                'group'  => 'Review',
                'title'  => 'review.settings_rating',
                'editor' => 'check',
            ],
            [
                'name'   => 'section_id',
                'group'  => 'Review',
                'title'  => 'review.section_id_editor',
                'editor' => 'string',
            ],
        ];
    }

    /** @inheritdoc */
    public function saveData() {
    }

    public function getInstallationParam(){
        return [];
    }


}