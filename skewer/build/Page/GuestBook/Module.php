<?php

namespace skewer\build\Page\GuestBook;

use skewer\base\section\Parameters;
use skewer\build\Adm\GuestBook\ar;
use skewer\base\section\Tree;
use skewer\base\site_module;
use skewer\components\catalog\GoodsSelector;
use skewer\components\forms;
use skewer\build\Tool\Review\Api;
use \skewer\components\seo;

/**
 * Пользовательский модуль отзывов
 * Class Module
 * @package skewer\build\Page\GuestBook
 */
class Module extends site_module\page\ModulePrototype {

    public $hideTitle;
    public $altTitle = '';

    public $objectId;
    public $className = '';
    public $actionForm = '';

    public $showList = false;

    public $titleOnMain= '';
    public $maxLen = 500;
    public $section_id;

    public $onPage = 10;

    public $revert = 0;

    /** @var bool Отдать только микроразметку? */
    public $bOnlyMicrodata = false;

    /** @var int Использовать голосование в форме? Если значение <0 то параметр будет браться из глобальной языковой метки Review */
    public $rating = -1;

    /** @var int Показать звёзды голосования в списке отзывов? Если значение <0 то параметр = параметру rating */
    public $rating_list = -1;

    public function init() {

        $this->onPage = abs($this->onPage);
        $this->maxLen = abs($this->maxLen);

        $this->setParser(parserTwig);

        // Если параметр не задан строго для раздела, то взять глобальную настройку парамера rating, использующуюся в модуле "Настройки параметров"
        if ($this->rating < 0) {
            if ($oParamRating = Parameters::getByName($this->sectionId(), 'Review', 'rating', true, true))
                $this->rating = $oParamRating->value;
            else
                $this->rating = 0;
        }
        $this->rating_list = ($this->rating_list < 0) ? $this->rating : $this->rating_list;
    }

    protected function dateToText($sDate) {

        $arr = explode(' ', $sDate);
        $res = array();
        $res['date'] = $arr[0];
        $res['time'] = $arr[1];
        list($res['year'], $res['month'], $res['day']) = explode('-', $arr[0]);
        list($res['hour'], $res['min'], $res['sec']) = explode(':', $arr[1]);

        //$sNewDate = $res['day'] . ' ' . \Yii::t('app', 'month_' . $res['month']) . ' ' . $res['year'];
        $sNewDate = $res['day'] . '.' . $res['month'] . '.' . $res['year'];
        return $sNewDate;
    }

    /** Вывод формы / обработка данных формы */
    private function showForm() {

        if (!$oForm = forms\Table::getByName('form_guestbook'))
            return;

        $oForm    = new forms\Entity($oForm, $this->getPost());
        $sLabel   = $this->oContext->getLabel();
        $formHash = $oForm->getHash((int)\Yii::$app->request->post('section', $this->sectionId()), $sLabel);

        if ( ($oForm->getState($sLabel) == forms\Entity::STATE_SEND_FORM) and $oForm->validate($formHash) ) {
            // Отправить форму

            $this->sendComment();

            $aData = $oForm->getData();

            if (isset($aData['parent_class'])
                && $aData['parent_class'] == 'GoodsReviews'
                && (isset($aData['parent']))
            ){
                $sGoodsAlias = GoodsSelector::getGoodsAlias($aData['parent']);

                /*Если обрабатываем отзыв с каталога*/
                $sRedirectUrl = Tree::getSectionAliasPath($this->sectionId()).$sGoodsAlias.DIRECTORY_SEPARATOR.'#tabs-reviews';
            } else {
                /*Стандартный отзыв*/
                $sRedirectUrl = Tree::getSectionAliasPath($this->sectionId());
            }

            /*Установим флаг о отправке и редиректим на себя же*/
            \Yii::$app->session->set('guestbook_send','1');
            header('Location: '.$sRedirectUrl);
            exit;

        } elseif(\Yii::$app->session->get('guestbook_send',0)=='1') {
            /*Снимаем флаг отправки и выводим сообщение*/
            \Yii::$app->session->set('guestbook_send','0');
            $this->setData('msg', \Yii::t('review', 'send_msg'));
            $this->setData('back_link', 1);

        } else {
            // Показать форму

            $this->setData('label', $sLabel);
            $this->setData('section', $this->sectionId());
            $this->setData('formHash', $formHash);

            $this->setData('form_rating', $this->rating);
            $this->setData('parent', $this->objectId ? $this->objectId : $this->sectionId());
            $this->setData('parent_class', $this->className);

            $this->setData('oForm', $oForm);
        }
    }

    /** Отправить отзыв в БД */
    private function sendComment() {

        $aData = $this->getPost();

        foreach($aData as &$psValue)
            $psValue = strip_tags($psValue);

        $oRow = new ar\GuestBookRow();
        $oRow->setData($aData);
        $oRow->date_time = date( 'Y-m-d H:i:s' );

        $oRow->status =  ar\GuestBook::statusNew;

        if (!$oRow->save())
            return false;

        Api::sendMailToClient( $oRow );
        Api::sendMailToAdmin( $oRow );

        return true;
    }

    public function execute() {

        if (!$this->onPage) return psComplete;

        // номер текущей страницы
        $iPage = $this->getInt('page', 1);

        $this->setData('show_rating', $this->rating_list);

        \Yii::$app->router->setLastModifiedDate(ar\GuestBook::getMaxLastModifyDate());

        /**
         * Вывод списка для правой/левой колонки
         */
        if ( $this->showList ) {

            $aData = ar\GuestBook::find()
                ->where('status', ar\GuestBook::statusApproved)
                ->where('on_main', 1);

            /**
             * Если указан раздел - тащим из раздела, если нет - по всему сайту
             */
            $this->section_id = (int)$this->section_id;
            if ($this->section_id){
                $aData->where('parent',$this->section_id)
                    ->where('parent_class','');
            }else{
                $aSections = Tree::getAllSubsection(\Yii::$app->sections->languageRoot());
                if ( $aSections ) {
                    $aData->whereRaw("(parent_class != '' OR parent IN (".implode(', ', $aSections).'))');
                }
            }

            $aData = $aData->order('date_time', 'DESC')
                ->limit($this->onPage)
                ->asArray()
                ->getAll();

            if (!empty($aData)){

                $this->setData('title', \Yii::t('review', $this->titleOnMain));
                $this->setData('items', $aData);
                $this->setData('maxLen',$this->maxLen);

                if ($this->section_id){
                    $this->setData('section_id', $this->section_id);
                }
            }

            $this->setTemplate('main.twig');

        } else {

            $count = 0;
            $aData = ar\GuestBook::find()
                ->where('status', ar\GuestBook::statusApproved)
                ->where('parent', $this->objectId ? $this->objectId : $this->sectionId())
                ->where('parent_class', $this->className)
                ->limit($this->onPage, ($iPage - 1) * $this->onPage)
                ->setCounterRef($count)
                ->order('date_time', 'DESC')
                ->asArray()
                ->getAll();

            $bIsReview4Good = $this->objectId;
            $this->setData('bIsReview4Good',$bIsReview4Good);

            // задать шаблон
            if ($this->bOnlyMicrodata && $bIsReview4Good)
                $this->setTemplate('MicroDataReviews.twig');
            else
                $this->setTemplate('view.twig');

            $this->setData('aOrganization',[
                'address' => Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationAddress'),
                'name'    => Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationName'),
                'phone'   => Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationPhone')
            ]);

            // расставить отформатированииые даты в записях
            foreach ($aData as &$aItem) { // todo перенести в Row

                $aItem['date_time'] = $this->dateToText($aItem['date_time']);
                $aItem['content'] = nl2br(strip_tags( $aItem['content']));
            }

            // задать данные для парсинга
            $this->setData('items', $aData);
            $this->setData('revert',$this->revert);

            if ( !$this->bOnlyMicrodata )
                $this->showForm();

            // ссыки постраничного
            $this->getPageLine($iPage, $count, $this->sectionId(), array(), array('onPage' => $this->onPage));

        }

        return psComplete;
    }

}