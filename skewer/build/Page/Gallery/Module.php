<?php

namespace skewer\build\Page\Gallery;

use skewer\build\Design\Zones;
use skewer\components\seo;
use skewer\components\gallery;
use skewer\base\site;
use skewer\base\site_module;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use skewer\base\SysVar;

/**
 * Модуль фотогаллереи
 */
class Module extends site_module\page\ModulePrototype {

    public $AlbumsList  = 'albumsList.twig';
    public $AlbumDetail = 'showAlbum.php';
    public $AlbumDetailFotorama = 'showAlbumFotorama.php';
    public $openAlbum = false;

    /** @var int количество выводимых галерей */
    public $galleryOnPage = 10;

    /** @var int ограничение вывода фото в альбоме. постраничного нет */
    public $photosLimit = 150;

    /*
     * @var int id раздела источника данных
     * Если заполнено - в качестве раздела будет изпользован заданный
     *   при этом ссылки будут вести на целевую страницу
     * Если альбом один - будет сразу развернут
     * Если альбомов несколько - откроется выбранный но в разделе указанном в параметре
     */
    public $sourceSection = 0;

    private $iCurrentSection;

    public function init() {

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->sectionId();

        if ( $this->sourceSection )
            $this->iCurrentSection = $this->sourceSection;

    }// func

    /**
     * Вывод альбома по алиасу
     * @param $alias
     * @return int
     * @throws NotFoundHttpException
     */
    public function actionShowByAlias($alias){

        $this->setStatePage( Zones\Api::DETAIL_LAYOUT );

        // Если альбомы открыты, но запрошен alias альбома то выдать 404
        if ($this->openAlbum)
            throw new NotFoundHttpException();

        if (!$aAlbum = gallery\Album::getByAlias($alias, $this->iCurrentSection))
            throw new NotFoundHttpException();

        if (!$aAlbum['visible'])
            throw new NotFoundHttpException();

        \Yii::$app->router->setLastModifiedDate($aAlbum['last_modified_date']);

        // меняем заголовок страницы
        site\Page::setTitle($aAlbum['title']);

        // добавляем элемент в pathline
        site\Page::setAddPathItem($aAlbum['title']);

        $this->setSeo(new \skewer\build\Adm\Gallery\Seo($aAlbum['id'], $this->sectionId(), $aAlbum));

        $this->setData('description', $aAlbum['description']);

        $this->setPhotos($aAlbum['id']);

        return psComplete;

    }// func

    /**
     * Вывод по разделу
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionIndex(){

        if (!$this->iCurrentSection)
            throw new NotFoundHttpException();

        $iPage = $this->getInt('page', 1);
        $iCount = 0;
        $aAlbums = $this->getListAlbumsWithPreview($this->galleryOnPage, $iPage, $iCount);

        \Yii::$app->router->setLastModifiedDate(gallery\models\Albums::getMaxLastModifyDate());

        if ($iCount == 1 and $iPage==1)
            $this->openAlbum = true;

        if ($this->openAlbum) {
            $this->setData('hideBack', true);
            $this->setPhotos(ArrayHelper::getColumn($aAlbums, 'id'));
            return psComplete;
        }

        $this->getPageLine(
            $iPage,
            $iCount,
            $this->iCurrentSection,
            [],
            ['onPage' => $this->galleryOnPage]
        );

        $this->setData('albums', $aAlbums);
        $this->setData('sectionId', $this->iCurrentSection);
        $this->setTemplate($this->AlbumsList);

        return psComplete;
    }// func


    /**
     * Получить список альбомов текущего раздела раздела c превью изображением
     * @param int $iOnPage - количество на страницу
     * @param int $iPage - номер страницы
     * @param int $iCount - сюда будет возвращено общее количество
     * @param bool $bWithoutHidden - Без скрытых альбомов?
     * @return array - Возвращает массив найденных альбомов
     */
    public function getListAlbumsWithPreview($iOnPage=0, $iPage=1, &$iCount=0, $bWithoutHidden = true){

        $aAlbums = gallery\Album::getBySection($this->sectionId(), $bWithoutHidden, $iOnPage, $iPage, $iCount);

        $aAlbums = gallery\Album::setCountsAndPreview($aAlbums, true);

        foreach ($aAlbums as &$aAlbum) {
            $oSeo = new \skewer\build\Adm\Gallery\Seo($aAlbum['id'], $this->sectionId(), $aAlbum);

            /** Если для превью alt не прописан, то парсим его по шаблону */
            if (empty($aAlbum['alt_title']))
                $aAlbum['alt_title'] = $oSeo->parseField('altTitle');
        }

        return $aAlbums;
    }

    /**
     * Добавляет изображения альбомов
     * @param array $aAlbumsId Альбомы
     */
    private function setPhotos($aAlbumsId) {
        $aPhotos = gallery\Photo::getListWithSeoData($aAlbumsId, $this->sectionId(), true, $this->photosLimit, false);
        $this->setData('images', $aPhotos);
        $this->setData('openAlbum', $this->openAlbum);
        $this->setData('aAlbums', $aAlbumsId);
        $this->setParser(parserPHP);
        if ( SysVar::get('Gallery.ShowFotorama') )
            $this->setTemplate($this->AlbumDetailFotorama);
        else
            $this->setTemplate($this->AlbumDetail);
        $this->setEnvParam('gallery_photos', 1);
    }


    public function setSeo(seo\SeoPrototype $oSeo){
        $this->setEnvParam(seo\Api::SEO_COMPONENT, $oSeo );
        $this->setEnvParam(seo\Api::OPENGRAPH, '');
        site\Page::reloadSEO();
    }

	/**
	 * Метод отдает флаг автоматической регистрации Asset для модуля
	 * Если необходимо активировать вручную - можно перекрыть метод и вернуть false
	 * @return bool
	 */
	public function autoInitAsset() {
		return false;
	}


}// class
