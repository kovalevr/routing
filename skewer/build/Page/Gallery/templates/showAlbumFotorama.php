<?php

use skewer\build\Page\Gallery\AssetFotorama;
use skewer\components\design\Design;
use skewer\components\gallery\Album;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var $_objectId
 * @var $description
 * @var $images
 * @var $openAlbum
 * @var $aAlbums
 */

AssetFotorama::register(\Yii::$app->view);

?>

<div class="b-galbox b-galbox-fotorama"  <? if (Design::modeIsActive()): ?>sktag="modules.gallery" sklabel="<?=$_objectId?>" <?endif;?> >
    <? if (!empty($description)): ?>
        <div class="galbox__contentbox"><?= Html::encode($description)?></div>
    <?endif;?>

    <? if (!empty($images)): ?>

        <?
        list($iMaxWidth, $iMaxHeight) = Album::getDimensions4Fotorama($aAlbums, 'med', $images);
        list($iThumbWidth, $iThumbHeight) = Album::getDimensions4Fotorama($aAlbums, 'mini', $images);
        ?>

        <div class="js-fotorama"  <? if ($iMaxWidth): ?>data-max-width="<?=$iMaxWidth?>px" <? endif; ?> <? if ($iMaxHeight): ?>data-height="<?=$iMaxHeight?>px" <? endif; ?> <? if ($iThumbHeight):?>data-thumbheight="<?=$iThumbHeight?>"<?endif;?> <? if ($iThumbWidth):?>data-thumbwidth="<?=$iThumbWidth?>"<?endif;?>  >
            <? foreach ($images as $aImage):?>

                <? if ( ($sBigPath = ArrayHelper::getValue($aImage->images_data, 'med.file', '')) && ($sMiniPath = ArrayHelper::getValue($aImage->images_data, 'mini.file', '')) ): ?>
                    <a href="<?= $sBigPath ?>" title="<? if (!empty($aImage['title'])):?><?=Html::encode($aImage['title'])?><?endif;?><? if (!empty($aImage['description'])):?><?=Html::encode($aImage['description'])?><?endif;?>" >
                        <img src="<?= $sMiniPath ?>" alt="<?=Html::encode($aImage['alt_title'])?>"  />
                    </a>
                <?endif;?>

            <? endforeach;?>
        </div>

    <? else: ?>
        <p></p>
    <?endif;?>



    <div class="g-clear"></div>

    <? if (!$openAlbum):?>
        <p class="galbox__linkback"><a href="javascript: history.go(-1);" rel="nofollow"><?= \Yii::t('page', 'back') ?></a></p>
    <? endif; ?>
</div>
