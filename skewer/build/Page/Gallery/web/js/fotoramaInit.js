$(function() {

    var fotoramaContainer = $('.js-fotorama');

    fotoramaContainer.fotorama({
        nav: "thumbs",
        width: "100%",
        'max-width': fotoramaContainer.data('max-width'),
        height: fotoramaContainer.data('height'),
        allowfullscreen: true,
        loop: true,
        arrows: true,
        keyboard: true,
        thumbwidth:  fotoramaContainer.data('thumbwidth'),
        thumbheight: fotoramaContainer.data('thumbheight')
    });

});
