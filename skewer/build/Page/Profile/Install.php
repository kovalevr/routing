<?php

namespace skewer\build\Page\Profile;


use skewer\base\section\Tree;
use skewer\base\section\Visible;
use skewer\components\config\InstallPrototype;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {

        $this->createProfileSections();

        return true;
    }// func

    public function uninstall() {
        return true;
    }

    protected function createProfileSections()
    {
        $iNewPageSection = \Yii::$app->sections->tplNew();
        foreach (\Yii::$app->sections->getValues('tools') as $sLang => $iSection) {

            $oSection = Tree::addSection($iSection, \Yii::t('data/auth', 'profile_sections_title', [], $sLang), $iNewPageSection, 'profile', Visible::HIDDEN_FROM_MENU);
            $this->setParameter( $oSection->id, 'object', 'content', 'Profile' );
            $this->setParameter( $oSection->id, 'hide_right_column', '.', '1' );

            \Yii::$app->sections->setSection('profile', \Yii::t('site', 'profile', [], $sLang), $oSection->id, $sLang);

        }

    }// func

}// class
