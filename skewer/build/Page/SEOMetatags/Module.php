<?php

namespace skewer\build\Page\SEOMetatags;

use skewer\components\seo;
use skewer\base\site;
use skewer\base\site_module;
use skewer\components\lp;

/**
 * Модуль вывода метотегов для страницы
 * Class Module
 * @package skewer\build\Page\SEOMetatags
 */
class Module extends site_module\page\ModulePrototype implements lp\ModuleInterface{
    /**
     * Выполнение модуля
     * @return int
     */
    public function execute() {

        $oRootModule = site\Page::getRootModule();

        // ждем выполнения главного модуля
        $oContentModule = site\Page::getMainModuleProcess();
        if ($oContentModule && !$oContentModule->isComplete())
            return psWait;

        /** @var seo\SeoPrototype $oSeoComponent */
        if ($oSeoComponent = $this->getEnvParam(seo\Api::SEO_COMPONENT, null)){
            $oSeoComponent->initSeoData();

            foreach ($oSeoComponent::getFieldList() as $sField => $sLabel)
                if ( in_array($sField, seo\SeoPrototype::getField4Parsing()) )
                    $oRootModule->setData($sLabel, (!empty($oSeoComponent->$sField))? $oSeoComponent->$sField : $oSeoComponent->parseField($sField));
                else
                    $oRootModule->setData($sLabel, $oSeoComponent->$sField);
        }


        $oRootModule->setData(seo\Api::OPENGRAPH, $this->getEnvParam(seo\Api::OPENGRAPH,''));

        return psRendered;
    }


}
