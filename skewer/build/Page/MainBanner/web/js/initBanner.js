jQuery(document).ready(function($) {

    var $slider = $('.bxslider');

    if ($slider.length) {

    	var config = $slider.attr('data-config');
	    config = $.parseJSON(config)
	    if (parseInt(config.maxHeight) > 0) {
	 		config.onSliderLoad = function() { 

	 			$('.bx-pager-link').html('');
	 			if (parseInt(config.responsive) == 0) {
	 				$('.js-slider-img').css('maxWidth', 'none'); 
	 			}
	 			$('.bx-viewport').css('height', config.maxHeight+'px'); 
	 		}
	 		config.adaptiveHeight = false;
	    }

	  
	    $slider.bxSlider(config);

    }


});
