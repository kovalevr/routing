<?php

namespace skewer\build\Page\Auth;


use skewer\build\Adm\Auth\ar\Users;
use skewer\build\Adm\Auth\ar\UsersRow;
use skewer\components\i18n\ModulesParams;
use skewer\base\section\Tree;
use skewer\base\site;
use skewer\components\auth\Auth;
use skewer\components\auth\CurrentUser;
use skewer\components\design\Design;
use skewer\base\site_module;
use skewer\base\SysVar;
use yii\web\NotFoundHttpException;

/**
 * Модуль регистрации и авторизации
 * Class Module
 * @package skewer\build\Page\Auth
 */
class Module extends site_module\page\ModulePrototype {

    public $mini_auth = 0;

    public $head = false;

    private $authSection = 0;
    
    /**
     * Отдает флаг использования правил разбора url
     * @return boolean
     */
    public function useRouting() {
        return !$this->mini_auth;
    }

    /**
     * Прототип - выполняется до вызова метода Execute
     */
    public function init()
    {
        parent::init();

        $this->authSection = \Yii::$app->sections->getValue('auth');
    }


    public function execute() {
        $this->setData('page', \Yii::$app->sections->auth());

        $this->setData('profile_url',  Api::getProfilePath());
        $this->setData('auth_url',  Api::getAuthPath());

        if ( Design::modeIsActive() ) {
            $this->setData('designMode', Design::getDirList() );
        }

        if ($this->head && $this->mini_auth){
            $this->setTemplate( 'AuthFormMiniHead.twig' );
        }else{

            if (isset($_SERVER['HTTP_REFERER']) && !count($_POST)) {
                $_SESSION['returnUrlFromAuth'] = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
            }

            $this->setTemplate( 'detail.twig' );
        }

        if ($this->mini_auth) {
            $this->actionIndex();
            return psComplete;
        }

        return parent::execute();
    }


    protected function actionIndex() {
        if ( CurrentUser::isLoggedIn()) {
            $this->setData( 'current_user', CurrentUser::getUserData() );
        } else {
            $this->actionShowAuthForm();
        }

        return psComplete;

    }


    /**
     * Выводит форму авторизации
     */
    protected function actionShowAuthForm() {
        $oAuthForm = new AuthForm();

        if ( $oAuthForm->load( $this->getPost() ) && $oAuthForm->isValid() && $oAuthForm->auth() ) {
            $this->setData( 'current_user', CurrentUser::getUserData() );
            Auth::reloadPolicy( 'public' );

            \Yii::$app->getResponse()
                ->redirect( isset($_SESSION['returnUrlFromAuth']) ? $_SESSION['returnUrlFromAuth'] : Api::getProfilePath() )
                ->send();
            exit;

        } else {
            $aParams = array(
                'page' => $this->authSection,
                'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
            ) ;

            $sTpl = $this->mini_auth ? ($this->head?'AuthFormMiniHead.twig':'AuthFormMini.twig') : 'AuthForm.twig';
            $this->setData( 'forms', $oAuthForm->getForm( $sTpl, __DIR__, $aParams ) );
        }

    }


    /**
     * Регистрация нового пользователя
     */
    protected function actionRegister() {

        $oRegForm = new RegForm();

        if ( $oRegForm->load( $this->getPost() ) && $oRegForm->isValid() && $oRegForm->saveUser() ) {

            $iActivateStatus = SysVar::get('auth.activate_status');

            if ( $iActivateStatus == 1 ) {

                $this->setData( 'msg', \Yii::t('auth', 'msg_instruct_auth' ) );
                $this->actionShowAuthForm();

            } else {

                $sTitle = \Yii::t('auth', 'head_register');

                site\Page::setTitle( $sTitle );

                site\Page::setAddPathItem( $sTitle );

                if ($iActivateStatus == 2)
                    $this->setData( 'msg', \Yii::t('auth', 'msg_instruct_reg' ) );
                elseif ($iActivateStatus == 3)
                    $this->setData( 'msg', \Yii::t('auth', 'msg_instruct_admin' ) );

            }

        } else {

            $sTitle = \Yii::t('auth', 'head_register');

            site\Page::setTitle( $sTitle );

            site\Page::setAddPathItem( $sTitle );

            $aParams = array(
                'page' => $this->authSection,
                'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" ),
                'license_agreement' => ModulesParams::getByName('auth', 'reg_license')
            ) ;

            $this->setData( 'forms', $oRegForm->getForm( 'RegForm.twig', __DIR__, $aParams ) );
        }

    }


    /**
     * Сохранение нового пароля
     */
    protected function actionSaveNewPass() {

        $sPassword = $this->getStr( 'pass' );
        $sWPassword = $this->getStr( 'wpass' );
        $sToken = $this->getStr( 'token' );

        $oTicket =  AuthTicket::get($sToken);
        if ($oTicket && $oTicket->moduleNameIs('auth') && $oTicket->actionNameIs('recover_pass') && $oTicket->getObjectId()){

            $iUserId = $oTicket->getObjectId();
            /** @var UsersRow $oItem */
            $oItem = Users::find()->where( 'id', $iUserId )->getOne();

            $oNewPassForm = new NewPassForm();
            $oNewPassForm->login = $oItem->login;
            $oNewPassForm->token = $sToken;
            $oNewPassForm->pass = $sPassword;
            $oNewPassForm->wpass = $sWPassword;

            if ( !$oItem || !$sToken ) {

                $this->setData( 'msg', \Yii::t('auth', 'msg_error_token' ) );

                $this->actionShowAuthForm();

            } elseif ( !$oNewPassForm->isValid() ) {

                $aParams = array(
                    'page' => $this->authSection,
                    'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
                ) ;

                $sTitle = \Yii::t('auth', 'head_restore');

                site\Page::setTitle( $sTitle );

                site\Page::setAddPathItem( $sTitle );

                $this->setData( 'forms', $oNewPassForm->getForm( 'NewPass.twig', __DIR__, $aParams ) );

            } else {

                Api::saveNewPass( $oItem, $sPassword );
                $oTicket->delete($sToken);

                $this->setData( 'msg', \Yii::t('auth', 'msg_new_pass' ) );

                $this->actionShowAuthForm();
            }
        }




    }


    /**
     * Форма ввода нового пароля
     */
    protected function actionNewPassForm() {

        $sToken = $this->getStr( 'token' );
        $oTicket =  AuthTicket::get($sToken);

        $sTitle = \Yii::t('auth', 'head_restore');

        site\Page::setTitle( $sTitle );

        site\Page::setAddPathItem( $sTitle );

        if ($oTicket && $oTicket->moduleNameIs('auth') && $oTicket->actionNameIs('recover_pass') && $oTicket->getObjectId()){
            $iUserId = $oTicket->getObjectId();

            /** @var UsersRow $oItem */
            $oItem = Users::find()->where( 'id', $iUserId )->getOne();

            if ( $oItem ) {

                $aParams = array(
                    'page' => $this->authSection,
                    'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
                ) ;

                $oNewPassForm = new NewPassForm();
                $oNewPassForm->login = $oItem->login;
                $oNewPassForm->token = $sToken;

                $this->setData( 'forms', $oNewPassForm->getForm( 'NewPass.twig', __DIR__, $aParams ) );

            } else
                $this->setData( 'msg', \Yii::t('auth', 'msg_error_token' ) );

        } else
            $this->setData( 'msg', \Yii::t('auth', 'msg_error_token' ) );
    }


    /**
     * Восстановление пароля
     */
    protected function actionRecover() {

        $oRecoverForm = new RecoverForm();

        $sLogin = $this->getStr( 'login' );

        $sTitle = \Yii::t('auth', 'restore');

        site\Page::setTitle( $sTitle );

        site\Page::setAddPathItem( $sTitle );

        try {

            if ( !$oRecoverForm->load( $this->getPost() ) || !$oRecoverForm->isValid() )
                throw new \Exception( '' );

            $oUser = $sLogin ? Api::getUserByLogin( $sLogin ) : null;

            if ( is_null( $oUser ) || !$oUser )
                throw new \Exception( \Yii::t('auth', 'msg_not_found_user' ) );

            // сгенерим токен и отправим на mail
            if ( !Api::recoverPass( $oUser ) )
                throw new \Exception( '' );

            $this->setData( 'msg', \Yii::t('auth', 'msg_recover_instruct' ) );

            //$this->actionShowAuthForm();

        } catch ( \Exception $e ) {

            // форма востановления пароля
            if ( $e->getMessage() )
                $oRecoverForm->addError( $e->getMessage() );

            $aParams = array(
                'page' => $this->authSection,
                'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
            ) ;

            $this->setData( 'forms', $oRecoverForm->getForm( 'RecoverForm.twig', __DIR__, $aParams ) );
        }

    }


    /**
     * выход из системы
     */
    protected function actionLogout() {

        CurrentUser::logout();

        /**
         * @fixme перезагрузка политик не отрабатывает как нужно. С политиками надо разбираться. Закрыл пока переадресацией
         */

        if (isset($_SERVER['HTTP_REFERER']))
            \Yii::$app->getResponse()->redirect(  parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH) )->send();
        else \Yii::$app->getResponse()->redirect(Tree::getSectionAliasPath($this->getEnvParam('sectionId')))->send();

    }


    /**
     * Активация аккаунта
     */
    protected function actionAccountActivation() {

        $sToken = $this->getStr( 'token', '' );

        if ( !$sToken )
            throw new NotFoundHttpException();

        $oTicket = AuthTicket::get($sToken);

        if (!$oTicket){
            throw new NotFoundHttpException();
        }

        if ($oTicket && $oTicket->moduleNameIs('auth') && $oTicket->actionNameIs('activate') && $oTicket->getObjectId()){
            $iUserId = $oTicket->getObjectId();

            /**
             * @var UsersRow $oUser
             */
            $oUser = Users::find()->where( 'id', $iUserId )->where( 'active', 0 )->getOne();

            if ( !$oUser )
                throw new NotFoundHttpException();

            if ( Api::accountActivate( $oUser ) ){

                $oTicket->delete($sToken);

                $this->setData( 'msg', \Yii::t( 'auth', 'msg_verifed' ) );

                $oAuthForm = new AuthForm();

                $aParams = array(
                    'page' => $this->authSection,
                    'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
                ) ;

                $sTpl = 'AuthForm.twig';
                $this->setData( 'forms', $oAuthForm->getForm( $sTpl, __DIR__, $aParams ) );

            } else {
                throw new NotFoundHttpException();
            }
        } else {
            throw new NotFoundHttpException();
        }

    }

} 
