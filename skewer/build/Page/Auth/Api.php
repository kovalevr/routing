<?php

namespace skewer\build\Page\Auth;


use skewer\base\site\Site;
use skewer\helpers\Mailer;
use skewer\build\Adm\Auth\ar\Users;
use skewer\build\Adm\Auth\ar\UsersRow;
use skewer\base\section\Tree;
use skewer\components\i18n\ModulesParams;
use skewer\components\auth\Auth;
use yii\helpers\ArrayHelper;
use skewer\base\SysVar;

class Api {

    const STATUS_NO_AUTH = 0;
    const STATUS_AUTH = 1;
    const STATUS_BANNED = 2;


    /**
     * Путь страницы личного кабинета
     * @return mixed|string
     */
    public static function getProfilePath(){

        return ArrayHelper::getValue(Tree::getCachedSection(), \Yii::$app->sections->getValue('profile') . '.alias_path', '');

    }

    /**
     * Путь страницы авторизации
     * @return mixed|string
     */
    public static function getAuthPath(){

        return ArrayHelper::getValue(Tree::getCachedSection(), \Yii::$app->sections->getValue('auth') . '.alias_path', '');

    }

    /**
     * список режимов активации
     * @return array
     */
    public static function getActivateStatusList(){
        return array(
            1 => \Yii::t('auth', 'activate_auto'),
            2 => \Yii::t('auth', 'activate_confirm'),
            3 => \Yii::t('auth', 'activate_admin'),
        );
    }

    /**
     * значения политик доступа пользователя
     * @return array
     */
    public static function getStatusList(){
        return array(
            0 => \Yii::t('auth', 'status_no_auth'),
            1 => \Yii::t('auth', 'status_auth'),
            2 => \Yii::t('auth', 'status_banned'),
        );
    }

    /**
     * Регистрация нового пользователя
     * @param array $aData Массив данных пользователя
     * @param string $sError Текст ошибки решистрации
     * @return bool;
     */
    public static function registerUser($aData, &$sError = '') {

        $oItem = self::setUserData($aData);

        if ( $oItem->validate() ) {

            // генерим токен
            $oItem->active = 0;

            $iStatus = SysVar::get( 'auth.activate_status' );

            //если автоматическая регистрация, то выставляем сразу активность
            $oItem->active = ( $iStatus == 1 ) ? 1 : 0;

            if ( $oItem->insert() ) {

                $oTicket = new AuthTicket();
                $oTicket->setModuleName('auth');
                $oTicket->setActionName('activate');
                $oTicket->setObjectId($oItem->id);
                $sToken = $oTicket->insert();

                $aParams = array();

                // активация по e-mail
                if ( $iStatus == 2 ) {
                    $sBody = ModulesParams::getByName('auth', 'mail_user_activate');

                    $aParams['link'] = Site::httpDomain().Api::getAuthPath().'?cmd=AccountActivation&token='.$sToken;

                    Mailer::sendMail( $oItem->email, ModulesParams::getByName('auth', 'mail_title_mail_activate'), $sBody, $aParams);
                }

                $sBody = ModulesParams::getByName('auth', 'mail_admin_activate');

                $aParams['link'] = Site::httpDomainSlash().'admin/#out.left.tools=Auth;out.tabs=tools_Auth';

                Mailer::sendMailAdmin( ModulesParams::getByName('auth', 'mail_title_admin_newuser'), $sBody, $aParams);

                return true;
            }
        } else
            $sError = $oItem->getValidateErrorMessage();

        return false;
    }

    /**
     * Установить данные пользователя
     * @param array $aData Новые данные пользователя
     * @param int $iUserId Id пользователя
     * @return UsersRow|bool
     */
    public static function setUserData($aData, $iUserId = 0) {

        $oUser = $iUserId ? Users::find($iUserId) : new UsersRow();
        if (!$oUser) return false;

        foreach($aData as $sField => $sVal) {
            if (isset($oUser->$sField))
                $oUser->$sField = $sVal;
        }
        return $oUser;
    }

    /**
     * Получение пользователя по логину
     * @param string $sLogin
     * @return UsersRow
     */
    public static function getUserByLogin( $sLogin ) {

        /** @var UsersRow $oUser */
        $oUser = Users::find()->where( 'login', $sLogin )->getOne();

        return $oUser;
    }


    /**
     * Генерация токена и отправка сообщения о возможности смены пароля
     * @param UsersRow $oUser
     * @return bool
     */
    public static function recoverPass( UsersRow $oUser ) {

        $oTicket = new AuthTicket();
        $oTicket->setModuleName('auth');
        $oTicket->setActionName('recover_pass');
        $oTicket->setObjectId($oUser->id);

        $sToken = $oTicket->insert();

        $bRes = $oUser->email and $oUser->save();

        if ( $bRes ) {

            $sBody = ModulesParams::getByName('auth', 'mail_reset_password');
            $aParams['link'] = Site::httpDomain().self::getAuthPath().'?cmd=newPassForm&token='.$sToken;

            Mailer::sendMail( $oUser->email, ModulesParams::getByName('auth', 'mail_title_reset_password'), $sBody, $aParams);

        }

        return $bRes;
    }


    /**
     * Сохранение нового пароля и сброс токена
     * @param UsersRow $oUser
     * @param $sPassword
     * @return bool
     */
    public static function saveNewPass( UsersRow $oUser, $sPassword ) {

        $oUser->pass = Auth::buildPassword( $oUser->login, $sPassword );

        $bRes = $oUser->save();

        // send mail
        if ( $bRes ){
            $bRes = Mailer::sendMail($oUser->email,
                ModulesParams::getByName('auth', 'mail_title_new_pass'),
                ModulesParams::getByName('auth', 'mail_new_pass')
            );
        }

        return $bRes;
    }


    /**
     * Активация аккаунта
     * @param UsersRow $oUser
     * @return bool
     */
    public static function accountActivate( UsersRow $oUser ) {

        $oUser->active = 1;

        if ( $oUser->save() ){
            $sBody = static::getTextMailActivate();

            Mailer::sendMail( $oUser->email, ModulesParams::getByName('auth', 'mail_title_mail_activate'), $sBody);

            return true;
        }

        return false;
    }

    /**
     * получить текст письма уведамления для активации пользователя
     */
    public static function getTextMailActivate(){
        return ModulesParams::getByName('auth', 'mail_activate');
    }


    /**
     * получить текст письма уведамления для снятия бана
     */
    public static function getTextMailCloseBan(){
        return ModulesParams::getByName('auth', 'mail_close_ban');
    }

    /**
     * получить текст письма уведамления о блокировании
     */
    public static function getTextMailBanned(){
        return ModulesParams::getByName('auth', 'mail_banned');
    }

} 