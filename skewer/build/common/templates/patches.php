<?php
/**
 * @var string $description
 */
?><?= '<?php' ?>

use skewer\components\config\PatchPrototype;

class PatchInstall extends PatchPrototype {

    public $sDescription = '<?= $description ?>';

    public $bUpdateCache = false;

    public function execute() {

    }

}