<?php

$aLanguage = array();

$aLanguage['ru']['FAQ.Adm.tab_name'] = 'Вопросы и ответы';
$aLanguage['ru']['id'] = 'Идентификатор записи';
$aLanguage['ru']['parent'] = 'Раздел';
$aLanguage['ru']['date_time'] = 'Дата публикации';
$aLanguage['ru']['name'] = 'Имя автора';
$aLanguage['ru']['email'] = 'E-mail';
$aLanguage['ru']['content'] = 'Вопрос';
$aLanguage['ru']['city'] = 'Город';
$aLanguage['ru']['status'] = 'Статус';
$aLanguage['ru']['answer'] = 'Ответ';

$aLanguage['ru']['alias'] = 'Псевдоним';
$aLanguage['ru']['tag_title'] = 'Заголовок';
$aLanguage['ru']['description'] = 'Описание';
$aLanguage['ru']['keywords'] = 'Ключевые слова';

$aLanguage['ru']['add'] = 'Добавить';

$aLanguage['ru']['SEO-fields'] = 'SEO поля';

$aLanguage['ru']['settings'] = 'Настройки';
$aLanguage['ru']['mail_title'] = 'Заголовок письма администратору';
$aLanguage['ru']['mail_body'] = 'Текст письма администратору о новом вопросе';
$aLanguage['ru']['mail_title_user'] = 'Заголовок письма автоответа';
$aLanguage['ru']['mail_body_user'] = 'Текст письма автоответа о новом вопросе';
$aLanguage['ru']['approve'] = 'Одобрить';
$aLanguage['ru']['reject'] = 'Отклонить';
$aLanguage['ru']['new'] = 'новый';
$aLanguage['ru']['approved'] = 'одобрен';
$aLanguage['ru']['rejected'] = 'отклонен';
$aLanguage['ru']['enable_notifications'] = 'Включить уведомления пользователю';
$aLanguage['ru']['email_approve_subject'] = 'Заголовок письма уведомления при одобрении';
$aLanguage['ru']['email_approve_text'] = 'Текст письма уведомления при одобрении';
$aLanguage['ru']['email_reject_subject'] = 'Заголовок письма уведомления при отклонении';
$aLanguage['ru']['email_reject_text'] = 'Текст письма уведомления при отклонении';
$aLanguage['ru']['answer_reject_template'] = 'Вопрос на сайте';
$aLanguage['ru']['answer_reject_content_template'] = 'Ваш вопрос на сайте {0} не был одобрен и не будет размещен';
$aLanguage['ru']['answer_approve_content_template'] = 'Ваш вопрос на сайте {0} был одобрен и размещен';

$aLanguage['ru']['head_mail_text'] = 'Метки для замены:<br>
[{0}] - Название сайта<br>
[{1}] - Адрес сайта<br>
[email] - email<br>
';

$aLanguage['ru']['back'] = 'Назад';
$aLanguage['ru']['ans_success'] = 'Спасибо за Ваш вопрос. После прохождения модерации он появится на сайте';
$aLanguage['ru']['ans_error'] = 'Ошибка! Данные Не были отправлены.';
$aLanguage['ru']['ans_captcha_error'] = 'Ошибка! Числа с картинки введены неверно.';
$aLanguage['ru']['ans_validation'] = 'Ошибка валидации!';
$aLanguage['ru']['ans_not_found'] = 'Ошибка! Такая форма не существует.';
$aLanguage['ru']['ans_not_exists'] = 'Ошибка! Необходимо создать и настроить форму.';
$aLanguage['ru']['ans_data_error'] = 'Ошибка данных!';
$aLanguage['ru']['ans_upload'] = 'Ошибка загрузки файла!';
$aLanguage['ru']['ans_maxsize'] = 'Ошибка! Превышен максимально допустимый размер загружаемого файла.';
$aLanguage['ru']['ans_format'] = 'Ошибка! Недопустимый формат файла.';
$aLanguage['ru']['ans_handler'] = 'Ошибка! Обработчик не найден.';
$aLanguage['ru']['question'] = 'Вопрос';
$aLanguage['ru']['answer'] = 'Ответ';
$aLanguage['ru']['message_not_found'] = 'Сообщение не найдено';
$aLanguage['ru']['letter_content'] = 'На адрес сайта оставлен новый вопрос. Чтобы ответить на него, зайдите в систему управления сайтом.';
$aLanguage['ru']['letter_subject'] = 'Вопрос с сайта';
$aLanguage['ru']['answer_title_template'] = 'Вопрос с сайта';
$aLanguage['ru']['asnwer_body_tempalte'] = 'На адрес сайта оставлен новый вопрос. Чтобы ответить на него, зайдите в систему управления сайтом.';

$aLanguage['en']['FAQ.Adm.tab_name'] = 'FAQ';
$aLanguage['en']['id'] = 'ID';
$aLanguage['en']['parent'] = 'Parent ID';
$aLanguage['en']['date_time'] = 'Date';
$aLanguage['en']['name'] = 'Author';
$aLanguage['en']['email'] = 'E-mail';
$aLanguage['en']['content'] = 'Question';
$aLanguage['en']['city'] = 'City';
$aLanguage['en']['status'] = 'Status';
$aLanguage['en']['answer'] = 'Answer';

$aLanguage['en']['alias'] = 'alias';
$aLanguage['en']['tag_title'] = 'tag_title';
$aLanguage['en']['description'] = 'description';
$aLanguage['en']['keywords'] = 'keywords';

$aLanguage['en']['add'] = 'Add';

$aLanguage['en']['SEO-fields'] = 'SEO fields';

$aLanguage['en']['settings'] = 'Settings';
$aLanguage['en']['mail_title'] = 'E-mail title';
$aLanguage['en']['mail_body'] = 'E-mail text';
$aLanguage['en']['mail_title_user'] = 'Subject Auto Answer';
$aLanguage['en']['mail_body_user'] = 'Text of the letter of the new auto-answer question';
$aLanguage['en']['approve'] = 'Approve';
$aLanguage['en']['reject'] = 'Reject';
$aLanguage['en']['new'] = 'new';
$aLanguage['en']['approved'] = 'approved';
$aLanguage['en']['rejected'] = 'rejected';
$aLanguage['en']['enable_notifications'] = 'Enable notifications';
$aLanguage['en']['email_approve_subject'] = 'Message subject with the approval notice';
$aLanguage['en']['email_approve_text'] = 'The text of the letter with the approval notice';
$aLanguage['en']['email_reject_subject'] = 'Message header notification when a deviation';
$aLanguage['en']['email_reject_text'] = 'The text of the letter notification when a deviation';
$aLanguage['en']['answer_reject_template'] = 'Question on the site';
$aLanguage['en']['answer_reject_content_template'] = 'Your question on the website {0} has not been approved and will not be placed';
$aLanguage['en']['answer_approve_content_template'] = 'Your question on the website {0} has been approved and posted';

$aLanguage['en']['head_mail_text'] = 'Tags for replacement: <br>
[{0}] - The name of the site <br>
[{1}] - Site Address <br>
[email] - Email <br>
';

$aLanguage['en']['back'] = 'Back';
$aLanguage['en']['ans_success'] = 'Thank you for your question. After passing the moderation it will appear on the site';
$aLanguage['en']['ans_error'] = 'Error! The data have not been sent.';
$aLanguage['en']['ans_captcha_error'] = 'Error! You have entered incorrect code.';
$aLanguage['en']['ans_validation'] = 'Validation error!';
$aLanguage['en']['ans_not_found'] = 'Error! This form does not exist.';
$aLanguage['en']['ans_not_exists'] = 'Error! You must create and customize the form.';
$aLanguage['en']['ans_data_error'] = 'Data error!';
$aLanguage['en']['ans_upload'] = 'Upload error!';
$aLanguage['en']['ans_maxsize'] = 'Error! Exceeded the maximum file upload size.';
$aLanguage['en']['ans_format'] = 'Error! Invalid file format.';
$aLanguage['en']['ans_handler'] = 'Error! The handler is not found.';
$aLanguage['en']['question'] = 'Question';
$aLanguage['en']['answer'] = 'Answer';
$aLanguage['en']['message_not_found'] = 'Message not found';
$aLanguage['en']['letter_content'] = 'On the website address by a new question. To answer this question, go to the content management system.';
$aLanguage['en']['letter_subject'] = 'The issue with the site';
$aLanguage['en']['answer_title_template'] = 'Question from site';
$aLanguage['en']['asnwer_body_tempalte'] = 'On the website address by a new question. To answer this question, go to the content management system.';

return $aLanguage;