<?php

namespace skewer\build\Adm\FAQ\ar;


use skewer\base\orm;
use skewer\base\ft;
use skewer\base\section\models\TreeSection;
use skewer\components\seo\Service;
use skewer\helpers\Transliterate;

class FAQRow extends orm\ActiveRecord{

    public $id = 0;
    public $parent = 0;
    public $date_time = '';
    public $name = '';
    public $email = '';
    public $content = '';
    public $status = 0;
    public $city = '';
    public $answer = '';
    public $last_modified_date = '';

    public $alias = '';

    function __construct() {
        $this->setTableName( 'faq' );
        $this->setPrimaryKey( 'id' );
    }

    /**
     * @inheritdoc
     */
    public function preSave()
    {
        if ( !$this->date_time || ($this->date_time == 'null') )
            $this->date_time = date("Y-m-d H:i:s", time());

        $this->last_modified_date = date('Y-m-d H:i:s', time());

        if (!$this->alias){
            $sValue = Transliterate::change( strip_tags(html_entity_decode($this->content)) );
        }else{
            $sValue = Transliterate::change( $this->alias );
        }

        // к числам прибавляем префикс
        if ( is_numeric($sValue) ){
            $sValue = 'faq-' . $sValue;
        }

        // приводим к нужному виду
        $sValue = Transliterate::changeDeprecated( $sValue );
        $sValue = Transliterate::mergeDelimiters( $sValue );
        $sValue = trim($sValue,'-');

        $this->alias = Service::generateAlias($sValue,$this->id,$this->parent,'FAQ');

        parent::preSave();
    }

    /**
     * Вернет урл
     * @return string
     */
    public function getUrl(){
        $sAlias =  ($this->alias)? "alias={$this->alias}" : "id={$this->id}";
        $sUrl   = "[{$this->parent}][FAQ?{$sAlias}]";
        return $sUrl;
    }
    
    public function preDelete(){
        TreeSection::updateLastModify($this->parent);
    }

}