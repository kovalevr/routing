<?php

namespace skewer\build\Adm\FAQ;


use skewer\base\orm\state\StateSelect;
use skewer\components\i18n\Languages;
use skewer\components\i18n\ModulesParams;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\components\seo;
use skewer\components\auth\CurrentAdmin;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\build\Adm\FAQ;

/**
 * Class Module
 * @package skewer\build\Adm\FAQ
 */
class Module extends Adm\Tree\ModulePrototype {

    // число элементов на страницу
    public  $onAdmPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    protected $iStatusFilter = Api::statusNew;

    protected $sLanguageFilter = '';

    private $paramKeys = [
        'title_admin', 'content_admin',
        'title_user', 'content_user', 'onNotif',
        'notifTitleApprove', 'notifContentApprove', 'notifTitleReject',
        'notifContentReject'
    ];

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        $this->iStatusFilter = $this->get('filter_status', false);

        $this->sLanguageFilter = $this->get('filter_language', \Yii::$app->language);

        // проверить права доступа
        if ( !CurrentAdmin::canRead($this->sectionId()) )
            throw new UserException( 'accessDenied' );

    }

    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'page' => $this->iPage,
            'filter_status' => $this->iStatusFilter,
            'filter_language' => $this->sLanguageFilter,
        ) );

    }

    protected function actionInit(){
        $this->actionList();
    }

    /**
     * list
     */
    protected function actionList(){

        $oFormBuilder = ui\StateBuilder::newList();

        /** Фильтр по статусу */
        $oFormBuilder->filterSelect( 'filter_status', Api::getStatusList(), $this->iStatusFilter, \Yii::t('faq', 'status') );

        /** Добавляем поля для списка */
        $oFormBuilder
            ->fieldString('name', \Yii::t('faq', 'name'))
            ->fieldString('date_time', \Yii::t('faq', 'date_time'))
            ->fieldString('content', \Yii::t('faq', 'content'), ['listColumns' => ['flex' => 3]])
            ->fieldString('answer', \Yii::t('faq', 'answer'), ['listColumns' => ['flex' => 3]])
            ->fieldString('status', \Yii::t('faq', 'status'))

            /** для статуса */
            ->widget( 'status', 'skewer\\build\\Adm\\FAQ\\Service', 'getStatusValue' )

            /** кнопки в записи */
            ->buttonRowUpdate('edit')
            ->buttonRowDelete('delete')

            /** кнопки общие */
            ->buttonAddNew('new', \Yii::t('faq', 'add'))
            ->buttonEdit('settings', \Yii::t('faq', 'settings'))
        ;

        /** @var StateSelect $aItems */
        $aItems = ar\FAQ::find();
        if ($this->iStatusFilter !== false){
            $aItems = $aItems->where('status', $this->iStatusFilter);
        }

        $iCount = 0;
        $aItems = $aItems
            ->where('parent', $this->sectionId())
            ->order('date_time', 'DESC')
            ->setCounterRef( $iCount )
            ->limit($this->onAdmPage, $this->iPage * $this->onAdmPage)
            ->asArray()
            ->getAll();

        $oFormBuilder->setValue($aItems, $this->onAdmPage, $this->iPage, $iCount);

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * edit
     */
    protected function actionEdit(){
        $this->showForm($this->getInDataValInt('id'));
    }

    /**
     * new
     */
    protected function actionNew(){
        $this->showForm();
    }

    /**
     * Форма редактирования
     * @param null $id
     */
    private function showForm($id = null){

        $oForm = ui\StateBuilder::newEdit()

            // добавление кнопок
            ->buttonSave('save')
            ->buttonCancel('list');

        /**
         * @var $oFAQRow ar\FAQRow
         */
        if ($id){
            $oFAQRow = ar\FAQ::find($id);

            // кнопки модерации
            if ( $oFAQRow->status == Api::statusNew ) {

                $oForm
                    ->buttonSeparator('-')

                    // одобрить
                    ->button('save', \Yii::t('faq', 'approve'), 'icon-commit', 'save', [
                        'addParams' => ['setStatus' => Api::statusApproved],
                        'unsetFormDirtyBlocker' => true
                    ])

                    // отклонить
                    ->button('save', \Yii::t('faq', 'reject'), 'icon-stop', 'save', [
                        'addParams' => ['setStatus' => Api::statusRejected],
                        'unsetFormDirtyBlocker' => true
                    ]);
            }

            // кнопка удаления
            $oForm
                ->buttonSeparator('->')
                ->button('delete', \Yii::t('adm', 'del'), 'icon-delete', 'list');
        }else{
            $oFAQRow = ar\FAQ::getNewRow(array('status' => Api::statusNew));
            $oFAQRow->date_time = date('Y-m-d H:i:s');
        }

        /** Добавим поля */
        $oForm
            ->fieldHide('id', 'id')
            ->fieldString('name', \Yii::t('faq', 'name'))
            ->fieldString('email', \Yii::t('faq', 'email'))
            ->fieldString('city', \Yii::t('faq', 'city'))
            ->field('date_time', \Yii::t('faq', 'date_time'), 'datetime');
        $oForm
            ->field('content', \Yii::t('faq', 'content'), 'text')
            ->field('answer', \Yii::t('faq', 'answer'), 'wyswyg', ['height'=> 200])
            ->fieldSelect('status', \Yii::t('faq', 'status'), Api::getStatusList(), [], false)

            ->fieldString('alias', \Yii::t('faq', 'alias'));

        $oForm->setValue($oFAQRow->getData());

        // добавление SEO блока полей
        seo\Api::appendExtForm( $oForm, new FAQ\Seo($oFAQRow->id, $this->sectionId(), $oFAQRow->getData()), ['seo_gallery', 'none_search'] );

        $this->setInterface($oForm->getForm());
    }

    /**
     * save
     */
    protected function actionSave(){

        $aData = $this->getInData();
        if( !isset($aData['parent']) )
            $aData['parent'] = $this->sectionId();

        // перекрытие статуса
        $iStatus = $this->get( 'setStatus', null );
        if ( !is_null( $iStatus ) )
            $aData['status'] = $iStatus;

        //todo Вытащено из skewer\build\Adm\FAQ::saveItem
        //todo Переписать этот участок и метод saveItem
        $id = isset($aData['id'])?$aData['id']:false;
        if ($id){
            $oFAQRow = ar\FAQ::find($id);
            $aOldAttributes = $oFAQRow->getData();
        }else{
            $oFAQRow = ar\FAQ::getNewRow();
            $aOldAttributes = $oFAQRow->getData();
        }

        $iId = Api::saveItem($aData);

        /** @var FAQ\ar\FAQRow $aItem */
        $aItem = Adm\FAQ\ar\FAQ::find($iId);

        if (seo\Service::$bAliasChanged)
            $this->addMessage(\Yii::t('tree','urlCollisionFlag',['alias'=>$aItem->alias]));

        $oSearch = new Search();
        $oSearch->updateByObjectId($iId);

        // сохранение SEO данных
        seo\Api::saveJSData(
            new FAQ\Seo(ArrayHelper::getValue($aOldAttributes, 'id', 0), $this->sectionId(), $aOldAttributes),
            new FAQ\Seo($aItem->id, $this->sectionId(), $aItem->getData()),
            $aData
        );

        seo\Api::setUpdateSitemapFlag();

        $this->actionList();
    }

    /**
     * delete
     */
    protected function actionDelete(){
        $id = $this->getInDataValInt('id');
        if ($id){
            $oFaqRow = Adm\FAQ\ar\FAQ::findOne(['id' => $id]);
            $oFaqRow->delete();
            $oSearch = new Search();
            $oSearch->deleteByObjectId($id);
        }
        $this->actionList();
    }


    /**
     * Форма настроек модуля
     */
    protected function actionSettings(){

        $oForm = ui\StateBuilder::newEdit()
            ->buttonSave('saveSettings')
            ->buttonCancel();


        $aLanguages = Languages::getAllActive();
        $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

        if (count($aLanguages) > 1) {
            $oForm->filterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
            $oForm->setFilterAction('settings');
        }

        $oForm
            ->field('info', '', 'show', ['hideLabel' => true])
            ->fieldString('title_admin', \Yii::t('faq', 'mail_title'))
            ->field('content_admin', \Yii::t('faq', 'mail_body'), 'wyswyg', ['listColumns.flex' => 1, 'growMax' => 500, 'grow' => true])
            ->fieldString('title_user', \Yii::t('faq', 'mail_title_user'))
            ->field('content_user', \Yii::t('faq', 'mail_body_user'), 'wyswyg', ['listColumns.flex' => 1, 'growMax' => 500, 'grow' => true])
            ->field('onNotif', \Yii::t('faq', 'enable_notifications'), 'check')
            ->fieldString('notifTitleApprove', \Yii::t('faq', 'email_approve_subject'))
            ->field('notifContentApprove', \Yii::t('faq', 'email_approve_text'), 'wyswyg', ['listColumns.flex' => 1, 'growMax' => 500, 'grow' => true])
            ->fieldString('notifTitleReject', \Yii::t('faq', 'email_reject_subject'))
            ->field('notifContentReject', \Yii::t('faq', 'email_reject_text'), 'wyswyg', ['listColumns.flex' => 1, 'growMax' => 500, 'grow' => true])
        ;

        $aModulesData = ModulesParams::getByModule('faq', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('faq', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);

        foreach( $this->paramKeys as $sKey ){
            $aItems[$sKey] = ArrayHelper::getValue($aModulesData, $sKey, '');
        }

        $oForm->setValue($aItems);

        // вывод данных в интерфейс
        $this->setInterface( $oForm->getForm() );
    }
    
    /**
     * Сохраняем настройки формы
     */
    protected function actionSaveSettings(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->paramKeys))
                    continue;

                ModulesParams::setParams( 'faq', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

}
