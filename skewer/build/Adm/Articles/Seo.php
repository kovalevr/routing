<?php
namespace skewer\build\Adm\Articles;

use skewer\base\section\Tree;
use skewer\base\site\Site;
use skewer\build\Page\Articles\Model;
use skewer\components\seo\SeoPrototype;
use yii\helpers\ArrayHelper;

class Seo extends SeoPrototype{

    public static function getGroup(){
        return 'articles';
    }

    /**
     * @inheritdoc
     */
    public static function getAlias(){
        return 'articlesDetail';
    }

    /**
     * @return array
     */
    public function extractReplaceLabels(){
        return array(
            'label_article_title_upper' => ArrayHelper::getValue($this->aDataEntity, 'title', ''),
            'label_article_title_lower' => $this->toLower( ArrayHelper::getValue($this->aDataEntity, 'title', '') )
        );
    }

    public function loadDataEntity(){

        if ($oRow = Model\Articles::getPublicById( $this->iEntityId )){
            $this->aDataEntity = $oRow->getData();
        }

    }

    /**
     * @inheritdoc
     */
    protected function getSearchClassName(){
        return Search::className();
    }


    /**
     * @inheritdoc
     */
    public function getRecordWithinEntityByPosition($iPosition){

        $aResult = Model\Articles::find()
            ->where('parent_section', $this->iSectionId)
            ->limit(1, $iPosition)
            ->order( 'publication_date', 'DESC' )
            ->get();

        if (!isset($aResult[0]))
            return false;

        /** @var Model\ArticlesRow $oCurrentRecord */
        $oCurrentRecord = $aResult[0];
        $this->setDataEntity($oCurrentRecord->getData());

        $aRow = array_merge($oCurrentRecord->getData(),[
            'url' => Site::httpDomain() . \Yii::$app->router->rewriteURL($oCurrentRecord->getUrl()),
            'seo' => $this->parseSeoData()
        ]);


        return $aRow;

    }


    /**
     * @inheritdoc
     */
    public function doExistRecord($sPath ){

        $sAlias = '';
        Tree::getSectionByPath($sPath, $sAlias);
        $sAlias = trim($sAlias, '/');

        /** @var Model\ArticlesRow $oRecord */
        return ($oRecord = Model\Articles::getPublicByAlias($sAlias))
            ? $oRecord->id
            : false;

    }

}