<?php

namespace skewer\build\Adm\Auth;

use skewer\components\auth\Users;
use skewer\helpers\Mailer;
use skewer\components\i18n\Languages;
use skewer\components\i18n\ModulesParams;
use skewer\base\section\Parameters;
use skewer\base\ui;
use skewer\build\Page\Auth\Api;
use skewer\build\Adm;
use skewer\components\auth\Auth;
use skewer\components\auth\CurrentAdmin;
use skewer\components\auth\Policy;
use skewer\components\ext;
use skewer\base\SysVar;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\build\Tool\Auth\Module as ToolModule;


/**
 * Модуль настройки вывода форм регистрации
 * Class Module
 * @package skewer\build\Adm\Catalog
 */
class Module extends Adm\Tree\ModulePrototype {

    protected $sLanguageFilter = '';

    /** @var array Поля настроек */
    protected $aSettingsKeys =
        [
            'mail_activate',
            'mail_close_ban',
            'mail_banned',
            'mail_admin_activate',
            'mail_user_activate',
            'mail_reset_password',
            'mail_title_admin_newuser',
            'mail_title_user_newuser',
            'mail_title_reset_password',
            'mail_title_new_pass',
            'mail_new_pass',
            'mail_title_mail_activate',
            'mail_title_mail_close_banned',
            'mail_title_mail_banned'
        ];

    protected $iStatusFilter = 0;

    // фильтр по тексту
    protected $sSearchNameFilter = '';
    protected $sSearchEmailFilter = '';
    protected $sSearchPhoneFilter = '';

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     */
    protected function preExecute() {
        // id текущего раздела
        $this->iStatusFilter = $this->get('filter_status', false);
        $this->sSearchNameFilter = $this->getStr('search');
        $this->sSearchEmailFilter = $this->getStr('email');
        $this->sSearchPhoneFilter = $this->getStr('phone');

        // проверить права доступа
        if (!CurrentAdmin::canRead($this->sectionId()))
            throw new UserException('accessDenied');

        $sLanguage = \Yii::$app->language;
        if ($this->sectionId()){
            $sLanguage = Parameters::getLanguage($this->sectionId());
        }

        $this->sLanguageFilter = $this->get('filter_language', $sLanguage);
    }


    /**
     * Первичное состояние
     */
    protected function actionInit() {
        $this->actionList();
    }

    /**
     * Сохраняем состояние
     */
    protected function actionSaveStatement(){

        $iStatus = $this->getInDataValInt('status',0);
        SysVar::set('auth.activate_status',$iStatus);

        $this->actionList();
    }

    /**
     * Выбор активации пользователя
     */
    protected function actionEditActivateStatement(){

        $this->render(new view\ActivateStatement([
            'list' => Api::getActivateStatusList(),
            'value' => SysVar::get('auth.activate_status')
        ]));

    }

    protected function actionChangeStatus(){

        $iUserId =  $this->getInDataValInt('id');
        $iActiveId = $this->getInDataValInt('active');

        /**
         * @var Adm\Auth\ar\UsersRow $oUser
         */
        $oUser = Adm\Auth\ar\Users::find($iUserId);

        if ($oUser){

            $prevStatus = $oUser->active;

            $oUser->active = $iActiveId;
            $oUser->save();


            $sMsg = \Yii::t('auth', 'change_status');
            $sSubject = \Yii::t('auth', 'change_status');

            if ($prevStatus == Api::STATUS_NO_AUTH && $iActiveId == Api::STATUS_AUTH){
                $sMsg = Api::getTextMailActivate();
                $sSubject = ModulesParams::getByName('auth', 'mail_title_mail_activate');
            }

            if ($prevStatus == Api::STATUS_BANNED && $iActiveId == Api::STATUS_AUTH){
                $sMsg = Api::getTextMailCloseBan();
                $sSubject = ModulesParams::getByName('auth', 'mail_title_mail_close_banned');
            }

            if ($iActiveId == Api::STATUS_BANNED){
                $sMsg = Api::getTextMailBanned();
                $sSubject = ModulesParams::getByName('auth', 'mail_title_mail_banned');
            }

            Policy::incPolicyVersion();

            Mailer::sendMail( $oUser->email, $sSubject, $sMsg);

        }

        $aActiveStatusList = Api::getStatusList();

        ui\StateBuilder::updRow($this, array_merge($oUser->getData(), array('text_active' => $aActiveStatusList[$oUser->active])));

    }

    /**
     * Список пользователей в магазине
     */
    protected function actionList() {
        $oList = new ext\ListView();

        $aActiveStatusList = Api::getStatusList();

        $oList->addFilterText( 'search', $this->sSearchNameFilter, \Yii::t('auth', 'name') );
        $oList->addFilterText( 'email', $this->sSearchEmailFilter, \Yii::t('auth', 'email') );
        $oList->addFilterText( 'phone', $this->sSearchPhoneFilter, \Yii::t('auth', 'contact_phone') );

        $oList->setFieldsByFtModel(ar\Users::getModel(), 'list');

        // добавляем фильтр по статусам
        $oList->addFilterSelect('filter_status', Api::getStatusList(), $this->iStatusFilter, \Yii::t('auth', 'field_status_active'));

        $oList->setFieldFlex('id', 1);
        $oList->setFieldFlex('login', 3);
        $oList->setFieldFlex('name', 3);


        $aUsersCatalog = ar\Users::find()->where('group_policy_id',3);

        if ($this->iStatusFilter !== false)
            $aUsersCatalog->where('active', $this->iStatusFilter);

        if ( $this->sSearchNameFilter ) {
            $aUsersCatalog->where( 'name LIKE ?', '%' . $this->sSearchNameFilter . '%' );
        }
        if ( $this->sSearchEmailFilter ) {
            $aUsersCatalog->where( 'email LIKE ?', '%' . $this->sSearchEmailFilter . '%' );
        }
        if ( $this->sSearchPhoneFilter ) {
            $aUsersCatalog->where( 'phone LIKE ?', '%' . $this->sSearchPhoneFilter . '%' );
        }

        $aUsersCatalog = $aUsersCatalog->getAll();

        foreach($aUsersCatalog as $k=>$item){
            $aUsersCatalog[$k]->login = htmlentities($aUsersCatalog[$k]->login);
            $aUsersCatalog[$k]->name = htmlentities($aUsersCatalog[$k]->name);
            $aUsersCatalog[$k]->postcode = htmlentities($aUsersCatalog[$k]->postcode);
            $aUsersCatalog[$k]->address = htmlentities($aUsersCatalog[$k]->address);
            $aUsersCatalog[$k]->phone = htmlentities($aUsersCatalog[$k]->phone);
            $aUsersCatalog[$k]->text_active = $aActiveStatusList[$item->active];
        }
        $oList->setValues($aUsersCatalog);

        $oField = new ext\field\StringField();
        $oField->setName('text_active');
        $oField->setTitle(sprintf('%s', \Yii::t('auth', 'activate_status')));
        $oList->addField($oField);
        $oList->setFieldFlex('text_active');


        $oList->addRowCustomBtn('StatusGroupBtn');

        $oList->addRowBtnUpdate('editUser');
        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBtnAdd('newUser');
        $oList->addDockedItem(array(
            'text' => \Yii::t('auth', 'status_edit'),
            'iconCls' => 'icon-edit',
            'state' => 'editActivateStatement',
            'action' => 'editActivateStatement',
            'addParams' => array()
        ));
        $oList->addDockedItem(array(
                'text' => \Yii::t('auth', 'mail_status_edit'),
                'iconCls' => 'icon-edit',
                'state' => 'showMail',
                'action' => 'showMail',
                'addParams' => array()
            )
        );
        $oList->addDockedItem(array(
            'text' => \Yii::t('auth', 'license_edit'),
            'iconCls' => 'icon-edit',
            'state' => 'editLicense',
            'action' => 'editLicense',
            'addParams' => array()
        ));

        $this->setInterface($oList);
    }

    /**
     * Редактируем письма активации
     */
    protected function actionSaveMail(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage) {
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->aSettingsKeys))
                    continue;

                ModulesParams::setParams( 'auth', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

    protected function actionShowMail(){

        $aModulesData = ModulesParams::getByModule('auth', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('auth', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);

        foreach( $this->aSettingsKeys as  $key ){
            $aItems[$key] = (isset($aModulesData[$key]))?$aModulesData[$key]:'';
        }

        if ($this instanceof ToolModule){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');
        } else {
            $aLanguages = [];
        }

        $this->render(new view\Letters([
            'items' => $aItems,
            'lang' => $this->sLanguageFilter,
            'langList' => $aLanguages
        ]));

    }

    protected function actionDelete(){
        $aData = $this->get('data');
        if (isset($aData['id'])){
            $iItemId = $aData['id'];
            ar\Users::delete()->where('id',$iItemId)->where('group_policy_id',3)->get();

            Policy::incPolicyVersion();
        }
        $this->actionList();
    }


    protected function actionEditUser(){

        $iUserId =  $this->getInDataValInt('id');

        /**
         * @var Adm\Auth\ar\UsersRow $oUser
         */
        $oUser = Adm\Auth\ar\Users::find($iUserId);
        $this->showForm($oUser);
    }


    /**
     * Форма добавления
     */
    protected function actionNewUser() {
        $this->showForm();
    }

    protected function actionSaveUser(){
        // получим данные
        $id = $this->getInDataValInt('id');
        $sLogin = $this->getInDataVal('login');
        $sPassword = $this->getInDataVal('pass');


        // редактирование
        if ($id){
            /**
             * @var ar\UsersRow $oUser
             */
            $oUser = ar\Users::find($id);
            $oUser->name = $this->getInDataVal('name');
            $oUser->postcode = $this->getInDataVal('postcode');
            $oUser->address = $this->getInDataVal('address');
            $oUser->phone = $this->getInDataVal('phone');
            $oUser->user_info = $this->getInDataVal('user_info');

            $oUser->save();
            $this->actionList();
        // новый юзер
        } else {
            // создадим в AR
            $oUser = new ar\UsersRow();
            $oUser->login = $sLogin;
            $oUser->pass =  $sPassword;
            $oUser->phone = $this->getInDataVal('phone');
            $oUser->address = $this->getInDataVal('address');
            $oUser->name = $this->getInDataVal('name');
            $oUser->postcode = $this->getInDataVal('postcode');
            $oUser->active = 1;
            $oUser->user_info = $this->getInDataVal('user_info');

            // отвалидируем и сохраним
            if ($oUser->validate() && $oUser->insert()){
                $this->actionList();
            } else {
                // если ошибка, то болт.
                $this->addMessage($oUser->getValidateErrorMessage());
            }
        }

    }

    protected function actionPass(){

        $oFormBuilder = ui\StateBuilder::newEdit();

        $oFormBuilder
            ->buttonSave('savePass')
            ->buttonBack()

            ->fieldHide('id','id')
            ->field('pass', \Yii::t('auth', 'password'), 'pass')
            ->field('wpass', \Yii::t('auth', 'wpassword'), 'pass')
        ;

        $aData['id'] = $this->getInDataValInt('id');
        $oFormBuilder->setValue($aData);

        // вывод данных в интерфейс
        $this->setInterface($oFormBuilder->getForm());
    }

    protected function actionSavePass(){
        $id = $this->getInDataVal('id');
        $sPass = $this->getInDataVal('pass');
        $sWpass = $this->getInDataVal('wpass');

        /**
         * @var ar\UsersRow $oUser
         */
        $oUser = ar\Users::find($id);

        if ( $oUser AND ($sPass != '') AND ($sPass == $sWpass) ) {

            // проверка сложности пароля
            if ( mb_strlen($sPass) < 6 )
                throw new UserException(\Yii::t('auth', 'err_short_pass'));

            $oUser->pass = Auth::buildPassword($oUser->login, $sPass);
            $oUser->save();

            $this->actionInit();
        } else {
            $this->addError( \Yii::t('auth', 'error_pass_fields') );
        }
    }

    /**
     * Отображение формы добавления/редактирования формы
     * @param Adm\Auth\ar\UsersRow $oItem
     */
    private function showForm($oItem = null) {

        $oFormBuilder = ui\StateBuilder::newEdit();

        $aActiveStatusList = Api::getStatusList();


        $oFormBuilder
            ->buttonSave('saveUser')
            ->buttonCancel()

        // добавляем поля

            ->field('id', 'id', ($oItem) ? 'string' : 'hide', ($oItem) ? ['readOnly' => true] : [])
            ->fieldString('login', \Yii::t('auth', 'email'), ($oItem)?['readOnly' => true]:[]);

        if (!$oItem){
            // для нового пользователя выведем филд пароля
            $oFormBuilder->fieldString('pass',\Yii::t('auth', 'password'));
        } else {
            // кнопка редактирования пароля
            if ( !Users::isCurrentSystemUser($oItem->id)) {
                $oFormBuilder->button('pass', \Yii::t('auth', 'pass'), 'icon-edit', ['id' => $oItem->id]);
            }
        }

        $oFormBuilder
            ->fieldString('name', \Yii::t('auth', 'name'))
            ->fieldString('postcode', \Yii::t('auth', 'postcode'))
            ->fieldString('address', \Yii::t('auth', 'address'))
            ->fieldString('phone', \Yii::t('auth', 'contact_phone'))
            ->field('user_info', \Yii::t('auth', 'user_info'), 'text');

        $aData = array();
        if ($oItem && $oItem->getData()) $aData = $oItem->getData();


        if ($oItem){
            $aData['active'] = $aActiveStatusList[$oItem->active];
            $oFormBuilder
                ->field('reg_date', \Yii::t('auth', 'reg_date'), 'show', ($oItem) ? ['readOnly' => true] : [])
                ->field('active', \Yii::t('auth', 'activate_status'), 'show', ($oItem) ? ['readOnly' => true] : []);
        };

        $aData['pass'] = '';


        // устанавливаем значения
        $oFormBuilder->setValue($aData);

        // вывод данных в интерфейс
        $this->setInterface($oFormBuilder->getForm());
    }


    protected function actionEditLicense() {

        $oFormBuilder = ui\StateBuilder::newEdit();

        $aData['license'] = ModulesParams::getByName('auth', 'reg_license', $this->sLanguageFilter);

        if (!$this->sectionId()){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->filterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->setFilterAction('editLicense');
            }
        }

        // добавляем поля
        $oFormBuilder
            ->field('license', \Yii::t('auth', 'license'), 'wyswyg')
            ->setValue( $aData )
            ->buttonSave('saveLicense')
            ->buttonBack('list')
        ;

        // вывод данных в интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    protected function actionSaveLicense() {

        ModulesParams::setParams('auth', 'reg_license', $this->sLanguageFilter, $this->getInDataVal( 'license' ));

        $this->actionList();

    }

}