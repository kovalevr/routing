<?php

namespace skewer\build\Adm\Auth;


use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Adm/Auth/web';
}