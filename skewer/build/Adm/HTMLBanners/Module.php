<?php

namespace skewer\build\Adm\HTMLBanners;

use skewer\base\ui;
use skewer\build\Adm;
use skewer\build\Adm\HTMLBanners\models\Banners as Banners;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use \skewer\build\Adm\HTMLBanners\Api as BannerApi;

/**
 * Class Module
 * @package skewer\build\Adm\Auth
 */
class Module extends Adm\Tree\ModulePrototype{

    var $iPage = 0;
    var $iOnPage = 50;

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');
    }

    protected function actionInit() {

        $this->actionList();
    }

    /**
     * Список баннеров
     */
    protected function actionList() {

        $sort = $this->getInnerData('sort','id');
        if ($sort !== 'id')
            $banners = Banners::find()
                ->orderBy([$sort=>SORT_DESC])
                ->asArray()
                ->all();
        else
            $banners = Banners::find()
                ->asArray()
                ->all();

        if ($sort == 'location')
            usort($banners, function($banner1, $banner2) {
                return (Api::getBannerLocationsPos()[$banner1['location']] > Api::getBannerLocationsPos()[$banner2['location']]);
            });

        $oFormBuilder = new ui\StateBuilder();
        $oFormBuilder = $oFormBuilder::newList();
        $oFormBuilder
            ->field('title', \Yii::t('HTMLBanners', 'field_title'), 'string', ['listColumns' => ['flex' => 3]])
            ->field('location', \Yii::t('HTMLBanners', 'field_location'), 'string', ['listColumns' => ['flex' => 2]])
            ->field('active', \Yii::t('HTMLBanners', 'field_active'), 'check', ['listColumns' => ['flex' => 1]])
            ->field('on_main', \Yii::t('HTMLBanners', 'field_onmain'), 'check', ['listColumns' => ['flex' => 1]])
            ->field('on_allpages', \Yii::t('HTMLBanners', 'field_allpages'), 'check', ['listColumns' => ['flex' => 1]])
            ->field('on_include', \Yii::t('HTMLBanners', 'field_include'), 'check', ['listColumns' => ['flex' => 1]])
            ->buttonRowUpdate()
            ->buttonRowDelete()
            ->buttonAddNew('new')
            ->button('sort', \Yii::t('adm', 'sort'), 'icon-add')
           // ->enableDragAndDrop('sortItems')
        ;

        $oFormBuilder->widget('location', '\\skewer\\build\\Adm\\HTMLBanners\\Api', 'getBannerLocation');

        $oFormBuilder->setValue($banners);
        $oFormBuilder->setEditableFields(array('active','on_main','on_allpages','on_include'),'saveFromList');

        $this->setInterface($oFormBuilder->getForm());

    }
    /*
     * Драг энд дроп
     * */
    protected function actionsortItems(){
        //TODO обработчик драг энд дропа добавить.
    }
    /*
     * Сохранение из списка
     * */
    protected function  actionSaveFromList(){

        $iId = $this->getInDataValInt( 'id' );

        $sFieldName = $this->get('field_name');

        $oRow = Banners::findOne(['id'=>$iId]);
        /** @var Banners $oRow */
        if ( !$oRow )
            throw new UserException( "Запись [$iId] не найдена" ); //@todo языковая метка

        $oRow->$sFieldName = $this->getInDataVal( $sFieldName );

        $oRow->save();

        $this->actionInit();

    }



    /**
     * Отображение формы
     */
    protected function actionShow() {

        $aData = $this->get('data');
        $iItemId = ArrayHelper::getValue( $aData, 'id', 0);
        /** @var Banners $oBannersRow */
        $oBannersRow = Banners::findOne(['id'=>$iItemId]);
        $this->showForm($oBannersRow);
    }

    protected function actionSort(){
        $this->setInnerData('sort','location');
        $this->actionList();
    }

    /**
     * Форма добавления
     */
    protected function actionNew() {
        $this->showForm( Banners::getBlankBanner() );
    }

    /**
     * Отображение формы добавления/редактирования Баннера
     * @param Banners $oItem
     * @throws UserException
     */
    private function showForm( Banners $oItem ) {

        if ( !$oItem )
            throw new UserException('Item not found');

        // -- сборка интерфейса
        $oFormBuilder = new ui\StateBuilder();
        // создаем форму
        $oFormBuilder = $oFormBuilder::newEdit();
        $oFormBuilder->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('HTMLBanners', 'field_title'))
            ->field('content', \Yii::t('HTMLBanners', 'field_content'), 'wyswyg')
            ->fieldSelect('location',\Yii::t('HTMLBanners' ,'field_location'),BannerApi::getBannerLocations(), [], false)
            ->fieldSelect('section',\Yii::t('HTMLBanners' ,'field_section'),BannerApi::getSectionList(), [], false)
            ->field('sort', \Yii::t('HTMLBanners', 'field_sort'), 'string')
            ->field('active', \Yii::t('HTMLBanners', 'field_active'), 'check')
            ->field('on_main', \Yii::t('HTMLBanners', 'field_onmain'), 'check')
            ->field('on_allpages', \Yii::t('HTMLBanners', 'field_allpages'), 'check')
            ->field('on_include', \Yii::t('HTMLBanners', 'field_include'), 'check')

            ->buttonSave()
            ->buttonBack()

            ->setValue($oItem->getAttributes());


        $this->setInterface($oFormBuilder->getForm());

    }

    /**
     * Сохранение баннера
     */
    protected function actionSave() {

        $aData = $this->get( 'data' );

        $iId = $this->getInDataValInt( 'id' );

        if ( !$aData )
            throw new UserException( 'Empty data' );

        if ( $iId ) {
            $oBannersRow = Banners::findOne(['id'=>$iId]);
            if ( !$oBannersRow )
                throw new UserException( "Запись [$iId] не найдена" );
        } else {
            $oBannersRow = Banners::getNewRow();

        }
        $oBannersRow->setAttributes($aData);

        $oBannersRow->save();

        $this->addModuleNoticeReport(\Yii::t('HTMLBanners', 'addBanner'), $aData);

        // вывод списка
        $this->actionInit();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        //todo Обработка ошибок
        $oBanner = Banners::findOne($aData['id']);
        $oBanner->delete();

        $this->addModuleNoticeReport(\Yii::t('HTMLBanners', 'deleteBanner'), $aData);

        // вывод списка
        $this->actionList();

    }

    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData( ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData([
            'page' => $this->iPage,
            // Параметр Идентификатора папки загрузки файлов модуля
            '_filebrowser_section' => 'Adm_HTMLBanners',
        ]);

    }
}// class