<?php

namespace skewer\build\Adm\Forms;

use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\components\forms;
use skewer\build\Design\Zones;
use skewer\components\auth\CurrentAdmin;
use skewer\components\ext\FormView;

/**
 * Модуль добавления формы для раздела
 */
class Module extends Adm\Tree\ModulePrototype {

    /** Имя модуля форм */
    protected $sFormModule = '';

    /** @inheritdoc */
    protected function actionInit() {

        $this->sFormModule = $this->getModuleName();

        $this->setPanelName( \Yii::t('forms', 'select_form') );
        $this->actionPreList();
    }

    /** Состояния: Настройка форм разделов */
    protected function actionPreList() {

        /** Список всех форм */
        $aTemplateForms = self::getFormsTemplates();

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit();

        forms\Table::get4Section($this->sectionId(), $this->sFormModule, $aForms, true);

        // Добавить для каждой формы раздела настройку
        $iKey = 0;
        foreach ($aForms as $sGroup => $iFormId) {
            $iKey++;

            $sFormTitle = ($sGroupTitle = Parameters::getValByName($this->sectionId(), $sGroup, Zones\Api::layoutTitleName, true)) ?
                "\"$sGroupTitle\"" :
                ((count($aForms) > 1) ? "№" . ($iKey) : "");

            // Добавить имя группы для каждой формы пользователя sys
            if ( CurrentAdmin::isSystemMode() and (count($aForms) > 1) )
                $sFormTitle .= " [$sGroup]";

            $oFormBuilder
                ->fieldSelect("form_$sGroup", rtrim(\Yii::t('forms', 'select_form') . " $sFormTitle"), $aTemplateForms, [], false)
                ->setValue(["form_$sGroup" => $iFormId]);
        }

        $oFormBuilder
            ->buttonSave('linkFormToSection')
            //->buttonAddNew('addNewForm', \Yii::t('forms', 'add_new_form'))
        ;

        // вывод данных в интерфейс
        $this->setInterface( $oFormBuilder->getForm()  );
    }

    /** Действие: привязка форм к разделу */
    protected function actionLinkFormToSection() {

        $aData = $this->get('data');

        foreach($aData as $sFormLabel => $iFormId)
            if (strpos($sFormLabel, 'form_') === 0) {

                $sGroup = substr($sFormLabel, 5);
                forms\Table::link2Section($iFormId, $this->sectionId(), $sGroup);

                /* Для формы с сохранением в базу добавить модуль просмотра заказов */
                /** @var forms\Row $oFormRow */
                $oFormRow = forms\Table::find($iFormId);
                // Если группа будет называться на точку, то объект в ней в админке не выполнится
                $sGroupFormOrders = (($sGroup == '.') ? 'root' : $sGroup) . "_orders";

                if ( $oFormRow and ($oFormRow->form_handler_type == 'toBase') ) {
                    Parameters::setParams( $this->sectionId(), $sGroupFormOrders, 'objectAdm', 'Tool\FormOrders' );
                    Parameters::setParams( $this->sectionId(), $sGroupFormOrders, 'formId', $iFormId );
                } else {
                    Parameters::removeByName( 'objectAdm', $sGroupFormOrders, $this->sectionId() );
                    Parameters::removeByName( 'formId', $sGroupFormOrders, $this->sectionId() );
                }
            }

        $oTree = Tree::getSection($this->sectionId());
        $oTree->last_modified_date = date( "Y-m-d H:i:s", time() );
        $oTree->save();

        $this->fireJSEvent( 'reload_section' );
        $this->addModuleNoticeReport( \Yii::t('forms', 'editingFormInSection'), \Yii::t('forms', 'section_id') .": $this->sectionId()");
        $this->actionPreList();
    }

    /** Получить формы для выбора */
    public static function getFormsTemplates() {

        $aFormList = forms\Table::find()->where( 'form_sys', '0' )->asArray()->getAll();
        $aTemplateForms = array( 0 => ' -- ' . (\Yii::t('forms', 'form_not_selected')) . ' --' );
        foreach ($aFormList as &$aItem)
            $aTemplateForms[$aItem['form_id']] = $aItem['form_title'];

        return FormView::markUniqueValue($aTemplateForms);
    }
}
