<?php

namespace skewer\build\Adm\Forms;


use skewer\base\orm\Query;

class Api {

    public static function checkTarget(\skewer\components\targets\CheckTarget $target ) {

        $aForms = Query::SelectFrom('forms')
            ->where('form_target',$target->sName)
            ->orWhere('form_target_google',$target->sName)
            ->asArray()
            ->getAll();

        foreach ($aForms as $form)
            $target->addCheckTarget( \Yii::t('Forms','Forms.Adm.tab_name').' : '.$form['form_title'] );

    }

    public static function className() {
        return get_called_class();
    }

}
