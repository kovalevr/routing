<?php

namespace skewer\build\Adm\Order\ar;

use skewer\base\orm;

class OrderRow extends orm\ActiveRecord {
    public $id = 0;
    public $date = '';
    public $address = '';
    public $person = '';
    public $phone = '';
    public $mail = '';
    public $status = '';
    public $postcode = '';
    public $type_payment = 0;
    public $type_delivery = 0;
    public $text = '';
    public $notes = '';
    public $token= '';
    public $auth= '';
    public $is_mobile = 0;

    function __construct() {
        $this->setTableName( 'orders' );
        $this->setPrimaryKey( 'id' );
    }

    public function preSave()
    {
        if (!$this->date || $this->date === 'null'){
            $this->date = date( "Y-m-d H:i:s", time() );
        }

        parent::preSave();
    }


}