<?php

namespace skewer\build\Adm\Order;

use skewer\components\i18n\Languages;
use skewer\components\i18n\ModulesParams;
use skewer\base\section\Parameters;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\build\Adm\Order\model\Status;
use skewer\components\catalog;
use skewer\components\auth\CurrentAdmin;
use skewer\helpers\Transliterate;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\build\Tool\Payments;
use skewer\build\Page\Cart\Api as ApiCart;
use skewer\base\SysVar;

/**
 * @todo рефакторить модуль
 * Class Module
 * @package skewer\build\Adm\Order
 */
class Module extends Adm\Tree\ModulePrototype {

    /** @var int Текущий номер станицы постраничника */
    var $iPageNum = 0;

    protected $iStatusFilter = 0;

    protected $sLanguageFilter = '';

    protected function preExecute() {

        // Получить номер страницы
        $this->iPageNum = $this->getInt('page');

        $this->iStatusFilter = $this->getInt('filter_status', 0);

        $sLanguage = \Yii::$app->language;
        if ($this->sectionId())
            $sLanguage = Parameters::getLanguage($this->sectionId()) ?: $sLanguage;

        $this->sLanguageFilter = $this->get('filter_language', $sLanguage);

    }

    protected function setServiceData(ui\state\BaseInterface $oIface) {

        parent::setServiceData($oIface);

        // расширение массива сервисных данных
        $oIface->setServiceData(array(
            'filter_status' => $this->iStatusFilter,
            'filter_language' => $this->sLanguageFilter,
        ));

    }


    protected function actionInit() {
        $this->actionList();
    }

    protected function actionDelete() {
        // запросить данные
        $aData = $this->get('data');

        // id записи
        $iItemId = (is_array($aData) and isset($aData['id'])) ? (int)$aData['id'] : 0;

        if (!$iItemId){
            $iItemId = $this->getInnerData('orderId');
        }

        ar\Order::delete($iItemId);
        // вывод списка
        $this->actionInit();
    }

    /**
     * Сохранение заказы
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get('data');
        $iId = $this->getInnerDataInt('orderId');

        if (!$aData)
            throw new UserException('Empty data');

        if ($iId) {
            /**
             * @var ar\OrderRow $oRow
             */
            $oRow = ar\Order::find($iId);

            if (!$oRow)
                throw new UserException("Not found [$iId]");

            if (isset($aData['status']) && $oRow->status != $aData['status']) {
                Service::sendMailChangeOrderStatus($iId, $oRow->status, $aData['status']);
            }

            $oRow->setData($aData);
        } else {
            $oRow = ar\Order::getNewRow($aData);
        }

        $oRow->save();
        // вывод списка
        $this->actionInit();
    }

    protected function actionShow() {

        // номер заказа
        $aData = $this->get('data');

        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;
        $iItemId = $this->getInDataVal('id_order',$iItemId);

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit();

        $statusList = Status::getListTitle();
        $paymentList = ArrayHelper::map(ar\TypePayment::find()->asArray()->getAll(),'id','title');
        $deliveryList = ArrayHelper::map(ar\TypeDelivery::find()->asArray()->getAll(),'id','title');

        $aHistoryList = Adm\Order\model\ChangeStatus::find()->asArray()->where(['id_order' => $iItemId])->all();

        $aOrder = ar\Order::find()->where('id',$iItemId)->asArray()->getOne();

        if (!$aOrder)
            throw new UserException('Item not found');


        if ($aHistoryList){
            foreach($aHistoryList as $k=>$item){
                $aHistoryList[$k]['title_old_status'] = ArrayHelper::getValue($statusList, $item['id_old_status'], sprintf('--- ['.$item['id_old_status'].']'));
                $aHistoryList[$k]['title_new_status'] = ArrayHelper::getValue($statusList, $item['id_new_status'], sprintf('--- ['.$item['id_new_status'].']'));
            }
            $aOrder['history'] = $this->renderTemplate('historyList.twig', array('historyList'=>$aHistoryList));
        }

        // добавляем поля
        $oFormBuilder
            ->field('id', 'ID', 'string', ['readOnly' => 1])
            ->field('date', \Yii::t('order', 'field_date'), 'datetime')
            ->field('person', \Yii::t('order', 'field_contact_face'), 'string')
            ->field('postcode', \Yii::t('order', 'field_postcode'), 'string')
            ->field('address', \Yii::t('order', 'field_address'), 'string')
            ->field('phone', \Yii::t('order', 'field_phone'), 'string')
            ->field('mail', \Yii::t('order', 'field_mail'), 'string')

            ->fieldSelect('status', \Yii::t('order', 'field_status'), $statusList, [], false)
            ->fieldSelect('type_payment', \Yii::t('order', 'field_payment'), $paymentList, [], false)
            ->fieldSelect('type_delivery', \Yii::t('order', 'field_delivery'), $deliveryList, [], false)

            ->field('text', \Yii::t('order', 'field_text'), 'text')
            ->field('notes', \Yii::t('order', 'field_notes'), 'text')
            ->field('good', \Yii::t('order', 'goods_info'), 'show', array('labelAlign' => 'top'))

            ->field('history', \Yii::t('order', 'history_list'), 'show', array('labelAlign' => 'top'))
        ;

        $aGoods = ar\Goods::find()->where('id_order',$aOrder['id'])->asArray()->getAll();
        foreach($aGoods as $k=>$item){
            //@TODO исправить это в каталоге
            // наверно это не правильно, но врятли у нас будет > 100 товаров в заказе
            $aGoods[$k]['object'] = catalog\GoodsSelector::get($item['id_goods'], 1); //fixme #GET_CATALOG_GOODS
        }
        $totalPrice = Api::getOrderSum($aOrder['id']);

        $sText = "";
        if ($aGoods && !empty($aGoods)) {
            $sText = $this->renderTemplate('admin.goods.twig', array('id'=>$aOrder['id'],'aGoods' => $aGoods,'totalPrice'=>$totalPrice));
        }



        $aOrder['good'] = $sText;
        $oFormBuilder->setValue($aOrder);

        // добавляем элементы управления
        $oFormBuilder->buttonSave();
        $oFormBuilder->buttonBack();

        if (isset($aOrder['id']) && $aOrder['id']){
            $oFormBuilder->buttonEdit('goodsShow', \Yii::t('order', 'field_goods_title_edit'));
            $this->setInnerData('orderId', $aOrder['id']);
        }

        $oFormBuilder
            ->buttonSeparator('->')
            ->buttonDelete();

        $this->setInterface($oFormBuilder->getForm());
    }


    /**
     * Список заказов
     */
    protected function actionList() {

        $aStatusList =  Status::getListTitle();

        // -- сборка интерфейса

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            ->filterSelect('filter_status', $aStatusList, $this->iStatusFilter, \Yii::t('order', 'field_status'))
            ->field('id', 'ID', 'string', array('listColumns' => array('flex' => 1)))
            ->field('date', \Yii::t('order', 'field_date'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('person', \Yii::t('order', 'field_contact_face'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('mail', \Yii::t('order', 'field_mail'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('total_price', \Yii::t('order', 'field_goods_total'), 'string', array('listColumns' => array('flex' => 2)))
            ->field('status', \Yii::t('order', 'field_status'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('is_mobile', 'Mobile', 'check', array( 'listColumns' => array('flex' => 1) ))
            ->widget( 'status', 'skewer\\build\\Adm\\Order\\Service', 'getStatusValue' );

        /** Общее число заказов */
        $iCount = 0;

        $oQuery = ar\Order::find()
            ->index('id')
            ->limit(self::maxOrdersOnPage(), $this->iPageNum * self::maxOrdersOnPage())
            ->setCounterRef($iCount)
            ->order('id', 'DESC');

        if ($this->iStatusFilter)
            $oQuery->where('status', $this->iStatusFilter);

        $aOrders = $oQuery->asArray()->getAll();

        // Добавить общую сумму заказа
        $aGoodsSatistic = Api::getGoodsStatistic(array_keys($aOrders));
        foreach ($aOrders as $iOrderId => &$aOrder)
            $aOrder['total_price'] = isset($aGoodsSatistic[$iOrderId]) ? round($aGoodsSatistic[$iOrderId]['sum'], 3) : '-';

        $oFormBuilder
            ->setValue($aOrders, self::maxOrdersOnPage(), $this->iPageNum, $iCount)
            ->buttonRowUpdate()
            ->buttonRowDelete()

            ->buttonEdit('statusList',      \Yii::t('order', 'field_orderstatus_title'))
            ->buttonEdit('emailShow',       \Yii::t('order', 'field_mailtext'))
            ->buttonEdit('typePaymentList', \Yii::t('order', 'field_payment'))
            ->buttonEdit('typeDeliveryList',\Yii::t('order', 'field_delivery'))
            ->buttonEdit('editLicense',     \Yii::t('auth', 'license'))
            ->buttonEdit('editSettings',    \Yii::t('order', 'settings'))
        ;

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * CRUD для типа доставки (list)
     */
    protected function actionTypeDeliveryList(){

        /* Сборка интерфейса */

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            ->field('id', 'id', 'string')
            ->field('title', \Yii::t('order', 'field_delivery'), 'string', array('listColumns' => array('flex' => 3)))
        ;

        $oFormBuilder->setValue(ar\TypeDelivery::find()->asArray()->getAll());

        $oFormBuilder
            ->buttonRowDelete('DeleteTypeDelivery')
            ->buttonAddNew('AddTypeDelivery')
            ->buttonCancel()

            ->setEditableFields(['title'],'SaveTypeDelivery');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * CRUD для типа доставки (add)
     */
    protected function actionAddTypeDelivery(){

        /**
         * @var ar\TypeDeliveryRow $oParameters
         */
        $oParameters = ar\TypeDelivery::getNewRow();


        // -- сборка интерфейса

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->field('title', \Yii::t('order', 'payment_field_title'), 'string')
        ;

        $oFormBuilder->setValue($oParameters);

        // добавляем элементы управления
        $oFormBuilder
            ->buttonSave('saveTypeDelivery')
            ->buttonCancel('TypeDeliveryList')
        ;

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * CRUD для типа доставки (delete)
     */
    protected function actionDeleteTypeDelivery(){

        $iId = $this->getInDataValInt('id');

        if ($iId){
            ar\TypeDelivery::delete($iId);
        }
        $this->actionTypeDeliveryList();
    }

    /**
     * CRUD для типа оплаты (save)
     */
    protected function actionSaveTypeDelivery(){

        $oRow = new ar\TypeDeliveryRow();
        $oRow->setData($this->getInData());
        $oRow->save();

        $this->actionTypeDeliveryList();
    }

    /**
     * CRUD для типа оплаты (list)
     */
    protected function actionTypePaymentList(){

        $oFormBuilder = ui\StateBuilder::newList();

        $oFormBuilder
            ->field('id', 'id', 'string')
            ->field('title', \Yii::t('order', 'payment_field_title'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('payment', \Yii::t('order', 'payment_field_payment'), 'string', array('listColumns' => array('flex' => 1)))

            ->widget( 'payment', 'skewer\\build\\Tool\\Payments\\Api', 'getPaymentsTitle')

            ->setValue(ar\TypePayment::find()->asArray()->getAll())

            ->buttonRowUpdate('EditTypePayment')
            ->buttonRowDelete('DeleteTypePayment')
            ->buttonAddNew('AddTypePayment')
            ->buttonCancel()
        ;

        $this->setInterface($oFormBuilder->getForm());
    }

    protected function actionEditTypePayment(){

        $id = $this->getInDataValInt( 'id' );
        $this->showTypePaymentEditForm( $id );

    }

    /**
     * CRUD для типа оплаты (add)
     */
    protected function actionAddTypePayment(){

        $this->showTypePaymentEditForm();

    }

    private function showTypePaymentEditForm( $id = null ){
        /**
         * @var ar\TypePayment $oParameters
         */
        if ($id){
            $oParameters = ar\TypePayment::find($id);
        }else{
            $oParameters = ar\TypePayment::getNewRow();
        }

        $oFormBuilder = ui\StateBuilder::newEdit();

        $aItems = ArrayHelper::map(Payments\Api::getPaymentsList( true ), 'type', 'title');

        // добавляем поля
        $oFormBuilder
            ->field('id', 'id', 'hide')
            ->field('title', \Yii::t('order', 'payment_field_title'), 'string')
        ;

        if ($aItems)
            $oFormBuilder->fieldSelect( 'payment', \Yii::t('order', 'payment_field_payment'), $aItems, [], false);

        $oFormBuilder->setValue($oParameters);

        // добавляем элементы управления
        $oFormBuilder->buttonSave('saveTypePayment');
        $oFormBuilder->buttonCancel('TypePaymentList');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * CRUD для типа оплаты (delete)
     */
    protected function actionDeleteTypePayment(){

        $iId = $this->getInDataValInt('id');

        if ($iId){
            ar\TypePayment::delete($iId);
        }
        $this->actionTypePaymentList();
    }

    /**
     * CRUD для типа оплаты (save)
     */
    protected function actionSaveTypePayment(){

        $oRow = new ar\TypePaymentRow();
        $oRow->setData($this->getInData());

        // #40084 Правка значения "0" при выборе типа оплаты по умолчанию
        if (!Payments\Api::getParams((string)$oRow->payment))
            $oRow->payment = '';

        $oRow->save();

        $this->actionTypePaymentList();
    }


    protected function actionEmailShow() {

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit();

        if (!$this->sectionId()){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->filterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->setFilterAction('emailShow');
            }
        }

        $oFormBuilder
            ->field('info', '', 'show', ['hideLabel' => true])

            ->fieldString('title_user_mail', \Yii::t('order', 'mail_title_to_user'))
            ->field('user_content', \Yii::t('order', 'mail_to_user'), 'wyswyg')

            ->fieldString('title_adm_mail', \Yii::t('order', 'mail_title_to_admin'))
            ->field('adm_content', \Yii::t('order', 'mail_to_admin'), 'wyswyg')

            ->fieldString('title_change_status_mail', \Yii::t('order', 'mail_title_to_change_status'))
            ->field('status_content', \Yii::t('order', 'mail_to_change_status'), 'wyswyg')
          ;

        $aModulesData = ModulesParams::getByModule('order', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('order', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);
        $aItems['title_change_status_mail'] = ArrayHelper::getValue($aModulesData, 'title_change_status_mail', '');
        $aItems['status_content'] = ArrayHelper::getValue($aModulesData, 'status_content', '');
        $aItems['title_user_mail'] = ArrayHelper::getValue($aModulesData, 'title_user_mail', '');
        $aItems['user_content'] = ArrayHelper::getValue($aModulesData, 'user_content', '');
        $aItems['title_adm_mail'] = ArrayHelper::getValue($aModulesData, 'title_adm_mail', '');
        $aItems['adm_content'] = ArrayHelper::getValue($aModulesData, 'adm_content', '');

        $oFormBuilder->setValue($aItems);

        $oFormBuilder->buttonSave('emailSave');
        $oFormBuilder->buttonCancel();

        $this->setInterface($oFormBuilder->getForm());
    }

    protected function actionEmailSave() {

        $aData = $this->getInData();

        $aKeys =
            [
                'title_change_status_mail', 'status_content',
                'title_user_mail', 'user_content',
                'title_adm_mail', 'adm_content'
            ];

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $aKeys))
                    continue;

                ModulesParams::setParams( 'order', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionEmailShow();
    }

    /**
     * Быстрое сохранение прям из листа
     */
    protected function actionEditDetailGoods(){

        // запросить данные
        $aData = $this->get('data');

        $sPrice = str_replace(',','.',$aData['price']);
        $sCount = $aData['count'];

        // проверяем, чтоб админ не ввел бурду
        if (is_numeric($sPrice) && is_numeric($sCount)){

            $iId = $this->getInDataValInt('id');
            if ($iId) {
                /**
                 * @var ar\GoodsRow $oRow
                 */
                $oRow = ar\Goods::find($iId);
                if (!$oRow)
                    throw new UserException("Запись [$iId] не найдена");

                if (!$sPrice) {
                    $sPrice = $oRow->price;
                    $aData['price'] = $oRow->price;
                }

                $total = $sPrice*$sCount;
                if ($total>0)
                    $aData['total'] = $total;

                $oRow->setData($aData);
                $oRow->save();

                $oRow = ar\Goods::find($iId);
                $aData = $oRow->getData();

                ui\StateBuilder::updRow($this, $aData);
            }
        } else throw new UserException( \Yii::t('order', 'valid_data_error') );
    }

    protected function actionDeleteDetailGoods(){
        // запросить данные
        $aData = $this->get('data');

        // id записи
        $iItemId = (is_array($aData) and isset($aData['id'])) ? (int)$aData['id'] : 0;
        ar\Goods::delete($iItemId);
        $this->actionGoodsShow($aData['id_order']);
    }

    protected function actionGoodsShow($id = 0) {
        // номер заказа
        $iItemId = $this->getInnerDataInt('orderId');

        if (!$iItemId || $id) $iItemId = $id;

        $aItems =  ar\Goods::find()->where('id_order',$iItemId)->asArray()->getAll();
        // -- сборка интерфейса
        // создаем форму
        $oFormBuilder = ui\StateBuilder::newList();
        // добавляем поля
        $oFormBuilder
            ->field('id', 'ID', 'string', array('listColumns' => array('flex' => 1)))
            ->field('title', \Yii::t('order', 'field_goods_title'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('count', \Yii::t('order', 'field_goods_count'), 'int', array('listColumns' => array('flex' => 3, 'editor' => ['minValue' => 1])))
            ->field('price', \Yii::t('order', 'field_goods_price'), 'money', array('listColumns' => array('flex' => 3, 'editor' => ['minValue' => 0])))
            ->field('total', \Yii::t('order', 'field_goods_total'), 'string', array('listColumns' => array('flex' => 3)))
        ;
        $oFormBuilder->setValue($aItems);
        $oFormBuilder->buttonRowDelete('deleteDetailGoods');

        $oFormBuilder->button('show', \Yii::t('adm', 'back'), 'icon-cancel', 'show', ['addParams' => ['data' => ['id' => $iItemId]]]);

        $oFormBuilder->setEditableFields(array('price','count'),'editDetailGoods');
        $this->setInterface($oFormBuilder->getForm());
    }


    protected function actionStatusList() {

        $oFormBuilder = ui\StateBuilder::newList();

        // добавляем поля
        if (CurrentAdmin::isSystemMode())
            $oFormBuilder->fieldShow('name', \Yii::t('order', 'status_name'), 's');

        $oFormBuilder->fieldString('title', \Yii::t('order', 'field_status'), ['listColumns' => ['flex' => 3]]);

        $aItems = Status::getList();

        $oFormBuilder->setValue($aItems);

        $oFormBuilder->setEditableFields(['title'], 'statusSave');

        $oFormBuilder->buttonRowUpdate('statusShow');
        $oFormBuilder->buttonRowDelete('statusDelete');
        $oFormBuilder->buttonAddNew('statusShow');
        $oFormBuilder->buttonBack();

        $this->setInterface($oFormBuilder->getForm());

    }

    protected function actionStatusShow(){

        $sName = $this->getInDataVal('name');

        if ($sName) {
            $oStatus = Status::find()
                ->where(['name' => $sName])
                ->multilingual()
                ->one()
            ;
        }
        else {
            $oStatus = new Status();
        }

        $aData = $oStatus->getAllAttributes();

        $oForm = ui\StateBuilder::newEdit();

        if (CurrentAdmin::isSystemMode())
            $oForm->field('name', \Yii::t('order', 'status_name'), $oStatus->isNewRecord ? 'string' : 'hide');

        $aLanguages = Languages::getAllActive();

        if (count($aLanguages) > 1)
            $sLabel = 'status_title_lang';
        else
            $sLabel = 'status_title';

        foreach($aLanguages as $aLanguage){
            $oForm->fieldString( 'title_' . $aLanguage['name'], \Yii::t('order', $sLabel, [$aLanguage['title']]));
        }

        $oForm->setValue($aData);

        $oForm->buttonSave('StatusSave');
        $oForm->buttonBack('StatusList');

        $this->setInterface($oForm->getForm());
    }

    protected function actionStatusDelete() {

        $name = ArrayHelper::getValue($this->get('data'), 'name', '');
        if ($name){
            if (!Status::canBeDeleted($name)){
                throw new UserException( \Yii::t('order', 'status_delete_error') );
            }
            Status::deleteAll(['name' => $name]);
        }

        $this->actionStatusList();
    }

    /**
     * Сохранение статуса
     */
    protected function actionStatusSave() {

        $aData = $this->get('data');
        $sName = $this->getInDataVal('name');
        if (empty($sName))
            $sName =  Transliterate::change($aData['title_ru']);

        if (!$aData || !$sName)
            throw new UserException(\Yii::t('order', 'valid_data_error'));

        if (isset($aData['title']))
            $aData['title_'.\Yii::$app->language] = $aData['title'];

        /**
         * @var Status $oStatus
         */
        $oStatus = Status::find()
            ->multilingual()
            ->where(['name' => $sName])
            ->one()
        ;

        if (!$oStatus){
            $oStatus = new Status();
            $oStatus->name = $sName;
        }

        $oStatus->setLangData($aData);

        $oStatus->save();

        $this->actionStatusList();
    }


    protected function actionEditLicense() {

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit();

        if (!$this->sectionId()){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->filterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->setFilterAction('editLicense');
            }
        }

        $aData['license'] = ModulesParams::getByName('order', 'license', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        // добавляем поля
        $oFormBuilder
            ->field('license', \Yii::t('order', 'license_edit'), 'wyswyg')
            ->setValue( $aData )
            ->buttonSave( 'saveLicense' )
            ->buttonCancel( 'list' )
        ;

        // вывод данных в интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }

    /** Состояние: Настройки для всех заказов */
    protected function actionEditSettings() {
        $oForm = ui\StateBuilder::newEdit();

        $oForm
            ->fieldInt('order_max_size', \Yii::t('order', 'field_order_max_size'), ['minValue' => 0])
            ->fieldInt('onpage_profile', \Yii::t('order', 'field_onpage_profile'), ['minValue' => 0])
            ->fieldInt('onpage_cms', \Yii::t('order', 'field_onpage_cms'), ['minValue' => 0])

            ->setValue([
                'order_max_size' => ApiCart::maxOrderSize(),
                'onpage_profile' => ApiCart::maxOrdersOnPage(),
                'onpage_cms'     => self::maxOrdersOnPage(),
            ])

            ->buttonSave('saveSettings')
            ->buttonCancel('list')
        ;

        $this->setInterface($oForm->getForm());
    }

    /** Действие: сохранить настройки для всех заказов */
    protected function actionSaveSettings() {

        $aData = $this->get('data');

        isset($aData['order_max_size']) and ApiCart::maxOrderSize($aData['order_max_size']);
        isset($aData['onpage_profile']) and ApiCart::maxOrdersOnPage($aData['onpage_profile']);
        isset($aData['onpage_cms']) and self::maxOrdersOnPage($aData['onpage_cms']);

        $this->actionList();
    }


    protected function actionSaveLicense() {

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage) {
            ModulesParams::setParams('order', 'license', $sLanguage, $this->getInDataVal('license'));
        }

        $this->actionList();
    }

    /** Получить/установить максильное число отображаемых заказов в админке */
    private static function maxOrdersOnPage() {

        return func_num_args() ? SysVar::set('Order.onpage_cms', (int)func_get_arg(0)) : SysVar::get('Order.onpage_cms', 100);
    }
}