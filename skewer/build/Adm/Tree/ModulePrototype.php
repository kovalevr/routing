<?php

namespace skewer\build\Adm\Tree;

use skewer\build\Adm;
use skewer\build\Cms;
use skewer\base\site_module;

/**
 * Родительский класс для модулей относящихся к дереву разделов
 */
class ModulePrototype extends Cms\Tabs\ModulePrototype implements site_module\SectionModuleInterface {

    /** @var int id раздела */
    protected $sectionId = 0;

    function sectionId() {
        return $this->sectionId;
    }

}
