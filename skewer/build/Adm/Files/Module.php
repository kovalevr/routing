<?php

namespace skewer\build\Adm\Files;


use skewer\base\ui;
use skewer\build\Adm;
use skewer\components\auth\CurrentAdmin;
use skewer\components\ext;
use skewer\base\log\Logger;
use yii\base\UserException;


/**
 * Класс для работы с набором файлов раздела
 * Class Module
 * @package skewer\build\Adm\Files
 */
class Module extends Adm\Tree\ModulePrototype {

    // возможность выбирать файлы
    protected $bCanSelect = false;

    // набор сообщений
    protected $aSysMessages = array();

    /**
     * @var ext\ListView
     */
    protected $sListBuilderClass = 'ExtList';

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     */
    protected function preExecute() {

        // проверить права доступа
        if ( $this->sectionId() and !CurrentAdmin::canRead($this->sectionId()) )
            throw new UserException( 'accessDenied' );

        // составление набора допустимых расширений для изображений
        $this->aImgExt = \Yii::$app->getParam(['upload','allow','images']);

        $this->aSysMessages = array();

    }

    /**
     * Первичная загрузка
     */
    protected function actionInit() {

        $this->actionList();

    }

    /**
     * Загрузка списка
     */
    protected function actionList() {

        $this->setPanelName( \Yii::t('Files', 'filesList') );

        // запрос файлов раздела
        $aItems = Api::getFiles($this->sectionId());

        if ( $this->hasImages($aItems) ) {
            $this->actionPreviewList( $aItems );
        } else {
            $this->actionSimpleList( $aItems );
        }

        // обработка сообщений
        if ( isset( $this->aSysMessages['loadResult'] ) ) {
            $iTotal = $this->aSysMessages['loadResult']['total'];
            $iLoaded = $this->aSysMessages['loadResult']['loaded'];
            $aErrors = $this->aSysMessages['loadResult']['errors'];

            if ( !$iTotal ) {
                $this->addError(\Yii::t('Files', 'loadingError'));
            } else {

                $time = 2000 + count($aErrors)*2000;

                if ( $iLoaded ){
                    $sMsg = \Yii::t('Files', 'loadingPro', [$iLoaded, $iTotal] );
                    if (count($aErrors)){
                        $sMsg .= "<br>" . implode('<br>', $aErrors);
                    }

                    $this->addMessage( $sMsg , '', $time);
                }
                else{
                    $sMsg = \Yii::t('Files', 'noLoaded' );
                    if (count($aErrors)){
                        $sMsg .= "<br>" . implode('<br>', $aErrors);
                    }
                    $this->addError($sMsg);
                }

            }

        }

    }

    /**
     * Отображение обычного списка фалов
     * @param array $aItems набор файлов
     */
    private function actionSimpleList( $aItems ) {

        // установить имя для используемого модуля
        $this->addLibClass('FileBrowserFiles');

        /**
         * объект для построения списка (может быть перекрыт)
         */
        $oList = ui\StateBuilder::newList(new ExtListModule());

        $oList
            ->fieldString('name', \Yii::t('Files','name'),['listColumns' => ['flex' => 1 ]])
            ->fieldString('size', \Yii::t('Files','size'))
            ->fieldString('ext', \Yii::t('Files','ext'))
            ->fieldString('modifyDate', \Yii::t('Files','modifyDate'),['listColumns' => ['width' => 150 ]])

            ->setGroups('ext')
            ->sortBy('ext')
            ->sortBy('name')
            ->enableSorting()

            ->buttonRowDelete()

            ->setValue($aItems);


        if ( $this->bCanSelect ) {
            $oList->button('', \Yii::t('Files', 'select'), 'icon-commit', 'selectFile');
            $oList->buttonSeparator();
        }

        // кнопка добавления
        $oList->buttonAddNew('addForm', \Yii::t('Files', 'load'));

        // вывод данных в интерфейс
        $this->setInterface( $oList->getForm() );

    }

    /**
     * Добавление кнопок к интерфейсу "список"
     * @param ext\ViewPrototype $oList
     */
    private function addListButtons( $oList ) {

        // кнопка выбора файла, если доступна  \Yii::t('files', 'noLoaded')
        if ( $this->bCanSelect ) {
            $oList->addDockedItem(array(
                'text' => \Yii::t('Files', 'select'),
                'iconCls' => 'icon-commit',
                'state' => 'selectFile'
            ));
            $oList->addBtnSeparator();

        }

        // кнопка добавления
        $oList->addDockedItem(array(
            'text' => \Yii::t('Files', 'load'),
            'iconCls' => 'icon-add',
            'action' => 'addForm',
        ));

    }

    /**
     * Отображение списка фалов с миниатюрами
     * @param array $aItems набор файлов
     */
    private function actionPreviewList( $aItems ) {

        // объект для построения списка
        $oIface = new ext\UserFileView( 'FileBrowserImages' );

        // Добавление библиотек для работы
        $this->addLibClass( 'FileImageListView' );

        // добавление миниатюр в спиок файллов
        $aItems = $this->makePreviewListArray( $aItems );

        // сортировка файлов по имени
        usort( $aItems, array( $this, 'sortFIlesByName' ) );

        // добавление кнопок
        $this->addListButtons( $oIface );

        // добавление кнопки удаления
        $oIface->addBtnSeparator('->');
        $oIface->addDockedItem(array(
            'text' => \Yii::t('adm','del'),
            'iconCls' => 'icon-delete',
            'state' => 'delete',
            'action' => ''
        ));

        // добавляем css файл для
        $this->addCssFile('files.css');

        // задать команду для обработки
        $this->setCmd('load_list');

        // задать список файлов
        $this->setData('files',$aItems);

        $this->setModuleLangValues(
            array(
                'delRowNoName',
                'delCntItems',
                'delRowHeader',
                'delRow'
            )
        );

        // вывод данных в интерфейс
        $this->setInterface( $oIface );

    }

    /**
     * Подготавливает список файлов для в виде миниатюр
     * @param array $aItems входной список фалйов
     * @return array
     */
    private function makePreviewListArray($aItems){

        $aOut = array();

        // перебор файлов
        foreach ( $aItems as $aItem ) {

            // флаг наличия миниатюры
            $bThumb = false;

            // если изображение
            if ( $this->isImage($aItem) ) {

                // и есть миниатюра
                $sThumbName = Api::getThumbName( $aItem['webPathShort'] );
                if ( file_exists(WEBPATH.$sThumbName) ) {
                    $aItem['preview'] = $sThumbName;
                    $aItem['thumb'] = 1;
                    $bThumb = true;
                }
            }

            // если нет миниатюры
            if ( !$bThumb ) {
                $aItem['preview'] = $this->getModuleWebDir().'/img/file.png';
                $aItem['thumb'] = 0;
            }
            $aOut[] = $aItem;
        }

        return $aOut;

    }

    /**
     * сортировка файлов по имени
     * @param array $a1 первый элемент
     * @param array $a2 второй элемент
     * @return int
     */
    protected function sortFIlesByName( $a1, $a2 ) {

        // заначения
        $s1 = $a1['name'];
        $s2 = $a2['name'];

        // сравнение
        if ($s1 == $s2)
            return 0;
        return ($s1 < $s2) ? -1 : 1;

    }

    /**
     * Удаляет файл
     * @throws UserException
     */
    protected function actionDelete() {

        // запросить данные
        $sName = $this->getInDataVal( 'name' );

        // удаление одного файла
        if ( $sName ) {

            // проверка наличия данных
            if ( !$sName )
                throw new UserException('no file name provided');

            // удалить файл
            $bRes = Api::deleteFile( $this->sectionId(), $sName );

            if ( $bRes ) {
                $this->addModuleNoticeReport(\Yii::t('files', 'deleting'),$sName);
                $this->addMessage( \Yii::t('Files', 'delete'));
            } else {
                $this->addError( \Yii::t('Files', 'noDelete') );
            }


        } else {

            $aData = $this->get( 'delItems' );

            // проверка наличия данных
            if ( !is_array($aData) or !$aData )
                throw new UserException('badData');

            // счетчики
            $iTotal = count($aData);
            $iCnt = 0;

            // удаление файлов
            foreach ( $aData as $sFileName ) {
                // удалить файл
                $iCnt += (int)(bool)Api::deleteFile( $this->sectionId(), $sFileName );
            }

            // сообщения
            if ( $iCnt ) {
                $this->addMessage( \Yii::t('Files', 'deletingPro', [$iCnt, $iTotal]) );
                $this->addModuleNoticeReport(\Yii::t('files', 'deleteFiles'), array('section'=>$this->sectionId(),'files'=>$aData));
            } else {
                $this->addError( \Yii::t('Files', 'noDeleteFiles') );
            }

        }

        // показать список
        $this->actionList();

    }

    /**
     * Отображает форму добавления
     */
    protected function actionAddForm() {

        // объект для построения списка
        $oIface = new ext\UserFileView( 'FileAddForm' );

        $this->setPanelName( \Yii::t('Files', 'loadFiles') );

        // добавить кнопки
        // загрузка
        $oIface->addDockedItem(array(
            'text' => \Yii::t('Files', 'load'),
            'iconCls' => 'icon-commit',
            'state' => 'upload'
        ));
//        $oIface->addBtnSeparator();
//        // еще один файл
//        $oIface->addDockedItem(array(
//            'text' => 'Добавить поле',
//            'iconCls' => 'icon-add',
//            'state' => 'addField'
//        ));
        $oIface->addBtnSeparator();
        // отмена
        $oIface->addBtnCancel();

        $this->setModuleLangValues(
            array(
                'fileBrowserFile',
                'fileBrowserSelect',
                'fileBrowserNoSelection'
            )
        );

        // вывод данных в интерфейс
        $this->setInterface( $oIface );

    }

    protected function actionUpload() {

        // отдать правильный заголовок
        header('Content-Type: text/html');

        // загрузить файлы
        $aRes = Api::uploadFiles( $this->sectionId() );

        Logger::addNoticeReport(\Yii::t('files', 'loadFile') , Logger::buildDescription($aRes), Logger::logUsers,$this->getModuleName());

        $this->aSysMessages['loadResult'] = $aRes;

        $this->setData('loadedFiles',$aRes['files']);

        $this->setModuleLangValues(
            array(
                'fileBrowserNoSelection'
            )
        );

        // вызвать состояние "список"
        $this->actionList();

    }

    /** @var array список расширений, считающихся картинками */
    private $aImgExt = array();

    /**
     * Определяет, относится ли расширение к картинкам
     * @param string|array $mExt расширение файла/описание файла
     * @return bool
     */
    private function isImage( $mExt ){
        return in_array(is_array($mExt)?$mExt['ext']:$mExt, $this->aImgExt);
    }

    /**
     * Вычисляет, есть ли среди списка файлов картинки
     * @param $aItems
     * @return bool
     */
    private function hasImages( $aItems ) {

        // перебор записей
        foreach ( $aItems as $aItem ) {
            // есть найдена картинка
            if ( $this->isImage($aItem) )
                return true;
        }

        // нет картинок
        return false;

    }

}
