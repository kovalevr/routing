<?php

namespace skewer\build\Adm\Params;

use skewer\base\section\Parameters;
use skewer\base\section\params\Type;
use skewer\base\site\Layer;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\build\Design\Zones;
use skewer\components\auth\CurrentAdmin;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\base\site_module\Module as SiteModule;

/**
 * Class Module
 * @package skewer\build\Adm\Params
 * todo можно избавиться от innerData['class']
 */
class Module extends Adm\Tree\ModulePrototype {

    protected $sFilter = '';

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     */
    protected function preExecute() {

        // id текущего раздела
        $this->sFilter = $this->getStr('filter', '');

        // проверить права доступа
        if (!CurrentAdmin::canRead($this->sectionId()))
            throw new UserException('accessDenied');

    }

    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData(array(
            'sectionId' => $this->sectionId(),
            'filter' => $this->sFilter
        ));

    }

    /**
     * Инициализация
     */
    public function actionInit() {

        // заголовок
        $this->setPanelName(\Yii::t('params', 'paramList'));

        $oList = ui\StateBuilder::newList();

        $oList
            ->fieldString( 'name', \Yii::t('params', 'name'), ['listColumns' => ['flex' => 5], 'sorted' => true] )
            ->fieldString( 'title', \Yii::t('params', 'title'), ['listColumns' => ['flex' => 5]] )
            ->fieldString( 'value', \Yii::t('params', 'value'), ['listColumns' => ['flex' => 5]] )
            ->fieldString( 'id', 'ID', ['listColumns' => ['flex' => 1]] )
            ->fieldString( 'parent', \Yii::t('params', 'parent'), ['listColumns' => ['flex' => 1]] )
        ;

        $oList->filterText('filter', $this->sFilter, \Yii::t('params', 'filter'));

        $aItems = $this->getParameters();

        if ($this->sFilter != '')
            $aItems = Api::filterParams($aItems, $this->sFilter, $this->sectionId());

        $oList->setValue($aItems);

        // Редактируемое поле
        $oList->setEditableFields(['value'], 'save');

        // Группировка
        $oList->setGroups('group');

        // Сортировка
        $oList->sortBy('name');

        // Кнопки
        $oList
            ->buttonRowCustomJs('ParamsAddObjBtn')
            ->buttonRowCustomJs('ParamsEditBtn')
            ->buttonRowCustomJs('ParamsDelBtn')
            ->buttonAddNew('show')
            ->buttonDelete()
        ;

        $oList = $oList->getForm();
        $oList->setModuleLangValues(
            array(
                'del',
                'upd',
                'paramDelRowHeader',
                'paramDelRow',
                'paramAddForSection',
                'paramCopyToSection'
            )
        );

        // вывод данных в интерфейс
        $this->setInterface($oList);

    }

    /**
     * Получение списка параметров текущего раздела
     * @return array
     */
    protected function getParameters(){

        $aItems = Parameters::getList( $this->sectionId()  )
            ->fields(['id', 'name', 'value', 'parent', 'title', 'show_val'])
            ->index('id')
            ->rec()->asArray()->get();


        /** Подменяем val на show_val для параметров-зон для удобства редактирования*/

        $aZones = ArrayHelper::getColumn(Zones\Api::getZoneList($this->sectionId()), 'id');

        foreach ($aZones as $item) {
            if (isset($aItems[$item]))
                $aItems[$item]['value'] = $aItems[$item]['show_val'];
        }

        return $aItems;
    }


    /**
     * Добавить параметр по шаблону
     * @throws \Exception
     */
    protected function actionAddParam(){

        $this->setPanelName(\Yii::t('params', 'addParam'));

        $iItemId = $this->getInDataValInt('id');

        $oTemplateParameters = Parameters::getById( $iItemId );

        if (!$oTemplateParameters) {
            throw new \Exception(\Yii::t('params', 'noFindParam'));
        }

        $aParamList = $this->getParams4Module($this->sectionId(), $oTemplateParameters->group);

        $oParameters = Parameters::createParam();

        $oParameters->group = $oTemplateParameters->group;
        $oParameters->parent = $this->sectionId();
        $oParameters->access_level = '0';

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit();

        $aData = $oParameters->getAttributes();
        $aData['class'] = $this->getInnerData('class');

        // добавляем поля
        $oFormBuilder
            ->fieldHide('id', 'id')
            ->fieldHide('parent', \Yii::t('params', 'parent'))
            ->fieldString('group', \Yii::t('params', 'group'), ['readOnly' => true])
            ->fieldString('class', \Yii::t('params', 'class'), ['readOnly' => true])
            ->fieldSelect('name', \Yii::t('params', 'name'), $aParamList, [
                'forceSelection' => false,
                'allowBlank' => false,
                'editable' => true,
                'onUpdateAction' => 'getValueParams'
            ], false)

            ->fieldString('value', \Yii::t('params', 'value'))
            ->fieldString('title', \Yii::t('params', 'title'))
            ->fieldSelect('access_level', \Yii::t('params', 'access_level'), Type::getParametersList(), [], false)
            ->field('show_val', \Yii::t('params', 'show_val'), 'text')
        ;
        // устанавливаем значения
        $oFormBuilder
            ->setValue($aData)

        // добавляем элементы управления

            ->buttonSave()
            ->buttonBack();

        $this->setInterface($oFormBuilder->getForm());

    }


    /**
     * Загружает форму для редактирования
     */
    protected function actionShow() {
        // номер записи
        $iItemId = $this->getInDataValInt('id');

        // запись параметра
        if ($iItemId) {
            $oParameters = Parameters::getById( $iItemId );

            if (!$oParameters) {
                throw new \Exception(\Yii::t('params', 'noFindParam'));
            }

            // если нельзя читать раздел
            if (!CurrentAdmin::canRead($oParameters->parent))
                throw new \Exception(\Yii::t('params', 'authError'));

            // если параметр не принадлежит данному разделу - отвязать от id
            if ((int)$oParameters->parent !== (int)$this->sectionId()) {
                $oParameters->id = 0;
                $this->setPanelName(\Yii::t('params', 'cloneParam'));
            } else {
                $this->setPanelName(\Yii::t('params', 'editParam'));
            }

        } else {

            $this->setPanelName(\Yii::t('params', 'addParam'));

            $oParameters = Parameters::createParam([
                'group' => '.',
                'parent' => $this->sectionId(),
                'access_level' => 0
            ]);

            $oParameters->id = 0;
        }

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit();

        $aData = $oParameters->getAttributes();
        $aData['class'] = $this->getInnerData('class');

        // добавляем поля
        $oFormBuilder
            ->fieldHide('id', 'id')
            ->fieldHide('parent' ,\Yii::t('params', 'parent'))
            ->fieldSelect('group', \Yii::t('params', 'group'), Api::getAllGroups($this->sectionId()), [
                'forceSelection' => false,
                'allowBlank' => false,
                'editable' => true,
                'onUpdateAction' => 'getModuleParams'
            ], false)
            ->fieldString('class', \Yii::t('params', 'class'), ['readOnly' => true])
            ->fieldSelect('name', \Yii::t('params', 'name'), $this->getParams4Module( $oParameters->parent, $oParameters->group ), [
                'forceSelection' => false,
                'allowBlank' => false,
                'editable' => true,
                'onUpdateAction' => 'getValueParams'
            ], false);

            if ($this->getInDataVal('type')=='obj'){
                $oParameters->name = 'object';
                $oParameters->value = '';
            } else {
                $oFormBuilder->field('value', \Yii::t('params', 'value'));
            }

            $oFormBuilder
                ->fieldString('title', \Yii::t('params', 'title'))
                ->fieldSelect('access_level', \Yii::t('params', 'access_level'), Type::getParametersList(), [], false)
                ->field('show_val', \Yii::t('params', 'show_val'), 'text')
            ;

        // устанавливаем значения
        $oFormBuilder->setValue($aData);

        // добавляем элементы управления
        $oFormBuilder
            ->buttonSave()
            ->buttonBack()
        ;

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Сохранение параметров
     * @throws \Exception
     */
    public function actionSave() {

        // массив на сохранение
        $aData = $this->get('data');
        if (!$aData)
            throw new \Exception(\Yii::t('params', 'noSaveData'));

        // id элемента
        $iId = $this->getInDataValInt('id');

        $aRowInBase = false;

        // если задан id
        if ($iId) {

            // проверить совпадение раздела
            $oParentParam = Parameters::getById($iId);

            // перекрываем все данные пришедшими
            if ($oParentParam && (int)$oParentParam->parent == $this->sectionId()) {

                $aRowInBase = $oParentParam;

            }

        }

        if (!$aRowInBase)
            $aRowInBase = Parameters::getByName( $this->sectionId(),
                isset($aData['group'])?$aData['group']:'',
                isset($aData['name'])?$aData['name']:'');
        if (!$aRowInBase)
            $aRowInBase = Parameters::createParam();

        unset($aData['id']);
        $aRowInBase->setAttributes($aData);
        $aRowInBase->parent = $this->sectionId();

        // сохранить параметр
        $iRes = $aRowInBase->save();

        if ($iRes) {
            $this->addMessage(\Yii::t('params', 'saveParam'));
        } else {
            $this->addError(\Yii::t('params', 'saveParamError'));
        }

        // отдать результат сохранения
        $this->setData('saveResult', $iRes);

        // отдать назад список параметров
        $this->actionInit();

    }

    /**
     * Удаление набора параметров
     * @throws \Exception
     */
    public function actionDelete() {

        // список/id параметра
        $iId = $this->getInDataValInt('id');

        if (!$iId)
            throw new \Exception(\Yii::t('params', 'noParamDelete'));

        // составление набора id для удаления
        $aItem = Parameters::getById($iId);
        if (!$aItem)
            throw new \Exception(\Yii::t('params', 'noFindParam'));

        if ((int)$aItem->parent !== $this->sectionId())
            throw new \Exception(\Yii::t('params', 'notDeleteCloneParam'));
            
        if ($aItem->group == Zones\Api::layoutGroupName && $aItem->parent == \Yii::$app->sections->root())
            throw new \Exception(\Yii::t('params', 'notDeleteParam'));
    
        // выполнение запроса на удаление
        $iRes = $aItem->delete();

        if ($iRes) {
            $this->addMessage(\Yii::t('params', 'deleteParam'));
        } else {
            $this->addError(\Yii::t('params', 'deleteParamError'));
        }

        // отдать назад список параметров
        $this->actionInit();

    }

    /**
     * Создать копию параметра для заданного раздел
     * @throws \Exception
     */
    public function actionClone() {

        // id параметра
        $iId = $this->getInDataValInt('id');

        // запросить дублируемое поле
        $aSrcRow = Parameters::getById($iId);

        if (!$aSrcRow)
            throw new \Exception(\Yii::t('params', 'noFindString'));

        $oNewParam = Parameters::copyToSection($aSrcRow, $this->sectionId());

        // отдать результат только в случае ошибки
        if (!$oNewParam)
            throw new \Exception(\Yii::t('params', 'cloneError'));

        // отдать назад список параметров
        $this->actionInit();

    }


    /**
     * Подстановка в форму возможных параметров для выбранной метки
     */
    public function actionGetModuleParams(){

        $aFormData = $this->get('formData', []);

        $iParent = isset($aFormData['parent'])?$aFormData['parent']:0;
        $sGroupName = isset($aFormData['group'])?$aFormData['group']:0;

        $aParams = $this->getParams4Module( $iParent, $sGroupName );
        $aParams = array_combine($aParams, $aParams);

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldString('class', '', ['readOnly' => true])
            ->fieldSelect('name',  '', $aParams, [], false)

            ->setValue([
                'class' => $this->getInnerData('class'),
            ]);

        $this->setInterfaceUpd($oForm->getForm());
    }


    /**
     * Подстановка в форму значения
     */
    public function actionGetValueParams(){

        $aFormData = $this->get('formData', []);

        $iParent = isset($aFormData['parent'])?$aFormData['parent']:0;
        $sGroupName = isset($aFormData['group'])?$aFormData['group']:'';
        $sName = isset($aFormData['name'])?$aFormData['name']:'';
        $sClass = isset($aFormData['class'])?$aFormData['class']:'';

        if ($iParent and $sGroupName and $sName and $sClass){
            $oParam = Parameters::getByName( $iParent, $sGroupName, $sName, true );

            if ($oParam) {

                $oForm = ui\StateBuilder::newEdit();
                $oForm
                    ->fieldString('value', '')
                    ->fieldString('access_level', '')
                    ->fieldString('show_val', '')
                    ->fieldString('title', '')

                    ->setValue([
                        'value'        => $oParam->value,
                        'access_level' => $oParam->access_level,
                        'show_val'     => $oParam->show_val,
                        'title'        => $oParam->title,
                    ]);

                $this->setInterfaceUpd($oForm->getForm());
            } else {

                $propertyList = $this->getModulePropertyList($sClass);

                if ( isset($propertyList[$sName]) ) {
                    $aProperty = $propertyList[$sName];

                    $oForm = ui\StateBuilder::newEdit();
                    $oForm
                        ->fieldString('value', '')
                        ->fieldString('access_level', '')
                        ->fieldString('show_val', '')
                        ->fieldString('title', '')

                        ->setValue([
                            'value'        => $aProperty['value'],
                            'access_level' => Type::paramSystem,
                            'show_val'     => '',
                            'title'        => $aProperty['title']
                        ])
                    ;
                    $this->setInterfaceUpd($oForm->getForm());
                }

            }
        }
    }


    /**
     * Список параметров для модуля по разделу и метке
     * @param $iParent
     * @param $sGroupName
     * @return array
     * @throws \Exception
     */
    protected function getParams4Module( $iParent, $sGroupName ){

        // если нельзя читать раздел
        if (!CurrentAdmin::canRead($iParent))
            throw new \Exception(\Yii::t('params', 'authError'));

        $sType = $this->getInDataVal('type');
        if ( !in_array($sType, [Parameters::object, Parameters::objectAdm]) )
            $sType = Parameters::object;

        $oParamObject = Parameters::getByName( $iParent, $sGroupName, $sType, true );

        if (!$oParamObject)
            return [];

        switch($oParamObject->name){
            default:
            case Parameters::object:
                $sLayer = Layer::PAGE;
                break;
            case Parameters::objectAdm:
                $sLayer = Layer::ADM;
                break;
        }

        $oMainModule = Parameters::getByName( $this->sectionId(), Parameters::settings, Parameters::object, true );
        if (strpos($oMainModule->value, Layer::LANDING_PAGE) === 0)
            $sLayer = Layer::LANDING_PAGE;
        if (strpos($oMainModule->value, Layer::LANDING_ADM) === 0)
            $sLayer = Layer::LANDING_ADM;

        $sObjectName = $oParamObject->value;

        $propertyList = $this->getModulePropertyList($sObjectName, $sLayer);

        $aList = [];
        foreach ($propertyList as $aProperty) {
            $aList[$aProperty['name']] = $aProperty['name'];//sprintf('%s (%s) [%s]', $aProperty['name'], $aProperty['title'], $aProperty['value']);
        }

        return $aList;

    }

    /**
     * Отдает набор параметров для указанного модуля
     * @param string $sModuleAlias псевдоним модуля в формате Adm\News
     * @param string $sLayer
     * @return \array[] отдает данные по параметрам модуля в виде массива массивов :
     * [
     * 'name' => 'tpl',        // имя
     * 'value' => 1,           // значение по умолчанию
     * 'title' => 'Tpl name'   // название параметра
     * ]
     */
    private function getModulePropertyList($sModuleAlias, $sLayer='') {

        $aOut = [];

        if ( strpos($sModuleAlias, '\\') ) {
            list($sLayer, $sModuleName) = explode('\\', $sModuleAlias);
        } else {
            $sModuleName = $sModuleAlias;
            $sLayer = $sLayer ?: Layer::PAGE;
        }

        $sFullName = SiteModule::getClassOrExcept($sModuleName, $sLayer);

        $oModule = new \ReflectionClass($sFullName);

        foreach ($oModule->getProperties() as $oProperty) {

            if ( !$oProperty->isPublic() )
                continue;

            $sName = $oProperty->getName();

            $aDefaultProperties = $oModule->getDefaultProperties();
            if ( isset($aDefaultProperties[$sName]) )
                $sValue = $aDefaultProperties[$sName];
            else
                $sValue = '';

            if (!(is_string($sValue) || is_numeric($sValue) || is_bool($sValue) || is_null($sValue)))
                continue;

            $sTitle = $oProperty->getDocComment() ?: '';
            if ($sTitle) {
                $sTitle = str_replace(['/**', '*/'], '', $sTitle);
                $sTitle = preg_replace('/\@var\s(string|bool|int)/', '', $sTitle);
                $sTitle = trim($sTitle);
            }

            $aOut[$sName] = [
                'name' => $sName,
                'value' => $sValue,
                'title' => $sTitle
            ];

        }

        $this->setInnerData('class', $sLayer . '\\' . $sModuleName);

        return $aOut;

    }

}
