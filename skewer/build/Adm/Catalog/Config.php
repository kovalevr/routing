<?php
/**
 * User: kolesnikiv
 * Date: 31.07.13
 */
use skewer\base\site\Layer;

$aConfig['name']     = 'Catalog';
$aConfig['title']    = 'Каталог';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

return $aConfig;
