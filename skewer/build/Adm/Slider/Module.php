<?php

namespace skewer\build\Adm\Slider;

use skewer\libs\jquery;
use skewer\base\orm\Query;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\build\Tool;
use skewer\build\Cms;
use skewer\components\ext;
use skewer\base\site_module\Context;
use skewer\base\site\Layer;
use skewer\base\site_module;


class Module extends Cms\Tabs\ModulePrototype implements site_module\SectionModuleInterface {

    const section = Layer::ADM;
    const tool = Layer::TOOL;

    public $sectionId = 0;

    /**
     * Отдает режим работы модуля
     * @return string
     */
    protected function getWorkMode() {
        return self::section;
    }

    protected function isSectionMode() {
        return $this->getWorkMode()===self::section;
    }

    protected function isToolMode() {
        return $this->getWorkMode()===self::tool;
    }

    function sectionId() {
        return $this->sectionId;
    }

    public function __construct(Context $oContext ) {
        parent::__construct( $oContext );
        $oContext->setModuleLayer( 'Adm' );
        $oContext->setModuleWebDir( '/skewer/build/Adm/Slider' );
    }

    protected $iCurrentBanner = 0;
    protected $iCurrentSlide = 0;

    /** @var int Параметр из раздела */
    protected $currentBanner = 0;


    protected function preExecute() {

        if ( $this->currentBanner )
            $this->iCurrentBanner = $this->currentBanner;

    }


    protected function actionInit() {

        // вывод списка
        if ( $this->currentBanner )
            $this->actionSlideList();
        else
            $this->actionBannerList();

        $this->addCssFile(\Yii::$app->getAssetManager()->getBundle(Asset::className())->baseUrl.'/css/SlideShower.css');
        $this->addJsFile(\Yii::$app->getAssetManager()->getBundle(Asset::className())->baseUrl.'/js/SlideLoadImage.js');
        $this->addJsFile(\Yii::$app->getAssetManager()->getBundle(Asset::className())->baseUrl.'/js/SlideShower.js');

    }


    /**
     * Список баннеров
     */
    protected function actionBannerList() {

        $this->iCurrentBanner = 0;

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            ->field('title', \Yii::t('slider', 'title'), 'string')
            ->field('preview_img', '', 'addImg', array('listColumns' => array('flex' => 1)))
            ->field('active', \Yii::t('slider', 'active'), 'check')
            ->widget( 'preview_img', 'skewer\\build\\Adm\\Slider\\Banner', 'getPreviewImg' )
            ->setEditableFields( array( 'active' ), 'saveBanner' );
        ;

        $aItems = Banner::find()->getAll();

        // добавляем данные
        $oFormBuilder->setValue( $aItems );

        // элементы управления
        $oFormBuilder
            ->buttonAddNew('editBannerForm', \Yii::t('slider', 'addBanner'))
            ->buttonEdit('toolsForm', \Yii::t('slider', 'displaySettings'))
            ->buttonRowUpdate( 'SlideList' )
            ->buttonRowDelete( 'delBanner' )
        ;

        $this->setInnerData('currentBanner', false);

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Форма редактирования баннера
     * todo default fields
     */
    protected function actionEditBannerForm() {

        $iBannerId = $this->getInnerData('currentBanner');

        if ( $iBannerId ) {
            $oBannerRow = Banner::find( $iBannerId );
        } else {
            $oBannerRow = Banner::getNewRow();
            $oBannerRow->setVal( 'title', \Yii::t('slider', 'new_banner') );
            $oBannerRow->setVal( 'active', true );
        }


        // создаем форму
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        //id	title	section	on_include
        $oFormBuilder
            ->field('id', 'id', 'hide')
            ->field('title', \Yii::t('slider', 'title'), 'string', ['listColumns' => ['flex' => 1]])
            ->fieldSelect( 'section', \Yii::t('slider', 'section'), Banner::getSectionTitle(), [], false )
            ->field('on_include', \Yii::t('slider', 'on_include'), 'check')
            ->field('bullet', \Yii::t('slider', 'bullet'), 'check')
            ->field('scroll', \Yii::t('slider', 'scroll'), 'check')
            ->field('active', \Yii::t('slider', 'active'), 'check')
        ;

        // устанавливаем значения
        $oFormBuilder->setValue( $oBannerRow );

        // добавляем элементы управления
        $oFormBuilder->buttonSave('saveBanner');
        if ( $iBannerId )
            $oFormBuilder->buttonEdit('slideList', \Yii::t('slider', 'editSlides'));

        $oFormBuilder->buttonCancel('bannerList');


        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Добавление/обновление баннера
     * todo отлов ошибок
     */
    protected function actionSaveBanner() {

        try {

            $aData = $this->getInData();
            $aData['last_modified_date'] = date( "Y-m-d H:i:s", time() );

            $oRow = Banner::getNewRow();
            $oRow->setData( $aData );

            $oRow->save();

            if ( $oRow->getError() )
                throw new \Exception( $oRow->getError() );

            $this->actionBannerList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаление баннера
     */
    protected function actionDelBanner() {

        try {

            $aData = $this->getInData();

            $iBannerId = iSset( $aData['id'] ) ? $aData['id'] : false;

            if ( !$iBannerId )
                throw new \Exception("not found banner id=$iBannerId");

            $oRow = Banner::find( $iBannerId );

            if ( !$oRow )
                throw new \Exception("not found banner id=$iBannerId");

            // удаление слайдов от банера
            Slide::removeBanner( $iBannerId );

            $oRow->delete();

            \Yii::$app->router->updateModificationDateSite();

            $this->actionBannerList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Список слайдов для баннера
     */
    protected function actionSlideList() {

        $aData = $this->getInData();
        if ( !$this->iCurrentBanner && isSet($aData['id']) && $aData['id'] )
            $this->iCurrentBanner = $aData['id'];

        $this->iCurrentSlide = 0;

        $this->setInnerData('currentBanner', $this->iCurrentBanner);
        if ( !$this->iCurrentBanner )
            throw new \Exception( \Yii::t('slider', 'noFindBanner') );

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            ->field('preview_img', '', 'addImg', array('listColumns' => array('flex' => 1)))
            ->field('active', \Yii::t('slider', 'active'), 'check')
            ->field('slide_link', \Yii::t('slider', 'slides_link'), 'string')
            ->widget( 'preview_img', 'skewer\\build\\Adm\\Slider\\Slide', 'getSlideImg' )
            ->setEditableFields( array( 'active' ), 'saveSlide' )
            ->enableDragAndDrop( 'sortSlideList' )
        ;

        $aItems = Slide::find()->where( 'banner_id', $this->iCurrentBanner )->order( 'position' )->getAll();

        //@TODO Активность приходит строкой и приведеним типов в JS превращается в true
        foreach( $aItems as $oItem )
            $oItem->active = (bool)$oItem->active;

        // добавляем данные
        $oFormBuilder->setValue( $aItems );

        // элементы управления
        $oFormBuilder
            ->buttonAddNew('editSlideForm', \Yii::t('slider', 'addSlide'));

        if ( !$this->currentBanner )
            $oFormBuilder
                ->buttonEdit('editBannerForm', \Yii::t('slider', 'editBanner'))
                ->buttonBack('bannerList');

        $oFormBuilder
            ->buttonRowUpdate( 'editSlideForm' )
            ->buttonRowConfirm( 'delSlide', \Yii::t('adm','del'), \Yii::t('slider','delete_slide'), 'icon-delete' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;

    }


    protected function actionSortSlideList() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['id']) || !$aData['id'] ||
            !isSet($aDropData['id']) || !$aDropData['id'] || !$sPosition )
            $this->addError( \Yii::t('slider', 'sortError') );

        if( ! Slide::sort( $aData['id'], $aDropData['id'], $sPosition ) )
            $this->addError( \Yii::t('slider', 'sortError') );

    }


    /**
     * Форма редактиорвания слайда
     * @return int
     */
    protected function actionEditSlideForm() {

        $aData = $this->getInData();
        $iItemId = isSet($aData['id']) ? $aData['id'] : $this->iCurrentSlide;
        $this->iCurrentSlide = $iItemId;

        $sUploadImage = isSet($aData['upload_image']) ? $aData['upload_image'] : '';

        $this->addJsFile( \Yii::$app->getAssetManager()->getBundle(jquery\Asset::className())->baseUrl.'/jquery.js' );
        $this->addJsFile( \Yii::$app->getAssetManager()->getBundle(jquery\UiAsset::className())->baseUrl.'/jquery-ui.min.js' );

        if ( $iItemId )
            $oSlideRow = Slide::find( $iItemId );
        else {
            $oSlideRow = Slide::getNewRow();
            $oSlideRow->setVal( 'active', true );
        }


        if ( $sUploadImage )
            $oSlideRow->setVal( 'img', $sUploadImage );

        if ( !$oSlideRow->getVal( 'img' ) )
            $oSlideRow->setVal( 'img', Slide::getEmptyImgWebPath() );

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->field('id', 'id', 'hide')
            ->field('img', '', 'hide')
            ->fieldSpec( 'canv', \Yii::t('slider', 'editSlide'), 'SlideShower', $oSlideRow )
            ->field('active', \Yii::t('slider', 'active'), 'check')
            ->field('slide_link', \Yii::t('slider', 'slides_link'), 'string')
        ;

        // устанавливаем значения
        $oFormBuilder->setValue( $oSlideRow );

        /* элементы управления */
        //$this->addLibClass( 'SlideLoadImage' );


        // добавляем элементы управления
        $oFormBuilder->buttonSave('saveSlide');
        $oFormBuilder->buttonCustomExt(
            ext\docked\UserFile::create( \Yii::t('slider', 'backLoad').'!', 'SlideLoadImage' )
            ->setIconCls( ext\docked\Api::iconAdd )
            ->setAddParam( 'slideId', $iItemId )
        );


        if ( $iItemId )
            $oFormBuilder->buttonEdit('editSlideText', \Yii::t('slider', 'editSlideText'));

        $oFormBuilder->buttonCancel('slideList');

        $this->addInitParam('lang', ['galleryUploadingImage' => \Yii::t('gallery', 'galleryUploadingImage')]);

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Форма расширенного редактирования текстов для слайда
     * @return int
     * @throws \Exception
     */
    protected function actionEditSlideText() {

        if ( !$this->iCurrentSlide )
            throw new \Exception( \Yii::t('slider', 'noFindSlide') );

        $oSlideRow = Slide::find( $this->iCurrentSlide );


        // создаем форму
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->field('id', 'id', 'hide')
            ->fieldWithValue( 'back2edit', '', 'hide', true )
            ->field('text1', \Yii::t('slider', 'slides_text1'), 'wyswyg')
            ->field('text2', \Yii::t('slider', 'slides_text2'), 'wyswyg')
            ->field('text3', \Yii::t('slider', 'slides_text3'), 'wyswyg')
            ->field('text4', \Yii::t('slider', 'slides_text4'), 'wyswyg')
        ;

        // устанавливаем значения
        $oFormBuilder
            ->setValue( $oSlideRow )

        // добавляем элементы управления
            ->buttonSave('saveSlide')
            ->buttonCancel('EditSlideForm');

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Обработка загруженного изображения для фона слайда
     */
    protected function actionUploadImage() {

        $iItemId = (int) $this->get( 'slideId' );

        $sSourceFN = Slide::uploadFile();

        $this->set(
            'data',
            array(
                'id' => $iItemId,
                'upload_image'=>$sSourceFN
            )
        );

        $this->actionEditSlideForm();
    }


    /**
     * Состояние сохранения слайда
     */
    protected function actionSaveSlide() {

        $aData = $this->getInData();
        $bBackToEdit = isSet( $aData['back2edit'] ) ? $aData['back2edit'] : false;

        try {

            if( !$this->iCurrentBanner )
                throw new \Exception( \Yii::t('slider', 'noFindBanner') );

            $iSlideId = (isSet($aData['id']) && $aData['id']) ? $aData['id'] : false;

            if ( $iSlideId )
                $oSlideRow = Slide::find( $iSlideId );
            else
                $oSlideRow = Slide::getNewRow();

            // при обновлении из списка приходят неактуальные данные
            unset( $aData['position'] );

            $oSlideRow->setData( $aData );

            $oSlideRow->setVal( 'banner_id', $this->iCurrentBanner );

            // не сохраняем картинку, если это загрушка
            if ( $oSlideRow->getVal( 'img' ) == Slide::getEmptyImgWebPath() )
                $oSlideRow->setVal( 'img', '' );

            if ( !$oSlideRow->getVal( 'position' ) )
                $oSlideRow->setVal( 'position', Slide::getMaxPos4Banner($this->iCurrentBanner)+1 );
            // todo position - перенести в модификаор и проверить механику с исключением текущего слайда по id

            $oSlideRow->save();

            if ($oBanner = Banner::findOne(['id' => $this->iCurrentBanner])){
                $oBanner->setVal('last_modified_date', date( "Y-m-d H:i:s", time() ));
                $oBanner->save();
            }

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

        $this->set( 'data', false );

        if ( $bBackToEdit )
            $this->actionEditSlideForm();
        else
            $this->actionSlideList();
    }


    /**
     * Состояние удаления слайда
     */
    protected function actionDelSlide() {

        $aData = $this->getInData();

        try {

            if( !$this->iCurrentBanner )
                throw new \Exception( \Yii::t('slider', 'noFindBanner') );

            $iSlideId = (isSet($aData['id']) && $aData['id']) ? $aData['id'] : false;

            if ( !$iSlideId )
                throw new \Exception( \Yii::t('slider', 'noFindSlide') );

            $oSlideRow = Slide::find( $iSlideId );

            if ( !$oSlideRow )
                throw new \Exception( \Yii::t('slider', 'noFindSlide') );

            if ($oBanner = Banner::findOne(['id' => $oSlideRow->getVal('banner_id')])){
                $oBanner->setVal('last_modified_date', date( "Y-m-d H:i:s", time() ));
                $oBanner->save();
            }

            $oSlideRow->delete();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

        $this->actionSlideList();
    }


    /**
     * Форма настройки параметров показа баннеров
     * @return int
     */
    protected function actionToolsForm() {

        $aToolData = array();
        $aItems = Query::SelectFrom( 'banners_tools' )->getAll();
        foreach ( $aItems as $aItem )
            $aToolData[ $aItem['bt_key'] ] = $aItem['bt_value'];


        // создаем форму
        $oFormBuilder = ui\StateBuilder::newEdit();

        /* добавляем поля */

        $aHeightParams = [
            'minValue' => 0,
            'allowDecimals' => false
        ];

        // #41095 Если задана адаптивность слайдеру, то запретить менять его высоту
        if ($aToolData['responsive']) {
            $aHeightParams['disabled'] = 1;
            $aToolData['maxHeight'] = '';
        }

        $oFormBuilder
            ->fieldSelect( 'mode', \Yii::t('slider', 'animtype'),
                [
                    'horizontal'=>'horizontal slide',
                    // 'vertical' => 'vertical slide',
                    'fade'=>'fade',
                ], [], false
            )
            ->field('pause', \Yii::t('slider', 'animduration'), 'int', ['minValue' => 0, 'allowDecimals' => false]) //def:450
            ->field('speed', \Yii::t('slider', 'animspeed'), 'int', ['minValue' => 0, 'allowDecimals' => false]) //def:4000
            ->field('autoHover', \Yii::t('slider', 'hoverpause'), 'check') //def:true
            ->field('auto', \Yii::t('slider', 'autostart'), 'check') //def:true
            ->field('infiniteLoop', \Yii::t('slider', 'infiniteLoop'), 'check') //def:true
            ->field('maxHeight', \Yii::t('slider', 'banner_h'), 'int', $aHeightParams) //def:330
            ->field('responsive', \Yii::t('slider', 'responsive'), 'check') //def:false
        ;

        // устанавливаем значения
        $oFormBuilder
            ->setValue( $aToolData )

        // добавляем элементы управления
            ->buttonSave('saveTools')
            ->buttonCancel('bannerList');

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Состояние сохранения параметров показа баннеров
     */
    protected function actionSaveTools() {

        $aData = $this->get('data');

        foreach ( $aData as $sKey => $sValue ) {
            Query::InsertInto( 'banners_tools' )
                ->set( 'bt_key', $sKey )
                ->set( 'bt_value', $sValue )
                ->onDuplicateKeyUpdate()
                ->set( 'bt_value', $sValue )
                ->get();
        }

        \Yii::$app->router->updateModificationDateSite();

        $this->actionBannerList();
    }

}