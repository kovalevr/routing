<?php

namespace skewer\build\Adm\GuestBook;

use skewer\components\config\InstallPrototype;

/**
 * Класс установки для модулей
 * Class Install
 * @package skewer\build\Adm\GuestBook
 */
class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}// class
