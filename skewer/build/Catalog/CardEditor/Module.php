<?php

namespace skewer\build\Catalog\CardEditor;


use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\components\auth\CurrentAdmin;
use skewer\components\catalog;
use skewer\base\ui;
use skewer\base\ft;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\components\gallery\Profile;

/**
 * Модуль редактора карточек товаров
 * Class Module
 * @package skewer\build\Catalog\CardEditor
 */
class Module extends ModulePrototype {

    // число элементов на страницу
    public $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    public $iPage = 0;

    /** Список системных каталожных полей базовой карточки */
    private static $SYSTEM_FIELDS = [
        'alias', 'title', 'article', 'announce', 'obj_description', 'price', 'measure', 'gallery', 'old_price',  // Характеристики товара
        'active', 'buy', 'fastbuy', 'on_main', 'hit', 'new', 'discount', 'countbuy',     // Элементы управления
        'tag_title', 'keywords', 'description', 'sitemap_priority', 'sitemap_frequency', // SEO
    ];

    /** Параметры полей, запрещённые для редактирования у системных полей базовой карточки */
    private static $PROTECTED_PARAMS = ['name', 'editor', 'link_id', 'validator'];

    /** Параметры полей, запрещённые для редактирования у системных полей базовой карточки для системного администратора сайта */
    private static $PROTECTED_PARAMS_SYS = ['name'];

    private function getCardId() {
        $card = $this->getInnerData( 'card', 0 );
        if ( !$card ) $card = $this->getInDataVal( 'id' );
        if ( !$card ) throw new UserException( 'Card not found' );
        $this->setInnerData( 'card', $card );
        return $card;
    }


    public function actionInit() {
        $this->actionCardList();
    }


    /**
     * Список карточек товара
     */
    public function actionCardList() {

        $this->setInnerData( 'card', 0 );

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newList();

        // установка заголовка
        $this->setPanelName( \Yii::t( 'card', 'title_card_list' ) );

        // добавляем поля
        $oFormBuilder
            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'type', \Yii::t('card', 'field_type') )
            ->fieldString( 'title', \Yii::t('card', 'field_title'), ['listColumns.flex' => 1] )
            ->widget( 'type', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyTypeWidget' );

        // добавляем данные
        $oFormBuilder
            ->setValue( catalog\Card::getGoodsCards( true ) );

        // элементы управления
        $oFormBuilder
            ->buttonAddNew('CardEdit', \Yii::t('card', 'btn_add_card'))
            ->button('GroupList', \Yii::t('card', 'btn_groups'), 'icon-edit')
            ->buttonRowUpdate( 'FieldList' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * Форма редактирования основных параметров карточки товара
     */
    public function actionCardEdit() {

        $iCardId = $this->getInnerData( 'card' );

        if ( $iCardId ) {
            $oCard = catalog\Card::get( $iCardId );
            $this->setPanelName( \Yii::t('card', 'title_card_editor', $oCard->title) );
        } else {
            $oCard = catalog\Card::get();
            $this->setPanelName( \Yii::t('card', 'title_new_card') );
        }


        // создание и заполнение формы
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('card', 'field_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('card', 'field_name'), ['disabled' => (bool)$iCardId] )
        ;

        if ( !$oCard->isBasic() )
            $oFormBuilder
                ->fieldSelect( 'parent', \Yii::t('card', 'field_base_card'), Api::getBasicCardList( $iCardId ), [], false )
                ->fieldHide( 'type', '', 's' )
            ;
        else
            $oFormBuilder
                ->fieldHide( 'parent', \Yii::t('card', 'field_base_card'), 's' )
                ->fieldHide( 'type', '' )
            ;

        // устанавливаем значения
        $oFormBuilder->setValue( $oCard );

        // добавляем элементы управления
        $oFormBuilder
            ->buttonSave('CardSave')
            ->buttonCancel($iCardId ? 'FieldList' : 'CardList')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Сохранение основных параметров карточки товара
     */
    public function actionCardSave() {

        $aData = $this->getInData();
        $id = ArrayHelper::getValue( $aData, 'id', null );
        $type = ArrayHelper::getValue( $aData, 'type', '' );
        $parent = ArrayHelper::getValue( $aData, 'parent', '' );

        if ( !($title = ArrayHelper::getValue( $aData, 'title', '' )) )
            throw new UserException( \Yii::t('card', 'error_no_card_name') );

        if ( $type != catalog\Card::TypeBasic && !$parent )
            throw new UserException( \Yii::t('card', 'error_no_base_card') );

        // всегда только расширенная карточка
        if ( $type != catalog\Card::TypeBasic )
            $aData['type'] = catalog\Card::TypeExtended;

        $aData['module'] = 'Catalog';

        $oCard = catalog\Card::get( $id );
        $oCard->setData( $aData );
        $oCard->save();
        $oCard->updCache();

        $this->setInnerData( 'card', $oCard->id );

        $this->actionFieldList();

    }


    /**
     * Удаление карточки товара
     */
    public function actionCardRemove() {

//        $aData = $this->getInData();
//        $id = ArrayHelper::getValue( $aData, 'id', null );
        $id = $this->getCardId();

        $oCard = catalog\Card::get( $id );

        if ( !$id || !$oCard )
            throw new UserException( \Yii::t('card', 'error_card_not_found') );

        $oCard->delete();

        // todo здесь вызов удаления карточек для новых товаров

        $this->actionCardList();
    }


    /**
     * Список полей для карточки товара
     */
    public function actionFieldList() {

        $oCard = catalog\Card::get( $this->getCardId() );

        $sHeadText = \Yii::t( 'card', 'head_card_name', $oCard->title );
        $this->setPanelName( \Yii::t('card', 'title_field_list',$oCard->title) );


        // создаем интерфейс
        $oFormBuilder = ui\StateBuilder::newList();

        $oFormBuilder
            ->headText( sprintf('<h1>%s</h1>', $sHeadText) );

        // добавляем поля
        $oFormBuilder
            //->fieldHide( 'id', 'id' )
            ->fieldString( 'name', \Yii::t('card', 'field_f_name') )
            ->fieldString( 'title', \Yii::t('card', 'field_f_title'), ['listColumns.flex' => 1] )
            //->fieldString( 'group', \Yii::t('card', 'field_f_group') )
            ->fieldString( 'editor', \Yii::t('card', 'field_f_editor') )
            ->setGroups( 'group' )
            ->widget( 'group', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyGroupWidget' )
            ->widget( 'editor', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyEditorWidget' );

        /* Устанавливаем значения */
        $aFields = $oCard->getFields();
        // Обработать языковую метку в заголовке поля, если есть разделитель
        foreach($aFields as &$oField)
            $oField->title = \Yii::tSingleString($oField->title);

        $oFormBuilder->setValue($aFields);

        // элементы управления
        $oFormBuilder
            ->buttonAddNew('FieldEdit', \Yii::t('card', 'btn_add_field'))
            ->buttonEdit('CardEdit', \Yii::t('card', 'btn_params'))
            ->buttonCancel('CardList', \Yii::t('card', 'btn_back'));

        if ( $oCard->isExtended() )
            $oFormBuilder
                ->buttonSeparator( '->' )
                ->buttonConfirm('CardRemove', \Yii::t('adm', 'del'), \Yii::t('card', 'remove_card_msg'), 'icon-delete')
            ;

        // обработчики на список
        $oFormBuilder
            ->buttonRowUpdate( 'FieldEdit' )
            ->buttonRowDelete( 'FieldRemove' )
            ->enableDragAndDrop( 'sortFields' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Обработчик события сортировки полей карточки товара
     */
    protected function actionSortFields() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        // fixme доработать передачу параметров и перестроение сущности
        if ( catalog\model\FieldTable::sort( $aData, $aDropData, $sPosition ) )
            catalog\Card::build( $aData['entity'] );

    }


    /**
     * Форма редактирование поля карточки товара
     * @return int
     */
    public function actionFieldEdit() {

        $iFieldId = $this->getInDataValInt( 'id' );
        $iCardId = $this->getInnerData( 'card', 0 );


        $oCard = catalog\Card::get( $iCardId );
        $sHeadText = \Yii::t('card', 'head_card_name', $oCard->title);

        if ( $iFieldId ) {
            $oField = catalog\Card::getField( $iFieldId );
            $oField->validator = $oField->getValidatorList();
            $this->setPanelName( \Yii::t('card', 'title_edit_field') );
        } else {
            $oField = catalog\Card::getField();
            $this->setPanelName( \Yii::t('card', 'title_new_field') );
        }


        // создание формы
        $oFormBuilder = ui\StateBuilder::newEdit();

        $oFormBuilder
            ->headText( sprintf('<h1>%s</h1>', $sHeadText) );

        // добавление полей
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('card', 'field_f_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('card', 'field_f_name'), ['disabled' => (bool)$iFieldId] )
            ->fieldSelect( 'group', \Yii::t('card', 'field_f_group'), catalog\Card::getGroupList(), [], false )
            ->fieldSelect( 'editor', \Yii::t('card', 'field_f_editor'), Api::getSimpleTypeList(), ['onUpdateAction'=>'updFields'], false )
            ->fieldSelect( 'link_id', \Yii::t('card', 'field_f_link_id'), Api::getEntityList( $oField ), ['disabled' => !$oField->isLinked()], false )
            ->fieldSelect( 'widget', \Yii::t('card', 'field_f_widget'), Api::getSimpleWidgetList( $oField ) )
            ->fieldMultiSelect( 'validator', \Yii::t('card', 'field_f_validator'), catalog\Validator::getListWithTitles(), $oField->getValidatorList() )
            ->fieldString( 'def_value', \Yii::t('card', 'field_f_def_value') )
        ;

        // добавление арибутов в редактор
        $aAttrList = $oField->getAttr();
        if ( count( $aAttrList ) ) {
            foreach ( $aAttrList as $aAttrParam ) {
                $oFormBuilder->fieldWithValue( 'attr_' . $aAttrParam['id'], \Yii::t( 'catalog', 'attr_' . $aAttrParam['name'] ), $aAttrParam['type'], $aAttrParam['value'] );
            }
        }

        // Запретить редактировать некоторые поля у системных полей базовой карточки
        if ( ($oCard->name == catalog\Card::DEF_BASE_CARD) and in_array($oField->name, self::$SYSTEM_FIELDS) ) {

            $aProtectedParams = CurrentAdmin::isSystemMode() ? self::$PROTECTED_PARAMS_SYS : self::$PROTECTED_PARAMS;
            foreach ($aProtectedParams as $sProtectedParam)
                if ($oFieldForm = $oFormBuilder->getField($sProtectedParam))
                    $oFieldForm->setDescVal('disabled', true);
        }

        // добавление значений
        $oFormBuilder
            ->setValue( $oField );

        // элементы управления
        $oFormBuilder
            ->buttonSave('FieldSave')
            ->buttonCancel('FieldList')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Обработчик изменения значения поля editor ("Тип отображения")
     */
    public function actionUpdFields() {

        $aFormData = $this->get('formData', array());
        $sEditor = isset($aFormData['editor']) ? $aFormData['editor'] : '';
        $iTypeId = isset($aFormData['link_id']) ? $aFormData['link_id'] : '';
        $sWidget = isset($aFormData['widget']) ? $aFormData['widget'] : '';

        $id = isset($aFormData['id']) ? $aFormData['id'] : null;

        $oField = catalog\Card::getField( $id );
        $oField->editor = $sEditor;

        $aLinkList = Api::getEntityList($oField);
        $aWidgetList = Api::getSimpleWidgetList( $oField );

        // Выбрать профиль для галереи по умолчанию
        if (($sEditor == ft\Editor::GALLERY) and !$iTypeId)
            $iTypeId = Profile::getDefaultId(Profile::TYPE_CATALOG);

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldSelect('link_id', '', $aLinkList, ['disabled' => !$oField->isLinked()], false)
            ->fieldSelect('widget',  '', $aWidgetList)

            ->setValue([
                'link_id' => isset($aLinkList[$iTypeId]) ? $iTypeId : 0,
                'widget'  => isset($aWidgetList[$sWidget]) ? $sWidget : 0,
            ]);

        $this->setInterfaceUpd($oForm->getForm());
    }


    /**
     * Состояние сохранение поля
     */
    public function actionFieldSave() {

        $card = $this->getInnerData( 'card', 0 );
        $data = $this->getInData();
        $id = ArrayHelper::getValue( $data, 'id', null );
        $title = ArrayHelper::getValue( $data, 'title', '' );
        $editor = ArrayHelper::getValue( $data, 'editor', '' );
        $validators = ArrayHelper::getValue( $data, 'validator', '' );

        $oField = catalog\Card::getField($id);
        $oCard  = catalog\Card::get($card);
        $bIsSysField = ( ($oCard->name == catalog\Card::DEF_BASE_CARD) and in_array($oField->name, self::$SYSTEM_FIELDS) );

        if ( !$title )
            throw new UserException( \Yii::t('card', 'error_no_field_name') );

        if ( !$card )
            throw new UserException( \Yii::t('card', 'error_card_not_found') );

        // В системных полях запрещено менять тип редактора, поэтому не нужно проверять этот параметр, поскольку он приходит пустой
        if ( !$editor and !$bIsSysField )
            throw new UserException( \Yii::t('card', 'error_no_editor_for_field') );

        // #39253 Проверка заполнения поля сущности для спец. редакторов
        if ( in_array($editor, [ft\Editor::SELECT, ft\Editor::MULTISELECT, ft\Editor::COLLECTION]) and
             !ArrayHelper::getValue($data, 'link_id') )
            throw new UserException( \Yii::t('card', 'error_no_link_id_for_editor') );

        $oField->setData( $data );
        $oField->entity = $card;
        $oField->save();

        // сохранение валидаторов, если это поле не системное
        if ( !$bIsSysField or CurrentAdmin::isSystemMode() )
            $oField->setValidator( $validators );

        // сохранение атрибутов для поля
        foreach ( $data as $sKey => $sValue )
            if ( !isSet( $oField->$sKey ) && strpos($sKey,'attr_') === 0 )
                $oField->setAttr( (int)substr($sKey, 5), $sValue );

        // rebuild card
        catalog\Card::build( $oField->entity );

        $this->actionFieldList();

    }


    /**
     * Удаление поля
     * @throws \skewer\base\ft\Exception
     */
    public function actionFieldRemove() {

        $data = $this->getInData();

        $iCardId = $this->getInnerData('card', 0);
        $oCard   = catalog\Card::get($iCardId);
        if (!$oCard)
            throw new UserException(\Yii::t('card', 'error_card_not_found'));

        $id = ArrayHelper::getValue( $data, 'id', null );

        if ( !$id )
            throw new UserException( \Yii::t('card', 'error_field_not_found') );

        $oField = catalog\Card::getField( $id );

        // Запретить удаление системных полей базовой карточки
        if ( ($oCard->name == catalog\Card::DEF_BASE_CARD) and in_array($oField->name, self::$SYSTEM_FIELDS) )
            throw new UserException( \Yii::t('dict', 'error_field_cant_removed') );

        $oField->delete();

        // rebuild card
        catalog\Card::build( $oField->entity );

        $this->actionFieldList();

    }


    /**
     * Список групп для карточки товара
     */
    public function actionGroupList() {

        $this->setPanelName( \Yii::t('card', 'title_group_list') );


        // создание формы
        $oFormBuilder = ui\StateBuilder::newList();

        // добавление полей
        $oFormBuilder
            ->fieldShow( 'id', 'id', 'i' )
            ->fieldString( 'name', \Yii::t( 'card', 'field_g_name' ) )
            ->fieldString( 'title', \Yii::t( 'card', 'field_g_title' ), ['listColumns.flex' => 1] )

        // добавление данных
            ->setValue( catalog\Card::getGroups() )

        // элементы управления
            ->buttonAddNew('GroupEdit')
            ->buttonCancel('CardList')
            ->buttonRowUpdate( 'GroupEdit' )
            ->buttonRowDelete( 'GroupRemove' )
            ->enableDragAndDrop( 'sortGroups' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }

    /** Обработчик события сортировки групп карточек */
    protected function actionSortGroups() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if ( catalog\model\FieldGroupTable::sortGroups( $aData['id'], $aDropData['id'], $sPosition ) ) {

            // Обновить кэши полей карточек, группы которых были отсортированы
            $aGroupFields = catalog\model\FieldTable::find()
                ->where('group', $aData['id'])
                ->orWhere('group', $aDropData['id'])
                ->asArray()
                ->getAll();

            $aUpdatingCards = [];

            if ($aGroupFields)
                foreach($aGroupFields as $aCardField)
                    $aUpdatingCards[$aCardField['entity']] = 1;

            foreach(array_keys($aUpdatingCards) as $sCardId)
                    catalog\Card::build( $sCardId );
        }

        $this->actionGroupList();
    }


    /**
     * Форма редактирования групп
     * @return int
     */
    public function actionGroupEdit() {

        $iGroupId = $this->getInDataVal( 'id' );

        if ( $iGroupId ) {
            $this->setPanelName( \Yii::t('card', 'title_edit_group') );
            $oGroup = catalog\Card::getGroup( $iGroupId );
        } else {
            $this->setPanelName( \Yii::t('card', 'title_new_group') );
            $oGroup = catalog\Card::getGroup();
        }


        // создание формы
        $oFormBuilder = ui\StateBuilder::newEdit();

        // установка полей
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('card', 'field_g_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('card', 'field_g_name'), ['disabled' => (bool)$iGroupId] )
            ->fieldSelect( 'group_type', \Yii::t('card', 'field_g_type'), [
                \Yii::t('card', 'field_g_type_default'),
                \Yii::t('card', 'field_g_type_collapsible'),
                \Yii::t('card', 'field_g_type_collapsed')
            ], [], false)
        ;

        // установка данных
        $oFormBuilder->setValue( $oGroup );

        // элементы управления
        $oFormBuilder
            ->buttonSave('GroupSave')
            ->buttonCancel('GroupList')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Сохранение группы
     */
    public function actionGroupSave() {

        $id = $this->getInDataVal('id');

        if ( !$this->getInDataVal('title') )
            throw new UserException( \Yii::t( 'card', 'error_no_group_name' ) );

        $oGroup = catalog\Card::getGroup($id);
        $oGroup->setData($this->get('data'));
        $oGroup->save();

        $this->actionGroupList();

    }


    /**
     * Удаление группы
     */
    public function actionGroupRemove() {

        $iGroupId = $this->getInDataVal( 'id' );

        $oGroup = catalog\Card::getGroup( $iGroupId );

        if ( $iGroupId && $oGroup )
            $oGroup->delete();

        $this->actionGroupList();
    }

}