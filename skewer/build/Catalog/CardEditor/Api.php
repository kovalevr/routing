<?php

namespace skewer\build\Catalog\CardEditor;


use skewer\components\catalog;
use skewer\base\ft;
use skewer\components\gallery\Profile;
use skewer\base\site\Layer;


/**
 * API для редактора карточек
 * Class Api
 * @package skewer\build\Tool\CatalogCardEditor
 */
class Api {

    const TypeSubEntity = 'sub_entity';

    /**
     * Кэш имен групп для спискового интерфейса
     * @var array
     */
    private static $aGroupWidgetList = array();


    /**
     * Форматированный список базовых карточек
     * @param int $id Идентификатор карты, которую исплючаем из списка
     * @return array
     */
    public static function getBasicCardList( $id = 0 ) {

        $aList = [];

        $query = catalog\model\EntityTable::find()
            ->where( 'module', catalog\Card::ModuleName )
            ->where( 'type', catalog\Card::TypeBasic )
            ->where( 'parent', '0' )
        ;

        /** @var catalog\model\EntityRow $card */
        while ( $card = $query->each() )
            if ( $card->id != $id )
                $aList[$card->id] = $card->title;

        return $aList;
    }


    /**
     * Отдает имя группы для спискового интерфейса
     * @param catalog\model\FieldRow $oItem
     * @param string $sField
     * @return string
     * Используется как метод для виджитирования полей
     */
    public static function applyGroupWidget( catalog\model\FieldRow $oItem, $sField ) {

        $iId = $oItem->$sField;

        // fixme ???
        if ( isset(self::$aGroupWidgetList[$iId]) )
            return self::$aGroupWidgetList[$iId];

        $oGroup = catalog\Card::getGroup( $iId );

        if ( $oGroup && $oGroup->id )
            return sprintf( '[%02d] %s', $oGroup->position, $oGroup->title ) ;
        else
            return '[00] '. \Yii::t('card', 'base_group');

    }


    /**
     * отдает имя редактора
     * @param $oItem
     * @param $sField
     * @return string
     * Используется как метод для виджитирования полей
     */
    public static function applyEditorWidget( $oItem, $sField ) {

        $aEditorList = ft\Editor::getSimpleList();

        if ( isSet($aEditorList[$oItem->$sField]) )
            return $aEditorList[$oItem->$sField];

        return $oItem->$sField;
    }


    /**
     * отдает метку для типа карточки
     * @param $oItem
     * @param $sField
     * @return mixed
     * Используется как метод для виджитирования полей
     */
    public static function applyTypeWidget( $oItem, $sField ) {

        $aTypeList = ['Dict','Base','Ext'];

        if ( isSet($aTypeList[$oItem->$sField]) )
            return $aTypeList[$oItem->$sField];

        return $oItem->$sField;

    }


    /**
     * Отдает набор типов для поля
     * @param bool $bAddLinkFields Флаг добавления в список типов для связных сущностей
     * @return array
     */
    public static function getSimpleTypeList( $bAddLinkFields = true ) {// fixme доработать метод

        $aList = ft\Editor::getSimpleList();

        unset( $aList[ft\Editor::SELECT] );

       if ( $bAddLinkFields ) {

            $aList[ft\Editor::SELECT] = \Yii::t('card', 'field_f_sub_link');

            $aList[ft\Editor::MULTISELECT] = \Yii::t('card', 'field_f_multisub_link');

            if ( \Yii::$app->register->moduleExists('Collections', Layer::CATALOG) )
                $aList[ft\Editor::COLLECTION] = \Yii::t('card', 'field_f_brands');

        }


//        // выпадающий список убираем, т.к. для него нет контента
//        /*
//         * todo сделать вместо добавления нового типа задание
//         *  типа поля, а потом выбор подчиненных сущностей или
//         *  типа поля в базе, в зависимости от первого выбора
//         */


        return $aList;

    }


    /**
     * Список сущностей для связи с полем
     * @param catalog\model\FieldRow $oField
     * @return array
     */
    public static function getEntityList( $oField ) {

        $aOut = [];

        if ( $oField->isLinked() ) {

            switch ( $oField->editor ) {
                case ft\Editor::SELECT:
                    $aOut = catalog\Card::getDictionaryList();
                    break;
                case ft\Editor::MULTISELECT:
                    $aOut = catalog\Card::getDictionaryList();
                    break;
                case ft\Editor::COLLECTION:
                    $aOut = catalog\Card::getCollectionList();
                    break;
                case ft\Editor::GALLERY:
                    $aOut = Profile::getActiveByType(Profile::TYPE_CATALOG, true);

                    // Добавить использующийся профиль галереи в независимости от его активности
                    if ($aProfileCurrent = Profile::getById($oField->link_id))
                        $aOut[$oField->link_id] = $aProfileCurrent['title'];

                    break;
            }

        }


        return $aOut;
    }


    /**
     * @param catalog\model\FieldRow $oField Объект поля
     * @return array
     */
    public static function getSimpleWidgetList( $oField ) {

        if ( $oField->isLinked() and
             !in_array($oField->editor, [ft\Editor::MULTISELECT]) )

                return [
                    'check_group' => \Yii::t('Card', 'widget_check_group'),
                    'select' => \Yii::t('Card', 'widget_select'),
                ];

        return [];
    }

}