<?php

namespace skewer\build\Catalog\Settings;

use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\base\ui;
use skewer\base\SysVar;
use yii\base\UserException;
use skewer\components\catalog;
use skewer\components\seo\Service;

/**
 * Модуль настройки каталога
 */
class Module extends ModulePrototype {

    public function actionInit() {

        $goodsInclude = SysVar::get('catalog.goods_include');
        $goodsRelated = SysVar::get('catalog.goods_related');
        $bHidePriceFractional = SysVar::get('catalog.hide_price_fractional');
        $iCountShowGoods = SysVar::get('catalog.countShowGoods');
        $iParametricSearch = SysVar::get('catalog.parametricSearch');
//        $iHide2LvlGoodsLinks = SysVar::get('catalog.hide2lvlGoodsLinks');
//        $iHideBuy1LvlGoods = SysVar::get('catalog.hideBuy1lvlGoods');
        $iShadowParamFilter = SysVar::get('catalog.shadow_param_filter');

        $iGuestBookShow = SysVar::get('catalog.guest_book_show');
        $sCurrencyType = SysVar::get('catalog.currency_type');
        $iShowRating = SysVar::get('catalog.show_rating');
        $iRandomRelated = SysVar::get('catalog.random_related');
        $iRandomRelatedCount = SysVar::get('catalog.random_related_count');

        if ( ($goodsModifications = SysVar::get('catalog.goods_modifications')) == null )
            $goodsModifications = 0;

        $form = ui\StateBuilder::newEdit();

        $form
            ->field('goods_include', \Yii::t('catalog', 'goods_include'), 'check')
            ->field('goods_related', \Yii::t('catalog', 'goods_related'), 'check')
            ->field('goods_modifications', \Yii::t('catalog', 'goods_modifications'), 'check')
            ->field('hide_price_fractional', \Yii::t('catalog', 'hide_price_fractional'), 'check')
            ->field('countShowGoods', \Yii::t('catalog', 'countShowGoods'), 'int', ['minValue' => 0, 'allowDecimals' => false])
            ->field('parametric_search', \Yii::t('catalog', 'parametric_search'), 'check')
//            ->field( 'hide2lvlGoodsLinks', \Yii::t('catalog', 'hide2lvlGoodsLinks'), 's', 'check' )
//            ->field( 'hideBuy1lvlGoods', \Yii::t('catalog', 'hideBuy1lvlGoods'), 's', 'check' )
            ->field('shadow_param_filter', \Yii::t('catalog', 'shadow_param_filter'), 'check')
            ->field('guest_book_show', \Yii::t('catalog', 'guest_book_show'), 'check')
            ->field('currency_type', \Yii::t('catalog','currency_type'),'string')
            ->field('show_rating', \Yii::t('catalog', 'show_rating'), 'check')
            ->fieldCheck('random_related',\Yii::t('catalog', 'random_related'))
            ->fieldInt('random_related_count',\Yii::t('catalog', 'random_related_count'))
        ;

        $form->buttonSave('save');
        
        $form->setValue([
            'goods_include' => $goodsInclude,
            'goods_related' => $goodsRelated,
            'goods_modifications' => $goodsModifications,
            'hide_price_fractional' => $bHidePriceFractional,
            'countShowGoods' => $iCountShowGoods,
            'parametric_search' => $iParametricSearch,
//            'hide2lvlGoodsLinks' => $iHide2LvlGoodsLinks,
//            'hideBuy1lvlGoods' => $iHideBuy1LvlGoods,
            'shadow_param_filter' => $iShadowParamFilter,
            'guest_book_show' => $iGuestBookShow,
            'currency_type' => $sCurrencyType,
            'show_rating' => $iShowRating,
            'random_related' => $iRandomRelated,
            'random_related_count' => $iRandomRelatedCount
        ]);

        $this->setInterface($form->getForm());
    }

    public function actionSave(){

        $countShowGoods = $this->getInDataValInt('countShowGoods');

        if ($countShowGoods <= 0)
            throw new UserException(\Yii::t('catalog', 'countShowGoodsError'));

        if ($this->getInDataValInt('random_related_count') <=0 )
            throw new UserException(\Yii::t('catalog', 'countShowGoodsError'));

        // Если были включены или выключены аналоги(модификации), то обновить поисковой индекс и карту сайта
        if ((int)$this->getInDataVal('goods_modifications') != (int)SysVar::get('catalog.goods_modifications')) {

            // ! Этот параметр нужно установить раньше обновления поискового индекса товара, т. к. используется там
            SysVar::set('catalog.goods_modifications', (int)$this->getInDataVal('goods_modifications'));

            catalog\Api::deactiveModificationsFromSearch();
            Service::makeSearchIndex();
            Service::updateSiteMap();
        }

        SysVar::set('catalog.goods_include',(int)$this->getInDataVal('goods_include'));
        SysVar::set('catalog.goods_related', (int)$this->getInDataVal('goods_related'));
        SysVar::set('catalog.hide_price_fractional', (int)$this->getInDataVal('hide_price_fractional'));
        SysVar::set('catalog.parametricSearch', (int)$this->getInDataVal('parametric_search'));
//        SysVar::set('catalog.hide2lvlGoodsLinks', (int)$this->getInDataVal('hide2lvlGoodsLinks'));
//        SysVar::set('catalog.hideBuy1lvlGoods', (int)$this->getInDataVal('hideBuy1lvlGoods'));
        SysVar::set('catalog.shadow_param_filter', (int)$this->getInDataVal('shadow_param_filter'));

        SysVar::set('catalog.countShowGoods', $countShowGoods);
        SysVar::set('catalog.guest_book_show', (int)$this->getInDataVal('guest_book_show'));
        SysVar::set('catalog.currency_type', $this->getInDataVal('currency_type'));
        SysVar::set('catalog.show_rating', (int)$this->getInDataVal('show_rating'));
        SysVar::set('catalog.random_related', (int)$this->getInDataValInt('random_related'));
        SysVar::set('catalog.random_related_count', (int)$this->getInDataValInt('random_related_count'));
        \Yii::$app->router->updateModificationDateSite();

        $this->actionInit();
    }
}