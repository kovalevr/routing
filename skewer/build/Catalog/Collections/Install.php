<?php

namespace skewer\build\Catalog\Collections;

use skewer\components\gallery\Profile;
use skewer\base\section\Parameters;
use skewer\components\seo;
use skewer\components\config\InstallPrototype;

class Install extends InstallPrototype {


    public function init() {
        return true;
    }


    public function install() {

        /**
         * TODO: когда будет реализован апи галереи, переписать косныль ниже
         *  тут должно быть добавление нового профиля с 3мя форматами
         */

        // добавляем новый профиль форматов
        $profile_id = Profile::setProfile([
            'type' => Profile::TYPE_CATALOG4COLLECTION,
            'alias' => 'collection',
            'title' => \Yii::t('data/gallery', 'profile_collection_name', [], \Yii::$app->language ),
            'active' => 1
        ]);

        if (!$profile_id)
            $this->fail('cant create gallery profile');

        // todo создание SEO шаблона для коллекций
        $tpl = new seo\TemplateRow();
        $tpl->alias = SeoCollectionList::getAlias();
        $tpl->name = 'Страница коллекции в каталоге';
        $tpl->title = '[Название коллекции]. [Название страницы]. [Цепочка разделов до главной] [Название сайта]';
        $tpl->description = '[Название сайта], [Цепочка разделов до главной], [Название страницы], [Название коллекции]';
        $tpl->keywords = '[название коллекции], [название страницы]';
        $tpl->info = "[Название коллекции] - название коллекции\n[название коллекции] - название коллекции с маленькой буквы";
        $tpl->save();

        $tpl = new seo\TemplateRow();
        $tpl->alias = SeoElementCollection::getAlias();
        $tpl->name = 'Страница элемента коллекции в каталоге';
        $tpl->title = '[Элемент коллекции]. [Название коллекции]. [Название страницы]. [Цепочка разделов до главной] [Название сайта]';
        $tpl->description = '[Название сайта], [Цепочка разделов до главной], [Название страницы], [Название коллекции], [Элемент коллекции]';
        $tpl->keywords = '[элемент коллекции], [название коллекции], [название страницы]';
        $tpl->info = "[Элемент коллекции] - название элемента коллекции\n[элемент коллекции] - название элемента коллекции с маленькой буквы";
        $tpl->altTitle = "[Название коллекции]";
        $tpl->save();

        // добавление модуля для вывода на главной
        $group = 'collection';
        Parameters::setParams( \Yii::$app->sections->main(), $group, '.title', 'Коллекции каталога' );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'layout', 'content' );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'titleOnMain', 'Catalog collection', '', \Yii::t('catalog','param_title_on_main', [], \Yii::$app->language ) );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'object', 'CatalogViewer' );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'onMainCollection', '1' );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'listTemplate', 'list', 'list editor.type_collection_list' . "\n"
            .'slider editor.type_collection_slider', \Yii::t('editor', 'type_category_view', [], \Yii::$app->language ) );



        return true;
    }

    public function uninstall() {
        $this->fail("Нельзя удалить компонент каталога");
        return true;
    }

}