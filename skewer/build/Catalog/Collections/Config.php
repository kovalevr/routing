<?php

namespace skewer\build\Catalog\Collections;

use skewer\build\Component;
use skewer\base\site\Layer;

$aConfig['name']     = 'Collections';
$aConfig['title']    = 'Коллекции';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::CATALOG;
$aConfig['useNamespace'] = true;

$aConfig['dependency'] = array(
    array('Collections', Layer::ADM),
);

$aConfig['events'][] = [
    'event' => \skewer\components\search\Api::EVENT_GET_ENGINE,
    'class' => Search::className(),
    'method' => 'getSearchEngine',
];

return $aConfig;
