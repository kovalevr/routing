<?php

namespace skewer\build\Catalog\Collections;

use skewer\build\Catalog\Dictionary as Dict;
use skewer\components\catalog;
use skewer\base\orm\Query;
use skewer\base\ui;
use skewer\base\ft;
use skewer\components\seo;
use skewer\helpers\Transliterate;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\components\gallery\Profile;

/**
 * Модуль настройки брендов
 * Class Module
 * @package skewer\build\Catalog\Collections
 */
class Module extends Dict\Module {


    public function getTitle() {
        return \Yii::t('collections', 'module_name');
    }


    protected function actionInit() {
        $this->actionList();
    }


    protected function actionTools() {}


    /**
     * Список коллекций каталога
     */
    protected function actionList() {

        $this->setInnerData( 'card', 0 );
        $this->setInnerData( 'rowId', 0 );

        $oFormBuilder = ui\StateBuilder::newList();

        // установка заголовка
        $this->setPanelName( \Yii::t('collections', 'coll_list_for_cat') );

        $oFormBuilder
            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('collections', 'coll_name'), ['listColumns.flex' => 1] )
            ->setValue( catalog\Card::getCollections() )
            ->buttonAddNew('Edit', \Yii::t('collections', 'create_coll'))
            ->buttonRowUpdate( 'View' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Форма редактирования параметров коллекции
     */
    protected function actionEdit() {

        $oCard = catalog\Card::get();
        $this->setPanelName( \Yii::t('collections', 'new_coll') );

        // Добавить галерейный профиль для коллекций по умолчанию
        $oCard->profile_id = Profile::getDefaultId( Profile::TYPE_CATALOG4COLLECTION );

        $oFormBuilder = ui\StateBuilder::newEdit();
        $oFormBuilder
            ->headText( '<h1>' . \Yii::t('collections', 'new_coll') . '</h1>' )
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('collections', 'coll_name'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('collections', 'system_name') )
            ->fieldSelect( 'profile_id', \Yii::t('collections', 'field_gallery_profile'), Profile::getActiveByType(Profile::TYPE_CATALOG4COLLECTION, true), [], false )
            ->setValue( $oCard )
            ->buttonSave('Save')
            ->buttonCancel('List')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Метод сохранения параметров коллекции
     * @throws UserException
     */
    protected function actionSave() {

        $aData = $this->getInData();

        $sDictTitle = $this->getInDataVal( 'title' );

        if ( !$sDictTitle )
            throw new UserException( \Yii::t('collections', 'error_noname_seted') );

        $oCard = catalog\Card::addCollection( $aData, $aData['profile_id'] );

        $this->setInnerData( 'card', $oCard->id );

        $this->actionView();
    }


    /**
     * Список позиций в коллекции
     * @throws UserException
     */
    protected function actionView() {

        $this->setInnerData( 'rowId', 0 );
        $title = $this->getStr( 'title', $this->getInnerData( 'title', '' ) );
        $active = $this->getStr( 'active', $this->getInnerData( 'active', '' ) );
        $card = $this->getCard();

        // получение контентных данных
        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        // набор объектов на вывод
        $query = $oTable->find()->asArray();
        if ( $title )
            $query->where( 'title LIKE ?', '%' . $title . '%' );
        if ( $active )
            $query->where( 'active', $active == 1 );


        // установка заголовка
        $this->setPanelName( \Yii::t('collections', 'coll_panel_name', catalog\Card::getTitle( $card )) );

        // контентная форма
        $oFormBuilder = ui\StateBuilder::newList();

        $oFormBuilder
            ->filterText( 'title', $title, \Yii::t('collections', 'field_title') )
            ->filterSelect( 'active', [
                1 => \Yii::t('page', 'yes'),
                2 => \Yii::t('page', 'no')
            ], $active, \Yii::t('collections', 'field_active') )
            ->setFilterAction( 'view' )

            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('collections', 'field_title'), ['listColumns.flex' => 2] )
            ->fieldCheck( 'active', \Yii::t('collections', 'field_active'), ['listColumns.flex' => 1] )
            ->fieldCheck( 'on_main', \Yii::t('collections', 'field_on_main'), ['listColumns.flex' => 1] )
            ->setValue( $query->getAll() )
            ->buttonAddNew('ItemEdit', \Yii::t('collections', 'add'))
            ->buttonEdit('EditCollection', \Yii::t('collections', 'edit_title'))
            ->buttonBack('List')
            ->buttonSeparator( '->' )
            ->buttonConfirm('Remove', \Yii::t('adm', 'del'), \Yii::t('collections', 'msg_remove_coll', catalog\Card::getTitle($card)), 'icon-delete')
            ->buttonRowUpdate( 'ItemEdit' )
            ->buttonRowDelete( 'ItemRemove' )

            ->setEditableFields( ['active','on_main'], 'ItemFastSave' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Форма редактирования значения для справочника
     * @throws UserException
     */
    protected function actionItemEdit() {

        $id = $this->getInDataVal( 'id', $this->getInnerData( 'rowId', 0 ) );
        $this->setInnerData( 'rowId', $id );
        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( "Card not found!" );

        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        if ( !$oItem )
            throw new UserException("Can not find good id = [$id]");

        // def values
        if ( !$id ) {
            $oItem->title = '';
            $oItem->active = 1;
        }

        // установка заголовка
        $oDict = catalog\Card::get( $card );
        $this->setPanelName( \Yii::t('collections', 'dict_panel_name', catalog\Card::getTitle( $card )) );

        $oFormBuilder = ui\StateBuilder::newEdit();

        $oFormBuilder
            ->fieldHide( 'id', 'id' )
        ;

        $aFields = $oDict->getFields();
        foreach ( $aFields as $oField ) {
            if ($oField->name !== 'last_modified_date')
                $oFormBuilder->fieldByEntity( $oField );
        }

        $oFormBuilder
            ->setValue( $oItem )
            ->buttonSave('ItemSave')
            ->buttonCancel('View')
        ;

        $oFormBuilder->getField('gallery')->setDescVal('seoClass', SeoElementCollection::className());
        $oFormBuilder->getField('gallery')->setDescVal('iEntityId', $oItem->id . ':' . $oItem->getModel()->getName());

        if ( $id ) {
            $oFormBuilder
                ->buttonSeparator()
                ->button('GoodsView', \Yii::t('collections', 'goods'), 'icon-view')
            ;
        }

        // добавление SEO блока полей
        seo\Api::appendExtForm( $oFormBuilder, new SeoElementCollection($oItem->id, $this->sectionId(), $oItem->getData(), $oItem->getModel()->getName() ), ['seo_gallery', 'none_search', 'none_index', 'add_meta'], false );

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Сохранение значения всех полей для коллекции
     */
    protected function actionItemSave() {

        $aData = $this->getInData();
        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( "Card not found!" );

        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();
        $aOldAtrributes = $oItem->getData();

        $oItem->setData( $aData );

        if ( !$oItem->title )
            throw new UserException( \Yii::t('collections', 'error_title_not_found') );

        // генерация alias для коллекции
        if ( !$oItem->alias )
            $oItem->alias = $oItem->title;

        $oItem->alias = $this->getUniqueAlias( $card, $oItem->alias, $id );
        $oItem->setVal('last_modified_date', date('Y-m-d H:i:s', time()));

        if ( $oItem->save() ) {

            seo\Api::saveJSData(
                new SeoElementCollection( ArrayHelper::getValue($aOldAtrributes, 'id', 0), 0, $aOldAtrributes,   $oItem->getModel()->getName() ),
                new SeoElementCollection( $oItem->id,                                      0, $oItem->getData(), $oItem->getModel()->getName() ),
                $aData,
                false
            );

            $oSearch = new Search();
            $oSearch->setCard( $oTable->getName() );
            $oSearch->updateByObjectId( $oItem->id );

            $this->actionView();

        } else {
            $this->addError( $oItem->getError() );
        }

    }

    /** Действие: Удаление элемента коллекции */
    protected function actionItemRemove() {

        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        $this->setInnerData( 'rowId', 0 );

        if ( !$id )
            throw new UserException( \Yii::t('collections', 'error_row_not_found') );

        $oCurTable = ft\Cache::getMagicTable( $card );
        if ( !$oCurTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        catalog\Dict::removeValue($card, $id);

        $oSearch = new Search();
        $oSearch->setCard( $oCurTable->getName() );
        $oSearch->deleteByObjectId( $id );

        $this->actionView();
    }


    /**
     * Генерация уникального алиаса для сущности
     * @param $card
     * @param $alias
     * @param $id
     * @return array|string
     * @throws ft\Exception
     * todo найти место для этой функции
     */
    private function getUniqueAlias( $card, $alias, $id ) {

        $model = ft\Cache::get( $card );

        $alias = Transliterate::generateAlias( $alias );

        $flag = (bool)Query::SelectFrom( $model->getTableName() )
            ->where( 'alias', $alias )
            ->andWhere( 'id!=?', $id )
            ->getCount()
        ;

        if ( !$flag )
            return $alias;

        preg_match( '/^(\S+)(-\d+)?$/Uis', $alias, $res );
        $aliasTpl = isSet( $res[1] ) ? $res[1] : $alias;
        $iCnt = isSet( $res[2] ) ? -(int)$res[2] : 0;
        while ( substr( $aliasTpl, -1 ) == '-' )
            $aliasTpl = substr( $aliasTpl, 0 , strlen($aliasTpl) - 1 );

        do {

            $iCnt++;
            $alias = $aliasTpl . '-' . $iCnt;

            $flag = (bool)Query::SelectFrom( $model->getTableName() )
                ->where( 'alias', $alias )
                ->andWhere( 'id!=?', $id )
                ->getCount()
            ;

        } while ( $flag );


        return $alias;
    }


    /**
     * Метод сохранения значения поля для коллекции (редактирование из списка)
     * @throws UserException
     */
    protected function actionItemFastSave() {


        $data = $this->get( 'data' );
        $field = $this->get( 'field_name' );
        $card = $this->getInnerData( 'card' );

        $id = ArrayHelper::getValue( $data, 'id', 0 );
        $value = ArrayHelper::getValue( $data, $field, '' );

        if ( !$id )
            throw new UserException( "Card not found!" );

        if ( !$card )
            throw new UserException( "Card not found!" );

        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        $oItem->setData( [$field => $value] );

        if ( $oItem->save() ) {

            $oSearch = new Search();
            $oSearch->setCard( $oTable->getName() );
            $oSearch->updateByObjectId( $oItem->id );
            \Yii::$app->router->updateModificationDateSite();

            $this->actionView();

        } else {
            $this->addError( $oItem->getError() );
        }
    }


    /**
     * Список товаров связанных с коллекцией
     */
    protected function actionGoodsView() {

        $id = $this->getInnerData( 'rowId', 0 );
        $card = $this->getInnerData( 'card' );

        // fixme Обработка нескольких связей
        /** @var catalog\model\FieldRow $field */
        $field = catalog\model\FieldTable::find()
            ->where( 'link_id', $card )
            ->getOne();

        $items = [];

        if ( $field ) {
            $items = catalog\GoodsSelector::getList4Collection( 0, $field->name, $id )->parse();
        }

        $oFormBuilder = ui\StateBuilder::newList();


        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );
        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        // установка заголовка
        //$this->setPanelName( \Yii::t('collections', 'coll_list_for_cat') );
        $this->setPanelName( \Yii::t('collections', 'goods_coll_list_panel', [$oItem->title, catalog\Card::getTitle( $card )] ) );

        $oFormBuilder
            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('collections', 'goods_name'), ['listColumns.flex' => 1] )
            ->setValue( $items )
            ->buttonBack('ItemEdit', null, ['skipData' => true])
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * Интерфейс редактиования коллекции
     */
    protected function actionEditCollection() {

        $card = $this->getInnerData( 'card' );
        $oDict = catalog\Card::get( $card );

        $aProfiles = Profile::getActiveByType(Profile::TYPE_CATALOG4COLLECTION, true);

        // Добавить использующийся профиль галереи в независимости от его активности
        foreach ($oDict->getFields() as $oField)
            if ($oField->editor == ft\Editor::GALLERY) {
                $oDict->profile_id = $iProfileId = $oField->link_id;

                if ($aProfileCurrent = Profile::getById($iProfileId))
                    $aProfiles[$iProfileId] = $aProfileCurrent['title'];
                break;
            }

        // создание и заполнение формы
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('collections', 'field_title'), ['listColumns.flex' => 1] )
            ->fieldSelect( 'profile_id', \Yii::t('collections', 'field_gallery_profile'), $aProfiles, [], false )
        ;

        // устанавливаем значения
        $oFormBuilder->setValue( $oDict );

        // добавляем элементы управления
        $oFormBuilder
            ->buttonSave('SaveCollection')
            ->buttonCancel('View')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );


    }


    /**
     * Состояние сохранения отредактированной коллекции
     * @throws \Exception
     * @throws UserException
     */
    protected function actionSaveCollection() {

        $aData = $this->getInData();
//        $type = ArrayHelper::getValue( $aData, 'type', '' );
//        $parent = ArrayHelper::getValue( $aData, 'parent', '' );

        $card = $this->getInnerData( 'card' );

        if ( !($title = ArrayHelper::getValue( $aData, 'title', '' )) )
            throw new UserException( \Yii::t('collections', 'error_no_card_name') );

//        if ( $type != catalog\Card::TypeBasic && !$parent )
//            $this->riseError( \Yii::t('collections', 'error_no_base_card') );

        // всегда только расширенная карточка
//        if ( $type != catalog\Card::TypeBasic )
//            $aData['type'] = catalog\Card::TypeExtended;

        $oDict = catalog\Card::get( $card );

        // Сохранить профиль галереи коллекции
        foreach ($oDict->getFields() as $oField)
            if ($oField->editor == ft\Editor::GALLERY) {
                $oField->link_id = $aData['profile_id'];
                $oField->save();
                break;
            }

        $oDict->setData( ['title' => $title] );
        $oDict->save();
        $oDict->updCache();

        $this->actionView();
    }

    /**
     * @inheritDoc
     */
    protected function actionRemove() {

        $mCardId = $this->getInnerData( 'card' );

        if (!$mCardId)
            throw new UserException( \Yii::t('collections', 'error_coll_not_selected') );

        if ( !$oCard = catalog\Card::get($mCardId) )
            throw new UserException( \Yii::t('collections', 'error_coll_not_found') );

        /** Имя карточки удаляемой коллекции */
        $sCollCardName = $oCard->name;

        $aErrorMessages = [];
        catalog\Dict::removeDict($mCardId, $aErrorMessages);

        if ($aErrorMessages)
            throw new UserException( \Yii::t('collections', 'error_del_usage_coll'). '<br>' . join('<br>', $aErrorMessages) );

        // Удалить из поискового индекса
        $oSearch = new Search();
        $oSearch->setCard($sCollCardName);
        $oSearch->deleteAll();

        $this->setInnerData( 'card', 0 );

        $this->actionList();
    }
}