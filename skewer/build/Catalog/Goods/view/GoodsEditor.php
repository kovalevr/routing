<?php

namespace skewer\build\Catalog\Goods\view;

use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Catalog\Goods\SeoGoodWithCard;
use skewer\components\catalog;
use skewer\components\gallery\Profile;
use skewer\base\section\Tree;
use skewer\base\ui;
use skewer\components\ext;
use skewer\base\ft;
use skewer\base\SysVar;
use skewer\components\seo\Api;

/**
 * Построение интерфейса редактирование товара
 * Class GoodsEditor
 * @package skewer\build\Catalog\Goods\view
 * todo рефакторинг после создания построителя интерфейсов
 */
class GoodsEditor extends FormPrototype {

    const categoryField = '__category';
    const mainLinkField = '__main_link';

    public static $bOnlyActive = false;

    public function build() {

        // Задание набора полей для интерфейса
        $this->initForm();

        /** @var catalog\GoodsRow $oGoodsRow */
        $oGoodsRow = $this->model->getGoodsRow();

        /* Кнопки */

        $this->_form->getForm()->addBtnSave('edit', 'edit');
        $this->_form->buttonCancel();

        /*Фильтр по "только активные"*/
        if (self::$bOnlyActive){

            $aFormFields = $this->_form->getForm()->getFields();

            $aOutFormFields = [];

            $oModelFieldList = $oGoodsRow->getFields();

            foreach ($aFormFields as $oFormField){

                if ( in_array($oFormField->getName(), ['id',self::mainLinkField,self::categoryField]) ) {
                    $bAdd = true;
                } else {
                    if ( !isset($oModelFieldList[$oFormField->getName()]) )
                        continue;

                    $oModelField = $oModelFieldList[$oFormField->getName()];

                    $bAdd = $oModelField->getAttr('active');

                }

                if ( $bAdd )
                    $aOutFormFields[] = $oFormField;

            }

            $this->_form->getForm()->setFields($aOutFormFields);
        }

        // Если редактируется существующая каталожная позиция
        if ($oGoodsRow->getRowId()) {

            $this->_form->buttonSeparator();
            $this->btnShowRelatedItems();
            $this->btnShowIncludedItems();
            $this->btnShowModificationsItems();

            if ( SysVar::get('catalog.goods_include') || SysVar::get('catalog.goods_related') )
                $this->_form->buttonSeparator();

            $this->_form->buttonDelete();
        }

        $this->_form->useSpecSectionForImages();

        /* Установка контента полей */

        // Обработка полей для вывода по типам
        $aFields = $oGoodsRow->getFields();
        $aData =  $oGoodsRow->getData();
        foreach($aFields as $oFtField) {

            // @todo приведение типов
            if ( isSet( $aData[ $oFtField->getName() ] ) && $oFtField->getDatatype() == 'int' )
                $aData[ $oFtField->getName() ] = (int)$aData[ $oFtField->getName() ];
            if ( isSet( $aData[ $oFtField->getName() ] ) && $oFtField->getDatatype() == 'decimal(12,2)' )
                $aData[ $oFtField->getName() ] = (float)$aData[ $oFtField->getName() ];
        }

        $this->_form->setValue($aData);

        /** @var bool $bIsCommonList Это общий список товаров(Фильтр = Раздел:Все)? */
        $bIsCommonList = !(bool)$this->_module->getCurrentSection();

        $this->_form->getField('gallery')->setDescVal('iEntityId', $oGoodsRow->getRowId() . ":" . $oGoodsRow->getExtCardName());
        $this->_form->getField('gallery')->setDescVal('seoClass', SeoGood::className());
        $this->_form->getField('gallery')->setDescVal('sectionId', $this->_module->getCurrentSection());

        if (!$bIsCommonList){
            $sCard = catalog\Section::getDefCard($this->_module->getCurrentSection());
            $aGood = catalog\GoodsSelector::get($oGoodsRow->getRowId());
            $aGood['id'] = $oGoodsRow->getRowId();
            $aGood['fields'] = catalog\Parser::parseFieldsByCard($sCard, $aGood['id'], $aData);

            $oSeo = new SeoGood();
            $oSeo->setDataEntity($aGood);
            $oSeo->setSectionId($this->_module->getCurrentSection());
            $oSeo->setExtraAlias($sCard);

            Api::appendExtForm($this->_form, $oSeo);
        } else
            $this->_form->fieldWithValue('warning_text',\Yii::t('SEO','warning'), 'show', \Yii::t('SEO','warning_text'),['groupTitle' => \Yii::t('SEO', 'group_title'), 'groupType'  => 1]);

    }

    /**
     * Создание формы по моделям полей
     * @param array $aAttr
     * @param array $aFields
     * @param bool $bDisableUniq
     * @throws \Exception
     */
    protected function initForm( array $aAttr = ['active'], array $aFields = ['id'], $bDisableUniq = false ) {

        $this->_form = ui\StateBuilder::newEdit();

        /** @var \skewer\components\catalog\GoodsRow $oGoodsRow */
        $oGoodsRow = $this->model->getGoodsRow();

        // Сущность расширенной карточки
        $sExtCardName = $oGoodsRow->getExtCardName();
        $oExtEntity = catalog\model\EntityTable::getByName( $sExtCardName );
        if ( !$oExtEntity )
            throw new \Exception( "Не найдена сущность с именем '$sExtCardName'" );

        /** Наборы групп и типов групп */
        $aGroupsTypes = [];
        $aGroupList = catalog\Card::getGroupList($aGroupsTypes);

        /* Построить поля формы */

        // Добавить в самый верх специальные поля, типа настройки раздела
        if ($oGoodsRow->isMainRow() and !$bDisableUniq)
            $this->addSpecialFields();

        /** Сортировать группы полей
         * @todo Данный метод будет не актуальный, после кэширования в таблице c_entity полей базовой и расширенной карточек
         * вместе для каждой расширенной карточки, так как сортировка полей по группам происходит в методе
         * \skewer\components\catalog\model\EntityRow::updCache() */

        $aFieldsByGroups = $aGroupList;
        foreach ($oGoodsRow->getFields() as $oFtField) {
            $iGroup = (int)$oFtField->getParameter( '__group_id' );
            if (!isset($aFieldsByGroups[$iGroup]))
                $iGroup = 0;
            if (is_array($aFieldsByGroups[$iGroup]))
                $aFieldsByGroups[$iGroup][] = $oFtField;
            else
                $aFieldsByGroups[$iGroup] = array($oFtField);
        }

        // Добавить поля из карточки
        foreach ($aFieldsByGroups as $iGroup => &$mFtFields)
            if (is_array($mFtFields))
                foreach ($mFtFields as $oFtField) {
                    /** @var  ft\model\Field $oFtField */
                $sGroupTitle = isset($aGroupList[$iGroup]) ? $aGroupList[$iGroup] : '';

                /** Исключить поле из вывода, если установлены определённые атрибуты */
                $bDisabled = false;
                $bAddFlag = true;
                foreach ($aAttr as $sAttrName)
                    $bAddFlag = ($bAddFlag and (bool)$oFtField->getAttr($sAttrName));
                if ($bAddFlag and $bDisableUniq and !in_array($oFtField->getName(), $aFields) and !$oFtField->getAttr('is_uniq'))
                    $bDisabled = true;

                $aAddParams = [
                    'groupTitle' => \Yii::tSingleString($sGroupTitle),
                    'groupType'  => isset($aGroupsTypes[$iGroup]) ? $aGroupsTypes[$iGroup] : 0,
                    'disabled'   => $bDisabled,
                ];

                $this->makeFormFieldByFtModel($oFtField, $aAddParams);
        }

        // Вывод ошибок для полей
        foreach ($oGoodsRow->getErrorList() as $sFieldName => $sErrorText) {
            if ($oField = $this->_form->getField($sFieldName))
                $oField->setError($sErrorText);
        }
    }

    /**
     * Добавление поля на форму
     * @param ft\model\Field $oFtField Объект добавляемого поля
     * @param array $aParams Дополнительные параметры поля
     */
    private function makeFormFieldByFtModel( ft\model\Field $oFtField, array $aParams = [] ) {

        // todo #link пересмотреть подход после внедрения ajax выборки для полей

        // Обработать языковую метку в заголовке поля, если есть разделитель
        $sFieldTitle = \Yii::tSingleString($oFtField->getTitle());

        // Обработка галерейного поля
        if ($oFtField->getEditorName() == ft\Editor::GALLERY) {

            // Получить id профиля галереи для каталога по умолчанию или выбранный через Сущность профиль в редакторе карточки
            try {
                $iDefProfileId = (int)$oFtField->getOption('link_id') ? : Profile::getDefaultId(Profile::TYPE_CATALOG);
            } catch (\Exception $e) {
                // Если не найден каталожный профиль галереи, то вывести предупреждение
                $this->_module->addError($e->getMessage());
                $iDefProfileId = 0;
            }
            $this->_form->fieldGallery($oFtField->getName(), $sFieldTitle, $iDefProfileId, $aParams);
        }
        // Если у поля есть связь и это поле справочное, то получить список возможных значений для выпадающих списков
        elseif ( ($oRelation = $oFtField->getFirstRelation()) and
                 (in_array($oFtField->getEditorName(), [ft\Editor::SELECT, ft\Editor::MULTISELECT, ft\Editor::COLLECTION])) ) {

            $oModel = ft\Cache::get($oRelation->getEntityName());

            $sValName = $sTitleField = $oRelation->getExternalFieldName();
            if ( $oModel->hasField('title') )
                $sTitleField = 'title';
            elseif ( $oModel->hasField('name') )
                $sTitleField = 'name';

            // Получение значений справочника
            if (in_array($oFtField->getEditorName(), [ft\Editor::SELECT, ft\Editor::MULTISELECT]))
                $aItems = catalog\Dict::getValues($oRelation->getEntityName());
            else
                // Или значений коллекицй
                $aItems = ft\Cache::getMagicTable( $oRelation->getEntityName() )->find()->getAll();

            $aList = [];
            foreach ( $aItems as $oItem )
                $aList[$oItem->$sValName] = $oItem->$sTitleField;

            if ( $oFtField->getEditorName() == ft\Editor::MULTISELECT )
                $this->_form->fieldMultiSelect($oFtField->getName(), $sFieldTitle, $aList, '', $aParams);
            else
                $this->_form->fieldSelect($oFtField->getName(), $sFieldTitle, $aList, $aParams);

        // Остальные поля
        } else
            $this->_form->field($oFtField->getName(), $sFieldTitle, $oFtField->getEditorName(), $aParams);
    }

    /** Добавить поля настройки разделов каталожной позиции */
    private function addSpecialFields() {

        $iItemId = $this->model->getGoodsRow()->getRowId();

        $aCatSection = catalog\Section::getList();
        $aGoodsSection = $this->model->getGoodsRow()->getViewSection();

        // Текущий раздел для нового товара
        if (!$iItemId and !$aGoodsSection)
            $aGoodsSection = array( $this->_module->sectionId() );

        $aGoodsSectionWithTitle = Tree::getSectionsTitle( $aGoodsSection ?: [], false, true );
        $iMainSectionId = $this->model->getGoodsRow()->getMainSection();

        // если главная секция не задана, но есть варианты - подставить первый
        if ( !$iMainSectionId and $aGoodsSectionWithTitle ) {
            $aSections = array_keys($aGoodsSectionWithTitle);
            $iMainSectionId = array_shift($aSections);
        }

        // Основной раздел
        $this->_form->fieldSelect(self::mainLinkField, \Yii::t('catalog', 'main_section'), $aGoodsSectionWithTitle, [
            'groupTitle' => \Yii::t('catalog', 'settings'),
            'value' => $iMainSectionId,
        ], false);

        // Категории
        $this->_form->fieldMultiSelect(self::categoryField, \Yii::t('catalog', 'category'), $aCatSection, $aGoodsSection, [
            'groupTitle' => \Yii::t('catalog', 'settings'),
            'onUpdateAction' => 'loadSection',
        ]);
    }
// Сопутствующие
    private function btnShowRelatedItems() {
        if ( SysVar::get('catalog.goods_related') )
            $this->_form->buttonCustomExt(ext\docked\Api::create(\Yii::t('catalog', 'relatedItems'))
                ->setIconCls(ext\docked\Api::iconEdit)
                ->setState('RelatedItems')
                ->setAction('RelatedItems')
                ->unsetDirtyChecker()
            );
    }
// В комплекте
    private function btnShowIncludedItems() {
        if ( SysVar::get('catalog.goods_include') )
            $this->_form->buttonCustomExt(ext\docked\Api::create(\Yii::t('catalog', 'includedItems'))
                ->setIconCls(ext\docked\Api::iconEdit)
                ->setState('IncludedItems')
                ->setAction('IncludedItems')
                ->unsetDirtyChecker()
            );
    }
// Модификации товаров
    private function btnShowModificationsItems() {
        if ( SysVar::get('catalog.goods_modifications') )
            $this->_form->buttonCustomExt(ext\docked\Api::create(\Yii::t('catalog', 'modificationsItems'))
                ->setIconCls(ext\docked\Api::iconEdit)
                ->setState('ModificationsItems')
                ->setAction('ModificationsItems')
                ->unsetDirtyChecker()
            );
    }
}