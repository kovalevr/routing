<?php

namespace skewer\build\Catalog\Goods\view;

use skewer\build\Catalog\Goods\SeoGoodModifications;
use skewer\components\seo\Api;
use skewer\components\catalog;


/**
 * Построение интерфейса редактирование модификации товара
 * Class ModGoodsEditor
 * @package skewer\build\Catalog\Goods\view
 */
class ModGoodsEditor extends GoodsEditor {

    protected $aUniqFields = array( 'id', 'alias', 'active' );

    public function build() {

        // задание набора полей для интерфейса
        $this->initForm(['active'], $this->aUniqFields, true);

        $aData = $this->model->getData();

        // кнопки
        $this->_form->buttonSave( 'EditModificationsItem' );
        $this->_form->buttonCancel( 'ModificationsItems' );

        if ( $this->model->getGoodsRow()->getRowId() ) {

            $this->_form->buttonSeparator();
            $this->_form->buttonDelete('DeleteModificationsItem');
        }

        $this->_form->useSpecSectionForImages();

        // контент
        $this->_form->setValue( $aData );

        /** @var bool Это общий список товаров(Фильтр = Раздел:Все)? $bIsCommonList */
        $bIsCommonList = !(bool)$this->_module->getCurrentSection();

        $this->_form->getField('gallery')->setDescVal('iEntityId', $this->model->getGoodsRow()->getRowId());
        $this->_form->getField('gallery')->setDescVal('seoClass', SeoGoodModifications::className());
        $this->_form->getField('gallery')->setDescVal('sectionId', $this->_module->getCurrentSection());

        if (!$bIsCommonList){

            $bNewGood = !(bool)$aData['id'];

            if ($bNewGood){
                $aGood = catalog\GoodsSelector::get($this->_module->getInnerData( 'currentGoods' ), catalog\Card::DEF_BASE_CARD);
                $aGood['id'] = 0;
            }else
                $aGood = catalog\GoodsSelector::get($aData['id'], catalog\Card::DEF_BASE_CARD);

            Api::appendExtForm($this->_form, new SeoGoodModifications(0, $this->_module->getCurrentSection(), $aGood) );
        }
        else
            $this->_form->fieldWithValue('warning_text',\Yii::t('SEO','warning'), 'show', \Yii::t('SEO','warning_text'),['groupTitle' => \Yii::t('SEO', 'group_title'), 'groupType'  => 1]);

    }
}