<?php

namespace skewer\build\Tool\TasksManager;

use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\base\queue;

/**
 * Интерфейс для работы с планировщиком задач
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function preExecute(){

    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( \Yii::t('TasksManager', 'tasks') );

        // объект для построения списка
        $oList = StateBuilder::newList();

        $oList
            ->field('id', \Yii::t('TasksManager', 'id'), 'int')
            ->field('global_id', \Yii::t('TasksManager', 'global_id'), 'int')
            ->field('title', \Yii::t('TasksManager', 'title'), 'string', ['listColumns' => ['flex' => 3]])
/*            ->field( 'class', \Yii::t('TasksManager', 'class'), 's', 'string')
            ->field( 'parameters', \Yii::t('TasksManager', 'parameters'), 's', 'string')*/
            ->field('priority', \Yii::t('TasksManager', 'priority'), 'string')
            ->field('resource_use', \Yii::t('TasksManager', 'resource_use'), 'string')
            ->field('upd_time', \Yii::t('TasksManager', 'upd_time'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('status', \Yii::t('TasksManager', 'status'), 'string')
        ;

        $oList->buttonConfirm('clear', \Yii::t('adm', 'clear'), \Yii::t('TasksManager', 'clear_confirm'), 'icon-stop');

        $aItems = Api::getListItems();

        $oList->setValue( $aItems );

        // вывод данных в интерфейс
        $this->setInterface( $oList->getForm() );

    }

    /**
     * Удалеение х задач из списка
     */
    protected function actionClear() {
        queue\Manager::clear();
        $this->actionList();
    }

}
