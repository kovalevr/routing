<?php

namespace skewer\build\Tool\Auth;

use skewer\build\Adm;
use skewer\build\Tool;
use skewer\base\site_module\Context;


/**
 * Проекция редактора баннеров для слайдера в панель управления
 * Class Module
 * @package skewer\build\Adm\Auth
 */
class Module extends Adm\Auth\Module implements Tool\LeftList\ModuleInterface {

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    public function __construct(Context $oContext ) {
        parent::__construct( $oContext );

        $oContext->setModuleWebDir('/skewer/build/Adm/Auth');
        $oContext->setModuleDir(RELEASEPATH.'build/Adm/Auth');

        $oContext->setModuleLayer('Adm');


    }

    function getName() {
        return $this->getModuleName();
    }

}