<?php

namespace skewer\build\Tool\Messages\models;

/**
 * This is the model class for table "messages_read".
 *
 * @property integer $id
 * @property integer $send_id
 */
class MessagesRead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages_read';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['send_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'send_id' => 'Send ID',
        ];
    }
}
