<?php

namespace skewer\build\Tool\HttpsMode;

use skewer\build\Tool;
use skewer\base\ui;
use skewer\base\SysVar;

use skewer\components\seo\Service as ServiceSeo;
use skewer\build\Tool\Domains;

/**
 * Модуль переключения сайта в работу на протоке https
 */
class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Вызывается в случае отсутствия явного обработчика
     * @return int
     */
    protected function actionInit() {

        $oFormBuilder = ui\StateBuilder::newEdit();

        $bActive = (bool)SysVar::get('Redirect.toHttps');

        $oFormBuilder->field('https_status', '', 'show', [
            'hideLabel' => true,
            'fieldStyle' => 'font-size: 14px; float: none; text-align: center;',
        ]);

        $oFormBuilder->setValue([
            'https_status' => ($bActive) ? \Yii::t('HttpsMode', 'redirects_on') : \Yii::t('HttpsMode', 'redirects_off'),
        ]);

        if ($bActive)
            $oFormBuilder->button('stop', \Yii::t('HttpsMode', 'button_off'), 'icon-stop');
        else
            $oFormBuilder->button('start', \Yii::t('HttpsMode', 'button_on'), 'icon-visible');

        $this->setInterface($oFormBuilder->getForm());
    }

    /** Отключить статистику */
    protected function actionStop() {

        SysVar::set('Redirect.toHttps', 0);
        $this->updateExportFiles();

        $this->actionInit();
    }

    /** Включить статистику */
    protected function actionStart() {

        SysVar::set('Redirect.toHttps', 1);
        $this->updateExportFiles();

        $this->actionInit();
    }

    /** Обновить файлы, содержащие ссылки на ресурсы сайта */
    private function updateExportFiles() {

        // Обновить карту xml-сайта
        ServiceSeo::makeSiteMap();

        // Обновить файл robots.txt
        ServiceSeo::updateRobotsTxt(Domains\Api::getMainDomain());
    }
}