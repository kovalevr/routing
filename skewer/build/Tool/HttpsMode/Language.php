<?php

$aLanguage = array();

$aLanguage ['ru']['HttpsMode.Tool.tab_name'] = 'HTTPS соединение';

$aLanguage ['ru']['button_on']     = 'Включить редиректы на HTTPS';
$aLanguage ['ru']['button_off']    = 'Выключить редиректы на HTTPS';
$aLanguage ['ru']['redirects_on']  = 'Редиректы на https включены';
$aLanguage ['ru']['redirects_off'] = 'Редиректы на https выключены';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage ['en']['HttpsMode.Tool.tab_name'] = 'HTTPS mode';

$aLanguage ['en']['button_on']     = 'Disable redirects to HTTPS';
$aLanguage ['en']['button_off']    = 'Enable redirects to HTTPS';
$aLanguage ['en']['redirects_on']  = 'Redirects to HTTPS enabled';
$aLanguage ['en']['redirects_off'] = 'Redirects to HTTPS disabled';

return $aLanguage;