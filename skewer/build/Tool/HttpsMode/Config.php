<?php

use skewer\build\Tool\LeftList;
use skewer\base\site\Layer;

$aConfig['name']         = 'HttpsMode';
$aConfig['title']        = 'HTTPS соединение';
$aConfig['version']      = '1.000';
$aConfig['description']  = 'Режим работы сайта через протокол https';
$aConfig['revision']     = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']        = Layer::TOOL;
$aConfig['group']        = LeftList\Group::SYSTEM;

return $aConfig;
