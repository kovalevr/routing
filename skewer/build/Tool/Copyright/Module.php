<?php

namespace skewer\build\Tool\Copyright;

use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\components\i18n\models\LanguageValues;


/**
 * Модуль антикопипаста
 * Class Module
 * @package skewer\build\Tool\Copyright
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {
        $this->stateEditModule();
    }


    /** Состояние: редактирование модуля */
    protected function stateEditModule(){

        $oForm = StateBuilder::newEdit();
        $oForm
            ->fieldCheck('activity', \Yii::t($this->languageCategory,'field_activity'))
            ->fieldMultiSelect('disabledSection', \Yii::t($this->languageCategory,'field_disable_in'),Api::getAllSections(),Api::getSectionsWithDisabledCopyrightModule() )
            ->fieldWysiwyg('text', \Yii::t($this->languageCategory, 'field_text'))
        ;

        $oForm->setValue(
            [
                'activity' => Api::getActivityModule(),
                'text'     => \Yii::t($this->languageCategory, 'templateText')
            ]
        );

        $oForm->buttonSave('save');

        $this->setInterface($oForm->getForm());
    }



    /** Действие: cохранение настройки модуля */
    protected function actionSave(){

        $aData = $this->get('data');

        Api::setActivityModule($aData['activity']);
        Api::setSectionsWithDisabledCopyrightModule($aData['disabledSection']);

        if (!$oLabel = LanguageValues::findOne(['message' => 'templateText', 'language'=> \Yii::$app->i18n->getTranslateLanguage()]))
            $oLabel = new LanguageValues();

        $oLabel->message = 'templateText';
        $oLabel->value = $aData['text'];
        $oLabel->language = \Yii::$app->i18n->getTranslateLanguage();
        $oLabel->category = $this->languageCategory;
        $oLabel->status = LanguageValues::statusTranslated;
        $oLabel->override = LanguageValues::overrideYes;
        $oLabel->data = 0;
        $oLabel->save();


        $this->actionInit();
    }

}