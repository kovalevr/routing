<?php

namespace skewer\build\Tool\Import;


use skewer\components\auth\models\GroupPolicy;
use skewer\components\import\ar\ImportTemplate;
use skewer\components\import\ar\Log;
use skewer\base\section\Visible;
use skewer\base\section\Tree;

use skewer\base\site\Type;
use skewer\components\auth\Policy;
use skewer\components\config\InstallPrototype;
use skewer\base\section\models\TreeSection;

/**
 * Class Install
 * @package skewer\build\Tool\Import
 */
class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {

        if (!Type::hasCatalogModule())
            $this->fail('Нельзя установить импорт на некаталожный сайт!');

        Log::rebuildTable();
        ImportTemplate::rebuildTable();

        $iSectionId = Tree::getSectionByAlias( 'Tool_Import', \Yii::$app->sections->library() );
        if (!$iSectionId){

            $oSection = new TreeSection();
            $oSection->parent = \Yii::$app->sections->library();
            $oSection->alias = 'Tool_Import';
            $oSection->title = 'Импорт';
            $oSection->visible = Visible::VISIBLE;
            $oSection->type = 1;
            $oSection->save();

            $iSectionId = $oSection->id;

        }

        if (!$iSectionId)
            $this->fail('Не удалось создать папку для модуля!');

        $aPolicyAdmin = GroupPolicy::find()->where(['alias' => 'admin'])->asArray()->all();

        if ($aPolicyAdmin)
            foreach ($aPolicyAdmin as $aPolicy)
                Policy::addModule( $aPolicy['id'], 'Import', 'Импорт и обновление товаров');
        // todo добавить перевод из словаря

    }// func

    public function uninstall() {
        return true;
    }// func

}//class