<?php

namespace skewer\build\Tool\Import;


use skewer\components\import\Api;
use skewer\components\import\ar\ImportTemplate;
use skewer\components\import\Config;
use skewer\components\import\Exception;
use skewer\components\import\Task;
use skewer\base\ui;
use skewer\build\Tool;
use skewer\components\auth\CurrentAdmin;
use skewer\base\queue\ar\Schedule;
use yii\base\UserException;

/**
 * Модуль настройки шаблонов для импорта данных в каталога, запуска импорта и чтения статистики
 * Class Module
 */
class Module extends Tool\LeftList\ModulePrototype {

    /** @var bool Флаг того, что админ sys */
    protected $isSys = false;

    protected function actionInit(){

        $this->isSys = $this->isSys();
        $this->actionList();
    }

    /**
     * id текущего шаблона
     * @return int
     */
    private function getTplId(){

        $iTpl = $this->getInDataValInt( 'id' );
        if (!$iTpl){
            $iTpl = $this->getInnerDataInt( 'tpl_id' );
        }

        $this->setInnerData( 'tpl_id', $iTpl );

        return $iTpl;
    }

    /**
     * Список шаблонов импорта
     */
    protected function actionList(){

        $this->setPanelName(\Yii::t('import', 'tpl_list'));

        $oList = ui\StateBuilder::newList();

        $oList
            ->field('id', 'id', $this->isSys ? 'string' : 'hide', ['listColumns' => ['flex' => 1]])
            ->field('title', \Yii::t('import', 'field_title'), 'string', ['listColumns' => ['flex' => 3]])
            ->buttonRow('runImport', \Yii::t('import', 'run_import'), 'icon-reload')
            ->buttonRowUpdate('headSettings')
        ;
        if ($this->isSys){
            $oList
                ->buttonRowDelete('delete')
                ->buttonAddNew('add')
            ;
        }

        $aList = Api::getTemplateList();

        $oList->setValue( $aList );

        if (!is_dir(ROOTPATH.'import/')){
            $oList->buttonSeparator('->');
            $oList->buttonAddNew('addFolder', \Yii::t('import', 'addUploadFolder'));
        }

        $this->setInterface($oList->getForm());

    }

    /**
     * Добавление нового шаблона
     */
    protected function actionAdd(){

        $this->showHeadSettingsForm();

    }

    /**
     * Основные настройки
     * @param null|int $iTpl
     */
    protected function actionHeadSettings( $iTpl = null ){

        if ( !$iTpl )
            $iTpl = $this->getTplId();

        if ($this->isSys)
            $this->showHeadSettingsForm( $iTpl );
        else
            $this->showClientForm( $iTpl );

    }


    /**
     * Форма основных настроек
     * @param null|int $id id шаблона
     */
    private function showHeadSettingsForm( $id = null ){

        $this->setPanelName(\Yii::t('import', 'head_settings_form'));

        $oTemplate = Api::getTemplate( $id );

        $oForm = ui\StateBuilder::newEdit();

        $aData = $oTemplate->getData();

        if ( $oTemplate->type == Api::Type_File ){
            $aData['source_file'] = $aData['source'];
        }else{
            $aData['source_str'] = $aData['source'];
        }

        $sGroup = \Yii::t('import', 'head_settings_form');

        $oForm
            ->field('id', 'ID', 'hide', ['groupTitle' => $sGroup])
            ->field('title', \Yii::t('import', 'field_title'), 'string', ['groupTitle' => $sGroup])
            ->fieldSelect( 'card', \Yii::t('import', 'field_card'), Api::getCardList(), ['groupTitle' => $sGroup], false )

            ->fieldSelect( 'provider_type', \Yii::t('import', 'field_provider_type'), Api::getProviderTypeList(), ['groupTitle' => $sGroup], false )

            ->fieldSelect( 'type', \Yii::t( 'import', 'field_type' ), Api::getTypeList(), ['customField' => 'ImportType', 'groupTitle' => $sGroup], false )

            ->field('source_file', \Yii::t('import', 'field_source'), 'file', ['groupTitle' => $sGroup])
            ->field('source_str', \Yii::t('import', 'field_source'), 'string', ['groupTitle' => $sGroup])

            ->fieldSelect( 'coding', \Yii::t( 'import', 'field_coding' ), Api::getCodingList(), ['groupTitle' => $sGroup], false );
        ;

        $oForm->addLib('ImportType');

        $oForm->setValue( $aData );

        if ( $id ){
            $oForm->buttonSave( 'save' );
            $this->addButton( $oForm, 'headSettings' );
        } else{

            $oForm
                ->buttonSave( 'save' )
                ->buttonCancel( 'list' )
            ;
        }

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Форма основных настроек для клиента
     * @param null|int $id id шаблона
     */
    private function showClientForm( $id = null ){

        $this->setPanelName(\Yii::t('import', 'head_settings_form'));

        $oTemplate = Api::getTemplate( $id );

        $oForm = ui\StateBuilder::newEdit();

        $aData = $oTemplate->getData();

        if ( $oTemplate->type == Api::Type_File ){
            $aData['source_file'] = $aData['source'];
        }else{
            $aData['source_str'] = $aData['source'];
        }

        $sGroup = \Yii::t('import', 'head_settings_form');

        $oForm
            ->field('id', 'ID', 'hide', ['groupTitle' => $sGroup])
            ->field('title', \Yii::t('import', 'field_title'), 'show', ['groupTitle' => $sGroup])
        ;

        if ( $oTemplate->type == Api::Type_File ){
            $oForm->field('source_file', \Yii::t('import', 'field_source'), 'file', ['groupTitle' => $sGroup]);
        }else{
            $oForm->field('source_str', \Yii::t('import', 'field_source'), 'string', ['groupTitle' => $sGroup]);
        }

        $oForm->setValue( $aData );

        $oForm->buttonSave( 'save' )
            ->buttonSeparator()
            ->button('runImport', \Yii::t('import', 'run_import'), 'icon-reload')
            ->buttonSeparator()
            ->button('logList', \Yii::t('import', 'log_list'), 'icon-page')
            ->buttonSeparator()
            ->buttonCancel( 'list' );

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Настройки провайдера
     */
    protected function actionProviderSettings(){
        $iTpl = $this->getTplId();
        $this->showProviderSettingsForm( $iTpl );
    }


    private function showProviderSettingsForm( $iTpl ){

        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'provider_settings_form'));

        $oForm = ui\StateBuilder::newEdit();

        /** Добавим поля настроек по провайдеру */
        View::getProviderForm( $oForm, $oTemplate );

        $oForm->buttonSave( 'saveProviderSettings' );
        $this->addButton( $oForm, 'providerSettings' );

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Настройки соответствия полей
     */
    protected function actionFields(){
        $iTpl = $this->getTplId();
        $this->showFieldsForm( $iTpl );
    }

    /**
     * Форма настроек соответствия полей
     * @param $iTpl
     * @throws UserException
     * @throws UserException
     */
    private function showFieldsForm( $iTpl ){

        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'fields_form'));

        $oForm = ui\StateBuilder::newEdit();

        /** Добавим поля настроек по провайдеру */
        View::getFieldsForm( $oForm, $oTemplate );

        // #39953 Поиск отсутствующих полей импорта в карточке и выдача сообщения
        if ($sMessages = Api::checkImportFields($iTpl))
            $this->addMessage(\Yii::t('Import', 'warning'), $sMessages, 5000);

        $oForm->buttonSave( 'saveFields' );
        $this->addButton( $oForm, 'fields' );

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Форма настройки полей
     */
    protected function actionFieldsSettings(){

        $iTpl = $this->getTplId();
        $this->showFieldSettingsForm( $iTpl );

    }


    private function showFieldSettingsForm( $iTpl = null ){

        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'fields_settings_form'));

        $oForm = ui\StateBuilder::newEdit();

        /** Добавим поля настроек по провайдеру */
        try{
            View::getFieldsSettingsForm( $oForm, $oTemplate );
        } catch (Exception $e){
            throw new UserException( $e->getMessage(), $e->getCode(), $e );
        }

        $oForm->buttonSave( 'saveSettingsFields' );
        $this->addButton( $oForm, 'fieldsSettings' );

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );

    }


    /**
     * Сохранение
     */
    protected function actionSave(){

        $aData = $this->getInData();

        $oTpl = Api::getTemplate( (isset($aData['id'])) ? $aData['id'] : null );

        if ( $this->isSys ) {
            $aRequiredList = ImportTemplate::getModel()->getColumnSet('required');
            foreach ($aRequiredList as $sFieldName) {
                if (!isset($aData[$sFieldName]) || !$aData[$sFieldName])
                    throw new UserException(\Yii::t('import', 'not_defined_field', \Yii::t('import', 'field_' . $sFieldName)));
            }
        }

        $sOldType = $oTpl->provider_type;
        $sOldCard = $oTpl->card;
        $oTpl->setData($aData);

        if ($oTpl->type == Api::Type_File){
            $oTpl->source = (isset($aData['source_file'])) ? $aData['source_file'] : '';
        }else{
            $oTpl->source = (isset($aData['source_str'])) ? $aData['source_str'] : '';
        }

        if (!$oTpl->source){
            throw new UserException( \Yii::t('import', 'not_defined_field', \Yii::t( 'import', 'field_source' )) );
        }

        $id = $oTpl->save();
        if ($id){
            /** Сменился провайдер или карточка - почистим конфиг */
            if ($sOldType != $oTpl->provider_type || $sOldCard != $oTpl->card){
                $oConfig = new Config( $oTpl );
                $oConfig->clearFields();
                $oTpl->settings = json_encode( $oConfig->getData() );
                $oTpl->save();
            }
            $this->actionHeadSettings( $id );
        }else{
            throw new UserException( \Yii::t('import', 'error_no_save') );
        }

    }

    /**
     * Сохранение настроек провайдера
     */
    protected function actionSaveProviderSettings(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oProvider = Api::getProvider( $oConfig );

        $aVars = $oConfig->getData();

        /** Параметры провайдера */
        foreach ($oProvider->getParameters() as $key => $value){
            $aVars[$key] = (isset($aData[$key])) ? $aData[$key] : ((isset($value['default'])) ? $value['default'] : '');
        }

        $oTpl->settings = json_encode( $aVars );
        $oTpl->save();
    }


    /**
     * Сохранение настроек соответсвия полей
     */
    protected function actionSaveFields(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oConfig->setFields( $aData, true );

        $oTpl->settings = $oConfig->getJsonData();

        $oTpl->save();

        /** Проверка на наличие уникального поля */
        $bUnique = false;
        foreach($aData as $sKey => $value){
            if (preg_match( '/type_(\w+)/', $sKey )){
                if ($value === 'Unique'){
                    $bUnique = true;
                    break;
                }
            }
        }

        if (!$bUnique)
            throw new UserException(\Yii::t('import', 'error_unique_field_not_found'));

    }


    /**
     * Сохранение настроек полей
     */
    protected function actionSaveSettingsFields(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oConfig->setFieldsParam( $aData );

        $oTpl->settings = $oConfig->getJsonData();
        $oTpl->save();
    }


    /**
     * Удаление шаблона
     */
    protected function actionDelete(){

        $id = $this->getInDataValInt( 'id' );

        if ($id){
            ImportTemplate::delete( $id );
            Api::deleteLog4Template( $id );
        }

        $this->actionList();

    }

    /**
     * Задача
     */
    protected function actionShowTask(){

        $this->setPanelName(\Yii::t('import', 'task_form'));

        $oForm = ui\StateBuilder::newEdit();

        $command = json_encode([
            'class' => '\skewer\components\import\Task',
            'parameters' => ['tpl' => $this->getTplId()]
        ]);

        $aData = [];
        /** @var Schedule $task */
        $task = Schedule::findOne(['command'=>$command]);

        if ($task){

            foreach($task as $key=>$val) $aData[$key]=$val;

            $aData['active'] = 1;
            $aData['schedule_id'] = $task->id;
        }

        $oForm
            ->field('schedule_id', 'schedule_id', 'hide')
            ->field('active', \Yii::t('import', 'active_task'), 'check')

            ->field('c_min', \Yii::t('schedule', 'c_min'), 'int', ['minValue' => 0, 'maxValue' => 59, 'allowDecimals' => false])
            ->field('c_hour', \Yii::t('schedule', 'c_hour'), 'int', ['minValue' => 0, 'maxValue' => 23, 'allowDecimals' => false])
            ->field('c_day', \Yii::t('schedule', 'c_day'), 'int', ['minValue' => 1, 'maxValue' => 31, 'allowDecimals' => false])
            ->field('c_month', \Yii::t('schedule', 'c_month'), 'int', ['minValue' => 1, 'maxValue' => 12, 'allowDecimals' => false])
            ->field('c_dow', \Yii::t('schedule', 'c_dow'), 'int', ['minValue' => 1, 'maxValue' => 7, 'allowDecimals' => false])

            ->setValue( $aData )

            ->buttonSave('saveTask')
            ->buttonCancel('headSettings')
        ;

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Сохранение задачи
     */
    protected function actionSaveTask(){

        $aData = $this->getInData();

        $command = json_encode([
            'class' => '\skewer\components\import\Task',
            'parameters' => ['tpl' => (int)$this->getTplId()]
        ]);

        if ($aData['active']){

            /** @var \skewer\components\import\ar\ImportTemplateRow $oTpl */
            $oTpl = ImportTemplate::find($this->getTplId());
            if (!$oTpl)
                throw new \Exception(\Yii::t('import', 'error_tpl_not_fount'));

            //save
            $aData['id'] = $aData['schedule_id'];
            $aData['command'] = $command;
            $aData['name'] ='import_' . $oTpl->id;
            $aData['title'] = \Yii::t( 'import', 'task_title', $oTpl->title);
            $aData['priority'] = Task::priorityHigh;
            $aData['resource_use'] = Task::weightHigh;
            $aData['target_area'] = 3;
            $aData['status'] = Task::stNew;

            if( !$scheduleItem = Schedule::findOne($aData['id']) ){
                $scheduleItem = new Schedule;
                unset($aData['id']);
            }

            $scheduleItem->setAttributes($aData);

            if ( !$scheduleItem->save() )
                throw new ui\ARSaveException( $scheduleItem );

        } else {

            //remove
            Schedule::deleteAll(['id'=>$aData['schedule_id']]);

        }

        $this->actionHeadSettings();
    }

    /**
     * Запуск импорта
     */
    protected function actionRunImport(){

        $aData = $this->getInData();
        if (empty($aData) && $this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $taskId = (isset($aData['taskId'])) ? $aData['taskId'] : 0;

        $iTpl = (isset($aData['id'])) ? $aData['id'] : 0;
        if (!$iTpl){
            $iTpl = $this->getInnerDataInt( 'tpl_id' );
        }

        if (!$iTpl && !$taskId){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        if ($iTpl){
            $this->setInnerData( 'tpl_id', $iTpl );
            $this->setInnerData( 'importRun', $iTpl );
        }

        /** Запуск импорта */
        $aRes = Api::runImport( $taskId, $iTpl );

        if ( in_array($aRes['status'], [Task::stFrozen, Task::stWait]) ){
            /** Крутим, ели еще нужно */
            $this->addJSListener( 'runImport', 'runImport' );
            $this->fireJSEvent( 'runImport', ['taskId' => $aRes['id']] );

        } elseif ($oTemplate = Api::getTemplate($iTpl)) {

            // #39953 Поиск отсутствующих полей импорта в карточке и выдача сообщения в конце импорта
            if ($sMessages = Api::checkImportFields($iTpl))
                $this->addMessage(\Yii::t('Import', 'warning'), $sMessages, 5000);
        }

        $this->showLog( $aRes['id'] );
    }

    /**
     * Список логов
     */
    protected function actionLogList(){

        $this->setPanelName(\Yii::t('import', 'logs_list'));

        $iTpl = $this->getInnerDataInt( 'tpl_id' );
        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oList = ui\StateBuilder::newList();

        $oList
            ->field('id_log', 'ID', 'hide')
            ->field('start', \Yii::t('import', 'log_start'), 'string', ['listColumns' => ['flex' => 3]])
            ->field('status', \Yii::t('import', 'log_status'), 'string', ['listColumns' => ['flex' => 1]])
        ;

        $oList->widget( 'status', __NAMESPACE__.'\View', 'getStatus' );

        $oList->setValue( Api::getLogs( $iTpl ) );

        $oList
            ->buttonRow('detailLog', \Yii::t('import', 'log_detail'), 'icon-page', 'edit_form')
            ->buttonRowDelete( 'deleteLog' )
            ->buttonCancel('headSettings', \Yii::t('import', 'back'))
        ;

        $this->setInnerData('importRun', false);

        $this->setInterface( $oList->getForm() );

    }

    /**
     * Детальная лога
     */
    protected function actionDetailLog(){

        $iTpl = $this->getInDataValInt( 'id_log' );

        $this->showLog( $iTpl );

    }


    private function showLog( $id ){
        if (!$id){
            throw new UserException( \Yii::t('import', 'error_task_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'log_show'));

        $log = Api::getLog( $id );

        if (isset($log['status'])){
            $log['status'] = View::getStatus($log);
        }

        $oForm = ui\StateBuilder::newEdit();
        $oForm->field('result', \Yii::t('import', 'log_result'), 'show', ['labelAlign' => 'top', 'height' => '100%']);

        $baskAction = $this->getInnerData('importRun')?'headSettings':'logList';
        $oForm->buttonBack($baskAction, \Yii::t('import', 'back'));

        $sText = $this->renderTemplate('log_template.twig', ['log' => $log]);
        $oForm->setValue(['result' => $sText]);

        $this->setInterface($oForm->getForm());
    }


    /**
     * Удаление записи лога
     */
    protected function actionDeleteLog(){

        $id = $this->getInDataValInt('id_log');
        if (!$id){
            throw new UserException( \Yii::t('import', 'error_task_not_fount') );
        }

        Api::deleteLog( $id );

        $this->actionLogList();
    }


    /**
     * Добавление папки импорта
     */
    protected function actionAddFolder(){

        if (mkdir(ROOTPATH.'import')){
            /**
             * @todo вот тут должно быть 755, но на наших sk* это не покатит, потом надо исправить
             */
            chmod(ROOTPATH.'import', 0777);
            $this->addMessage(\Yii::t('import', 'folderCreateHeader'), \Yii::t('import', 'folderCreate'));
        }else{
            $this->addMessage(\Yii::t('import', 'folderCreateHeader'), \Yii::t('import', 'folderNonCreate'));
        }

        $this->actionInit();

    }


    /**
     * Определяет, является ли текущий админ sys-ом
     * @return bool
     */
    protected function isSys(){

        $aUserData = CurrentAdmin::getUserData();
        if (!isset($aUserData['login']))
            return false;

        return $aUserData['login'] == 'sys';

    }


    /**
     * Докидывание кнопок на формы
     * @param ui\builder\FormBuilder $oForm
     * @param $state - текущее состояние
     */
    private function addButton(ui\builder\FormBuilder &$oForm, $state ){

        $oForm
            ->buttonSeparator()
            ->button('runImport', \Yii::t('import', 'run_import'), 'icon-reload')
            ->buttonSeparator()
            ->button('headSettings', \Yii::t('import', 'head_settings_form'), 'icon-edit', 'init', ['disabled' => ($state == 'headSettings')])
            ->button('providerSettings', \Yii::t('import', 'provider_settings_form'), 'icon-edit', 'init', ['disabled' => ($state == 'providerSettings')])
            ->button('fields', \Yii::t('import', 'fields_form'), 'icon-edit', 'init', ['disabled' => ($state == 'fields')])
            ->button('fieldsSettings', \Yii::t('import', 'fields_settings_form'), 'icon-edit', 'init', ['disabled' => ($state == 'fieldsSettings')])
            ->buttonSeparator()
            ->button('showTask', \Yii::t('import', 'task_form'), 'icon-configuration')
            ->button('logList', \Yii::t('import', 'log_list'), 'icon-page')
            ->buttonSeparator()
            ->buttonCancel( 'list' );
    }
}