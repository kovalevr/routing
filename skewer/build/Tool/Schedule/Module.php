<?php

namespace skewer\build\Tool\Schedule;

use skewer\base\queue\Api as QueueApi;
use skewer\base\queue\Task;
use skewer\base\ui\ARSaveException;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\base\queue\ar\Schedule;

/**
 * Интерфейс для работы с планировщиком задач
 */
class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( \Yii::t('schedule', 'taskList') );

        // создаем форму
        $oFormBuilder = StateBuilder::newList();

        $oFormBuilder
            ->field('id', 'ID', 'string', ['listColumns' => ['flex' => 1]])

            ->field('title',    \Yii::t('schedule', 'title'), 'string', ['listColumns' => ['flex' => 3]])

            ->field('c_min',    \Yii::t('schedule', 'c_min'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('c_hour',   \Yii::t('schedule', 'c_hour'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('c_day',    \Yii::t('schedule', 'c_day'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('c_month',  \Yii::t('schedule', 'c_month'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('c_dow',    \Yii::t('schedule', 'c_dow'), 'string', ['listColumns' => ['flex' => 2]])

            ->field('priority',     \Yii::t('schedule', 'priority'), 'string', ['listColumns' => ['flex' => 2]])
            ->field('resource_use', \Yii::t('schedule', 'resource_use'), 'string', ['listColumns' => ['flex' => 2]])
            ->field('target_area',  \Yii::t('schedule', 'target_area'), 'string', ['listColumns' => ['flex' => 2]])
            ->field('status',   \Yii::t('schedule', 'status'), 'string', ['listColumns' => ['flex' => 2]])

            ->buttonRowUpdate()

            ->buttonAddNew('show')
        ;

        $items = Schedule::find()
            ->orderBy('title')
            ->asArray()
            ->all();

        foreach($items as &$item){ // декорируем значения подписями

            foreach(['c_min','c_hour','c_day','c_month','c_dow'] as $key)
                if (is_null($item[$key]))
                    $item[$key] = '*';

            $item['status'] = \Yii::t('schedule', Api::getStatusArray()[$item['status']]);
            $item['priority'] = \Yii::t('schedule', Api::getPriorityArray()[$item['priority']]);
            $item['target_area'] = \Yii::t('schedule', Api::getTargetArray()[$item['target_area']]);
            $item['resource_use'] = \Yii::t('schedule', Api::getResourceArray()[$item['resource_use']]);

        }

        $oFormBuilder->setValue($items);

        $this->setInterface($oFormBuilder->getForm());

    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // взять id ( 0 - добавление, иначе сохранение )
        $iItemId = (int)$this->getInDataVal('id');

        // заголовок
        $this->setPanelName( $iItemId ? \Yii::t('schedule', 'edit') : \Yii::t('schedule', 'add') );

        // элемент
        if($iItemId) {
            $aItem = Schedule::findOne(['id' => $iItemId]);
            // если нет требуемой записи
            if (!$aItem) throw new \Exception(\Yii::t('schedule', 'selectError'));
        }
        else {
            $aItem = new Schedule;
            $aItem->setDefaultValues();
        }

        $oForm = StateBuilder::newEdit()

            ->fieldHide('id', 'id')
            ->fieldString('title',  \Yii::t('schedule', 'title'))
            ->fieldString('name',   \Yii::t('schedule', 'name'))
            ->fieldString('command',\Yii::t('schedule', 'command'))
            ->fieldSelect('priority',  \Yii::t('schedule','priority'), Api::getPriorityArray(), [], false)
            ->fieldSelect('resource_use', \Yii::t('schedule','resource_use'), Api::getResourceArray(), [], false)
            ->fieldSelect('target_area', \Yii::t('schedule','target_area'), Api::getTargetArray(), [], false)
            ->fieldSelect('status', \Yii::t('schedule','status'), Api::getStatusArray(), [], false)
            ->field('c_min',     \Yii::t('schedule','c_min'), 'string')
            ->field('c_hour',    \Yii::t('schedule','c_hour'), 'string' )
            ->field('c_day',     \Yii::t('schedule','c_day'))
            ->field('c_month',   \Yii::t('schedule','c_month'))
            ->field('c_dow',     \Yii::t('schedule','c_dow'))

            ->buttonSave()
            ->buttonCancel();

        $oForm->buttonConfirm('tryTask',\Yii::t('schedule','try_task'),\Yii::t('schedule','try_task_confirm'));

        if($iItemId){
            $oForm
                ->buttonSeparator('->')
                ->buttonDelete();
        }

        $oForm->setValue($aItem);
        $this->setInterface($oForm->getForm());

        return;

    }

    /**
     * Принудительно запускает указанную задачу
     */
    protected function actionTryTask(){

        $aData = $this->get( 'data' );

        // есть данные
        if ( $aData ){

            // сохранить
            $scheduleItem = new Schedule;
            unset($aData['id']);

            $scheduleItem->setAttributes($aData);

            $aData['c_min']   = Api::validateVal($aData['c_min'],\Yii::t('schedule','c_min'),59);
            $aData['c_hour']  = Api::validateVal($aData['c_hour'],\Yii::t('schedule','c_hour'),23);
            $aData['c_dow']   = Api::validateVal($aData['c_dow'],\Yii::t('schedule','c_dow'),7,true);
            $aData['c_day']   = Api::validateVal($aData['c_day'],\Yii::t('schedule','c_day'),12);
            $aData['c_month'] = Api::validateVal($aData['c_month'],\Yii::t('schedule','c_month'),31);

            if ( !$scheduleItem->save(false) )
                throw new ARSaveException( $scheduleItem );

            $iTmpId = $scheduleItem->getAttribute('id');

            $aTaskData = [
                'title' => $scheduleItem->getAttribute('title'),
                'priority' => $scheduleItem->getAttribute('priority'),
                'resource_use' => $scheduleItem->getAttribute('resource_use'),
                'target_area' => $scheduleItem->getAttribute('target_area')
            ];

            $command = json_decode($scheduleItem->getAttribute('command'), true);

            //Если указан метод - запустим универсальную задачу на выполнение одного метода
            if (isset($command['method'])){
                $aTaskData['class'] = '\skewer\base\queue\MethodTask';
                $aTaskData['parameters'] = [
                    'parameters' => $command['parameters'],
                    'class' => $command['class'],
                    'method' => $command['method']
                ];
            }else{
                $aTaskData['class'] = $command['class'];
                $aTaskData['parameters'] = $command['parameters'];
            }

            $iTaskId = QueueApi::addTask(
                            $aTaskData
                        );

            //Вручную дернем cron
            file_get_contents(WEBPROTOCOL.WEBROOTPATH.'cron');

            $aTask = \skewer\base\queue\ar\Task::find()
                ->where('id',$iTaskId)
                ->asArray()
                ->getOne();

            if ($aTask['status']==Task::stClose){
                $this->addMessage(\Yii::t('schedule','success_execute'),\Yii::t('schedule','task_executed'));
            } else {
                $this->addError(\Yii::t('schedule','error_execute'),\Yii::t('schedule','task_executed_error'));
            }

            /*Удалим временную задачу*/
            Schedule::deleteAll(['id'=>$iTmpId]);

        }
    }


    /**
     * Сохранение
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // есть данные
        if ( $aData ){

            $aData['c_min']   = Api::validateVal($aData['c_min'],\Yii::t('schedule','c_min'),59);
            $aData['c_hour']  = Api::validateVal($aData['c_hour'],\Yii::t('schedule','c_hour'),23);
            $aData['c_dow']   = Api::validateVal($aData['c_dow'],\Yii::t('schedule','c_dow'),7,true);
            $aData['c_day']   = Api::validateVal($aData['c_day'],\Yii::t('schedule','c_day'),12);
            $aData['c_month'] = Api::validateVal($aData['c_month'],\Yii::t('schedule','c_month'),31);
            // сохранить

            if(($aData['id']==0) or (!$scheduleItem = Schedule::findOne($aData['id']))){
                $scheduleItem = new Schedule;
                unset($aData['id']);
            }

            $scheduleItem->setAttributes($aData);

            // сохранение с валидацией
            if ( !$scheduleItem->save(false) )
                throw new ARSaveException( $scheduleItem );

            // добавление записи в лог
            if ( isset($aData['id']) && $aData['id'] ) {
                $this->addMessage(\Yii::t('schedule', 'updateOk'));
                $this->addModuleNoticeReport(\Yii::t('schedule', 'editTabName'),$aData);
            } else {
                unset($aData['id']);
                $this->addMessage(\Yii::t('schedule', 'addOk'));
                $this->addModuleNoticeReport(\Yii::t('schedule', 'addTabName'),$aData);
            }
        }

        // вывод списка
        $this->actionInit();

    }


    /**
     * Удаление
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        Schedule::deleteAll(['id'=>$iItemId]);


        // добавление записи в лог
        $this->addMessage('Запись удалена');
        $this->addModuleNoticeReport("Удаление записи планировшика",$aData);

        // вывод списка
        $this->actionInit();

    }

}
