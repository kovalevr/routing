<?php

namespace skewer\build\Tool\Redirect301;

use skewer\base\ui\Api;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\components\redirect\models\Redirect;
use yii\base\Exception;
use yii\base\UserException;

class Module extends Tool\LeftList\ModulePrototype {

    protected function preExecute() {

    }

    protected function actionInit() {

        $this->actionList();

    }

    /**
     * Список редиректов
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( \Yii::t('redirect301', 'urlList') );

        $oList = StateBuilder::newList(); // объект для построения списка
        $oList
            ->field('old_url', \Yii::t('redirect301', 'old_url'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('new_url', \Yii::t('redirect301', 'new_url'), 'string', ['listColumns' => ['flex' => 1]])
            ->buttonRowUpdate('editForm')
            ->buttonRowDelete()
            ->buttonAddNew('addForm')
            ->button('testAll',\Yii::t('redirect301','test'))

            ->setValue(
                Redirect::find()->orderBy('priority')->asArray()->all()
            )
            ->enableDragAndDrop('sortRedirects')
        ;

        $this->setInterface($oList->getForm());

    }

    /**
     * Сортировка редиректов
     */
    protected function actionSortRedirects() {

        $aItemDrop   = $this->get('data');
        $aItemTarget = $this->get('dropData');
        $sOrderType  = $this->get('position');

        if ($aItemDrop and $aItemTarget and $sOrderType)
            Api::sortObjects($aItemDrop['id'],$aItemTarget['id'],new Redirect(),$sOrderType);

        $this->actionList();

    }

    /**
     * Показ формы добавления
     * @param array $aData
     */
    public function actionAddForm($aData = []){

        $this->setPanelName(\Yii::t('redirect301', 'newRedirect'));

        $oForm = StateBuilder::newEdit();
        $oForm
            ->fieldHide('id')
            ->fieldString('old_url', \Yii::t('redirect301', 'old_url'))
            ->fieldString('new_url', \Yii::t('redirect301', 'new_url'))
            ->fieldText('input_url', \Yii::t('redirect301', 'input_url'), 300)
            ->fieldShow('test_results', \Yii::t('redirect301', 'test_results'));

        if ((isset($aData['id'])) and ($aData['id']))
            $oForm->fieldHide('id')->buttonSave('update');
        else
            $oForm->buttonSave('add');

        $oForm->button('test',\Yii::t('redirect301','test'),'','init',['unsetFormDirtyBlocker'=>true])
            ->buttonCancel('list');

        if (!empty($aData))
            $oForm->setValue($aData);
        else
            $oForm->setValue(new Redirect());

        $this->setInterface($oForm->getForm());

    }

    /**
     * Добавление редиректа
     */
    public function actionAdd(){

        $aData = $this->get('data');

        unset($aData['input_url']);

        if ((substr($aData['old_url'],-1)!=='/') and (substr($aData['old_url'],-1)!=='$'))
            $aData['old_url'] = $aData['old_url'].'/';

        $aData['old_url'] = str_replace('http://','',$aData['old_url']);
        $aData['old_url'] = str_replace('https://','',$aData['old_url']);
        $aData['old_url'] = str_replace($_SERVER['HTTP_HOST'],'',$aData['old_url']);

        try {
            \skewer\components\redirect\Api::checkRule($aData['old_url'],$aData['new_url'],'/test');
            $redirect301 = new Redirect();

            $redirect301->setAttributes($aData);
            if ( !$redirect301->save(false) )
                throw new \Exception('Ошибка: правило не добавлено!');

            // переход к списку
            $this->actionList();
        } catch (Exception $e){
            throw new UserException(
                \Yii::t('redirect301','validationError', ['message'=>$e->getMessage()]),
                0,
                $e
            );

        }

    }

    /**
     * Запуск тестирования по всем редиректам.
     * @param array $aData
     */
    public function actionTestAll($aData = []){

        $this->setPanelName(\Yii::t('redirect301', 'newRedirect'));

        $oForm = StateBuilder::newEdit();
        $oForm
            ->fieldText('input_url', \Yii::t('redirect301', 'input_url'), 300)
            ->fieldShow('test_results', \Yii::t('redirect301', 'test_results'))
            ->button('test',\Yii::t('redirect301','test'),'','init',['unsetFormDirtyBlocker'=>true])
            ->buttonCancel('list');

        if (!empty($aData))
            $oForm->setValue($aData);
        else
            $oForm->setValue(['input_url'=>\skewer\components\redirect\Api::getTestUrls()]);
        ;

        $this->setInterface($oForm->getForm());

    }

    /**
     * Запуск тестирования по 1 правилу
     */
    public function actionTest(){

        $aData = $this->get('data');

        if ((isset($aData['old_url'])) and (isset($aData['new_url']))) {

            try {
                \skewer\components\redirect\Api::checkRule($aData['old_url'],$aData['new_url'],'/test');
                $aOut = [];
                $aInputUrls = explode("\n", $aData['input_url']);
                foreach ($aInputUrls as $key=>$item){
                    $aOut['items'][$key]['old'] = $item;
                    $aOut['items'][$key]['new'] = \skewer\components\redirect\Api::checkRule($aData['old_url'],$aData['new_url'],$item);
                    if (!$aOut['items'][$key]['new']) $aOut['items'][$key]['new'] = \Yii::t('redirect301','no_redirect');
                }

                $aData['test_results'] = \Yii::$app->getView()->renderFile(\skewer\components\redirect\Api::getDir().'/template/test_redirect.php',$aOut);
            } catch (Exception $e){
                throw new UserException(
                    \Yii::t('redirect301','validationError', ['message'=>$e->getMessage()]),
                    0,
                    $e
                );

            }

            $this->actionAddForm($aData);

        }else{
            $aData['test_results'] = \skewer\components\redirect\Api::testUrls(explode("\n",$aData['input_url']));
            $this->actionTestAll($aData);
        }

    }

    /**
     * Удаление записи
     */
    public function actionDelete(){

        try {

            $aData = $this->get('data');

            if ( !Redirect::deleteAll(['id'=>$aData['id']]) )
                throw new \Exception('Ошибка: не удалось удалить правило!');


        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

    }

    /**
     * Отрисовка формы редактирования
     * @param array $aData
     */
    public function actionEditForm($aData = []){

        if (empty($aData))
            $aData = $this->get('data');

        $this->setPanelName(\Yii::t('redirect301', 'editUrl'));

        $oForm = StateBuilder::newEdit();
        $oForm
            ->fieldHide('id', 'id')
            ->fieldString('old_url', \Yii::t('redirect301', 'old_url'))
            ->fieldString('new_url', \Yii::t('redirect301', 'new_url'))
            ->fieldText('input_url', \Yii::t('redirect301', 'input_url'), 300)
            ->fieldShow('test_results', \Yii::t('redirect301', 'test_results'))
            ->buttonSave('update')
            ->button('test',\Yii::t('redirect301','test'),'','init',['unsetFormDirtyBlocker'=>true])
            ->buttonCancel('list');

        if (count($aData)>2)
            $oForm->setValue($aData);
        else
            $oForm->setValue(
                Redirect::findOne(['id'=>$aData['id']])
            );

        $this->setInterface($oForm->getForm());

    }

    /**
     * Обновление записи
     */
    public function actionUpdate(){

        $aData = $this->get('data');

        if ((substr($aData['old_url'],-1)!=='/') and (substr($aData['old_url'],-1)!=='$'))
            $aData['old_url'] = $aData['old_url'].'/';

        $aData['old_url'] = str_replace('http://','',$aData['old_url']);
        $aData['old_url'] = str_replace('https://','',$aData['old_url']);
        $aData['old_url'] = str_replace($_SERVER['HTTP_HOST'],'',$aData['old_url']);

        try {
            \skewer\components\redirect\Api::checkRule($aData['old_url'],$aData['new_url'],'/test');
            /** @var Redirect $redirect301 */
            if($redirect301 = Redirect::findOne(['id'=>$aData['id']])) {
                $redirect301->setAttributes($aData);
                if (!$redirect301->save())
                    throw new \Exception('Ошибка: правило не было изменено!');
            }
            $this->actionList();

        } catch (Exception $e){
            throw new UserException(
                \Yii::t('redirect301','validationError', ['message'=>$e->getMessage()]),
                0,
                $e
            );

        }

    }

}
