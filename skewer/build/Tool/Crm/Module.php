<?php

namespace skewer\build\Tool\Crm;


use skewer\base\ui;
use skewer\build\Tool;
use skewer\base\SysVar;
use yii\helpers\ArrayHelper;


class Module extends Tool\LeftList\ModulePrototype  {

    protected function actionInit() {

        $formBuilder = ui\StateBuilder::newEdit();
        $formBuilder->field('token', \Yii::t('crm', 'token'), 'str');
        $formBuilder->field('email', \Yii::t('crm', 'email'), 'str');

        $values['token'] = SysVar::get('crm_token');
        $values['email'] = SysVar::get('crm_email');
        $formBuilder->setValue($values);

        // добавляем элементы управления
        $formBuilder->buttonSave('save');

        $this->setInterface( $formBuilder->getForm() );

        return psComplete;
    }

    protected function actionSave(){
        try {
            $aData = $this->getInData();
            SysVar::set('crm_token', ArrayHelper::getValue($aData,'token',''));
            SysVar::set('crm_email', ArrayHelper::getValue($aData,'email',''));
            $this->actionInit();
        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }
    }
}