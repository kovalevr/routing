<?php

namespace skewer\build\Tool\Order;

use skewer\build\Adm;
use skewer\build\Tool;
use skewer\base\site_module\Context;


/**
 * Проекция редактора баннеров для слайдера в панель управления
 * Class Module
 * @package skewer\build\Adm\Order
 */
class Module extends Adm\Order\Module implements Tool\LeftList\ModuleInterface {

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    public function __construct(Context $oContext ) {
        parent::__construct( $oContext );
        //$oContext->setTplDirectory('/skewer/build/Adm/Order/templates');
        $oContext->setModuleWebDir('/skewer/build/Adm/Order');
        $oContext->setModuleDir(RELEASEPATH.'build/Adm/Order');

        $oContext->setModuleLayer('Adm');

    }

    function getName() {
        return $this->getModuleName();
    }

}