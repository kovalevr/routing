<?php

namespace skewer\build\Tool\SectionSettings;

use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\base\SysVar;

/**
 * Модуль Настроек
 * Class Module
 * @package skewer\build\Tool\SectionSettings
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {

        $oForm = StateBuilder::newEdit();

        $oForm
            ->field('menuIcons', \Yii::t('page', 'menuIcons'), 'check')
            ->field('newsDetailLink', \Yii::t('page', 'newsDetailLink'), 'check')
            ->field('showGalleryInNews', \Yii::t('page', 'showGalleryInNews'), 'check')
            ->field('onCheckSizeOpenGraphImage', \Yii::t('page', 'onCheckSizeOpenGraphImage'), 'check')
            ->field('showGalleryInNews', \Yii::t('page', 'showGalleryInNews'), 'check')
            ->field('onCheckSizeOpenGraphImage', \Yii::t('page', 'onCheckSizeOpenGraphImage'), 'check')
            ->field('onCheckSizeOpenGraphImage', \Yii::t('page', 'onCheckSizeOpenGraphImage'), 'check')
            ->field('lock_section_flag', \Yii::t('page', 'lock_section_flag'), 'check')
            ->field('album_fotorama_flag', \Yii::t('page', 'album_fotorama_flag'), 'check')
        ;

        $oForm->setValue(
            [
                'menuIcons' => SysVar::get('Menu.ShowIcons'),
                'newsDetailLink' => SysVar::get('News.showDetailLink'),
                'showGalleryInNews' => SysVar::get('News.hideGallery'),
                'onCheckSizeOpenGraphImage' => SysVar::get('OpenGraph.onCheckSizeImage'),
                'lock_section_flag' => SysVar::get('lock_section_flag'),
                'album_fotorama_flag' => SysVar::get('Gallery.ShowFotorama'),
            ]
        );

        $oForm->buttonSave('save');
        $oForm->buttonCancel();

        $this->setInterface($oForm->getForm());
    }

    /**
     * Сохранение
     */
    protected function actionSave(){

        SysVar::set('Menu.ShowIcons', $this->getInDataVal('menuIcons'));
        SysVar::set('News.showDetailLink', $this->getInDataVal('newsDetailLink'));
        SysVar::set('News.hideGallery', $this->getInDataVal('showGalleryInNews'));
        SysVar::set('OpenGraph.onCheckSizeImage', $this->getInDataVal('onCheckSizeOpenGraphImage'));
        SysVar::set('lock_section_flag',$this->getInDataVal('lock_section_flag'));
        SysVar::set('Gallery.ShowFotorama',$this->getInDataVal('album_fotorama_flag'));

        $this->actionInit();
    }

}