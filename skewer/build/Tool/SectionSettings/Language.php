<?php

$aLanguage = array();

$aLanguage['ru']['SectionSettings.Tool.tab_name'] = 'Разделы';
$aLanguage['ru']['menuIcons'] = 'Иконки пунктов меню';
$aLanguage['ru']['newsDetailLink'] = 'Ссылка "Подробнее" в новостях';
$aLanguage['ru']['showGalleryInNews'] = 'Отключить фотогалерею в новостях';
$aLanguage['ru']['onCheckSizeOpenGraphImage'] = 'Включить проверку размера opengraph - изображений';
$aLanguage['ru']['lock_section_flag'] = 'Использовать сообщение "раздел в стадии наполнения"';
$aLanguage['ru']['lock_section_text'] = 'Сообщение "раздел в стадии наполнения"';
$aLanguage['ru']['lock_section_text_value'] = 'Приносим свои извинения, раздел находится в стадии наполнения.';
$aLanguage['ru']['album_fotorama_flag'] = 'Включить фотораму для фотоальбома';

$aLanguage['en']['SectionSettings.Tool.tab_name'] = 'Sections';
$aLanguage['en']['menuIcons'] = 'Icons menu';
$aLanguage['en']['newsDetailLink'] = '"Read more" in the news';
$aLanguage['en']['showGalleryInNews'] = 'Disable photo gallery news';
$aLanguage['en']['onCheckSizeOpenGraphImage'] = 'Enable check open graph image size';
$aLanguage['en']['lock_section_flag'] = 'Message "Section in development stage"';
$aLanguage['en']['lock_section_text'] = 'Message for development stage';
$aLanguage['en']['lock_section_text_value'] = 'We are sorry. This section in development stage.';
$aLanguage['en']['album_fotorama_flag'] = 'Include Fotorama for photo album';

return $aLanguage;