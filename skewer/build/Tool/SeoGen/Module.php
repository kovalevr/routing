<?php

namespace skewer\build\Tool\SeoGen;


use skewer\base\queue\Task;
use skewer\base\section\Tree;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * Модуль импорта/экспорта seo данных
 * Class Module
 * @package skewer\build\Tool\SeoGen
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {

        $oForm = StateBuilder::newEdit();
        $oForm
            ->button('importState','Импорт', 'icon-configuration')
            ->button('exportState','Экспорт', 'icon-configuration')
        ;

        $this->setInterface($oForm->getForm());

    }

    public function actionUpdateImportState(){

        $aData = $this->get('formData', []);
        $sTypeData = ArrayHelper::getValue($aData, 'data_type', '');
        $sMode = ArrayHelper::getValue($aData, 'mode', '');

        $this->render(
            new view\ImportFormSettings($this, [
                'sTypeData' => $sTypeData,
                'aValues'   => $aData,
                'sMode'     => $sMode
            ])
        );

    }


    protected function actionImportState(){

        $this->setInnerData('state', 'importState');

        $this->render(
            new view\ImportFormSettings($this)
        );
    }

    protected function actionExportState(){

        $this->setInnerData('state', 'exportState');

        $this->render(
            new view\ExportFormSettings()
        );

    }


    public function actionUpdateExportState(){

        $aData = $this->get('formData', []);
        $sTypeData = ArrayHelper::getValue($aData, 'data_type', '');

        $this->render(
            new view\ExportFormSettings([
                'sTypeData' => $sTypeData,
                'aValues'   => $aData
            ])
        );

    }

    public function actionShowLastLog(){
        $sState = $this->getInnerData('state');

        switch ($sState){
            case 'exportState':
                $sClass = ExportTask::className();
                break;
            case 'importState':
                $sClass = ImportTask::className();
                break;
        }

        // Берём лог последней задачи заданного класса
        $aTask = \Yii::$app->db->createCommand("
            SELECT * 
            FROM `task` 
            WHERE `class` = :className ORDER BY `upd_time` DESC 
        ", ['className' => $sClass])->queryOne();


        if (!$aTask)
            throw new UserException('Нет данных по данной операции');

        $this->showLog($aTask['id']);

    }



    protected function actionExportRun(){

        $aData = $this->get('data', []);

        /** Валидация пришедших данных */

        if (!$sDataType = ArrayHelper::getValue($aData, 'data_type', ''))
            throw new UserException('Не выбран тип данных');

        switch ($sDataType){
            case Api::DATATYPE_SECTIONS:

                $iSectionId = $this->getInDataVal('sectionId',0);

                if (!$iSectionId)
                    throw new UserException('Не задан id выгружаемого раздела');

                if (!Tree::getSection($iSectionId))
                    throw new UserException("Раздел [{$iSectionId}] не существует");

                if (!Tree::getSubSections($iSectionId, true,true))
                    throw new UserException("Раздел [{$iSectionId}] не имеет дочерних разделов");

                break;

            case Api::DATATYPE_GOODS:

                if (!$sCatalogSections = $this->getInDataVal('catalog_sections', ''))
                    throw new UserException('Не задано поле "раздел каталога"');

                break;
        }

        $this->actionRepeatExportRun($aData);

    }

    protected function actionRepeatExportRun($aParam = []){

        $aData = $this->getInData();

        if (empty($aData) && $this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $taskId = (isset($aData['taskId'])) ? $aData['taskId'] : 0;

        /** Запуск экспорта */
        $aRes = Api::runTask($taskId, ExportTask::getConfig($aParam));

        if ( in_array($aRes['status'], [Task::stFrozen, Task::stWait]) ) {
            /** Ставим на повторный запуск */
            $this->addJSListener('repeatRun', 'repeatExportRun');
            $this->fireJSEvent('repeatRun', ['taskId' => $aRes['id']]);
        }

        $this->showLog($aRes['id']);

    }


    protected function actionImportRun(){

        $aData = $this->get('data');

        /** Валидация пришедших данных */

        if (!$sFile = $this->getInDataVal('file',''))
            throw new UserException('Не загружен файл');

        if (!preg_match('{(.*)\.xlsx$}i', $sFile))
            throw new UserException('Загрузите файл с расширением [.xlsx]');

        if (!$sDataType = ArrayHelper::getValue($aData, 'data_type', ''))
            throw new UserException('Не выбран тип данных');

        if (!$sMode = $this->getInDataVal('mode',''))
            throw new UserException("Выберите режим импорта");

        switch ($sDataType){
            case Api::DATATYPE_SECTIONS:

                if (ArrayHelper::getValue($aData, 'mode', '') == ImportTask::$sCreateMode)
                    if (!$iSectionId = $this->getInDataVal('sectionId',0))
                        throw new UserException('Укажите раздел в которых будут импортированы данные');

                break;

            case Api::DATATYPE_GOODS:

                if (!ArrayHelper::getValue($aData, 'catalog_sections', ''))
                    throw new UserException('Не задано поле "раздел каталога"');

                break;
        }

        $this->actionRepeatImportRun($aData);

    }


    public function actionRepeatImportRun($aParam = []){

        $aData = $this->getInData();

        if (empty($aData) && $this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $iTaskId = ArrayHelper::getValue($aData, 'taskId', 0);

        $aRes = Api::runTask($iTaskId, ImportTask::getConfig($aParam));

        if ( in_array($aRes['status'], [Task::stFrozen, Task::stWait]) ){
            $this->addJSListener( 'reloadImport', 'repeatImportRun' );
            $this->fireJSEvent( 'reloadImport', ['taskId' => $aRes['id']] );
        }

        // Показываем лог
        $this->showLog($aRes['id']);
    }

    /** Выводить лог. Работает и для импорта и для экспорта
     * @param  int $iTaskId - id задачи соответствующей логу
     * @throws UserException
     */
    private function showLog( $iTaskId ){
        if (!$iTaskId){
            throw new UserException( \Yii::t('seoGen', 'error_task_not_fount') );
        }

        $aLogParams = \skewer\components\import\Api::getLog( $iTaskId );

        if (isset($aLogParams['status'])){
            $aLogParams['status'] = Tool\Import\View::getStatus($aLogParams);
        }

        $oForm = StateBuilder::newEdit();
        $oForm->field('result', \Yii::t('seoGen', 'log_result'), 'show', ['labelAlign' => 'top', 'height' => '100%']);

        $baskAction = $this->getInnerData('state');

        $oForm->buttonBack($baskAction);

        $sText = \Yii::$app->view->renderPhpFile(
            __DIR__ . DIRECTORY_SEPARATOR . $this->getTplDirectory() . DIRECTORY_SEPARATOR . 'log_template.php',
            ['aLogParams' => $aLogParams]
        );

        $oForm->setValue(['result' => $sText]);

        $this->setInterface($oForm->getForm());
    }


}