<?php

namespace skewer\build\Tool\ReserveOnline;


use skewer\components\config\InstallPrototype;
use skewer\components\i18n\Languages;
use skewer\base\section;
use skewer\components\catalog\Card;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {

        /** Массив имён всех используемых языков на сайте */
        $aLangs = Languages::getAllActiveNames();

        /** Сайт мультиязычный? */
        $bIsMultiLangsSite = ( (count($aLangs) > 1) or !isset($aLangs['ru']) );

        // Создания разделов с модулем для нужных языковых веток
        foreach($aLangs as $sLang)
            if (in_array($sLang, ['ru', 'en', 'de']))
                $this->createReserveOnlineSection($sLang);


        // Параметры модуля виджета
        Api::setSectionParam([
            'name'   => '.title',
            'group'  => Api::ADM_WIDGET_GROUP_NAME,
            'value'  => $this->lang('widget_design_title'),
            'parent' => \Yii::$app->sections->tplNew(),

        ]);
        Api::setSectionParam([
            'name'   => 'object',
            'group'  => Api::ADM_WIDGET_GROUP_NAME,
            'value'  => $this->getModuleName(),
            'parent' => \Yii::$app->sections->tplNew(),
        ]);
        Api::setSectionParam([
            'name'   => 'layout',
            'group'  => Api::ADM_WIDGET_GROUP_NAME,
            'value'  => 'head',
            'parent' => \Yii::$app->sections->tplNew(),
        ]);
        Api::setSectionParam([
            'name'   => 'isWidget',
            'group'  => Api::ADM_WIDGET_GROUP_NAME,
            'value'  => '1',
            'parent' => \Yii::$app->sections->tplNew(),
        ]);

        // Языковые параметры модуля виджета
        Api::setLanguageParam([
            'name'   => 'show_discount',
            'group'  => Api::ADM_WIDGET_GROUP_NAME,
            'value'  => '1',
        ]);
        Api::setLanguageParam([
            'name'   => 'discount_img',
            'group'  => Api::ADM_WIDGET_GROUP_NAME,
            'value'  => '/images/' . Api::DISCOUNT_DEF_IMG,
        ]);

        // Переименование кнопки заказа
        if ( ($oCardBase = Card::get(Card::DEF_BASE_CARD)) and
            ($oBuyField = Card::getFieldByName($oCardBase->id, 'buy')) ) {
            $oBuyField->title = ($bIsMultiLangsSite) ? 'ReserveOnline.buy_title' : $this->lang('buy_title');
            $oBuyField->save();
            $oCardBase->updCache();
        }

        return true;
    }// func

    /**
     * Создание в Панели управления модуля бронирования
     * @param string $sLang Псевдоним языка (ru/en/de)
     * @throws \Exception
     */
    private function createReserveOnlineSection($sLang) {

        /** Id родительского раздела для раздела модуля */
        $iParentSectionId = \Yii::$app->sections->topMenu($sLang);

        if (!$iParentSectionId)
            throw new \Exception($this->lang('error_parent_section', [$sLang]));

        $sSectionName = $this->lang('section_name', [], $sLang);

        $oModuleSection = section\Tree::addSection($iParentSectionId, $sSectionName, \Yii::$app->sections->tplNew());
        if (!$oModuleSection)
            throw new \Exception($this->lang('error_section_create', [$sSectionName]));

        /** Id раздела, где будет форма */
        $iSectionId = $oModuleSection->id;

        // 1. Системный модуль онлайн-сервиса
        Api::setSectionParam([
            'name'   => 'object',
            'group'  => Api::ADM_GROUP_NAME,
            'value'  => $this->getModuleName(),
            'parent' => $iSectionId,
        ]);
        Api::setSectionParam([
            'name'   => '.title',
            'group'  => Api::ADM_GROUP_NAME,
            'value'  => $this->getConfigParam('title'),
            'parent' => $iSectionId,
        ]);
        Api::setSectionParam([
            'name'   => 'layout',
            'group'  => Api::ADM_GROUP_NAME,
            'value'  => 'content',
            'parent' => $iSectionId,
        ]);

        //  Параметр для режима дизайна, дающий возможность кастомизировать этот раздел

        //  Установка нового раздела заказа для кнопки заказа языка $sLang методом ссылки
        $iOrderLangSectionId = \Yii::$app->sections->getValue('orderForm', $sLang);
        if ($iOrderLangSectionId and ($oOrderLangSection = section\Tree::getSection($iOrderLangSectionId))) {
            $oOrderLangSection->link = "[$iSectionId]";
            $oOrderLangSection->save(false);
        }
    }

    public function uninstall() {

        /** Массив имён всех используемых языков на сайте */
        $aLangs = Languages::getAllActiveNames();

        /** Сайт мультиязычный? */
        $bIsMultiLangsSite = ( (count($aLangs) > 1) or !isset($aLangs['ru']) );

        $this->removeObjectFromLayouts(Api::ADM_WIDGET_GROUP_NAME);

        $aObjectReserveOnlineParams = section\Parameters::getList()
            ->name('object')
            ->group(Api::ADM_GROUP_NAME)
            ->get();

        // Удалить раздел, где используется текущий модуль
        $aIdFormReserve = Api::getReserveOnlineSection();
        foreach ($aIdFormReserve as $id)
            section\Tree::removeSection($id);

        // Удалить параметры виджета
        $aWidgetParams = section\Parameters::getList()
            ->group(Api::ADM_WIDGET_GROUP_NAME)
            ->get();
        foreach($aWidgetParams as $oParam)
            $oParam->delete();

        // Переименование кнопки заказа
        if ( ($oCardBase = Card::get(Card::DEF_BASE_CARD)) and
             ($oBuyField = Card::getFieldByName($oCardBase->id, 'buy')) ) {
            $oBuyField->title = ($bIsMultiLangsSite) ? 'data/catalog.field_buy_title' : \Yii::t('data/catalog', 'field_buy_title');
            $oBuyField->save();
            $oCardBase->updCache();
        }

        // Возвращение раздела заказа для кнопки

        /** Массив имён всех используемых языков на сайте */
        $aLangs = Languages::getAllActiveNames();
        foreach ($aLangs as $sLang) {
            $iOrderLangSectionId = \Yii::$app->sections->getValue('orderForm', $sLang);
            if ($iOrderLangSectionId and ($oOrderLangSection = section\Tree::getSection($iOrderLangSectionId))) {
                $oOrderLangSection->link = "";
                $oOrderLangSection->save(false);
            }
        }

        // Удалить параметры из дизайнерского режима
        \Yii::$app->db->createCommand("DELETE FROM `css_data_groups` WHERE `name` LIKE 'modules.bronline%'")->execute();
        \Yii::$app->db->createCommand("DELETE FROM `css_data_params` WHERE `name` LIKE 'modules.bronline%'")->execute();

        return true;
    }// func
}
