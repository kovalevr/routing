<?php
/* main */
use skewer\build\Tool\LeftList;
use skewer\base\site\Layer;

$aConfig['name']         = 'ReserveOnline';
$aConfig['version']      = '1.0';
$aConfig['title']        = 'Бронирование гостиниц';
$aConfig['description']  = 'Модуль бронирования гостиниц online';
$aConfig['revision']     = '0002';
$aConfig['layer']        = Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;
$aConfig['useNamespace'] = true;

$aConfig['dependency'] = [
    ['ReserveOnline', Layer::PAGE],
];

return $aConfig;
