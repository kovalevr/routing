<?php

$aLanguage = [];

$aLanguage['ru']['ReserveOnline.Tool.tab_name'] = 'Бронирование гостиниц';

$aLanguage['ru']['section_name'] = 'Бронирование';

$aLanguage['ru']['widget_design_title'] = 'Виджет бронирования гостиниц';

$aLanguage['ru']['error_section_create'] = 'Ошибка: нельзя создать раздел "{0}"!';
$aLanguage['ru']['error_parent_section'] = 'Ошибка: не найден родительский раздел для установки модуля в языковую ветку "{0}"!';

$aLanguage['ru']['id_hotel']      = 'Идентификатор гостиницы';
$aLanguage['ru']['show_discount'] = 'Показать акцию';
$aLanguage['ru']['discount_img']  = 'Иконка акции';
$aLanguage['ru']['discount_link'] = 'Ссылка на акцию';
$aLanguage['ru']['widget_group']  = 'Настройки виджета в шапке сайта';
$aLanguage['ru']['buy_title']     = 'Забронировать';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['ReserveOnline.Tool.tab_name'] = 'Hotel reservation';

$aLanguage['en']['section_name'] = 'Reservation';

$aLanguage['en']['widget_design_title'] = 'Widget hotel reservation';

$aLanguage['en']['error_section_create'] = 'Error: can not creating section "{0}"!';
$aLanguage['en']['error_parent_section'] = 'Error: parent section for module installation not found in language branch "{0}"!';

$aLanguage['en']['id_hotel']      = 'Id hotel';
$aLanguage['en']['show_discount'] = 'Show discount';
$aLanguage['en']['discount_img']  = 'Icon for discount';
$aLanguage['en']['discount_link'] = 'Link for discount';
$aLanguage['en']['widget_group']  = 'Settings for widget in head';
$aLanguage['en']['buy_title']     = 'Reserve';

// ********************************************************************************************************************
// ***************************************************** GERMAN *******************************************************
// ********************************************************************************************************************

$aLanguage['de']['ReserveOnline.Tool.tab_name'] = 'Hotelreservierung';

$aLanguage['de']['section_name'] = 'Reservierung';

$aLanguage['de']['widget_design_title'] = 'Widget Hotelreservierung';

$aLanguage['de']['error_section_create'] = 'Fehler: Abschnitt kann nicht erstellt werden "{0}"!';
$aLanguage['de']['error_parent_section'] = 'Fehler: kein Stammabschnitt für die Modulinstallation in der Sprachversion gefunden "{0}"!';

$aLanguage['de']['id_hotel']      = 'Hotel ID-Nummer';
$aLanguage['de']['show_discount'] = 'Sonderangebot zeigen';
$aLanguage['de']['discount_img']  = 'Symbol des Sonderangebotes';
$aLanguage['de']['discount_link'] = 'Link zum Sonderangebot';
$aLanguage['de']['widget_group']  = 'Widget-Einstellungen im Seitenkopf';
$aLanguage['de']['buy_title']     = 'reservieren';

return $aLanguage;