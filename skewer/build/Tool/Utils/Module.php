<?php

namespace skewer\build\Tool\Utils;


use skewer\base\queue\Task;
use skewer\components\content_generator\Asset;
use skewer\components\search\SearchIndex;
use skewer\components\seo\Service;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use \skewer\build\Tool\Domains;
use skewer\components\ext;
use skewer\base\site\Site;
use skewer\base\SysVar;
use yii\base\Exception;

/**
 * Модуль c системными утилитами
 * Class Module
 * @package skewer\build\Tool\Utils
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit( $sText = '' ) {

        $form = StateBuilder::newEdit();

        $form->button('dropCache', \Yii::t('cache', 'drop_cache_act'), ext\docked\Api::iconDel, 'dropCache');
        $form->button('Logs', \Yii::t('utils', 'logs'), ext\docked\Api::iconConfiguration, 'Logs');
        $form->button('OptimizeDB', \Yii::t('utils', 'optimize_db'), ext\docked\Api::iconInstall);
        $form->button('Search', \Yii::t('utils', 'search'), ext\docked\Api::iconNext, 'init');


        if ( $sText )
            $form->headText( $sText );

        $this->setInterface($form->getForm());
    }

    protected function actionDropCache() {
        
        \Yii::$app->router->updateModificationDateSite();
        \Yii::$app->rebuildRegistry();
        \Yii::$app->rebuildCss();
        \Yii::$app->rebuildLang();
        \Yii::$app->clearParser();

        $this->actionInit( \Yii::t('cache', 'drop_cache_text') );

    }

    protected function actionLogs( $sText = '' ) {

        $oShow = new ext\ShowView();

        if ( $sText )
            $oShow->setAddText( sprintf( '<p>%s</p>', $sText ) );

        $oShow->addExtButton( ext\docked\Api::create( \Yii::t('utils', 'view_access') )
            ->setIconCls( ext\docked\Api::iconReload )
            ->setState( 'viewAccess' )
            ->setAction( 'viewAccess' )
        );

        $oShow->addExtButton( ext\docked\Api::create( \Yii::t('utils', 'view_error') )
            ->setIconCls( ext\docked\Api::iconReload )
            ->setState( 'viewError' )
            ->setAction( 'viewError' )
        );


        $oShow->addExtButton( ext\docked\Api::create( \Yii::t('utils', 'clear_logs') )
            ->setIconCls( ext\docked\Api::iconDel )
            ->setState( 'clearLogs' )
            ->setAction( 'clearLogs' )
        );


        $oShow->addExtButton( ext\docked\Api::create( \Yii::t('adm','back') )
            ->setIconCls( ext\docked\Api::iconCancel )
            ->setState( 'init' )
            ->setAction( 'init' )
        );

        $this->setInterface($oShow);
    }


    protected function actionViewAccess() {

        $sText = '';

        $sFile = ROOTPATH . 'log/access.log';
        if ( file_exists($sFile) ) {

            $arr = file( $sFile );

            $iStrCount = count($arr);
            if ( $iStrCount ) {
                $iFirstStr = ($iStrCount > 30) ? $iStrCount - 30 : 0;
                for ( $i = $iFirstStr; $i<$iStrCount; $i++ )
                    $sText .= $arr[ $i ] . '<br>';
            }
        }

        $this->actionLogs( $sText );
    }


    protected function actionViewError() {

        $sText = '';

        $sFile = ROOTPATH . 'log/error.log';
        if ( file_exists($sFile) ) {

            $arr = file( $sFile );

            $iStrCount = count($arr);
            if ( $iStrCount ) {
                $iFirstStr = ($iStrCount > 30) ? $iStrCount - 30 : 0;
                for ( $i = $iFirstStr; $i<$iStrCount; $i++ )
                    $sText .= $arr[ $i ] . '<br>';
            }
        }

        $this->actionLogs( $sText );
    }


    protected function actionClearLogs() {

        $sLogFile = ROOTPATH . '/log/access.log';
        $fh = fopen( $sLogFile, 'w' );
        fclose($fh);

        $sLogFile = ROOTPATH . '/log/error.log';
        $fh = fopen( $sLogFile, 'w' );
        fclose($fh);

        $this->actionLogs();
    }

    protected function actionSearch($sHeadText = '', $sText = ''){
        $form = StateBuilder::newEdit();

        $form
            ->button('searchDropAll', \Yii::t('utils', 'SearchDropAll'), ext\docked\Api::iconReinstall)
            ->button('resetActive', \Yii::t('utils', 'resetActive'), ext\docked\Api::iconReload)
            ->button('reindex', \Yii::t('utils', 'reindex'), ext\docked\Api::iconReload)
            ->button('RebuildSitemap', \Yii::t('utils', 'rebuildSitemap'), ext\docked\Api::iconInstall)
            ->button('RebuildRobots', \Yii::t('utils', 'rebuildRobots'), ext\docked\Api::iconReload)

            ->buttonBack();

        if ( $sHeadText )
            $form->headText( $sHeadText );

        if ( $sText ){
            $form->field('text', 'text', 'show', array('hideLabel' => 1));
            $form->setValue(array('text' => $sText));
        }

        $this->setInterface($form->getForm());
    }

    /**
     * "Пересобрать заново" - dropAll(), затем restoreAll()
     */
    protected function actionSearchDropAll(){

        Service::rebuildSearchIndex();
        $sText = \Yii::t('utils', 'search_drop_index');

        $this->actionSearch($sText);
    }

    /**
     * "Сбросить автивность записей" - сбросить флаг активности для всех записей
     */
    protected function actionResetActive(){

        $iAffectedRow =  SearchIndex::update()->set('status',0)->where('status',1)->get();
        $sText = \Yii::t('utils', 'record_update') . ": $iAffectedRow";

        $this->actionSearch($sText);
    }

    /**
     * "Переиндексировать неактивные" - запуск переиндексации для записей со статусом 0
     */
    protected function actionReindex(){

        $aData = $this->getInData();
        if ($this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $iTask = (isset($aData['idTask']))?$aData['idTask']:0;

        $iTask = Service::makeSearchIndex( $iTask );

        $aCollisions = Service::findUrlCollisions();
        if (!empty($aCollisions)){

            $aCollisionsUrls = [];

            foreach ($aCollisions as $item){
                $aCollisionsUrls[] = $item['href'];
            }

            throw new Exception(\Yii::t('utils', 'collision', ['collisions'=>implode("\r\n",$aCollisionsUrls)]));
        }

        $iRefreshCount = SysVar::get('Search.updated');
        $sText = \Yii::t('utils', 'record_update') . ": $iRefreshCount";

        if ( $iTask ){
            $this->addJSListener( 'search_reindex', 'reindex' );
            $this->fireJSEvent( 'search_reindex', ['idTask' => $iTask] );

        }

        $this->actionSearch($sText);
    }

    /**
     * Перестроим сайтмап
     */
    protected function actionRebuildSitemap(){

        $aData = $this->getInData();
        if ($this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $iTask = (isset($aData['idTask']))?$aData['idTask']:0;

        $aResult = Service::makeSiteMap( $iTask );
        if ( $aResult['id'] )
            $iTask = (int)$aResult['id'];

        if ($aResult['status'] != Task::stError) {

            $sUrl = Site::httpDomainSlash()."sitemap.xml";
            $sPath = WEBPATH."sitemap.xml";

            if ( file_exists($sPath) ) {
                $sText = htmlentities(file_get_contents($sPath));
                $sText = str_replace(' ', '&nbsp;', $sText);
                $sText = preg_replace('(http://.*\.xml)', '<a target="_blank" href="$0">$0</a>', $sText);
            } else {
                $sText = '';
            }

            /** @noinspection HtmlUnknownTarget */
            $msg = nl2br(sprintf(
                '%s

                <a target="_blank" href="%s">%s</a>

                %s',
                \Yii::t('utils', 'sitemap_update_msg'),
                $sUrl,
                $sUrl,
                $sText
            ));
        }
        else
            $msg = \Yii::t('utils', 'sitemap_update_error');

        if ( $iTask and $aResult['status'] == Task::stFrozen ){
            $this->addJSListener( 'rebuildSitemap', 'rebuildSitemap' );
            $this->fireJSEvent( 'rebuildSitemap', ['idTask' => $aResult['id']] );
        }

        $this->actionSearch($msg);
    }

    /**
     * Перестроим роботс
     */
    protected function actionRebuildRobots(){

        $res = Service::updateRobotsTxt(Domains\Api::getMainDomain());

        if ($res){
            $msg = \Yii::t('utils', 'robots_update_msg');
        } else $msg = \Yii::t('utils', 'robots_update_error');

        $sRobots = file_get_contents(WEBPATH.'robots.txt');
        $sRobots = nl2br($sRobots);

        $this->actionSearch($msg, $sRobots);
    }

    /** Оптимизация таблиц БД */
    protected function actionOptimizeDB() {

        // Оптимизация таблиц типа MyISAM делает так же дефрагментацию
        $oQuery = \Yii::$app->db->createCommand("SHOW TABLE STATUS WHERE engine IN ('MyISAM')")->query();
        while($sTableName = $oQuery->readColumn(0))
            \Yii::$app->db->createCommand("OPTIMIZE TABLE `$sTableName`")->execute();

        // Таблицы innoDB не нуждаются в оптимизации. Ниже делается только дефрагментация
        $oQuery = \Yii::$app->db->createCommand("SHOW TABLE STATUS WHERE engine IN ('innoDB')")->query();
        while($sTableName = $oQuery->readColumn(0))
            \Yii::$app->db->createCommand("ALTER TABLE `$sTableName` ENGINE = InnoDB")->execute();

        $this->actionInit( \Yii::t('utils', 'optimize_db_text') );
    }
}