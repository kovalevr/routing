<?php

namespace skewer\build\Tool\FormOrders;



class Api {

    /**
     * Набор статусов для заказов из форм
     * @return array
     */
    public static function getStatusList() {
        return array(
            'new' => \Yii::t('forms', 'status_new' ),
            'ready' => \Yii::t('forms', 'status_ready' ),
        );
    }


    /**
     * Виджет на вывод статуса в списке
     * @param $oItem
     * @param $sField
     * @return mixed
     */
    public static function getWidget4Status( $oItem, $sField  ) {

        $aStatusList = self::getStatusList();

        $sVal = isSet( $oItem[$sField] ) ? $oItem[$sField] : $sField;

        return isSet( $aStatusList[$sVal] ) ? $aStatusList[$sVal] : $sVal;
    }

} 