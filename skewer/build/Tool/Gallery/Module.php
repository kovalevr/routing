<?php

namespace skewer\build\Tool\Gallery;


use skewer\base\ui;
use skewer\build\Tool;
use skewer\components\gallery;
use skewer\components\auth\CurrentAdmin;

/**
 * @todo Рефакторить. Избавиться от передачи параметров через кнопки и настроить отлов ошибок
 * Модуль редактирования форматов для галереи
 */
class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Массив полей, выводимых колонками в списке форматов для профиля настроек
     * @var array
     */
    protected $aFormatsListFields = ['id', 'title', 'width', 'height', 'active'];

    /**
     * Id текущего открытого профиля
     * @var int
     */
    protected $iCurrentProfile = 0;

    /**
     * Иницализация
     */
    protected function preExecute() {
        /* Восстанавливаем Id текущего открытого профиля */
        $this->iCurrentProfile = $this->getInt('currentProfile');
    }

    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData([
            'currentProfile' => $this->iCurrentProfile,
        ]);
    }

    /**
     * Вызывается в случае отсутствия явного обработчика
     * @return int
     */
    protected function actionInit() {
        return $this->actionGetProfilesList();
    }

    /**
     * Обработчик состояния списка профилей
     */
    protected function actionGetProfilesList() {

        $this->setPanelName(\Yii::t('gallery', 'tools_profileImageSettings'),true);
        $this->iCurrentProfile = 0;

        $aProfiles = gallery\Profile::getAll();

        // НАЧАЛО: Проверка и установка профилей по умолчанию
        $aDefaults = [];
        foreach ($aProfiles as $iId => &$paProfile) {

            $sType    = $paProfile['type'];
            $iActive  = $paProfile['active'];
            $iDefault = $paProfile['default'];

            // Запомнить тип профиля
            isset($aDefaults[$sType]) or $aDefaults[$sType] = 0;

            if ($iActive and ($iDefault or !$aDefaults[$sType]))
                $aDefaults[$sType] = $iId;
        }
        foreach ($aDefaults as $iProfileId)
            if ($iProfileId and !$aProfiles[$iProfileId]['default']) {
                gallery\Profile::setDefaultProfile($iProfileId);
                $aProfiles[$iProfileId]['default'] = 1;
            }
        // КОНЕЦ: Проверка и установка профилей по умолчанию

        $oFormBuilder = ui\StateBuilder::newList();

        $oFormBuilder
            ->fieldString('title', \Yii::t('gallery', 'profiles_title'),['listColumns' => ['flex' => 1]])
            ->fieldString('type', \Yii::t('gallery', 'profiles_type'),['listColumns' => ['flex' => 1]])
            ->fieldIf(CurrentAdmin::isSystemMode(),  'default', \Yii::t('gallery', 'profiles_default'), 'check', ['listColumns' => ['flex' => 1]])
            ->fieldCheck('active', \Yii::t('gallery', 'profiles_active'))

            ->setValue( $aProfiles ?: [] )

            ->setEditableFields(['default', 'active'], 'ListChange')

            ->buttonRowUpdate('addUpdProfile')
            ->buttonAddNew('AddUpdProfile')
        ;

        if (CurrentAdmin::isSystemMode())
            $oFormBuilder->buttonRowDelete('delProfile');
        $this->setInterface($oFormBuilder->getForm());
    }// func

    /**
     * Состояние редактирования профиля
     */
    protected function actionAddUpdProfile() {

        // Данные по профилю
        $aData = $this->get('data');

        // Id профиля
        if ($this->iCurrentProfile) // Отрабатывает возврат из списка форматов к профилю
             $iProfileId = $this->iCurrentProfile;
        else $iProfileId = $this->iCurrentProfile = (is_array($aData) AND isset($aData['id'])) ? (int)$aData['id'] : 0;

        $this->setPanelName(\Yii::t('gallery', 'tools_addProfile'),true);

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit(); // создаем форму

        // Получаем данные профиля или заготовку под новый профиль
        $aItem = $iProfileId ? gallery\Profile::getById($iProfileId) : gallery\Profile::getProfileBlankValues();

        // добавляем поля
        $oFormBuilder
            ->fieldHide('id', 'id')
            ->fieldSelect('type', \Yii::t('gallery', 'profiles_type'), gallery\Profile::getTypes(), [], false )
            ->fieldString('title', \Yii::t('gallery', 'profiles_title'))
            ->fieldString('alias', \Yii::t('gallery', 'profiles_alias'))
            ->fieldColor('watermark_color',\Yii::t('gallery', 'watermark_color'))
        ;

        /*Если не установлен цвет, поставим белый*/
        if (!$aItem['watermark_color'])
            $aItem['watermark_color'] = '#ffffff';

        $oFormBuilder->setValue($aItem);

        // добавляем элементы управления
        $oFormBuilder
            ->buttonSave('saveProfile')
            ->buttonBack();

        if ($iProfileId)
            $oFormBuilder
                ->buttonEdit('formatsList', \Yii::t('gallery', 'tools_formats'));

        $this->setInterface($oFormBuilder->getForm());
    }// func

    /**
     * Сохраняет профиль
     * @throws \Exception
     */
    protected function actionSaveProfile() {

        $aData = $this->get('data');

        if (isset($aData['watermark_color'])){
            Api::validateColor($aData['watermark_color']);
        }

        if (!count($aData)) throw new \Exception ('Error: Data is not sent!');
        gallery\Profile::setProfile($aData, $aData['id']);

        /* вывод списка */
        $this->actionGetProfilesList();

    }// func

    /**
     * Удаляет выбранный профиль
     * @throws \Exception
     */
    protected function actionDelProfile() {
        /* Данные по профилю */
        $aData = $this->get('data');
        try {
            if (!CurrentAdmin::isSystemMode())
                throw new \Exception('Нет прав на выполнение действия.');

            if ( !isSet($aData['id']) OR (!$aData['id'] = (int)$aData['id']) ) throw new \Exception('Error: Element is not removed!');
            gallery\Profile::removeProfile($aData['id']);

        } catch (\Exception $e) {
            $this->addError( $e->getMessage() );
        }
        /*Вывод списка профилей*/
        $this->actionGetProfilesList();

    }// func

    /** Действие: Установка настроек из спискапрофилей (активность, по умолчанию) */
    protected function actionListChange() {

        if ($aData = $this->get('data')) {

            // Профилем по умолчанию может быть только один активный профиль
            $iActive = (int)$aData['active'];
            $iDefault = $iActive ? (int)$aData['default'] : 0;

            gallery\Profile::setProfile(['active' => $iActive], $aData['id']);
            $iDefault ? gallery\Profile::setDefaultProfile($aData['id']) : gallery\Profile::unsetDefaultProfile($aData['id']);

            $this->actionGetProfilesList();
        }
    }

    /**
     * Выводит список форматов для профиля
     * @throws \Exception
     */
    protected function actionFormatsList() {

        /* Данные по профилю */
        $aData = $this->get('data');

        $this->setPanelName(\Yii::t('gallery', 'tools_formatsImage'),true);

        if ($this->iCurrentProfile)
            $iProfileId = $this->iCurrentProfile;
        elseif ( !isSet($aData['id']) OR (!$iProfileId = (int)$aData['id']) ) throw new \Exception('Error: Formats not received!');

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newList(); // создаем форму

        // добавляем поля
        $oFormBuilder
            ->fieldString('title', \Yii::t('gallery', 'formats_title'), ['listColumns' => ['flex' => 2]])
            ->fieldString('width', \Yii::t('gallery', 'formats_width'), ['listColumns' => ['flex' => 1]])
            ->fieldString('height', \Yii::t('gallery', 'formats_height'),['listColumns' => ['flex' => 1]])
            ->fieldString('active', \Yii::t('gallery', 'formats_active'), ['listColumns' => ['flex' => 1]]);

        $aItems = gallery\Format::getByProfile($iProfileId);
        $oFormBuilder->setValue($aItems);

        $oFormBuilder->buttonRowUpdate('addUpdFormat');

        if ( CurrentAdmin::isSystemMode() )
            $oFormBuilder->buttonRowDelete('delFormat');

        /**
         * @todo передавать параметры через кнопку - это не ок ни разу, пересмотреть это место
         */
        $oFormBuilder->button('addUpdProfile', \Yii::t('gallery', 'tools_backToProfile'), 'icon-cancel', 'init', ['addParams' => ['data' => ['profile_id' => $aData['id']]]]);
        $oFormBuilder->buttonAddNew('addUpdFormat', \Yii::t('adm', 'add'), ['addParams' => ['data' => ['profile_id' => $aData['id']]]]);
        $oFormBuilder->enableDragAndDrop('sortFormats');

        $this->setInterface($oFormBuilder->getForm());

    }// func

    /**
     * Сортировка форматов
     */
    protected function actionSortFormats() {

        $aData = $this->get('data');
        $aDropData = $this->get('dropData');
        $sPosition = $this->get('position');

        if (!isSet($aData['id']) OR !$aData['id'] OR
            !isSet($aDropData['id']) OR !$aDropData['id'] OR !$sPosition)
                $this->addError('Ошибка! Неверно заданы параметры сортировки'); //@todo lang

        if (!gallery\Format::sortFormats($aData['id'], $aDropData['id'], $sPosition))
                $this->addError('Ошибка! Неверно заданы параметры сортировки');
    }

    /** Вывести список выравнивания водяного знака относительно изображения
     * @return array
    */
    private static function getWatermarkCalibrateList() {
        return [
            gallery\Config::alignWatermarkTopLeft => \Yii::t('gallery', 'water_top_left'),
            gallery\Config::alignWatermarkTopRight => \Yii::t('gallery', 'water_top_right'),
            gallery\Config::alignWatermarkBottomLeft => \Yii::t('gallery', 'water_bottom_left'),
            gallery\Config::alignWatermarkBottomRight => \Yii::t('gallery', 'water_bottom_right'),
            gallery\Config::alignWatermarkCenter => \Yii::t('gallery', 'water_center')
        ];
    }

    /**
     * @TODO дорефакторить
     * Состояние добавления/редактирования формата
     */
    protected function actionAddUpdFormat() {

        try {
            $oFormBuilder = ui\StateBuilder::newEdit();

            // Данные по формату
            $aData = $this->get('data');

            $iFormatId  = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;
            $iProfileId = $this->iCurrentProfile;

            $this->setPanelName(\Yii::t('gallery', 'tools_addFormat'),true);
   
            $this->setPanelName(\Yii::t('gallery', 'tools_addFormat'),true);
            if($iFormatId)
                $this->setPanelName(\Yii::t('gallery', 'tools_editFormat'),true);

            // Получаем данные формата или заготовку под новый формат
            $aValues = $iFormatId ? gallery\Format::getById($iFormatId) : gallery\Format::getFormatBlankValues(['profile_id' => $iProfileId]);

            // Установить набор элементов формы
            $oFormBuilder
                ->fieldString('title', \Yii::t('gallery', 'formats_title'))
                ->fieldString('name', \Yii::t('gallery', 'formats_name'))
                ->field('width', \Yii::t('gallery', 'formats_width'), 'int', ['minValue' => 0, 'allowDecimals'=>false])
                ->field('height', \Yii::t('gallery', 'formats_height'), 'int', ['minValue' => 0, 'allowDecimals'=>false])
                ->field('active', \Yii::t('gallery', 'formats_active'), 'check')
                ->fieldHide('id', '')
                ->field('resize_on_larger_side', \Yii::t('gallery', 'formats_resize_on_larger_side'), 'check')
                ->field('scale_and_crop', \Yii::t('gallery', 'formats_scale_and_crop'), 'check')
                ->field('use_watermark', \Yii::t('gallery', 'formats_use_watermark'), 'check')
                ->field('watermark', \Yii::t('gallery', 'formats_watermark'), 'file')
                ->fieldHide('profile_id', \Yii::t('gallery', 'formats_profile_id'), 'i')
                ->fieldSelect('watermark_align', \Yii::t('gallery', 'formats_watermark_align'), self::getWatermarkCalibrateList(), [], false)
                ->fieldHide('position', \Yii::t('gallery', 'formats_position'), 'i');

            // Установить значения для элементов
            $oFormBuilder->setValue($aValues);

            // Добавление кнопок
            $oFormBuilder->buttonSave('saveFormat');
            $oFormBuilder->buttonCancel('formatsList');

            if ($iFormatId AND CurrentAdmin::isSystemMode()) {
                $oFormBuilder->buttonSeparator('->');
                $oFormBuilder->getForm()->addBtnDelete('delFormat');
            }

            // вывод данных в интерфейс
            $this->setInterface( $oFormBuilder->getForm() );

        } catch (\Exception $e) {
            echo $e; //@todo echo ???
        }
    }// func

    /**
     * Сохраняет формат в профиле настроек
     * @throws \Exception
     */
    protected function actionSaveFormat() {

        /* Сохранение данных формата */

        $aData = $this->get('data');

        if (!count($aData)) throw new \Exception ('Error: Data is not send!');

        $iFormatId = ($aData['id'])? $aData['id']: false;

        /* Привязка к профилю обязательна */
        if ( !isSet($aData['profile_id']) OR (!$aData['profile_id'] = (int)$aData['profile_id']) ) throw new \Exception('Error: Data is not send!');

        // если водяной знак - файл, то проверить, что он png
        $sWatermark = isset($aData['watermark']) ? $aData['watermark'] : '';
        if ($sWatermark) {
            $sPossibleFileName = WEBPATH.$sWatermark;
            if (file_exists($sPossibleFileName)) {
                $aFile = getimagesize($sPossibleFileName);
                if ($aFile[2] !== 3)
                    $this->addError( \Yii::t('gallery', 'invalid_file_format_for_the_watermark') );
            }
        }

        // Добавляем либо обнавляем формат
        gallery\Format::setFormat($aData, $iFormatId);

        $this->addMessage( \Yii::t('gallery', 'tools_saveFormatMessage') );

        /* вывод списка */
        $this->actionFormatsList();

    }// func

    /**
     * Удаляет выбранный формат
     * @throws \Exception
     */
    protected function actionDelFormat() {

        /* Данные по формату */
        $aData = $this->get('data');

        try {
            if (!CurrentAdmin::isSystemMode())
                throw new \Exception('Нет прав на выполнение действия.');

            if (!isSet($aData['id']) OR (!$aData['id'] = (int)$aData['id'])) throw new \Exception('Error: Element(Format) is not removed!');

            /*Удаление формат*/
            gallery\Format::removeFormat($aData['id']);

        } catch (\Exception $e) {

            $this->addError( $e->getMessage() );
        }

        /*Вывод списка профилей*/
        $this->actionFormatsList();

    }// func

}
