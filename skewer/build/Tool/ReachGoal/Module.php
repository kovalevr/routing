<?php

namespace skewer\build\Tool\ReachGoal;

use skewer\base\ui;
use skewer\build\Tool;
use skewer\components\targets;
use skewer\components\ext;
use yii\base\UserException;

/**
 * Модуль управления Целями (ReachGoal)
 * Class Module
 * @package skewer\build\Tool\ReachGoal
 */
class Module extends Tool\LeftList\ModulePrototype {

    /** имя параметра для счетчика яндекса */
    const yaCounter = 'yaReachGoalCounter';
    
    /** имя параметра с js кодами для счетчиков */
    const jsCounters = 'countersCode';

    protected function actionInit() {

        $aTargets = targets\models\Targets::find()
            ->asArray()
            ->all();

        $oFormBuilder = new ui\StateBuilder();
        // создаем форму

        $oFormBuilder = $oFormBuilder::newList();
        $oFormBuilder
            ->field('id', 'ID', 'string', ['listColumns' => ['flex' => 1]])

            ->field('title', \Yii::t('ReachGoal', 'field_title'), 'string', ['listColumns' => ['flex' => 3]])
            ->field('type', \Yii::t('ReachGoal', 'field_type'), 'string', ['listColumns' => ['flex' => 3]])
            ->buttonRowUpdate()
            ->buttonRowDelete();

        $aTypes = targets\Creator::getTypes();
        foreach ($aTypes as $type){
            $oFormBuilder->buttonAddNew('showForm',\Yii::t('ReachGoal', 'btn_add_'.strtolower($type)),['addParams' => ['type' => strtolower($type)]]);
        }

        $oFormBuilder
            ->button('Settings',\Yii::t('ReachGoal', 'btn_settings'))
            ->button('ShowSelectors',\Yii::t('ReachGoal', 'btn_selectors'))
        ;

        $oFormBuilder->setValue($aTargets);

        $this->setInterface($oFormBuilder->getForm());

    }

    protected function actionShow(){
        $this->actionShowForm();
    }

    /**
     * Отображение формы для добавления Яндекс ReachGoal
     * @throws UserException
     */
    protected function actionShowForm( ) {

        $aData = $this->get('data');
        $iItemId = isset($aData['id']) ? (int)$aData['id'] : 0;

        if ($iItemId) {
            /** @var targets\types\Prototype $oType */
            $oType = targets\Creator::getObject($aData['type']);
            $oTargetRow = targets\models\Targets::findOne(['id' => $iItemId]);
        }else{
            /** @var targets\types\Prototype $oType */
            $oType = targets\Creator::getObject($this->get('type'));

            $oTargetRow = $oType->getNewTargetRow([]);
        }

        if ( !$oTargetRow )
            throw new UserException('Item not found');

        // -- сборка интерфейса
        $oFormBuilder = $oType->getFormBuilder($oTargetRow);
        $this->setInterface($oFormBuilder->getForm());

    }

    /**
     * Удаление цели
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        $oMatches = new targets\CheckTarget();

        if (!isset($aData['name'])){
            $aTarget = targets\models\Targets::find()
                        ->where(['id'=>$aData['id']])
                        ->one();
            $aData['name'] = $aTarget['name'];
        }

        $oMatches->sName = $aData['name'];
        \Yii::$app->trigger('target_delete', $oMatches);
        $aMatches = $oMatches->getList();

        if (!empty($aMatches))
            throw new UserException(\Yii::t('ReachGoal','used_in_').'<br>'.implode(',<br>',$aMatches));

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        targets\models\Targets::deleteAll(['id'=>$iItemId] );

        $this->actionInit();

    }

    /**
     * Сохранение данных
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );
        $iId = $this->getInDataValInt( 'id' );

        if ( !$aData )
            throw new UserException( 'Empty data' );

        if ( $iId ) {
            $oTargetRow = targets\models\Targets::findOne(['id'=>$iId]);
            if ( !$oTargetRow )
                throw new UserException( "Запись [$iId] не найдена" );
        } else {

            if (targets\Api::checkDuplicate($aData['name']))
                throw new UserException( \Yii::t('ReachGoal','target_exists') );

            /** @var targets\types\Prototype $oType */
            $oType = targets\Creator::getObject($aData['type']);

            $oTargetRow = $oType->getNewTargetRow($aData);

        }
        $oTargetRow->setAttributes($aData);

        $oTargetRow->save();

        // вывод списка
        $this->actionInit();

    }


    /**
     * Отображение интерфейса настроек
     */
    protected function actionSettings() {

        $aFields = targets\Creator::getParams();
        $oForm = ui\StateBuilder::newEdit();
        $aData = [];

        foreach ($aFields as $field){
            $oForm->field($field['name'],$field['title'],$field['type']);
            $aData[$field['name']] = $field['value'];
        }

        $oForm->buttonSave('saveSettings');
        $oForm->buttonCancel();

        $oForm->setValue($aData);

        $this->setInterface( $oForm->getForm() );
    }

    /**
     * Сохранение настроек
     */
    protected function actionSaveSettings() {

        $aInputData = $this->getInData();

        targets\Creator::setParams($aInputData);

        $this->actionInit();
    }


    /****************СОСТОЯНИЯ СЕЛЕКТОРОВ********************/

    /**
     * Список селекторов
     */
    protected function actionShowSelectors(){

        $aSelectors = targets\models\TargetSelectors::find()
            ->groupBy(['selector'])
            ->asArray()
            ->all();

        $oFormBuilder = new ui\StateBuilder();
        // создаем форму

        $oFormBuilder = $oFormBuilder::newList();
        $oFormBuilder
            ->field('title', \Yii::t('ReachGoal', 'field_title_selector'), 'string', ['listColumns' => ['flex' => 3]])
            ->field('selector', \Yii::t('ReachGoal', 'field_selector'), 'string', ['listColumns' => ['flex' => 3]])

            ->buttonRowUpdate('showSelector')
            ->buttonRowDelete('DeleteSelector')

            ->buttonAddNew('addSelector',\Yii::t('ReachGoal', 'btn_add_selector'))
            ->buttonBack('Init');
        ;

        $oFormBuilder->setValue($aSelectors);

        $this->setInterface($oFormBuilder->getForm());

    }

    /**
     * Добавление селектора
     * @throws UserException
     */
    protected function actionAddSelector() {
        $this->showEditFormSelector();
    }

    /**
     * Вывод формы
     * @throws UserException
     */
    private function showEditFormSelector( ) {

        $aData = $this->get('data');
        $sSelector = isset($aData['selector']) ? $aData['selector'] : '';

        /**
         * @var \skewer\components\targets\models\TargetSelectors $oTargetSelectors
         */

        if ($sSelector) {
            $aTargetSelectors = targets\models\TargetSelectors::find()
                ->where(['selector' => $sSelector])
                ->asArray()
                ->all();
            $aParams = [];
            $aParams['selector'] = $sSelector;
            foreach ($aTargetSelectors as $item){
                if (!isset($aParams[$item['type'].'_target'])){
                    $aParams[$item['type'].'_target'] = $item['name'];
                    $aParams['title'] = $item['title'];
                }
            }
        }else{
            $aParams = [];
        }

        // -- сборка интерфейса
        $oFormBuilder = new ui\StateBuilder();
        // создаем форму

        $aTypes = targets\Creator::getTypes();

        $oFormBuilder = $oFormBuilder::newEdit();
        $oFormBuilder->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('ReachGoal', 'field_title_selector'), 'string')
            ->field('selector', \Yii::t('ReachGoal', 'field_selector'), 'string');

        /*Перебираем все возможные типы событий и создаем селекторы*/
        foreach ($aTypes as $type){
            $oFormBuilder->fieldSelect(strtolower($type).'_target', \Yii::t('ReachGoal', 'field_'.strtolower($type).'_target'), targets\models\Targets::getByTypeArray($type));
        }

        $oFormBuilder->buttonSave('SaveSelector')
            ->buttonBack('ShowSelectors');
        
        $oFormBuilder->setValue($aParams);
        $this->setInterface($oFormBuilder->getForm());

    }

    /**
     * Сохранение селектора
     * @throws UserException
     */
    protected function actionSaveSelector() {

        // запросить данные
        $aData = $this->get( 'data' );

        if ( !$aData )
            throw new UserException( 'Empty data' );

        /*1. Уничтожим цели с тем же селектором*/
        targets\models\TargetSelectors::deleteAll(['selector'=>$aData['selector']]);
        /*2. Попробуем создать их*/
        foreach ($aData as $key=>$item){

            if (strpos($key,'_target')!==false){
                targets\models\TargetSelectors::addNewTarget($aData['selector'],str_replace('_target','',$key),$item,$aData['title']);
            }

        }

        \Yii::$app->clearAssets();
        // вывод списка
        $this->actionShowSelectors();

    }

    /**
     * Удаление селектора
     */
    protected function actionDeleteSelector() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $sSelector = ( is_array($aData) and isset($aData['selector']) ) ? $aData['selector'] : '';

        targets\models\TargetSelectors::deleteAll(['selector'=>$sSelector] );

        $this->actionShowSelectors();

        \Yii::$app->clearAssets();

    }

    /**
     * Форма селектора
     * @throws UserException
     */
    protected function actionShowSelector() {

        $this->showEditFormSelector();

    }


}