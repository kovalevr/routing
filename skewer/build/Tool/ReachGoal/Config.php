<?php

use skewer\build\Tool\LeftList;
use skewer\base\site\Layer;

/* main */
$aConfig['name']     = 'ReachGoal';
$aConfig['title']    = 'ReachGoal';
$aConfig['version']  = '2.000';
$aConfig['description']  = '';
$aConfig['revision'] = '0002';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;

$aConfig['events'][] = [
    'event' => 'target_delete',
    'class' => skewer\build\Tool\ReachGoal\Api::className(),
    'method' => 'checkTarget'
];

return $aConfig;
