<?php
namespace skewer\build\Tool\Patches;

use skewer\base\ui;
use skewer\build\Tool;
use skewer\components\config\UpdateException;
use yii\base\UserException;


class Module extends Tool\LeftList\ModulePrototype {

    protected $aListFields = array('patch_uid', 'install_date', 'description');

    protected function actionInit() {
        $this->actionList();
    }



    protected function actionList() {

        $this->setPanelName(\Yii::t('patches', 'availablepatches'));

        $oForm = ui\StateBuilder::newList();

        $oForm
            ->fieldString('patch_uid', \Yii::t('patches', 'patch_uid'))
            ->fieldString('install_date', \Yii::t('patches', 'install_date'), ['listColumns' => ['width' => 150]])
            ->fieldString('description', \Yii::t('patches', 'description'), ['listColumns' => ['flex' => 1]]);
        $aPaths = Api::getLocalList();

        $aItems = [];

        // но нам надо только за посление пол года
        foreach ($aPaths as $aItem) {
            $oDate = \DateTime::createFromFormat("Y-m-d H:i:s",$aItem['install_date']);

            if ($oDate && $oDate->diff(new \DateTime("now"))->m>6){
                break;
            } else $aItems[] = $aItem;
        }

        $oForm->setValue($aItems);
        $oForm->buttonRow('installPatchForm', \Yii::t('patches', 'install'), 'icon-install', 'edit_form');

        // вывод данных в интерфейс
        $this->setInterface($oForm->getForm());
    }



    protected function actionInstallPatchForm() {

        $oForm = ui\StateBuilder::newEdit();

        try {

            $this->setPanelName(\Yii::t('patches', 'installPatch'), false);
            $aData = $this->get('data');


            if (!isSet($aData['file']) OR
                empty($aData['file']) OR
                !isSet($aData['patch_uid']) OR
                empty($aData['patch_uid'])
            ) throw new UserException(\Yii::t('patches', 'patchError'));

            /* Относительный путь к директории обновления */

            $aVal = [
                'patch_file' => $aData['file'],
                'patch_uid' => $aData['patch_uid'],
                'status' => ($aData['is_install']) ? \Yii::t('patches', 'installed') . $aData['install_date'] : $aData['install_date'],
                'description' => $aData['description']
            ];

            $oForm
                ->fieldHide('patch_file', \Yii::t('patches', 'patch_file'), 's')
                ->field('patch_uid', \Yii::t('patches', 'patch_id'), 'show')
                ->field('status', \Yii::t('patches', 'status'), 'show')
                ->fieldIf(!empty($aData['description']), 'description', \Yii::t('patches', 'description'), 'show', [])

                ->setValue($aVal)

                ->buttonIf( /* Патч не устанавливали - разрешаем ставить */
                    !$aData['is_install'],
                    \Yii::t('patches', 'install'), 'installPatch','icon-install', 'allow_do',
                    ['actionText' => \Yii::t('patches', 'installText')]
                )
                ->buttonCancel('List')
                ->buttonSeparator('->');

        } catch (UserException $e) {

            $this->addError($e->getMessage());
        }
        $this->setInterface($oForm->getForm());

        return psComplete;

    }

    protected function actionInstallPatch() {

        try {

            $aData = $this->get('data');

            if (!isSet($aData['patch_file']) OR empty($aData['patch_file']))
                throw new UpdateException('Wrong parameters!');

            Api::installPatch($aData['patch_file']);

        } catch (UpdateException $e) {

            $this->addError($e->getMessage());
        }

        $this->actionList();

    }

}
