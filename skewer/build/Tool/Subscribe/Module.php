<?php

namespace skewer\build\Tool\Subscribe;


use skewer\base\SysVar;
use skewer\base\ui\StateBuilder;
use skewer\build\Page\Subscribe\ar\SubscribeMessage;
use skewer\build\Page\Subscribe\ar\SubscribeMessageRow;
use skewer\build\Page\Subscribe\ar\SubscribePosting;
use skewer\build\Page\Subscribe\ar\SubscribeTemplate;
use skewer\build\Page\Subscribe\ar\SubscribeUser;
use skewer\build\Page\Subscribe\ar\SubscribeUserRow;
use skewer\build\Tool;
use skewer\components\ext;
use skewer\components\i18n\Languages;
use skewer\components\i18n\ModulesParams;
use yii\base\UserException;
use yii\helpers\ArrayHelper;

class Module extends Tool\LeftList\ModulePrototype {

    protected $sLanguageFilter = '';
    protected $iOnPage = 30;
    protected $iPage = 0;

    /** @var array Поля настроек */
    protected $aSettingsKeys =
        [
            'mail.mailText',
            'mail.mailTitle',
            'mail.resultText',
            'mail.resultTitle',
            'mail.successText',
            'mail.successTitle',
            'mail.errorText',
            'mail.errorTitle',
        ];

    protected function preExecute() {

        $this->iPage = $this->getInt('page');

    }

    public function actionInit(){

        if (( $this->getConfigParam('mode.firstState') == 'user' ) or ($this->getInnerData('users_mode')))
            $this->actionUsers();
        else
            $this->actionList();
    }

    /********************************** USER *********************************/

    public function actionUsers($iPage = 0){

        if ($iPage) $this->iPage = $iPage;

        $iCount = SubscribeUser::find()
            ->getCount();

        $oList = StateBuilder::newList();

        $oList
            ->headText('<h1>'.\Yii::t('subscribe', 'list').'</h1>')

            ->field('id', 'ID', 'hide')
            ->field('email', 'E-mail', 'string', array('listColumns' => array('flex' => 1)));

            if (SysVar::get('subscribe_mode')==Api::withConfirm){
                $oList->fieldCheck('confirm', \Yii::t('subscribe', 'confirm'));
            }

            $aUsers = SubscribeUser::find()
                ->limit( $this->iOnPage, $this->iPage * $this->iOnPage)
                ->asArray()
                ->getAll();

            foreach ($aUsers as &$user){
                if ($user['confirm']!= '1'){
                    $user['confirm'] = '0';
                }
            }

        if ($iCount)
            $oList->setValue($aUsers, $this->iOnPage, $this->iPage, $iCount);
        else
            $oList->setValue($aUsers);

        $oList->buttonRowUpdate('editUser')
            ->buttonRowDelete('delUser','allow_do',\Yii::t('subscribe', 'deleteUser'));

        if ( $this->getConfigParam('mode.fullButtons') != false ) {
            $oList
                ->button('users', \Yii::t('subscribe', 'subscribers'))
                ->button('templates', \Yii::t('subscribe', 'templates'))
                ->button('list', \Yii::t('subscribe', 'subscribe_btn'));
        } else {
            $oList
                ->button('editTemplate', \Yii::t('subscribe', 'editTemplate'));
        }

        $oList
            ->buttonSeparator()
            ->buttonAddNew('editUser', \Yii::t('subscribe', 'addSubscriber'))
            ->buttonAddNew('importFormStep1',\Yii::t('subscribe', 'import_button'))
            ->buttonAddNew('exportForm',\Yii::t('subscribe', 'export_button'))
            ->button('SettingsForm', \Yii::t('subscribe', 'settings'));

        $oList->setEditableFields(['confirm'],'saveFromList');

        $this->setInnerData('users_mode', 1);
        $this->setInnerData('current_page',$this->iPage);

        $this->setInterface($oList->getForm());
    }

    protected function  actionSaveFromList(){

        $iId = $this->getInDataValInt( 'id' );

        $sFieldName = $this->get('field_name');

        $oRow = SubscribeUser::findOne(['id'=>$iId]);
        /** @var SubscribeUserRow $oRow */
        if ( !$oRow )
            throw new UserException( "Запись [$iId] не найдена" ); //@todo языковая метка

        $oRow->$sFieldName = $this->getInDataVal( $sFieldName );

        $oRow->save();

        $iPage = 0;

        if ($this->getInnerData('current_page'))
            $iPage = $this->getInnerData('current_page');

        $this->actionUsers($iPage);

    }

    public function actionSettingsForm(){

        $oForm = StateBuilder::newEdit();

        $oForm
            ->fieldSelect('mode',\Yii::t('subscribe', 'subscribe_mode'),Api::getModes(),[],false)
            ->buttonSave('saveSettings')
            ->buttonCancel('users');

        $oForm->setValue([
            'mode'=>(int)SysVar::get('subscribe_mode')
        ]);
        $this->setInterface($oForm->getForm());

        return psComplete;
    }

    public function actionSaveSettings(){

        $iMode = $this->getInDataValInt('mode');

        /**При переходе в режим "с подтверждением" всем старым поставим статус "подтвержден" */
        if ($iMode == Api::withConfirm){
            \Yii::$app->db->createCommand("UPDATE `subscribe_users` SET confirm=1 WHERE confirm=''")->execute();
        }

        SysVar::set('subscribe_mode',$iMode);

        $this->actionList();
    }

    public function actionEditUser() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;

        $oForm = StateBuilder::newEdit();
        $oForm->headText('<h1>'.\Yii::t('subscribe', 'editSubscriber').'</h1>');

        $aItems = array();
        if( $iItemId )
            $aItems = SubscribeUser::find($iItemId);

        $oForm
            ->field('id', 'ID', 'hide')
            ->field('email', 'E-mail', 'string')

            ->buttonSave('saveUser')
            ->buttonCancel('users');

        $oForm->setValue($aItems);
        $this->setInterface($oForm->getForm());

        return psComplete;
    }

    public function actionSaveUser() {

        $aData = $this->get('data');

        /*При добавлении пользователя из админки, он будет подтвержденным*/
        $aData['confirm'] = 1;

        try{
            // проверка входных данных
            if ( !isset($aData['email']) )
                throw new \Exception('email field not found');

            $sEmail = $aData['email'];

            // валидация поля email
            if ( !$sEmail )
                throw new \Exception(\Yii::t('subscribe', 'email_expected'));

            if ( !filter_var( $sEmail, FILTER_VALIDATE_EMAIL ) )
                throw new \Exception(\Yii::t('subscribe', 'invalid_email'));

            /** @var SubscribeUserRow $row */
            $row = SubscribeUser::load($aData);
            if ($row){
                $row->save();
            }

            $this->addModuleNoticeReport(\Yii::t('subscribe', 'saving_mailing_user'), "email: $sEmail");

        } catch ( \Exception $e ) {
            $this->addError($e->getMessage());
        }

        $iPage = 0;

        if ($this->getInnerData('current_page'))
            $iPage = $this->getInnerData('current_page');

        $this->actionUsers($iPage);
    }

    public function actionDelUser() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId ){
            SubscribeUser::delete($iItemId);
        }

        $iPage = 0;

        if ($this->getInnerData('current_page'))
            $iPage = $this->getInnerData('current_page');

        $this->actionUsers($iPage);
        $this->addModuleNoticeReport(\Yii::t('subscribe', 'deleting_mailing_user'), "User: $aData[email]");
    }

    /********************************** TEMPLATES *********************************/

    public function actionTemplates() {

        $oList = StateBuilder::newList();
        $oList->headText('<h1>'.\Yii::t('subscribe', 'subscribeList').'</h1>');

        $oList
            ->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('subscribe', 'title'), 'string', array('listColumns' => array('flex' => 1)));

        $aItems = SubscribeTemplate::find()->getAll();
        $oList->setValue($aItems);

        $oList->buttonRowUpdate('editTemplate');
        $oList->buttonRowDelete('delTemplate','allow_do',\Yii::t('subscribe', 'deleteTemplate'));

        $oList->button('users', \Yii::t('subscribe', 'subscribers'));
        $oList->button('templates', \Yii::t('subscribe', 'templates'));
        $oList->button('list', \Yii::t('subscribe', 'subscribe_btn'));
        $oList->buttonSeparator();
        $oList->buttonAddNew('editTemplate', \Yii::t('subscribe', 'addTemplate'));


        $this->setInterface($oList->getForm());
    }

    public function actionEditTemplate() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;

        $oForm = StateBuilder::newEdit();
        $oForm->headText('<h1>'.\Yii::t('subscribe', 'editTemplateH1').'</h1>');

        $info = Api::addTextInfoBlock();

        $oForm->headText('<h1>'.\Yii::t('subscribe', 'editTemplateH1').'</h1>'
            .'<div>'.$info['value'].'</div>');

        $oForm
            ->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('subscribe', 'title'), 'string')
            ->field('content', \Yii::t('subscribe', 'text'), 'wyswyg');

        if ($iItemId)
            $row = SubscribeTemplate::find($iItemId);
        else $row = SubscribeTemplate::getNewRow();

        $oForm->setValue($row);

        $oForm->useSpecSectionForImages();
        $oForm->buttonSave('saveTemplate');

        if ( !$this->getConfigParam('mode.multiTemplates') )
            $oForm->buttonCancel('users');
        else
            $oForm->buttonCancel('templates');

        $this->setInterface($oForm->getForm());
        $this->addModuleNoticeReport(\Yii::t('subscribe', 'saving_template'), "id $iItemId");
        return psComplete;

    }

    public function actionSaveTemplate() {

        $aData = $this->get('data');

        $row = SubscribeTemplate::load($aData);
        if ($row) {
            $row->save();
        }

        $this->addModuleNoticeReport(\Yii::t('subscribe', 'saving_template'), "id $aData[id]");
        if ( !$this->getConfigParam('mode.multiTemplates') )
            $this->actionUsers();
        else
            $this->actionTemplates();

    }

    public function actionDelTemplate() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId )
            SubscribeTemplate::delete($iItemId);

        $this->actionTemplates();
        $this->addModuleNoticeReport(\Yii::t('subscribe', 'deleting_template'), "id $iItemId");

    }


    /********************************** SUBSCRIBE *********************************/

    public function actionList($iPage = 0) {

        if ($iPage) $this->iPage = $iPage;

        $oList = StateBuilder::newList();
        $oList->headText('<h1>'.\Yii::t('subscribe', 'subList').'</h1>');

        $oList
            ->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('subscribe', 'title'), 'string', array('listColumns' => array('flex' => 1)))
            ->field('status', \Yii::t('subscribe', 'status'), 'string', array('listColumns' => array('flex' => 1)));

        $iCount = SubscribeMessage::find()
            ->getCount();

        $aItems = SubscribeMessage::find()
            ->order('id','DESC')
            ->limit( $this->iOnPage, $this->iPage * $this->iOnPage)
            ->asArray()
            ->getAll();

        foreach($aItems as $iKey=>$aItem){
            $aItems[$iKey]['status'] = Api::getStatusName( $aItem['status'] );
        }

        if ($iCount)
            $oList->setValue($aItems, $this->iOnPage, $this->iPage, $iCount);
        else
            $oList->setValue($aItems);

        $oList
            ->buttonRowUpdate('editSubscribe')
            ->buttonRowDelete('delSubscribe','delete',\Yii::t('subscribe', 'subListText'))

            ->button('users', \Yii::t('subscribe', 'subscribers'))
            ->button('templates', \Yii::t('subscribe', 'templates'))
            ->button('list', \Yii::t('subscribe', 'subscribe_btn'))
            ->buttonSeparator()
            ->buttonAddNew('addSubscribeStep1', \Yii::t('subscribe', 'addSubscribe'))
            ->button('SettingsForm', \Yii::t('subscribe', 'settings'));
        ;

        if (SysVar::get('subscribe_mode')==Api::withConfirm){
            $oList->buttonEdit('settings', \Yii::t('subscribe','language_settings'));
        }

        $this->setInnerData('current_page',$this->iPage);
        $this->setInnerData('users_mode',0);

        $this->setInterface($oList->getForm());

    }

    public function actionAddSubscribeStep1() {

        $oForm = StateBuilder::newEdit();

        $oForm->headText('<h1>'.\Yii::t('subscribe', 'getTemplate').'</h1>');
        /* установить набор элементов формы */
        $aModel = Api::getChangeTemplateInterface();

        $oForm->fieldSelect('tpl',\Yii::t('subscribe', 'letter_template_title'),$aModel,[],false);

        $oForm->button(
            'addSubscribeStep2',
            \Yii::t('subscribe', 'next'),
            ext\docked\Api::iconNext,
            'addSubscribeStep2',
            ['unsetFormDirtyBlocker'=>true]
            );

        $oForm->buttonCancel();

        $this->setInterface($oForm->getForm());

        return psComplete;

    }

    public function actionAddSubscribeStep2() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['tpl']) ? $aData['tpl'] : false;

        $info = Api::addTextInfoBlock();

        $oForm = StateBuilder::newEdit();
        $oForm->headText('<h1>'.\Yii::t('subscribe', 'editSubscribeText').'</h1>'.
            "<div>".$info['value']."</div>"
        );

        $oForm
            ->field('id', 'id', 'hide')
            ->field('title', \Yii::t('subscribe', 'title'), 'string')
            ->field('text', \Yii::t('subscribe', 'text'), 'wyswyg')
            ->fieldSelect('status', \Yii::t('subscribe', 'status'), Api::getStatusArr(), ['disabled' => true], false)
            ->field('template', \Yii::t('subscribe', 'template'), 'hide')
        ;

        $aItems = array();
        $aTempItems =  SubscribeTemplate::find()->where('id',$iItemId)->asArray()->getOne();

        $aItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aItems['text'] = isSet($aTempItems['content']) ? $aTempItems['content'] : '';
        $aItems['status'] = Api::statusFormation;
        $aItems['template'] = $iItemId;

        $oForm->setValue($aItems);

        $oForm->buttonSave('saveSubscribe');
        $oForm->buttonCancel('addSubscribeStep1');
        $oForm->useSpecSectionForImages();
        $this->setInterface($oForm->getForm());

        return psComplete;
    }

    public function actionEditSubscribe() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;
        if( !$iItemId )
            throw new \Exception (\Yii::t('subscribe', 'wrong_subscribe_id'));

        $oForm = StateBuilder::newEdit();

        $info = Api::addTextInfoBlock();
        $oForm->headText('<h1>'.\Yii::t('subscribe', 'editSubscribe').'</h1>'.
            "<div>".$info['value']."</div>");

        $aTempItems = SubscribeMessage::find()->where('id',$iItemId)->asArray()->getOne();

        $bTypeView = true;
        // блокироум редактирование по статусу
        if( !isSet($aTempItems['status']) ||  $aTempItems['status'] != Api::statusFormation ){
            $bTypeView = false;
        }

        $oForm
            ->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('subscribe', 'title'), ($bTypeView) ? 'string' : 'show')
            ->field('text', \Yii::t('subscribe', 'text'), ($bTypeView) ? 'wyswyg' : 'show')
            ->fieldSelect('status', \Yii::t('subscribe', 'status'), Api::getStatusArr(), ['disabled' => true])
            ->field('template', \Yii::t('subscribe', 'template'), 'hide')
        ;

        $aTempItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aTempItems['text'] = isSet($aTempItems['text']) ? $aTempItems['text'] : '';
        $aTempItems['template'] = $iItemId;

        $oForm->setValue($aTempItems);

        if( isSet($aTempItems['status']) && $aTempItems['status'] == Api::statusFormation ) {

            $oForm->button('sendSubscribeForm', \Yii::t('subscribe', 'send'), 'icon-commit', 'init');
            $oForm->buttonSeparator();
            $oForm->buttonSave( 'saveSubscribe' );
        }
        $oForm->useSpecSectionForImages();
        $oForm->buttonCancel('list');

        $this->setInterface($oForm->getForm());
        return psComplete;
    }

    public function actionSaveSubscribe() {

        $aData = $this->get('data');

        /** @var SubscribeMessageRow $row  */
        $row = SubscribeMessage::load($aData);
        if ($row){
            $row->status = Api::statusFormation;
            $row->save();
        }

        $this->addModuleNoticeReport(\Yii::t('subscribe', 'saving_mailing'), "id $row->id");
        $iPage = 0;

        if ($this->getInnerData('current_page'))
            $iPage = $this->getInnerData('current_page');

        $this->actionList($iPage);
    }

    public function actionDelSubscribe() {

        $aData = $this->get('data');
        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId ){
            SubscribePosting::delete()->where('id_body',$iItemId)->get();
            SubscribeMessage::delete($iItemId);
        }

        $this->addModuleNoticeReport(\Yii::t('subscribe', 'deleting_mailing'), "id $iItemId");
        $iPage = 0;

        if ($this->getInnerData('current_page'))
            $iPage = $this->getInnerData('current_page');

        $this->actionList($iPage);
    }

    public function actionSendSubscribeForm() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;
        if( !$iItemId )
            throw new \Exception (\Yii::t('subscribe', 'wrong_subscribe_id'));


        $oForm = StateBuilder::newEdit();
        $oForm->headText('<h1>'.\Yii::t('subscribe', 'sending').'</h1>');
        $oForm
            ->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('subscribe', 'title'), 'show')
            ->field('text', \Yii::t('subscribe', 'text'), 'show')
            ->field('test_mail', \Yii::t('subscribe', 'test_email_title'), 'string', array('subtext' => \Yii::t('subscribe', 'testMail')))
        ;

        $aTempItems = SubscribeMessage::find()->where('id',$iItemId)->asArray()->getOne();
        $aTempItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aTempItems['text'] = isSet($aTempItems['text']) ? $aTempItems['text'] : '';

        $oForm->setValue($aTempItems);

        $oForm->button('sendSubscribe', \Yii::t('subscribe', 'sendSubscribers'), 'icon-commit', 'allow_do', array('actionText' => \Yii::t('subscribe', 'sendSubscribersText')));
        $oForm->button('sendToEmailSubscribe', \Yii::t('subscribe', 'testMailText'), 'icon-commit', 'init', array('doNotUseTimeout' => true));

        $oForm->buttonCancel();

        $oForm->setTrackChanges(false);

        $this->setInterface($oForm->getForm());
        return psComplete;

    }

    public function actionSendSubscribe() {

        try{

            $aData = $this->get('data');

            $iItemId = iSset($aData['id']) ? $aData['id'] : false;

            if( !$iItemId )
                throw new \Exception (\Yii::t('subscribe', 'wrong_message_id'));

            $iSubId = Api::addMailer($iItemId);
            if( !$iSubId )
                throw new \Exception (\Yii::t('subscribe', 'not_added'));
            /** @var SubscribeMessageRow $row */
            $row = SubscribeMessage::find($iItemId);
            if ($row) {
                $row->status = Api::statusWaiting;
                $row->save();
            }

            Api::makeTask( $iSubId );

            $this->addMessage(\Yii::t('subscribe', 'mailing_created'));
            $this->addModuleNoticeReport(\Yii::t('subscribe', 'log_create_mailing'));

        } catch ( \Exception $e ) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

        return psComplete;

    }

    public function actionSendToEmailSubscribe() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;
        $sTestMail = iSset($aData['test_mail']) ? $aData['test_mail'] : false;

        if( !$iItemId )
            throw new UserException(\Yii::t('subscribe', 'wrong_message_id'));

        if ( !$sTestMail )
            throw new UserException( \Yii::t('subscribe', 'email_expected') );

        if( !Api::sendTestMailer($iItemId, $sTestMail) )
            throw new \Exception (\Yii::t('subscribe', 'test_messages_fail'));

        $this->addMessage(\Yii::t('subscribe', 'test_email_sended'));
        $this->addModuleNoticeReport(\Yii::t('subscribe', 'test_email_sended'));

        $this->actionList();

        return psComplete;

    }


    /**
     * Форма настроек модуля
     */
    protected function actionSettings(){

        $oFormBuilder = StateBuilder::newEdit();

        $this->sLanguageFilter = $this->get('filter_language',\Yii::$app->i18n->getTranslateLanguage());

        $aLanguages = Languages::getAllActive();
        $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

        if (count($aLanguages) > 1) {
            $oFormBuilder->filterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
            $oFormBuilder->setFilterAction('settings');
        }


        $oFormBuilder
            ->field('info', '', 'show', ['hideLabel' => true])
            ->fieldString('mail.mailTitle', \Yii::t('subscribe', 'mailTitle'))
            ->field('mail.mailText', \Yii::t('subscribe', 'mailText'), 'wyswyg')
            ->fieldString('mail.resultTitle', \Yii::t('subscribe', 'resultTitle'))
            ->field('mail.resultText', \Yii::t('subscribe', 'resultText'), 'wyswyg')
            ->fieldString('mail.successTitle', \Yii::t('subscribe', 'successTitle'))
            ->field('mail.successText', \Yii::t('subscribe', 'successText'), 'wyswyg')
            ->fieldString('mail.errorTitle', \Yii::t('subscribe', 'errorTitle'))
            ->field('mail.errorText', \Yii::t('subscribe', 'errorText'), 'wyswyg')


        ;

        $aModulesData = ModulesParams::getByModule('subscribe', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        foreach( $this->aSettingsKeys as  $key ){
            $aItems[$key] = (isset($aModulesData[$key]))?$aModulesData[$key]:'';
        }

        $oFormBuilder
            ->buttonSave('saveMessageSettings')
            ->buttonCancel()
        ;

        $aItems['info'] = \Yii::t('subscribe', 'head_mail_text',  [\Yii::t('app', 'site_label', [], $this->sLanguageFilter),
                \Yii::t('app', 'url_label', [], $this->sLanguageFilter),
                \Yii::t('review', 'label_user', [], $this->sLanguageFilter)
                , $this->sLanguageFilter]
            , $this->sLanguageFilter
        );

        $oFormBuilder->setValue($aItems);

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Сохраняем настройки формы
     */
    protected function actionSaveMessageSettings(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->aSettingsKeys))
                    continue;

                ModulesParams::setParams( 'subscribe', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

    /***************************Импорт подписчиков******************************/

    /**
     * Показ формы выбора источника для импорта
     */
    protected function actionImportFormStep1(){

        $oList = StateBuilder::newEdit();

        $oList->button(
            'ImportForm',
            \Yii::t('subscribe', 'next'),
            ext\docked\Api::iconNext,
            'ImportForm',
            ['unsetFormDirtyBlocker'=>true]
        )
            ->buttonCancel();

        $oList->fieldSelect('mode',\Yii::t('subscribe', 'mode_title'),[
            'csv'=>\Yii::t('subscribe','csv_file'),
            'text'=>\Yii::t('subscribe','text')
        ],[],false);
        $oList->setValue(['mode'=>'csv']);
        $this->setInterface($oList->getForm());
    }

    /**
     * Форма импорта
     */
    protected function actionImportForm(){

        $sMode = $this->getInDataVal('mode','text');

        $oList = StateBuilder::newEdit();
        $oList->buttonSave('import')
            ->buttonCancel();

        $sClassName = 'skewer\build\Tool\Subscribe\import\Type'.strtoupper($sMode);
        /** @var \skewer\build\Tool\Subscribe\import\Prototype $oProvider */
        $oProvider = new $sClassName();

        $oList = $oProvider->getFields($oList);

        $this->setInnerData('mode',$sMode);
        $this->setInterface($oList->getForm());
    }

    /*
     * импорт
     */
    protected function actionImport(){

        $sMode = $this->getInnerData('mode');

        $sClassName = 'skewer\build\Tool\Subscribe\import\Type'.strtoupper($sMode);
        /** @var \skewer\build\Tool\Subscribe\import\Prototype $oProvider */
        $oProvider = new $sClassName();

        $oProvider->validate($this->getInData());

        $oProvider->import($this->getInData());

        $this->addMessage(\Yii::t('subscribe','import_result'),\Yii::t('subscribe','import_result_text',[
            'iCount'=>$oProvider->iCount,
            'iSuccess'=>$oProvider->iSuccess,
            'iFailed'=>$oProvider->iFailed
            ]
        ));

        $oList = StateBuilder::newEdit();

        $aData['import_result'] = \Yii::$app->getView()->renderFile(__DIR__.'/templates/import_results.php',[
            'results'=>\Yii::t('subscribe','import_result_text',[
                    'iCount'=>$oProvider->iCount,
                    'iSuccess'=>$oProvider->iSuccess,
                    'iFailed'=>$oProvider->iFailed
                ]
            ),
            'log'=>$oProvider->aLog
            ]
        );

        $oList->fieldShow('import_result',\Yii::t('subscribe', 'import_result'));
        $oList->setValue($aData);

        $oList->buttonCancel('users');
        $this->setInterface($oList->getForm());
    }


    /*****************Экспорт подписчиков******************/
    protected function actionExportForm(){

        $oList = StateBuilder::newEdit();

        $oList->button(
            'export',
            \Yii::t('subscribe', 'next'),
            ext\docked\Api::iconNext,
            'Export',
            ['unsetFormDirtyBlocker'=>true]
        )
            ->buttonCancel();

        $oList->fieldSelect('mode',\Yii::t('subscribe', 'mode_title'),[
            'csv'=>\Yii::t('subscribe','csv_file'),
            'text'=>\Yii::t('subscribe','txt_file')
        ],[],false);
        $oList->setValue(['mode'=>'csv']);
        $this->setInterface($oList->getForm());
    }


    protected function actionExport(){

        $sMode = $this->getInDataVal('mode');

        $sClassName = 'skewer\build\Tool\Subscribe\import\Type'.strtoupper($sMode);
        /** @var \skewer\build\Tool\Subscribe\import\Prototype $oProvider */
        $oProvider = new $sClassName();

        $sFileHash  = $oProvider->export($sMode);

        $oList = StateBuilder::newEdit();

        $oList->buttonCancel();

        $oList->fieldShow('out_file',\Yii::t('subscribe','out_file'));

        $this->setInterface($oList->getForm());

        $aParams = [
            'file_hash'=>$sFileHash,
            'mode'=>$sMode
        ];

        $oList->setValue(['out_file'=>'<a target="_blank" href="http://'.WEBROOTPATH.'download/?'.http_build_query($aParams).'">File</a>']);

        $this->setInterface($oList->getForm());
    }
}//class