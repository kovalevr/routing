<?php

use skewer\build\Tool\LeftList;
use skewer\base\site\Layer;

$aConfig['name']     = 'Subscribe';
$aConfig['title']    = 'Рассылка';
$aConfig['version']  = '2';
$aConfig['description']  = 'Админ-интерфейс управления рассылкой новостей';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;
$aConfig['useNamespace'] = true;
$aConfig['dependency'] = [
    ['Subscribe', Layer::PAGE],
];

/**
 * @todo что должны делать эти настройки?
 */
$aConfig['mode'] = array(
    'firstState' => 'list', // допустимые состояния: 'user' / 'list'
    'fullButtons' => true,
    'multiTemplates' => true,
    'extFields' => true,
);

return $aConfig;
