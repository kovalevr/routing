<?php

namespace skewer\build\Tool\LeftList;

use skewer\build\Cms;
use skewer\components\i18n\Languages;
use skewer\components\auth\CurrentAdmin;
use skewer\base\SysVar;

/**
 * Родительский класс для модулей панели управления
 */
abstract class ModulePrototype extends Cms\Tabs\ModulePrototype implements ModuleInterface {


    public static function updateLanguage()
    {
        //@todo хак
        $sLanguage = \Yii::$app->i18n->getTranslateLanguage();
        $oLanguage = Languages::getByName($sLanguage);
        if (!$oLanguage || !$oLanguage->active) {
            $sLanguage = SysVar::get('language');
        }
        \Yii::$app->language = $sLanguage;
    }


    /**
     * @inheritDoc
     */
    public function init()
    {
        self::updateLanguage();

        parent::onCreate();

        parent::init();
    }


    /**
     * Проверка доступа
     */
    final protected function checkAccess() {

        CurrentAdmin::testControlPanelAccess();
        CurrentAdmin::testUsedModule( $this->getModuleName() . ($this->useNamespace() ? '' : 'ToolModule') );

    }

    function getName() {
        return $this->getModuleName();
    }

}
