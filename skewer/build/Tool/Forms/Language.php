<?php

$aLanguage = array();

$aLanguage['ru']['Forms.Tool.tab_name'] = 'Формы';

$aLanguage['ru']['editFormReport'] = "Форма в Конструктор форм изменена";
$aLanguage['ru']['deleteFormReport'] = "Форма в Конструктор форм удалена";
$aLanguage['ru']['sortParamError'] = 'Ошибка! Неверно заданы параметры сортировки';
$aLanguage['ru']['defaultDesc'] =  "<ul>
        <li> • Текстовое поле - строка</li>
        <li> • Параметр галочка - поле \"значение по умолчанию\" должно быть обязательно заполнено текстом, который будет отправлен при установленной галочке!</li>
        <li> • Выпадающий список, переключатели - строка вида <b>value:title;</b> (каждая запись с новой строки)</li>
        <li> • Выпадающий список, переключатели - строка вида <b>ClassName.methodName()</b></li>
        <li> • Где <b>methodName()</b> - публичный метод класса, принимающий необязательный аргумент-массив</li>
    </ul>";
$aLanguage['ru']['maxlength_desc'] = 'Максимальная длина в символах для текстовых полей. Для поля "Загрузка файла" - максимальный размер загружаемого файла в мегабайтах. (Ограничение {0, number}Мб)';
$aLanguage['ru']['deleteButton'] = 'Удалить';
$aLanguage['ru']['license'] = 'Лицензионное соглашение';
$aLanguage['ru']['label_position'] = 'Позиционирование заголовка';
$aLanguage['ru']['new_line'] = 'Вывод с новой строки';
$aLanguage['ru']['width_factor'] = 'Множитель ширины';
$aLanguage['ru']['manParamsDesc'] = '<ul><li> • защита от заполнения <b>readonly="readonly"</b></li>
<li> • подсказка для ввода телефона +7 () _-_-__ <b>data-mask="phone"</b></li>
<li> • текст подсказки <b>placeholder="Текст"</b>, где вместо <i>Текст</i> добавляем нужную фразу</li>
<li> • скрытие текста placeholder при наведении <b>data-hide-placeholder="1"</b></li>
</ul>';
$aLanguage['ru']['form_field'] = 'Поле из формы';
$aLanguage['ru']['card_field'] = 'Поле из карточки товара';
$aLanguage['ru']['from_settings'] = 'Настройка формы';
$aLanguage['ru']['from_cloned'] = 'Форма успешно клонирована';
$aLanguage['ru']['head_mail_text'] = 'Метки для замены:<br>
[{0}] - Название сайта<br>
[{1}] - Адрес сайта
';

$aLanguage['ru']['form_send_crm'] = 'Отправлять данные в CRM';
$aLanguage['ru']['field_modifydate'] = 'Дата последнего изменения';
$aLanguage['ru']['show_phrase_required_fields'] = "Вывод фразы \"* - обязательные для заполнения поля\"";
$aLanguage['ru']['error_form_not_found'] = 'Не найдена форма!';
$aLanguage['ru']['form_succ_answer'] = 'Текст после успешной отправки формы';
$aLanguage['ru']['form_show_header'] = 'Выводить заголовок формы';
$aLanguage['ru']['invalid_email'] = 'Некорректный e-mail';
$aLanguage['ru']['invalid_method'] = 'Некорректный метод';
$aLanguage['ru']['no_method'] = 'Метод не существует или не может быть вызван';
$aLanguage['ru']['no_class'] = 'Класс не существует';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['Forms.Tool.tab_name'] = 'Forms';

$aLanguage['en']['editFormReport'] = "Form in Form Designer changed" ;
$aLanguage['en']['deleteFormReport'] = "Form in Form Designer deleted" ;
$aLanguage['en']['sortParamError'] = 'Error! Incorrectly set collation';
$aLanguage['en']['defaultDesc'] = "<ul>
        <li> • Text field - string </li>
        <li> • Parameter check - the \"default\" must be filled text that will be sent at the set tick ! </li>
        <li> • The drop-down list , the switches - a string of the form <b> value:title; </b> ( each entry on a new line ) </li>
        <li> • The drop-down list , the switches - a string of the form <b> ClassName.methodName() </b> </li>
        <li> • Where <b> methodName()</b> - public class method that accepts an optional argument - array </li>
    </ul> ";
$aLanguage['en']['maxlength_desc'] = 'The maximum length in characters for text fields. For the "File Download" - the maximum upload file size in megabytes. (Restriction {0, number}Mb)';
$aLanguage['en']['deleteButton'] = 'Delete';
$aLanguage['en']['license'] = 'License agreement';
$aLanguage['en']['label_position'] = 'Label position';
$aLanguage['en']['new_line'] = 'Field with new line';
$aLanguage['en']['width_factor'] = 'Width factor';
$aLanguage['en']['manParamsDesc'] = '<ul><li> • protection from filling <b>readonly="readonly"</b></li>
<li> • prompt to enter your phone +7 () _-_-__ <b>data-mask="phone"</b></li>
<li> • for placeholder enter <b>placeholder="Text"</b></li>
<li> • to hide placeholder on select or mouse over - enter <b>data-hide-placeholder="1"</b></li>
</ul>';
$aLanguage['en']['form_field'] = 'Field from form';
$aLanguage['en']['card_field'] = 'Field from card';
$aLanguage['en']['from_settings'] = 'Form settings';
$aLanguage['en']['from_cloned'] = 'Form successfully cloned';
$aLanguage['en']['head_mail_text'] = 'Tags for replacement:<br>
[{0}] - Site Title<br>
[{1}] - Site Address
';

$aLanguage['en']['form_send_crm'] = 'Send to CRM';
$aLanguage['en']['field_modifydate'] = 'Last modify date';
$aLanguage['en']['show_phrase_required_fields'] = "Show phrase \"* - required fields\"";
$aLanguage['en']['error_form_not_found'] = 'Not found form!';
$aLanguage['en']['form_succ_answer'] = 'Text after successfully sending';
$aLanguage['en']['form_show_header'] = 'Show form header';

$aLanguage['en']['invalid_email'] = 'Invalid e-mail';
$aLanguage['en']['invalid_method'] = 'Invalid method';
$aLanguage['en']['no_method'] = 'Unknown method';
$aLanguage['en']['no_class'] = 'Unknown class';

return $aLanguage;