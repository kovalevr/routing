<?php

namespace skewer\build\Tool\Poll;

use skewer\base\ui;
use skewer\build\Tool;
use skewer\build\Tool\Poll\models\Poll;
use skewer\build\Tool\Poll\models\PollAnswer;
use yii\base\UserException;

/**
 * Class Module
 * @package skewer\build\Tool\Poll
 */
class Module extends Tool\LeftList\ModulePrototype{

    protected $iSectionId;
    protected $iPage = 0;
    protected $iOnPage = 0;
    protected $iCurrentPoll = 0;

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        $this->iCurrentPoll = $this->getInt('poll_id', 0);
    }

    protected function actionInit() {

        $this->actionList();
    }

    /**
     * Список опросов
     */
    protected function actionList() {

        $this->iCurrentPoll = 0;
        $oList = ui\StateBuilder::newList();
        $oList

            ->field('title', \Yii::t('Poll', 'title'), 'string', ['listColumns' => ['flex' => 3]])
            ->field('question', \Yii::t('Poll', 'question'), 'string', ['listColumns' => ['flex' => 2]])
            ->field('section', \Yii::t('Poll', 'section'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('active', \Yii::t('Poll', 'active'), 'check', ['listColumns' => ['width' => 70]])
            ->field('on_main', \Yii::t('poll', 'onMainTitle'), 'check', ['listColumns' => ['width' => 70]])
            ->field('on_allpages', \Yii::t('Poll', 'onAllTitle'), 'check', ['listColumns' => ['width' => 50]])
            ->field('on_include', \Yii::t('Poll', 'onInternalTitle'), 'check', ['listColumns' => ['width' => 90, 'align' => 'center']])

            ->buttonRowUpdate()
            ->buttonRowDelete()
            ->setGroups('locationTitle')    // разбиение на группы (горизонтальная полоска с названием зоны вывода)
            ->enableDragAndDrop('sortPolls')

            ->buttonAddNew('show');         // Кнопка "Добавить" в боковой колонке

        $aItems = Api::getPollList();
        $oList->setValue( $aItems['items'] );

        $this->setInterface($oList->getForm());

    }

    /** Сортировка опросов */
    protected function actionSortPolls() {

        $aData     = $this->get('data');
        $aDataDrop = $this->get('dropData');
        $sPosition = $this->get('position', 'before');

        $iItemId =$aData['id'];
        $iTargetItemId = $aDataDrop['id'];

        if ( !$iItemId or !$iTargetItemId or !$sPosition )
            throw new \Exception("Error getting objects id's or sorting position!");

        Api::sortPolls($iItemId, $iTargetItemId, $sPosition);

        $this->actionList();
    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // читаем запись заголовка голосоваия
        // если запись новая - заполняем значениями по умолчанию

        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;
        if ( !$iItemId ) $iItemId = $this->iCurrentPoll;

        $data = $iItemId ? Api::getPollById($iItemId): Api::getPollBlankValues();

        // собираем структуру интерфейса

        $oForm = ui\StateBuilder::newEdit()
            ->fieldHide('id',           \Yii::t('Poll','field_id'))
            ->fieldString('title',      \Yii::t('Poll','title'))
            ->fieldString('question',   \Yii::t('Poll','question'))
            ->fieldSelect('location',   \Yii::t('Poll','location'), Api::getPollLocations(), [], false)
            ->fieldSelect('section',    \Yii::t('Poll','section'),  Api::getSectionTitle(), [], false)
            ->fieldCheck('active',      \Yii::t('Poll','active'))
            ->fieldCheck('on_main',     \Yii::t('poll','onMainTitle'))
            ->fieldCheck('on_allpages', \Yii::t('Poll','onAllTitle'))
            ->fieldCheck('on_include',  \Yii::t('Poll','onInternalTitle'))

            // добавление кнопок в боковую (слева) колонку
            ->buttonSave()
            ->buttonCancel();

        if ( $iItemId ){

            // кнопка на список с вариантами ответов
            $oForm->button(
                'answerList', \Yii::t('poll', 'answersTitle'), 'icon-edit', 'answerList'
            );

            $this->iCurrentPoll = $iItemId;

            $oForm
                ->buttonSeparator('->')
                ->buttonDelete();
        }

        // вносим данные и выводим форму

        $oForm->setValue($data);
        $this->setInterface($oForm->getForm());

    }

    /**
     * Список ответов
     */
    protected function actionAnswerList() {

        $oList = ui\StateBuilder::newList();
        $oList

            ->field('title', \Yii::t('Poll', 'answer_title'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('value', \Yii::t('Poll', 'answer_value'), 'string')

            ->buttonRowUpdate('showAnswerForm')
            ->buttonRowDelete('answerDelete')

            // кнопки "добавить" и "назад" в левой боковой колонке
            ->buttonAddNew('showAnswerForm')
            ->buttonCancel('show', \Yii::t('poll', 'back'))
            ->enableDragAndDrop('sortAnswers')
        ;

        // определяем текущее голосование
        // todo переписать на однозначную протяжку id голосования вместо каскада поиска по трем местам

        $aData = $this->get( 'data' );
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        if ( !$iItemId )
            $iItemId = (isset($aData['parent_poll']) ) ? (int)$aData['parent_poll'] : $iItemId;

        if ( !$iItemId )
            $iItemId = $this->iCurrentPoll;


        // добавление набора данных и вывод в интерфейс

        $aItems = Api::getAnswerList($iItemId);

        foreach ($aItems as &$aItem)
            $aItem['title'] = \yii\helpers\Html::encode($aItem['title']);

        $oList->setValue($aItems);
        $this->setInterface($oList->getForm());

    }

    /** Сортировка ответов */
    protected function actionSortAnswers() {

        $aData     = $this->get('data');
        $aDataDrop = $this->get('dropData');
        $sPosition = $this->get('position', 'before');

        $iItemId =$aData['answer_id'];
        $iTargetItemId = $aDataDrop['answer_id'];

        if ( !$iItemId or !$iTargetItemId or !$sPosition )
            throw new \Exception("Error getting objects id's or sorting position!");

        Api::sortAnswers($iItemId, $iTargetItemId, $sPosition);

        $this->actionAnswerList();
    }

    /**
     * Отображение формы редактирования ответа
     */
    protected function actionShowAnswerForm() {

        $oForm = ui\StateBuilder::newEdit()

            ->fieldHide('answer_id', 'id')
            ->fieldString('title', \Yii::t('Poll','answer_title'), ['listColumns' => ['flex' => 1]])
            ->fieldInt('value',    \Yii::t('Poll','answer_value'), ['minValue' => 0])

            ->buttonSave('answerSave')
            ->buttonCancel('answerList');

        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['answer_id'])) ? (int)$aData['answer_id'] : 0;

        if($iItemId){
            $aItem = Api::getAnswerById($iItemId);
            $oForm
                ->buttonSeparator('->')
                ->buttonDelete('answerDelete');
        }
        else {
            $aItem = Api::getAnswerBlankValues();
        }

        $oForm->setValue($aItem);
        $this->setInterface($oForm->getForm());

    }

    /**
     * Сохранение опроса
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // есть данные - сохранить
        if ( $aData )
            Api::updPoll( $aData );

        if ( isset($aData['id']) && $aData['id'] )
            $this->addModuleNoticeReport(\Yii::t('poll', 'editPoll'), $aData);
        else {
            unset($aData['id']);
            $this->addModuleNoticeReport(\Yii::t('poll', 'addPoll'), $aData);
        }

        // вывод списка
        $this->actionList();

    }

    /** Действие: Сохранение варианта ответа */
    protected function actionAnswerSave() {

        // запросить данные
        $aData = $this->get( 'data' );
        $iId = $this->getInDataValInt( 'answer_id' );

        $aData['parent_poll'] = $this->iCurrentPoll;

        if ( !$aData )
            throw new UserException(\Yii::t('poll','error_empty_data'));

        if ( $iId ) {
            if (!$oAnswer = PollAnswer::findOne($iId))
                throw new UserException(\Yii::t('poll','error_row_not_found', [$iId]));
        } else{
            $oAnswer = new PollAnswer();
        }
        
        $oAnswer->setAttributes($aData);
        
        if (!$oAnswer->save())
            throw new ui\ARSaveException($oAnswer);

        if ( isset($aData['id']) && $aData['id'] )
            $this->addModuleNoticeReport(\Yii::t('poll', 'editPollAnswer'), $aData);
        else {
            unset($aData['id']);
            $this->addModuleNoticeReport(\Yii::t('poll', 'addPollAnswer'), $aData);
        }

        // вывод списка
        $this->actionAnswerList();

    }

    /** Действие: Удалить голосование  */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );
        $iItemId = $this->getInDataValInt('id');

        if (!$iItemId)
            throw new UserException(\Yii::t('poll','error_id_not_found'));

        if (!$oPoll = Poll::findOne($iItemId))
            throw new UserException(\Yii::t('poll','error_row_not_found', [$iItemId]));

        $oPoll->delete();

        $this->addModuleNoticeReport(\Yii::t('poll', 'deletePoll'), $aData);

        // вывод списка
        $this->actionList();

    }

    /** Действие: Удаление варианта ответа */
    protected function actionAnswerDelete() {

        // запросить данные
        $aData = $this->get( 'data' );
        $iAnswerId = $this->getInDataValInt('answer_id');

        if (!$iAnswerId)
            throw new UserException(\Yii::t('poll','error_id_not_found'));

        if (!$oAnswer = PollAnswer::findOne($iAnswerId))
            throw new UserException(\Yii::t('poll','error_row_not_found', [$iAnswerId]));

        $oAnswer->delete();

        $this->addModuleNoticeReport(\Yii::t('poll', 'deletePollAnswer'), $aData);

        // вывод списка
        $this->actionAnswerList();

    }


    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData(
            [
                'page' => $this->iPage,
                'poll_id' => $this->iCurrentPoll
            ]
        );

    }

}