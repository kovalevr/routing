<?php

namespace skewer\build\Tool\Rss;

use skewer\base\queue;
use skewer\components\search;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\base\SysVar;


/**
 * Модуль Настроек
 * Class Module
 * @package skewer\build\Tool\SectionSettings
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {

        $aSections4Rss = Api::getSection4Rss();
        $aSectionsIncludedInRss = $aSections4Rss ? Api::getSectionsIncludedInRss(): array();

        $oForm = StateBuilder::newEdit();
        $oForm
            ->fieldMultiSelect('sections', \Yii::t('rss','field_sections'),$aSections4Rss,$aSectionsIncludedInRss )
            ->field('image', \Yii::t('rss', 'field_image'), 'file')
            ->fieldLink('link',\Yii::t('rss', 'field_link'), Api::getRssLink(), Api::getRssLink())
        ;

        $oForm->setValue(
            [
                'image' => SysVar::get('Rss.image')
            ]
        );

        $oForm->buttonSave('save');

        $this->setInterface($oForm->getForm());
    }

    /**
     * Сохранение
     */
    protected function actionSave(){

        $sOldLogo = SysVar::get('Rss.image');

        if ($this->getInDataVal('image') and ($sOldLogo !== $this->getInDataVal('image'))){

            if (file_exists(WEBPATH  . $sOldLogo))
                @unlink(WEBPATH . $sOldLogo);
        }

        SysVar::set('Rss.image', $this->getInDataVal('image'));
        SysVar::set('Rss.sections', $this->getInDataVal('sections'));

        $oTask = queue\Api::getTaskByClassName(Api::CLASS_TASK);

        if (!$oTask){
            $iTaskId = Api::rebuildRss();
            $oTask = queue\Api::getTaskById($iTaskId);
        } else{
            $iTaskId = $oTask->getId();
        }

        if (!$iTaskId)
            throw new \Exception(\Yii::t('rss','run_error'));
        
        $oManager = queue\Manager::getInstance();

        $oManager->executeTask($oTask);

        if ($oTask->getStatus() == queue\Task::stError)
            throw new \Exception(\Yii::t('rss','run_error'));

        $this->actionInit();
    }

}