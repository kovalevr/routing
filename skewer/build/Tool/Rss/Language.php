<?php

$aLanguage = array();

$aLanguage['ru']['field_link'] = 'Ссылка';
$aLanguage['ru']['field_image'] = 'Изображение';
$aLanguage['ru']['field_sections'] = 'Используемые разделы';
$aLanguage['ru']['run_error'] = 'Не удалось обновить rss ленту';


$aLanguage['en']['field_link'] = 'Link ';
$aLanguage['en']['field_image'] = 'Image';
$aLanguage['en']['field_sections'] = 'Used sections';
$aLanguage['en']['run_error'] = 'Could not update rss feed';


return $aLanguage;