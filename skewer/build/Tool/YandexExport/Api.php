<?php
namespace skewer\build\Tool\YandexExport;


use skewer\base\queue;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\section\Visible;
use skewer\components\catalog\Section;
use skewer\components\i18n\Languages;

class Api{

    /**
     * Соберает все каталожные разделы, экспортируемые в Яндекс и их родителей
     * @return array
     */
    public static function getSections() {

        $aSectionsAll = Tree::getCachedSection(0, true);

        $aSectionsOut = [];
        foreach ($aSectionsAll as $iSectionId => &$paSection) {

            /** Это каталожной раздел с заданной карточкой? */
            $bIsCatalog = (Parameters::getValByName( $iSectionId, 'content', Parameters::objectAdm, true ) === 'Catalog');
            $bIsCatalog = ($bIsCatalog and (bool)Section::getDefCard($iSectionId));

            if ($bIsCatalog and self::checkVisibleSection($iSectionId))
                $aSectionsOut[$iSectionId] = [
                    'id'               => $iSectionId,
                    'title'            => $paSection['title'],
                    'isCatalogSection' => $bIsCatalog,
                    'parent'           => self::getParentSections($paSection['parent'], $aSectionsOut, $aSectionsAll),
                ];
        }

        return $aSectionsOut;
    }

    /**
     * Проверка видимости раздела на сайте
     * @param int $iSectionId Id раздела
     * @return bool
     */
    public static function checkVisibleSection($iSectionId) {

        /** Массив id родительских разделов всего дерева сайта для всех языков */
        static $aStopSections = [];
        if (!$aStopSections)
            foreach (Languages::getAllActiveNames() as $sLang) {
                $aStopSections[\Yii::$app->sections->topMenu($sLang)]  = 1;
                $aStopSections[\Yii::$app->sections->leftMenu($sLang)] = 1;
            }

        if (isset($aStopSections[$iSectionId]))
            return true;

        if (!$iSectionId or !$aSection = Tree::getCachedSection($iSectionId))
            return false;

        if ($aSection['visible'] == Visible::HIDDEN_NO_INDEX)
            return false;

        $iParentId = $aSection['parent'];

        if ($aSection['visible'] == Visible::VISIBLE)
            // Если родитель ссылается на другой раздел
            if ($iRedirectId = (int)trim($aSection['link'], '[]'))
                $iParentId = $iRedirectId;

        return self::checkVisibleSection($iParentId);
    }

    /**
     * Добавить разделы-родителей
     * @param int $iSectionId Id текущего родительского раздела
     * @param array $paSectionsOut Указатель на выходной массив разделов
     * @param array $paSectionsAll Указатель на все видимые разделы сайта
     * @return bool|int Вернёт id обработанного родительского раздела или false
     */
    public static function getParentSections($iSectionId, &$paSectionsOut, &$paSectionsAll) {

        // Если родитель уже есть, то выйти
        if (isset($paSectionsOut[$iSectionId]))
            return $iSectionId;

        /** Массив id родительских разделов всего дерева сайта для всех языков */
        static $aStopSections = [];
        if (!$aStopSections)
            foreach (Languages::getAllActiveNames() as $sLang) {
                $aStopSections[\Yii::$app->sections->root($sLang)]        = 1;
                $aStopSections[\Yii::$app->sections->topMenu($sLang)]     = 1;
                $aStopSections[\Yii::$app->sections->leftMenu($sLang)]    = 1;
                $aStopSections[\Yii::$app->sections->serviceMenu($sLang)] = 1;
                $aStopSections[\Yii::$app->sections->tools($sLang)]       = 1;
            }

        if (isset($aStopSections[$iSectionId]))
            return false;

        if (!isset($paSectionsAll[$iSectionId])) {

            $aSection = Tree::getCachedSection($iSectionId);
            if (!$aSection) return false;

            // Если родитель скрыт из пути, то обработать родителя уровнем выше
            if ($aSection['visible'] == Visible::HIDDEN_FROM_PATH)
                return self::getParentSections($aSection['parent'], $paSectionsOut, $paSectionsAll);

            if ($aSection['visible'] != Visible::VISIBLE)
                return false;

            // Если родитель ссылается на другой раздел
            if ($iRedirectId = (int)trim($aSection['link'], '[]'))
                return self::getParentSections($iRedirectId, $paSectionsOut, $paSectionsAll);

            return false;

        } else {
            $aSection = $paSectionsAll[$iSectionId];
        }

        // Добавить родителя
        $bIsCatalog = (Parameters::getValByName( $iSectionId, 'content', Parameters::objectAdm, true ) === 'Catalog');
        $paSectionsOut[$iSectionId] = [
            'id'               => $iSectionId,
            'title'            => $aSection['title'],
            'isCatalogSection' => $bIsCatalog,
            'parent'           => self::getParentSections($aSection['parent'], $paSectionsOut, $paSectionsAll),
        ];

        return $iSectionId;
    }

    /**
     * Метод запускает задачу на генерацию файла выгрузки
     * @param $iTask - id Задачи
     * @return array Массив со статусом и ID текущей задачи
     * @throws \Exception
     */
    public static function runExport($iTask){

        $oManager = queue\Manager::getInstance();

        if (!$iTask)
            $iTask = queue\Api::addTask(Task::getConfig());


        $oTask = queue\Api::getTaskById( $iTask );

        if (!$oTask)
            throw new \Exception(\Yii::t( 'import', 'error_run'));

        $oManager->executeTask( $oTask );

        return ['status' => $oTask->getStatus(), 'id' => $iTask];
    }


} //class