<?php

namespace skewer\build\Tool\YandexExport;

use skewer\base\queue\Task;
use skewer\build\Adm;
use skewer\components\catalog;
use skewer\build\Tool;
use skewer\base\ui;
use skewer\base\SysVar;
use skewer\base\queue\ar\Schedule;
use skewer\build\Tool\YandexExport;

class Module extends Adm\Order\Module implements Tool\LeftList\ModuleInterface {

    private $iExportLimit = 50;

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    function getName() {
        return $this->getModuleName();
    }

    /**
     * Действие: Обновление файла выгрузки
     */
    protected function actionRunExport(){

        $aData = $this->getInData();
        if (empty($aData) && $this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $taskId = (isset($aData['taskId'])) ? $aData['taskId'] : 0;

        /** Запуск импорта */
        $aRes = Api::runExport($taskId);

        if ( in_array($aRes['status'], [Task::stFrozen, Task::stWait]) ){
            /** Ставим на повторный запуск */
            $this->addJSListener( 'runExport', 'runExport' );
            $this->fireJSEvent( 'runExport', ['taskId' => $aRes['id']] );
        }

        $this->actionInit();

    }

    public function actionSettings() {

        $oFormBuilder = new ui\StateBuilder();
        $oFormBuilder = $oFormBuilder::newEdit();

        $oFormBuilder
            ->field('shopName', \Yii::t('yandexExport', 'shopName'), 'string')
            ->field('companyName', \Yii::t('yandexExport', 'companyName'), 'string')
            ->field('localDeliveryCost', \Yii::t('yandexExport', 'localDeliveryCost'), 'string');

        $oFormBuilder
            ->setValue([
                'shopName' => SysVar::getSafe('YandexExport.shopName', ''),
                'companyName' => SysVar::getSafe('YandexExport.companyName', ''),
                'localDeliveryCost'=> SysVar::getSafe('YandexExport.localDeliveryCost', '')
            ])
            ->buttonSave('saveSettings')
            ->buttonBack();

        $this->setInterface($oFormBuilder->getForm());
    }

    public function actionSaveSettings() {

        $data = $this->getInData();
        $sShopName = $data['shopName'];
        $sCompanyName = $data['companyName'];
        $sLocalDeliveryCost = $data['localDeliveryCost'];

        SysVar::set('YandexExport.shopName', $sShopName);
        SysVar::set('YandexExport.companyName', $sCompanyName);
        SysVar::set('YandexExport.localDeliveryCost', $sLocalDeliveryCost);

        $this->actionSettings();
    }

    public function actionUtils(){
        $oFormBuilder = ui\StateBuilder::newList();

        $aTree = Api::getSections();

        $group = catalog\Card::getGroupByName('yandex');
        $fields = $group->getFields();

        $oFormBuilder->field('title', \Yii::t('yandexExport', 'section'), 'string', array('listColumns' => array('flex' => 3)));

        $aEditableFields = array();
        // собираем чекбоксы
        foreach($fields as $field){
            if ($field->group == $group->id && $field->editor=='check'){
                $aEditableFields[] = $field->name;

                $oFormBuilder->field($field->name, $field->title, 'check', array('listColumns' => array('flex' => 1)));

                foreach ($aTree as $k=>$item) {
                    $aTree[$k][$field->name] = $field->def_value;

                    // Убрать не каталожные разделы из настроек
                    if (!$item['isCatalogSection'])
                        unset($aTree[$k]);
                }
            }
        }

        $oFormBuilder->buttonRow('saveParam', \Yii::t('adm', 'save'), 'icon-save', 'add');
        $oFormBuilder->setValue($aTree);

        $oFormBuilder->setEditableFields($aEditableFields,'saveCheck');

        $oFormBuilder->buttonCancel();

        $this->setInterface($oFormBuilder->getForm());
        return psComplete;
    }

    /**
     * Метод сохраняет набор настроек для товаров выбранного раздела
     * Сам список галочек не сохраняется, только изменяются данные товаров
     * @throws \Exception
     * @throws catalog\Exception
     */
    public function actionSaveParam(){

        $aData = $this->getInData();

        if (!$aData){
            $aParams = $this->get('params');
            if (isset($aParams[0]['data'])){
                $aData = $aParams[0]['data'];
            }
        }

        if (!isset($aData['id'])) return;

        $sectionId = $aData['id'];

        // оставляем только метки от яндекс каталога, удаляем все лишнее
        unset($aData['id']);
        unset($aData['title']);
        unset($aData['parent']);

        $iPage = (isset($aData['page']))?$aData['page']:1;

        $query = catalog\GoodsSelector::getList4Section( $sectionId )
            ->limit( $this->iExportLimit, $iPage )
        ;

        $iCount = 0;
        while ( $aGoods = $query->parseEach() ) {

            $oGoodsRow = catalog\GoodsRow::get( $aGoods['id'] );

            $oGoodsRow->setData( $aData );

            $iCount++;
            $oGoodsRow->save();
        }

        $aData['id'] = $sectionId;

        if ($iCount >= $this->iExportLimit){
            $iPage++;
            $aData['page'] = $iPage;
            $this->addJSListener( 'reload_save_param', 'saveParam' );
            $this->fireJSEvent( 'reload_save_param', array('data' => $aData) );
        }

    }


    public function actionSaveCheck(){
        return psComplete;
    }

    public function actionInit() {
        $oFormBuilder = ui\StateBuilder::newEdit();
        $oFormBuilder
            ->buttonAddNew(
                'runExport',
                (!is_file(WEBPATH . YandexExport\Task::sFilePath)) ?
                    \Yii::t('yandexExport', 'create') :
                    \Yii::t('yandexExport', 'refresh')
            )
            ->buttonEdit('settings', \Yii::t('yandexExport', 'settings'))
            ->button('showTask', \Yii::t('yandexExport', 'task_form'), 'icon-configuration')
            ->buttonEdit('Utils', \Yii::t('yandexExport', 'utils'));

        $sReportText = SysVar::get('Yandex.report');
        $sLogs = SysVar::get('Yandex.log');

        if ($sReportText && is_file(WEBPATH.YandexExport\Task::sFilePath)){
            $oFormBuilder->headText($sReportText . $sLogs);
        } else {
            $oFormBuilder->headText("Выгрузка не создана" . $sLogs);
        }

        $this->setInterface($oFormBuilder->getForm());
        return psComplete;
    }

    /** Состояние: "Настройка задания" */
    protected function actionShowTask(){

        $this->setPanelName(\Yii::t('yandexExport', 'task_form'));

        $oForm = ui\StateBuilder::newEdit();

        $aTaskConfig = \skewer\build\Tool\YandexExport\Task::getConfig();

        $aCommand = json_encode([
            'class' => $aTaskConfig['class'],
            'parameters' => $aTaskConfig['parameters']
        ]);


        $aData = [];

        if ($oTask = Schedule::findOne(['command'=>$aCommand])){
            $aData = $oTask->attributes;
            $aData['active'] = 1;
        }

        $oForm
            ->fieldHide('id', 'ID')
            ->fieldCheck('active', \Yii::t('yandexExport', 'active_task'))

            ->fieldInt('c_min', \Yii::t('schedule', 'c_min'), ['minValue' => 0, 'maxValue' => 59])
            ->fieldInt('c_hour', \Yii::t('schedule', 'c_hour'), ['minValue' => 0, 'maxValue' => 23])
            ->fieldInt('c_day', \Yii::t('schedule', 'c_day'), ['minValue' => 1, 'maxValue' => 31])
            ->fieldInt('c_month', \Yii::t('schedule', 'c_month'), ['minValue' => 1, 'maxValue' => 12])
            ->fieldInt('c_dow', \Yii::t('schedule', 'c_dow'),  ['minValue' => 1, 'maxValue' => 7])

            ->setValue( $aData )

            ->buttonSave('saveTask')
            ->buttonCancel()
        ;

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Действие: Сохранение задачи в планировщике
     */
    protected function actionSaveTask(){

        $aData = $this->getInData();

        $aSettingsSchedule = \skewer\build\Tool\YandexExport\Task::getConfig();

        $aSettingsSchedule['command'] = json_encode(['class' => $aSettingsSchedule['class'], 'parameters' => $aSettingsSchedule['parameters']]);

        $aSettingsSchedule['status'] = Task::stNew;


        if (!$aData['active'])
            Schedule::deleteAll(['id'=>$aData['id']]);
        else{

            if (!$oSchedule = Schedule::findOne($aData['id']))
                $oSchedule = new Schedule();

            $oSchedule->setAttributes($aData + $aSettingsSchedule);

            if ( !$oSchedule->save() )
                throw new ui\ARSaveException( $oSchedule );

        }

        $this->actionInit();
    }


}