<?php

namespace skewer\build\Tool\Payments;

use skewer\base\ui;
use skewer\build\Tool;
use skewer\build\Adm\Order\ar\TypePayment;


/**
 * Class Module
 * @package skewer\build\Tool\Payments
 */
class Module extends Tool\LeftList\ModulePrototype  {

    /** @var string Заглушка для пароля */
    private $pwdFake = '--------';

    protected function actionInit(){
        $this->actionList();
    }

    protected function actionList() {

        $oList = ui\StateBuilder::newList();

        $oList
            ->field('title', \Yii::t('payments', 'title'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('value', \Yii::t('payments', 'active'), 'check', array('listColumns' => array('flex' => 1)))
            ;

        $aItems = Api::getPaymentsList();

        $oList->setValue( $aItems );

        $oList->setEditableFields( array('value'), 'saveActive' );

        $oList->buttonRowUpdate( 'edit' );

        $this->setInterface( $oList->getForm() );

        return psComplete;
    }

    protected function actionEdit(){
        $sType = $this->getInDataVal( 'type' );

        $aPaymentsFields = Api::getFields4PaymentType( $sType );

        if (!$aPaymentsFields){
            $this->actionList();
        }

        $oForm = ui\StateBuilder::newEdit();

        $aFields = array( 'active' );
        $oForm->field('active', \Yii::t('payments', 'active'), 'check');

        foreach ($aPaymentsFields as $aField){
            $aFields[] = $aField[0];
            $oForm->field($aField[0], $aField[1], $aField[3]);
        }

        $aItems = Api::getParams( $sType, $aFields );

        if (!$aItems){
            $aItems = array();
        }

        $oForm->field('type', $sType, 'hide');
        $aItems['type'] = $sType;

        $oForm->setValue( $aItems );

        $oForm->buttonSave( 'save' );
        $oForm->buttonCancel( 'list' );

        $this->setInterface( $oForm->getForm() );
    }

    /**
     * Сохранение активности из списка
     * @return int
     */
    protected function actionSaveActive(){
        $aType = $this->getInDataVal( 'type' );

        if (!$aType OR !Api::existType($aType)) {
            return $this->actionList();
        }

        $oParam = ar\Params::getParam( $aType, 'active');
        $oParam->value = $this->getInDataVal('value');
        $oParam->save();

        // Сбросить деактивируемый тип оплаты в таблице типов оплат заказов
        if ( !$oParam->value )
            TypePayment::update()
                ->set('payment', '')
                ->where('payment', $aType)
                ->get();

        return $this->actionList();
    }

    protected function actionSave() {
        $aType = $this->getInDataVal('type');

        if (!$aType OR !Api::existType($aType))
            return $this->actionList();

        /**
         * @todo нехорошо
         */
        $aData = $this->getInData();
        unset($aData['type']);

        if ($aData){
            foreach( $aData as $sKey => $sVal){
                $oParam = ar\Params::getParam( $aType, $sKey);
                $oParam->value = $sVal;
                $oParam->save();
            }
        }
        return $this->actionList();
    }
}