<?php

namespace skewer\build\LandingAdm\Slider;

use skewer\base\section\Parameters;
use skewer\build\Adm;
use skewer\base\orm;
use skewer\components\ext;
use skewer\build\Adm\Slider\Asset as AdmAsset;


/**
 * Класс для построения интерфейса типового редактора для LandingPage блока
 * Class Module
 * @package skewer\build\Adm\LandingPageEditor
 */
class Module extends Adm\Slider\Module {// implements Tool\LeftList\ModuleInterface extends Adm\Tree\ModulePrototype

    protected $currentBanner = 0;

    function getName() {
        return $this->getModuleName();
    }

    protected function getWorkMode() {
        return self::tool;
    }

    /**
     * Инициализация
     */
    protected function actionInit() {

        if ( $this->currentBanner ) {
            $this->iCurrentBanner = $this->currentBanner;
            $this->actionSlideList();
        } else {
            $this->actionNonBanner();
        }

        $sWebDir = \Yii::$app->getAssetManager()->getBundle(AdmAsset::className())->baseUrl;
        $this->getAssetWebDir( $sWebDir );

        $this->addCssFile($sWebDir.'/css/SlideShower.css');
        $this->addJsFile($sWebDir.'/js/SlideLoadImage.js');
        $this->addJsFile($sWebDir.'/js/SlideShower.js');
    }


    /**
     * Интерфейс создания баннера
     */
    protected function actionNonBanner() {

        $oList = new ext\ListView();

        $oList->addExtButton(
            ext\docked\AddBtn::create()->setAction('CreateBanner')
        );

        $this->setInterface( $oList );
    }


    /**
     * Состояние создания нового банера
     */
    protected function actionCreateBanner() {

        $this->currentBanner =
            $this->iCurrentBanner =
                $this->actionNewBanner();

        // todo группа должна браться текущая
        Parameters::setParams( $this->sectionId(), 'object', 'currentBanner', $this->currentBanner );

        $this->actionSlideList();
    }


    public function actionNewBanner() {

        $iBannerId = orm\Query::InsertInto( 'banners_main' )
            ->set( 'title', 'LandingPage' )
            ->set( 'active', 1 )
            ->get();

        return $iBannerId;
    }

} 