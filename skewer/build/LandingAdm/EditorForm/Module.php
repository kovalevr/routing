<?php

namespace skewer\build\LandingAdm\EditorForm;


use skewer\base\section\Parameters;
use skewer\build\LandingAdm\Editor;

/**
 * Класс для редактирования данных для формы
 * Class Module
 * @package skewer\build\LandingAdm\EditorForm
 */
class Module extends Editor\Module {

    public function getTplFields() {

        $aOut = parent::getTplFields();

        $oFormField = new Editor\TplField();
        $oFormField->name = 'form';
        $oFormField->title = 'Форма'; //@todo Перевод!
        $oFormField->type = 'text_html';
        $oFormField->text = Parameters::getShowValByName(
            $this->sectionId(),
            $oFormField->group,
            $oFormField->name,
            true
        );
        $oFormField->addParams = [
            'height' => 300,
        ];

        $aOut[$oFormField->name] = $oFormField;

        $oAns = new Editor\TplField();
        $oAns->name = 'answer';
        $oAns->title = 'Ответ';
        $oAns->type = 'text_html';

        $oAns->text = Parameters::getShowValByName(
            $this->sectionId(),
            $oAns->group,
            $oAns->name,
            true
        );
        $oAns->addParams = array(
            'height' => 300,
        );

        $aOut[$oAns->name] = $oAns;

        return $aOut;

    }


} 