<?php

$aLanguage = array();

$aLanguage ['ru']['tab_name'] = 'Редактор';
$aLanguage ['ru']['template'] = 'Шаблон';
$aLanguage ['ru']['empty'] = 'Нет данных для редактирования';
$aLanguage ['ru']['data'] = 'Данные';
$aLanguage ['ru']['helper'] = 'класс для текущего блока';


$aLanguage ['en']['tab_name'] = 'Editor';
$aLanguage ['en']['template'] = 'Template';
$aLanguage ['en']['empty'] = 'Empty';
$aLanguage ['en']['data'] = 'Data';
$aLanguage ['en']['helper'] = 'class for current block';

return $aLanguage;