<?php

namespace skewer\build\LandingAdm\Editor;


use skewer\base\section\Parameters;
use skewer\base\section\params\ListSelector;
use skewer\base\section\params\Type;
use skewer\base\section\Tree;
use skewer\components\lp;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\components\auth\CurrentAdmin;
use skewer\components\ext;
use skewer\base\site\Layer;
use skewer\base\site_module\Context;
use skewer\helpers\ImageResize;
use yii\web\ServerErrorHttpException;


/**
 * Класс для построения интерфейса типового редактора для LandingPage блока
 * Class Module
 * @package skewer\build\Adm\LandingPageEditor
 */
class Module extends Adm\Tree\ModulePrototype {

    /**
     * Инициализация
     */
    protected function actionInit() {

        $this->actionShowDataForm();

    }

    protected function canBeParent() {
        return true;
    }

    protected function getAllowedChildClass() {
        return 'skewer\components\lp\PagePrototype';
    }


    /**
     * Отдает набор полей для редактирования шаблона
     * @return TplField[]
     */
    protected function getTplFields() {

        $aOut = array();

        /*
         * CSS
         */
        $oCss = new TplField();
        $oCss->name = lp\Css::paramName;
        $oCss->title = 'CSS';
        $oCss->type = 'text_css';
        $oCss->text = $this->getCss();
        $oCss->addParams = array(
            'height' => 300,
            'subtext' => lp\Css::classLabel." - ".\Yii::t('editor', 'helper')//класс для текущего блока"
        );

        $aOut[$oCss->name] = $oCss;

        /*
         * Шаблон
         */
        $oTpl = new TplField();
        $oTpl->name = lp\Tpl::paramName;
        $oTpl->title = \Yii::t('editor', 'template');
        $oTpl->text = $this->getTpl();
        $oTpl->type = 'text_html';
        $oTpl->addParams = array(
            'height' => 300,
            'subtext' => $this->getTplDescription()
        );

        $aOut[$oTpl->name] = $oTpl;

        return $aOut;

    }


    /**
     * Отображение формы для редактирования шаблона
     */
    protected function actionShowTplForm() {

        $oForm = new ext\FormView();

        $oForm->setPanelTitle( 'Редактирование шаблона' );

        $oFormModel = new ui\Form();

        foreach ( $this->getTplFields() as $oField ) {

            $oUIField = new ui\form\Field(
                $oField->name,
                $oField->title,
                $oField->type,
                $oField->addParams
            );
            $oUIField->setValue( $oField->text );

            $oFormModel->addField( $oUIField );

        }

        // установить набор элементов формы
        $oForm->setFieldsByUiForm( $oFormModel );

        $oForm->addBtnSave( 'saveTpl' );
        $oForm->addBtnCancel( 'ShowTplForm' );

        $oForm->addBtnSeparator('->');
        $oForm->addExtButton( ext\docked\Api::create(\Yii::t('editor', 'data') )->setAction( 'showDataForm' ) );

        $this->setInterface( $oForm );

    }

    /**
     * Сохраняет шаблон
     */
    protected function actionSaveTpl() {

        foreach ( $this->getTplFields() as $oField ) {

            $oParam = Parameters::getByName( $this->sectionId(), $oField->group, $oField->name );
            if (!$oParam)
                $oParam = Parameters::createParam([
                    'parent' => $this->sectionId(),
                    'group' => $oField->group,
                    'name' => $oField->name
                ]);

            $oParam->title = $oField->title;
            $oParam->show_val = $this->getInDataVal( $oField->name, '' );

            $oParam->save();

        }

        $this->actionShowTplForm();

    }

    /**
     * Отрисовывает форму по шаблону
     */
    protected function actionShowDataForm() {

        $oCurSection = Tree::getSection($this->sectionId());
        $oCurSection->last_modified_date = date( "Y-m-d H:i:s", time() );
        $oCurSection->save();


        $oForm = new ext\FormView();

        $oForm->setPanelTitle( 'Редактирование данных' );

        /*
         * Форма
         */

        $oFormModel = new ui\Form();

        /** Динамические параметры из шаблона */
        $aRowList = lp\Parser::getRowsByTpl( $this->getTpl() );

        foreach ( $aRowList as $oRow ) {

            $oFormModel->addField( new ui\form\Field(
                $oRow->name,
                $oRow->title,
                $oRow->type
            ) );

        }

        /** Данные динамеческих полей из шаблона */
        $aData = lp\Api::getData( $this->sectionId() );

        // Добавить редактируемые параметры раздела
        $aParams = Parameters::getList( $this->sectionId() )
            ->level(ListSelector::alEdit)
            ->get();

        foreach($aParams as $oParam)
            // Не добавлять системные параметры шаблонов
            if (!in_array($oParam->group, [lp\Api::groupMain, lp\Api::groupData])) {

                $sParamName = $oParam->id . '-field';
                $oFormModel->addField(new ui\form\Field(
                    $sParamName,
                    $oParam->title,
                    Type::getParamTypes()[$oParam->access_level]['type']
                ));
                $aData[$sParamName] = $oParam->hasUseShowVal() ? $oParam->show_val : $oParam->value;
            }

        // установить набор элементов формы
        $oForm->setFieldsByUiForm( $oFormModel );

        // снять обертку для всплывающих изображений
        foreach ( $aData as $sKey => $sVal )
            $aData[$sKey] = ImageResize::restoreTags( $sVal );

        $oForm->setValues( $aData );

        if ( empty($aRowList) ) {
            $oForm->setAddText( $this->get('empty') );
        } else {
            $oForm->addBtnSave( 'saveData' );
            $oForm->addBtnCancel( 'ShowDataForm' );
        }

        if ( CurrentAdmin::isSystemMode() ) {
            $oForm->addBtnSeparator('->');
            $oForm->addExtButton( ext\docked\Api::create( \Yii::t('editor', 'template') )->setAction( 'showTplForm' ) );
        }

        $this->setInterface( $oForm );

    }

    /**
     * Сохраняет данные формы
     */
    protected function actionSaveData() {

        /** Динамические параметры из шаблона */
        $aRowList = lp\Parser::getRowsByTpl( $this->getTpl() );

        // пришедшие данные с фильтрацией
        $aData = $this->getInData();

        // сохранение
        foreach ( $aData as $sName => $sVal ) {

            // установить обертку для всплывающих изображений
            $sVal = ImageResize::wrapTags($sVal,$this->sectionId());

            if (isset($aRowList[$sName])) {
                // Если это параметр шаблона
                $oParam = Parameters::getByName($this->sectionId(), lp\Api::groupData, $sName);

                // Создать, если ещё не создан
                $oParam = $oParam ?: Parameters::createParam([
                    'parent' => $this->sectionId(),
                    'group' => lp\Api::groupData,
                    'name' => $sName
                ]);

                $oParam->show_val = $sVal;

            } else {
                // Иначе это редактируемый параметр раздела
                if (!$oParam = Parameters::getById((int)$sName))
                    continue;

                if ($oParam->hasUseShowVal())
                    $oParam->show_val = $sVal;
                else
                    $oParam->value = $sVal;
            }

            $oParam->title = isset($aRowList[$sName]) ? $aRowList[$sName]->title : $oParam->title;

            $oParam->save();
        }

        $this->actionShowDataForm();

    }

    /**
     * Отдает шаблон текущей страницы
     */
    protected function getTpl() {
        return lp\Tpl::getForSection( $this->sectionId() );
    }

    /**
     * Отдает текущие CSS параметры
     */
    private function getCss() {
        return lp\Css::getForSection( $this->sectionId() );
    }

    /**
     * Отдаёт контент для клиентского модуля
     * @return string
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    protected function getPageModuleText() {

        $sSubLabel = 'lp_block';

        // id подраздела
        $iSubSectionId = $this->sectionId();

        // параметр объекта
        $aObjParam =  Parameters::getByName( $iSubSectionId, Parameters::settings, Parameters::object, true );
        if ( !$aObjParam )
            throw new ServerErrorHttpException( 'Не найден клиентский корневой модуль' );

        // набор параметров
        $aParams = Parameters::getList( $iSubSectionId )
            ->fields(['value'])
            ->group(Parameters::settings)
            ->rec()
            ->asArray()
            ->get();

        $aParams['sectionId'] = $iSubSectionId;

        // имя модуля
        $sClassName = \skewer\base\site_module\Module::getClassOrExcept($aObjParam['value'], Layer::PAGE);

        // добавление подчиненного модуля
        $this->addChildProcess( new Context(
            $sSubLabel,
            $sClassName,
            ctModule,
            $aParams
        ));

        $oProcess = $this->getChildProcess( $sSubLabel );

        if ( !$oProcess )
            throw new ServerErrorHttpException( 'Не задан процесс клиентсого корневого модуля' );

        $oProcess->execute();

        $oModule = $oProcess->getModule();
        if ( !$oModule )
            throw new ServerErrorHttpException( 'Не задан клиентский корневой модуль' );

        if ( !$oModule instanceof lp\PagePrototype )
            throw new ServerErrorHttpException( sprintf( 'Модуль [%s] должен быть наследником LandingPage\Prototype', $oModule->getModuleName() ) );

        $sOut = $oProcess->getOuterText();

        $this->removeChildProcess( $sSubLabel );

        unset($oProcess);

        return $sOut;

    }

    /**
     * Отдает описание текстовое описание для шаблона
     * @return string
     */
    private function getTplDescription() {

        $aTplDesc = Parameters::getByName(
            $this->sectionId(),
            lp\Api::groupMain,
            lp\Tpl::descParamName,
            true
        );

        return $aTplDesc ? $aTplDesc[ 'show_val' ] : '';

    }

}
