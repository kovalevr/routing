<?php

namespace skewer\build\LandingAdm\Form;

use skewer\base\ui;
use skewer\components\lp;
use skewer\components\forms;
use skewer\build\Adm;
use skewer\base\site\Layer;

/**
 * Класс для построения интерфейса типового редактора для LandingPage блока
 * Class Module
 */
class Module extends Adm\Forms\Module {

    /** @inheritdoc */
    protected function actionInit() {

        $this->sFormModule = Layer::LANDING_PAGE . '\\' . $this->getModuleName();

        $this->setPanelName( \Yii::t('forms', 'select_form') );
        $this->actionPreList();
    }
}