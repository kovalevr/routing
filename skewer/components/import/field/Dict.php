<?php

namespace skewer\components\import\field;

use skewer\components\import;
use skewer\components\catalog;

/**
 * Обработчик поля типа справочник
 */
class Dict extends Prototype {

    /** @var bool Создавать новые */
    protected $create = false;

    protected static $parameters = [
        'create' => [
            'title' => 'field_dict_create',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => 0
        ]
    ];


    /** @var int Id карточки справочника */
    private $sCardDictId = false;

    /**
     * @inheritdoc
     */
    public function init(){
        $this->getDict();

        if (!$this->sCardDictId)
            throw new import\Exception( \Yii::t( 'import', 'error_dict_not_found', $this->fieldName) );
    }


    /**
     * Отдает значение на сохранение в запись товара
     * @return mixed
     */
    public function getValue(){

        $val = implode( ',', $this->values );

        if ($this->sCardDictId){

            //ищем в справочнике
            $aElement = catalog\Dict::getValByTitle($this->sCardDictId, $val, true);

            if ($aElement)
                return $aElement['id'];

            //создадим, если надо
            if ($this->create and $val != ''){
                $mId = catalog\Dict::setValue($this->sCardDictId, [
                    'title' => $val
                ]);

                if ($mId)
                    return $mId;
            }
        }

        return '';

    }


    /**
     * Получение таблицы справочника
     */
    private function getDict(){
        $this->sCardDictId = catalog\Dict::getDictIdByCatalogField($this->fieldName, $this->getCard());
    }

}