<?php


namespace skewer\components\seo;

use skewer\build\Catalog\Collections\SeoElementCollection;
use skewer\build\Catalog\Goods\SeoGood;
use skewer\components\catalog\GoodsRow;
use skewer\base\orm\Query;
use skewer\base\queue\Task;
use skewer\components\search;
use skewer\components\auth\Auth;
use skewer\base\site\Site;
use skewer\base\Twig;

/**
 * Задача на обновление карты сайта
 */
class SitemapTask extends Task{

    private $sTemplateDir = '';

    /** @var int номер файла */
    private $file_num = 0;

    private $sMainDomain = '';

    const LIMIT = 5000;

    /**
     * @inheritdoc
     */
    public function init(){

        $dir = WEBPATH.'sitemap_files/';

        if (!is_dir($dir))
            @mkdir($dir);

        $this->clearSubFiles();

    }

    /**
     * Удаляет все файлы второго уровня
     */
    private function clearSubFiles() {

        $dir = WEBPATH.'sitemap_files/';

        /** чистка старых сайтмапов */
        if (file_exists($dir)) {
            $aList = glob( $dir . '/*' );
            if ( is_array($aList) ) {
                foreach ( $aList as $file )
                    unlink( $file );
            }
        }

    }


    /**
     * @inheritdoc
     */
    public function recovery(){

        $argc = func_get_args();

        $this->file_num = (isset($argc[0]['file_num'])) ? $argc[0]['file_num'] : 0;
    }


    /**
     * @inheritdoc
     */
    public function beforeExecute(){

        $this->sTemplateDir = __DIR__.'/templates/';

        $this->sMainDomain = Site::httpDomain();

        $aConfigPaths = \Yii::$app->getParam(['parser','default','paths']);
        if ( !is_array($aConfigPaths) ) $aConfigPaths = [];
        $aConfigPaths[] = $this->sTemplateDir;
        Twig::setPath( $aConfigPaths );

    }


    /**
     * @inheritdoc
     */
    public function execute(){

        $dir = WEBPATH.'sitemap_files/';

        // взять записи для заданного номера файла
        $aItems = $this->getPageList($this->file_num);

        // если ничего не выбрано - закончить
        if (count($aItems)==0){
            $this->setStatus(static::stComplete);
            return true;
        }

        // -- parse

        Twig::assign('items', $aItems);

        $out = Twig::render('sitemap.twig');

        // -- save - rewrite file

        $title = sprintf('sitemap.%d.xml', $this->file_num+1);

        $filename = $dir.$title;

        /** @todo проверка на существование и выставить права */
        if (!$handle = fopen($filename, 'w+'))   return false;

        if (fwrite($handle, $out) === FALSE)    return false;

        fclose($handle);

        // увеличение счетчика файлов
        $this->file_num += 1;

        return true;

    }


    /**
     * @inheritdoc
     */
    public function afterExecute(){

        $dir = WEBPATH.'sitemap_files/';

        // забацаем главный сайтмап
        $aFiles = array();
        $oDateTime = new \DateTime();
        $currentTime = $oDateTime->format(\DateTime::W3C);

        $aFilesOnDisk = glob($dir . '*');

        // если у нас только один файл
        if (count($aFilesOnDisk) === 1 ) {

            // будем использовать его контент как корневой файл
            $out = file_get_contents($aFilesOnDisk[0]);

            // а сам файл сотрем
            $this->clearSubFiles();

        } else {

            // если файлов несколько - собираем файл со списком
            foreach($aFilesOnDisk as $file){
                $aFiles[] = array(
                    'url' => $this->sMainDomain.'/sitemap_files/'.basename($file),
                    'modify_date' => $currentTime
                );
            }
            Twig::assign('files', $aFiles);
            $out = Twig::render('main_sitemap.twig');

        }

        $filename = WEBPATH."sitemap.xml";
        if (!$handle = fopen($filename, 'w+'))
            return false;
        if (fwrite($handle, $out) === FALSE)
            return false;
        fclose($handle);

        return true;

    }


    /**
     * @inheritdoc
     */
    public function reservation(){

        $this->setParams( ['file_num' => $this->file_num] );

    }


    /**
     * Отдает набор записей для составления карты сайта
     * @param int $file_num
     * @throws \Exception
     * @return array
     *
     */
    private function getPageList($file_num = 0){

        $oQuery = search\SearchIndex::find()->where('status',1)->andWhere('use_in_sitemap', 1);

        if($aDenySections = Auth::getDenySectionByUserId())
            $oQuery->where( 'section_id NOT IN ?', $aDenySections );

        $oQuery
            ->limit(self::LIMIT, $file_num * self::LIMIT)
            ->order('modify_date', 'DESC');

        $aPages = [];
       /** @var search\Row $oRow */
        while ( $oRow = $oQuery->each() ) {

            $oDateTime = \DateTime::createFromFormat('Y-m-d H:i:s',$oRow->modify_date);

            $aPages[] = array(
                'modify_date' => $oDateTime->format(\DateTime::W3C),
                'url'         => $this->sMainDomain . $oRow->href,
                'priority'    => $oRow->priority,
                'frequency'   => $oRow->frequency
            );
        }

        return $aPages;
    }

}