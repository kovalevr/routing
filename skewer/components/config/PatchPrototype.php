<?php
namespace skewer\components\config;

/**
 * Прототип патча
 */
abstract class PatchPrototype extends UpdateHelper {

    /**
     * Описание патча
     * @var string
     */
    public $sDescription = '';

    /**
     * Флаг необходимости перестроения кэша
     * Пересобираются:
     *  * конфиги модулей
     *  * языковые метки
     *  * css настройки
     * @var bool
     */
    public $bUpdateCache = false;

    /**
     * Базовый метод запуска обновления
     * @throws \Exception
     * @throws UpdateException
     * @return bool
     */
    abstract public function execute();

}// class

