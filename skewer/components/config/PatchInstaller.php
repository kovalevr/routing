<?php

namespace skewer\components\config;

use PatchInstall;
use skewer\build\Tool\Patches\Api;
use skewer\base\log\Logger;
use yii\web\ServerErrorHttpException;

/**
 * Класс запуска обновлений на установку
 *
 * Что делать с патчами?
 * Патчи могут запускаться 3-мя разными способами:
 * 1. Локальные патчи - лежат в директории сборки сайта и видны в админке в списке доступных либо недоступных к установке.
 * Для их запуска нужно указать путь к ним.
 * 2. Remote патчи - лежат в сборках кластера - не видны из админки запускаются в процессе установки обновлений.
 * Для запуска нужно указать путь.
 * 3.
 * Получается, что патч всегда выполняется только в рамках конкретной площадки с конкретной версией сборки. Т.е. проблем
 * с именами реестра нет.
 * В случае если на площадке с версией blue0010 запускается патч из версии сборки blue0011, то у него проблем с реестром нет
 * (работает с текущим, потом переименование вручную), но есть проблемы с путями до файлов шаблонов генератора кода, Пути нужно
 * указывать руками относительно корня кластера с учетом сборки.
 *
 */
class PatchInstaller {

    /**
     * Экземпляр файла установки патча
     * @var null|UpdateHelper
     */
    protected $oPatch = null;

    /**
     * Имя файла патча
     * @var string
     */
    protected $sPatchFile = '';

    public function __construct($sPatchFile) {

        if (!is_file($sPatchFile)) throw new UpdateException("Patch install error: Patch file not found [$sPatchFile]!");
        $this->sPatchFile = basename($sPatchFile);

        require_once($sPatchFile);
        /* Создать экземпляр*/

        $this->oPatch = new PatchInstall();

        /* Проверка на правильность формата */
        if (!($this->oPatch instanceof PatchPrototype))
            throw new UpdateException('Patch install error: Patch has invalid format');

        return true;
    }// func

    /**
     * Запускает установку обновления
     * @throws ServerErrorHttpException
     * @return bool|array true/false/массив для отправки
     */
    public function install() {

        /* Проверить был ли установлен данный патч до текущего момента */
        if (Api::alreadyInstalled($this->sPatchFile))
            throw new ServerErrorHttpException("Patch " . (int)$this->sPatchFile . " already installed.");

        /* Установить патч */

        $fileName = $this->sPatchFile;
        $fileName = explode(".", $fileName);
        $fileName = $fileName[0];

        Logger::dump('Patch ' . $fileName . ' install started  At ' . date('r'));
        try {
            $mResult = $this->oPatch->execute();
        } catch (\Exception $e) {
            Logger::dump('Patch ' . $fileName . ' install --FAILED-- At ' . date('r'));
            throw $e;
        }

        if (is_null($mResult))
            $mResult = true;

        Logger::dump('Patch ' . $fileName . ' install complete At ' . date('r'));

        if ($this->oPatch->bUpdateCache) {
            \Yii::$app->rebuildRegistry();
            \Yii::$app->rebuildLang();
            \Yii::$app->rebuildCss();
            \Yii::$app->clearParser();
        }

        return $mResult;

    }// func

    /**
     * Возвращает описание патча, если таковое присутствует
     * @return string
     */
    public function getDescription() {
        return $this->oPatch->sDescription;
    }// func

}// class
