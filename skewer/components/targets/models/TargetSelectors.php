<?php

namespace skewer\components\targets\models;

use Yii;
use skewer\build\Component;

/**
 * This is the model class for table "target_selectors".
 *
 * @property integer $id
 * @property string $selector
 * @property string $name
 * @property string $type
*/
class TargetSelectors extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'target_selectors';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['selector'], 'required'],
            [['name','type','title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'                 => Yii::t('ReachGoal', 'field_id'),
            'selector'           => Yii::t('ReachGoal', 'field_selector'),
            'name'      => Yii::t('ReachGoal', 'field_yandex_target'),
            'type'      => Yii::t('ReachGoal', 'field_google_target'),
            'title'     => Yii::t('ReachGoal', 'field_title'),
        ];
    }


    public static function getNewRow($aData = array()) {
        $oRow = new TargetSelectors();

        if ($aData)
            $oRow->setAttributes($aData);
        return $oRow;
    }

    public static function addNewTarget($sSelector,$sType,$sValue,$sTitle){

        $model = new TargetSelectors();

        $model->selector = $sSelector;
        $model->type = $sType;
        $model->name = $sValue;
        $model->title = $sTitle;

        $model->save(false);

    }
}
