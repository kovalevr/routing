<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 03.06.2016
 * Time: 16:09
 */
namespace skewer\components\targets\types;

use skewer\base\section\Parameters;
use skewer\base\ui;
use skewer\build\Tool\ReachGoal\Module;
use skewer\components\targets\models\Targets;

class Yandex extends Prototype{

    /**
     * @return string
     */
    public function getType(){
        return 'yandex';
    }

    /**
     * @param $oTargetRow
     * @return ui\builder\FormBuilder
     */
    public function getFormBuilder($oTargetRow){

        // создаем форму

        $aNameParams = [];
        if (!is_null($oTargetRow->id))
            $aNameParams['disabled']='disabled';

        $oFormBuilder = ui\StateBuilder::newEdit();
        $oFormBuilder->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('ReachGoal', 'field_title'), 'string')
            ->field('category', \Yii::t('ReachGoal', 'field_category'), 'hide')
            ->field('name', \Yii::t('ReachGoal', 'field_name'), 'string',$aNameParams)
            ->field('type', \Yii::t('ReachGoal', 'field_type'), 'hide')

            ->buttonSave()
            ->buttonBack();

        if ( $oTargetRow->id ) {
            $oFormBuilder
                ->buttonSeparator('->')
                ->buttonDelete();
        }

        $oFormBuilder->setValue($oTargetRow->getAttributes());

        return $oFormBuilder;
    }

    /**
     * @param array $aData
     * @return Targets
     */
    public function getNewTargetRow($aData = array()) {

        return Targets::getNewRow($aData,$this->getType());
    }

    /**
     * Отдает конфиг необходимых параметров
     * @return array
     */
    public function getParams(){

        $aScriptCodes = Parameters::getByName(
            \Yii::$app->sections->root(),
            Parameters::settings,
            Module::jsCounters,
            false
        );

        return [
            '0'=>[
                'name'=>\skewer\components\targets\Yandex::contName,
                'value'=>(string)\skewer\components\targets\Yandex::getCounter(),
                'type'=>'string',
                'title'=>\Yii::t( 'reachGoal', 'yaCounter'),
            ],
            '1'=>[
                'name'=>Module::jsCounters,
                'value'=>$aScriptCodes['show_val'],
                'type'=>'text',
                'title'=>\Yii::t('editor', 'countersCode'),
            ]
        ];

    }

    /**
     * Сохраняет параметры
     * @param $aData
     */
    public function setParams($aData){

        foreach ($aData as $key=>$item){
            switch ($key) {
                case \skewer\components\targets\Yandex::contName:
                    \skewer\components\targets\Yandex::setCounter($item);
                    break;
                case Module::jsCounters:
                    Parameters::setParams(\Yii::$app->sections->root(), Parameters::settings,
                        Module::jsCounters, '200', $item);
                    break;
            }

        }

    }

}