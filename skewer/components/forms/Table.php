<?php

namespace skewer\components\forms;

use skewer\base\orm;
use skewer\base\ft;
use skewer\base\section\Parameters;
use yii\helpers\ArrayHelper;
use skewer\build\Design\Zones;

class Table extends orm\TablePrototype {

    protected static $sTableName = 'forms';

    protected static $sKeyField = 'form_id';


    protected static function initModel() {

        ft\Entity::get( 'forms' )
            ->clear( false )
            ->setPrimaryKey( self::$sKeyField )
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'form_name', 'varchar(255)', 'forms.form_name' )
            ->addField( 'form_title', 'varchar(255)', 'forms.form_title' )
            ->addField( 'form_handler_type', 'varchar(255)', 'forms.form_handler_type' )
            ->addField( 'form_handler_value', 'varchar(255)', 'forms.form_handler_value' )
            ->addField( 'active', 'int(1)', 'forms.form_active' )
                ->setDefaultVal( 1 )
            ->addField( 'form_captcha', 'int(1)', 'forms.form_captcha' )
            ->addField( 'form_is_template', 'int(1)', 'forms.form_is_template' )
            ->addField( 'form_answer', 'int(1)', 'forms.form_answer' )
            ->addField( 'form_redirect', 'varchar(255)', 'forms.form_redirect' )
            ->addField( 'form_agreed', 'int(1)', 'forms.form_agreed' )
            ->addField( 'form_target', 'varchar(255)', 'forms.form_target' )
            ->addField( 'form_target_google', 'varchar(255)', 'forms.form_target_google' )
            ->addField( 'form_send_crm', 'int(1)', 'forms.form_send_crm' )
            ->addField( 'last_modified_date', 'date', 'forms.field_modifydate' )
            ->save()
        ;

        ft\Entity::get( 'forms_add_data' )
            ->clear( false )
            ->setPrimaryKey( self::$sKeyField )
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'answer_title', 'varchar(255)', 'forms.answer_title' )
            ->addField( 'answer_body', 'text', 'forms.answer_body' )
            ->addField( 'agreed_title', 'varchar(255)', 'forms.field_agreed_title' )
            ->addField( 'agreed_text', 'text', 'forms.field_agreed_text' )
            ->save()
        ;
    }


    public static function getNewRow( $aData = array() ) {

        $oRow = new Row();
        //ActiveRecord::init( static::$sTableName, static::getModel()->getAllFieldNames(), array() );

        if ( $aData )
            $oRow->setData( $aData );

        return $oRow;
    }


    /**
     * Получение форм для раздела
     * @param int $iSectionId Id раздела
     * @param string $sModuleName Имя модуля форм
     * @param array $aForms Массив всех групп параметров разделов с объектами форм и значениеми id форм, если фома задана
     * @param bool $bSort флаг необходимости сортировки
     * @return Row[]
     */
    public static function get4Section($iSectionId, $sModuleName = 'Forms', &$aForms = [], $bSort = false) {

        $aForms = [];

        /** Все области страницы */
        $aZones = Zones\Api::getZoneList($iSectionId);


        /** Параметры с объектом форм */
        $aFormsParams = Parameters::getList($iSectionId)
            ->name(Parameters::object)
            ->value($sModuleName)
            ->index('group')
            ->rec()->asArray()->get();

        if (!$aFormsParams) return [];

        /** Массив с ключами всех групп параметров, содержащих объект форм */
        $aFormsGroups = ArrayHelper::map($aFormsParams, 'group', '');

        if ( $bSort ) {
            /** Список всех используемых групп в разделе */
            $aLabels = [];
            foreach ($aZones as &$paZone)
                foreach (Zones\Api::getLabelList($paZone['id'], $iSectionId) as $aLabel)
                    $aLabels[$aLabel['name']] = 0;

            // Отсортировать согласно следованию зон на странице, а отсутствующие группы добавить в конец
            $aFormsGroups = array_intersect_key($aLabels, $aFormsGroups) + array_diff_key($aFormsGroups, $aLabels);
        }

        /** Id всех используемых форм в разделе */
        $aFormsIdsParams = Parameters::getList($iSectionId)
            ->group(array_keys($aFormsParams))
            ->name('FormId')
            ->index('group')
            ->rec()->asArray()->get();

        // array_merge здесь нужен для сохранения стабильной сортировки групп форм и включения в результат групп без параметра FormId
        return ( $aForms = array_merge($aFormsGroups, ArrayHelper::getColumn($aFormsIdsParams, 'value')) ) ?
            static::find()->where('form_id', $aForms)->index('form_id')->getAll() :
            [];
    }


    /**
     * Привязка формы к разделу
     * @param int $iFormId ид формы
     * @param int $iSectionId ид раздела
     * @param string $sGroup Группа параметров с объектом формы
     * @return bool
     */
    public static function link2Section( $iFormId, $iSectionId, $sGroup = 'forms' ) {

        return (bool) Parameters::setParams($iSectionId, $sGroup, 'FormId', $iFormId);
    }


    /**
     * Получение записи формы по имени
     * @param $sFormName
     * @return Row
     */
    public static function getByName( $sFormName ) {

        $oFormRow = static::find()
            ->where( 'form_name', $sFormName )
            ->getOne()
        ;

        return $oFormRow;
    }

    /**
     * Получение записи формы по id
     * @param int $iFormId
     * @return Row
     */
    public static function getById( $iFormId ) {

        return static::find()
            ->where( self::$sKeyField, $iFormId )
            ->getOne()
        ;

    }


    /**
     * Сборка сущности формы
     * @param $iFormId
     * @param array $aData
     * @return Entity
     * @throws \Exception
     */
    public static function build( $iFormId, $aData = array() ) {

        $oFormRow = static::find( $iFormId );

        if ( !( $oFormRow  instanceof Row ) )
            throw new \Exception( "Form not found [id=$iFormId]" );

        $oForm = new Entity( $oFormRow, $aData );

        return $oForm;
    }


    /**
     * Виджет для вывода текста для типа формы
     * @param orm\ActiveRecord $oItem
     * @param $sField
     * @return string
     */
    public static function getTypeTitle( $oItem, $sField ) {

        switch ( $oItem->getVal( $sField ) ) {

            case 'toMail':
                $out = \Yii::t( 'forms', 'send_to_mail');
                break;
            case 'toMethod':
                $out = \Yii::t( 'forms', 'send_to_method');
                break;
            case 'toBase':
                $out = \Yii::t( 'forms', 'send_to_base');
                break;
            default:
                $out = $oItem->getVal( $sField );
        }
        return $out;
    }


    public static function getTypeList() {
        return array(
            'toMail' => \Yii::t( 'forms', 'send_to_mail'),
            'toBase' => \Yii::t( 'forms', 'send_to_base'),
            'toMethod' => \Yii::t( 'forms', 'send_to_method')
        );
    }
} 