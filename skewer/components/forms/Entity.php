<?php

namespace skewer\components\forms;

use skewer\base\site\Site;
use skewer\base\site_module\Parser;
use skewer\components\crm\Crm;
use skewer\helpers\Files;
use skewer\base\log\Logger;
use skewer\helpers\Mailer;
use skewer\components\catalog;
use skewer\base\orm;
use skewer\helpers\Captcha;
use skewer\base\SysVar;
use yii\helpers\ArrayHelper;

/**
 * Объект для обработки формы
 * Class Entity
 * @package skewer\build\Page\Forms
 */
class Entity {

    /** Состояние показа формы */
    const STATE_SHOW_FORM = 1;
    /** Состояние отправки формы */
    const STATE_SEND_FORM = 2;

    /** Тип формы: отправка на e-mail */
    const TYPE_TOMAIL = 'toMail';
    /** Тип формы: отправка в метод */
    const TYPE_TOMETHOD = 'toMethod';
    /** Тип формы: сохренение в базу */
    const TYPE_TOBASE = 'toBase';

    /** @var Row Описание формы */
    private $oFormRow = null;

    /** @var FieldRow[] Описание полей формы */
    private $aFieldRows = array();

    /** @var array Данные для формы */
    private $aData = array();

    /** @var string Ошибка при валидации формы */
    private $sError = '';


    public function __construct( Row $oFormRow, $aData = array() ) {

        $this->oFormRow = $oFormRow;

        $this->aFieldRows = $oFormRow->getFields();

        $this->aData = $aData;
    }

    /**
     * Получить состояние формы
     * @param string $sModuleGroup Метка/группа из которой выполняется модуль, использующий форму
     * @return int
     */
    public function getState($sModuleGroup = '') {

        $bCmdIsSend  = isset($this->aData['cmd']) and ($this->aData['cmd'] == 'send');
        $bCheckGroup = isset($this->aData['label']) ? ($this->aData['label'] == $sModuleGroup) : true;

        return ($bCmdIsSend and $bCheckGroup) ?
            self::STATE_SEND_FORM :
            self::STATE_SHOW_FORM;
    }

    /**
     * Получение полей для формы
     * @param bool $bModif
     * @return FieldRow[]
     */
    public function getFields( $bModif = true ) {

        $aFields = $this->aFieldRows;

        foreach ($aFields as $oFieldRow) {

            switch ($oFieldRow->param_type) {

                case FieldTable::FIELD_CHECK:

                    $oFieldRow->param_default = isset($this->aData[$oFieldRow->param_name]) ?
                        $this->aData[$oFieldRow->param_name] ?: \Yii::t('forms', 'no') :
                        $oFieldRow->param_default ?: \Yii::t('forms', 'yes');

                    break;

                case FieldTable::FIELD_FILE:
                    break;

                default:
                    if (isset($this->aData[$oFieldRow->param_name]))
                        $oFieldRow->param_default = $this->aData[$oFieldRow->param_name];

                    $bModif and $oFieldRow->param_default = $this->modifyValue($oFieldRow, $oFieldRow->param_default);
                    break;
            }
        }

        return $aFields;
    }

    /**
     * Получить распарсенные значения полей param_default = пришедшее значение
     * @return FieldRow[]
     */
    public function getFieldsParsed() {
        $oFieldsSource = $this->oFormRow->getFields();
        $aFieldsOut = $this->getFields();

        foreach ($aFieldsOut as $iKey => $oFieldRow)
            switch ($oFieldRow->param_type) {

                // Заменить ключи на значения в полях с вариантами значений
                case FieldTable::FIELD_RADIO:
                case FieldTable::FIELD_SELECT:
                    $aList = $oFieldsSource[$iKey]->parseDefaultAsList();
                    if (isset($aList[$oFieldRow->param_default]))
                        $oFieldRow->param_default = $this->aData[$oFieldRow->param_name] = $aList[$oFieldRow->param_default];
                    break;
            }

        return $aFieldsOut;
    }

    /**
     * Получение именовонных полей для формы
     * @param bool $bModif
     * @return FieldRow[]
     */
    public function getNamedFields( $bModif = true ) {

        $aFields = $this->aFieldRows;

        $aOut = array();

        foreach ( $aFields as $oFieldRow ) {

            if ( isSet( $this->aData[ $oFieldRow->param_name ] ) ) {

                $sValue = $this->aData[ $oFieldRow->param_name ];

                if ( $oFieldRow->param_type != FieldTable::FIELD_FILE and $bModif )
                    $sValue = $this->modifyValue( $oFieldRow, $sValue );

                $oFieldRow->param_default = $sValue;

            }

            $aOut[$oFieldRow->param_name] = $oFieldRow;
        }


        return $aOut;
    }

    /**
     * Модифицирует значение поля в соответствии с типом
     * @param FieldRow $oFieldRow
     * @param mixed $mValue
     * @throws \Exception
     * @return mixed
     */
    private function modifyValue( /** @noinspection PhpUnusedParameterInspection */
        FieldRow $oFieldRow, $mValue ) {

        $mValue = strip_tags( $mValue );

//        switch ( $oFieldRow->param_type ) {
//
//            case 'string':
//            case 'text':
//                $mValue = strip_tags( $mValue );
//                break;
//            case 'int':
//                $mValue = (int)$mValue;
//                break;
//            default:
//                throw new \Exception( 'Unknown type ['.$oFieldRow->param_type.']' );
//        }

        return $mValue;

    }

    public function getId() {
        return $this->oFormRow->form_id;
    }

    /**
     * Заголовок формы
     * @return string
     */
    public function getTitle() {
        return $this->oFormRow->form_title;
    }

    /**
     * Значение target для ReachGoal
     * @return string
     */
    public function getTarget() {
        return $this->oFormRow->form_target;
    }

    /**
     * Значение target для Google
     * @return string
     */
    public function getTargetGoogle() {
        return $this->oFormRow->form_target_google;
    }


    /**
     * Тип обработчика формы
     * @return string
     */
    public function getHandlerType() {
        return $this->oFormRow->form_handler_type;
    }


    /**
     * Данные по соглашению на обработку персональных данных
     * @return array|bool|string
     */
    public function getAgreedData() {
        return $this->oFormRow->form_agreed ? $this->oFormRow->getAddData() : false;
    }


    /**
     * Данные для ответа пользовател.
     * @return array|bool|string
     */
    public function getAnswerData() {
        return $this->oFormRow->form_answer ? $this->oFormRow->getAddData() : false;
    }


    public function getFormRedirect() {
        return $this->oFormRow->form_redirect;
    }


    public function getFormCaptcha() {
        return $this->oFormRow->form_captcha;
    }

    /** Получить ответ в случае успешной отправки формы из админки */
    public function getFormSuccAnswer() {
        return $this->oFormRow->form_succ_answer;
    }

    /**
     * Возвращает значение параметра $sParamName формы
     * @param string $sParamName Имя параметра формы
     * @return string
     */
    public function getFormParam( $sParamName ) {
        return isSet( $this->oFormRow->$sParamName ) ? $this->oFormRow->$sParamName : '';
    }


    /**
     * Уникальный хеш код формы на странице
     * @param $iSection
     * @param string $sLabel
     * @return string
     */
    public function getHash( $iSection, $sLabel = 'out' ) {
        return md5( md5( $sLabel . $this->getId() ) . $iSection );
    }

    /**
     * Возвращает флаг, отвечающий за
     * вывод фразы "* - обязательные для заполнения поля"
     * @return bool
     */
    public function doShowPhraseRequiredFields(){

        $bHasRequiredFields = false;
        foreach ($this->getFields() as $item) {
            if ($item->param_required){
                $bHasRequiredFields = true;
                break;
            }
        }

        return ($bHasRequiredFields && $this->getFormParam('form_show_required_fields'))? true : false;
    }


    /**
     * Возвращает флаг, отвечающий за вывод заголовка формы
     * @return bool
     */
    public function doShowHeader(){
        return ($this->getFormParam('form_show_header'))? true : false;
    }


    /**
     * Правила для валидации формы
     * @return string
     */
    public function getRules() {

        $aFieldList = $this->getFields();
        $aRules = array();

        /* @var $oField FieldRow */
        foreach ( $aFieldList as $oField ){

            $aTempRow = array();
            $aTempRow['required']  = $oField->param_required ? true: false;

            if ($oField->param_type == FieldTable::FIELD_FILE){
                $iMaxFileSize = $oField->getMaxFileSize();
                /** Передаю 2 параметра, по первому - js валидация, второй для красивого сообщения об ошибке */
                $aTempRow['filesize'] = array($iMaxFileSize * 1024 * 1024, $iMaxFileSize);
            }else{
                $aTempRow['maxlength'] = $oField->param_maxlength;
            }

            if($oField->param_validation_type != 'text')
                $aTempRow[$oField->param_validation_type] = true;

            $aRules['rules'][$oField->param_name] = $aTempRow;
        }

        // Если есть капча
        if( $this->oFormRow->form_captcha ) {

            $aRules['rules']['captcha'] = array(
                'required'=>1,
                'maxlength'=>50,
                'digits'=>1
            );
        }

        //Если есть галочка соглашения
        if ( $this->oFormRow->form_agreed ) {
            $aRules['rules']['agreed'] = array(
                'required' => 1
            );
        }

        return json_encode($aRules);
    }


    public function validate( $formHash ) {

        try {

            // проверка капчи
            if ( $this->oFormRow->form_captcha ) {

                if ( !isset($this->aData['captcha']) )
                    throw new \Exception( 'send_captcha_error' );

                /** Указатель на поле формы с каптчей */
                $oFieldCaptcha = isset($this->aFieldRows['captcha']) ? $this->aFieldRows['captcha'] : null;
                $sCaptcha      = $this->aData['captcha'];

                if( !$sCaptcha OR !Captcha::check( $sCaptcha, $formHash, true ) ) {

                    // Установить полю сообщение о неверной валидации
                    $oFieldCaptcha and $oFieldCaptcha->addError(\Yii::t('forms', 'captcha_error'));
                    throw new \Exception('captcha_error');
                }

            }

            // проверка согласия пользователя
            if ( $this->oFormRow->form_agreed ) {

                if ( !isset( $this->aData['agreed'] ) || empty( $this->aData['agreed'] ) )
                    throw new \Exception( 'agreed_error' );

            }

            if ( count($this->aFieldRows) )
                foreach ( $this->aFieldRows as $oFieldRow ) {

                    $sVal = '';

                    switch ( $oFieldRow->param_type ) {
                        case FieldTable::FIELD_FILE:

                            if (isset($_FILES[$oFieldRow->param_name]['tmp_name'])){

                                if ( is_uploaded_file( $_FILES[$oFieldRow->param_name]['tmp_name'] ) ) {

                                    // Ошибка загрузки
                                    if ( $_FILES[$oFieldRow->param_name]['error'] )
                                        throw new \Exception( 'fileupload_error' );

                                    // проверяем размер # размеры недопустим, выходим
                                    if ( $_FILES[$oFieldRow->param_name]['size'] > $oFieldRow->getMaxFileSize()*1024*1024 )
                                        throw new \Exception( 'file_maxsize_error' );

                                    $sUploadAllowFiles = \Yii::$app->getParam(['upload', 'allow', 'files']);
                                    if (!is_array($sUploadAllowFiles)){
                                        $sUploadAllowFiles = array($sUploadAllowFiles);
                                    }

                                    /**
                                     * @todo Нельзя тут смотреть разширение по имени. Надо отслеживать mime-type
                                     */
                                    $sExt = Files::getExtension($_FILES[$oFieldRow->param_name]['name']);
                                    if (!$sExt || in_array($sExt, $sUploadAllowFiles) === false){
                                        throw new \Exception( 'filetype_error' );
                                    }

                                    $sVal = file_get_contents( $_FILES[$oFieldRow->param_name]['tmp_name'] );

                                    $oFieldRow->param_title = $_FILES[$oFieldRow->param_name]['name'];
                                    //todo $oParam->setTitle( $_FILES[$oFieldRow->param_name]['name'] );

                                }

                            }

                            break;
                        default:

                            if ( isSet( $this->aData[$oFieldRow->param_name] ) ) {

                                $sVal = $this->aData[$oFieldRow->param_name];

                                // Валидация параметров на стороне сервера
                                if ( $oFieldRow->validate( $sVal ) === false )
                                    throw new \Exception( 'validation_error' );

                            }
                    }

                    $this->aData[ $oFieldRow->param_name ] = $sVal;
                }



        } catch ( \Exception $e ) {

            $this->sError = $e->getMessage();

            return false;
        }

        return true;
    }


    public function getError() {
        return $this->sError;
    }


    /**
     * Поиск значения email пользователя в пришедших данных
     * @return bool|string
     */
    public function findEmailField() {

        $mResult = false;

        if ( $this->aFieldRows )
            foreach ( $this->aFieldRows as $oFieldRow ) {

                if( $oFieldRow->param_validation_type == 'email' )
                    if ( isSet( $this->aData[$oFieldRow->param_name] ) )
                        $mResult = $this->aData[$oFieldRow->param_name];
                    else
                        $mResult = $oFieldRow->param_default;

            }

        return $mResult;
    }


    private function sendMail( $sLetterTemplate = 'letter.twig', $sTemplateDir = '') {

        if ( !$sLetterTemplate ) return false;
        if ( !$this->oFormRow->form_handler_value ) return false;

        $sBody = Parser::parseTwig( $sLetterTemplate, array( 'oForm' => $this ), $sTemplateDir );

        // add attach file
        $aAttachFile = array();
        foreach ( $this->getFields() as $oFieldRow )
            if ( $oFieldRow->param_type == FieldTable::FIELD_FILE and isset($this->aData[$oFieldRow->param_name]) and $this->aData[$oFieldRow->param_name] != '' )
                $aAttachFile[ $oFieldRow->param_title ] = $this->aData[$oFieldRow->param_name];

        if ( count( $aAttachFile ) ){
            $sRes = Mailer::sendMailWithAttach( $this->oFormRow->form_handler_value, $this->oFormRow->form_title, $sBody, array(), $aAttachFile );
        }
        else{
            $sRes = Mailer::sendMail( $this->oFormRow->form_handler_value, $this->oFormRow->form_title, $sBody );
        }

        return $sRes;

    }


    /**
     * Отправление результатов формы письмом
     * @param $sLetterTemplate
     * @param $sLetterDir
     * @return bool
     */
    public function send2Mail( $sLetterTemplate, $sLetterDir ) {

        // Если в форме(шаблон!) не задано значение обработчика(куда отсылаем!), берем системный e-mail
        if( !$this->oFormRow->form_handler_value )
            $this->oFormRow->form_handler_value = Site::getAdminEmail();

        $sMailTo = $this->findEmailField();

        // Посылаем e-mail
        $bRes = self::sendMail( $sLetterTemplate, $sLetterDir );

        // отправляем уведомление об отправки сообщения - автоответ
        if ( $bRes && $this->oFormRow->form_answer && $sMailTo ) {

            $aFormAnswer = $this->oFormRow->getAddData();

            if ( is_array($aFormAnswer) && !empty($aFormAnswer) ) {

                Mailer::sendMail( $sMailTo, $aFormAnswer['answer_title'], $aFormAnswer['answer_body'] );

            }
        }

        // а теперь дублируем месседж в CRM
        if ($this->oFormRow->form_send_crm){
            $this->send2Crm();
        }

        return $bRes;
    }


    /**
     * Обработка результатов формы внутренним методом
     * @return bool|mixed
     * @throws \Exception
     */
    public function send2Method() {

        if ( !$this->oFormRow->form_handler_value ) return false;

        list ( $sObjectName, $sMethodName ) = explode( '.', $this->oFormRow->form_handler_value );

        if ( !isset($sObjectName) || !isset($sMethodName) )
            throw new \Exception( \Yii::t( 'forms', 'wrong_format') );

        $oCurClass = new \ReflectionClass($sObjectName);

        if ( !( $oCurClass instanceof \ReflectionClass ) )
            throw new \Exception( \Yii::t( 'forms', 'class_not_created') );

        if ( $oCurClass->getParentClass()->name != 'skewer\base\site\ServicePrototype' )
            throw new \Exception( \Yii::t( 'forms', 'wrong_class') );

        $oCurObj = new $sObjectName();

        if ( !method_exists($oCurObj,$sMethodName) )
            throw new \Exception( \Yii::t( 'forms', 'wrong_method') );

        $sRes = call_user_func_array( array( $oCurObj, $sMethodName ), array( $this ) );

        // а теперь дублируем месседж в CRM
        if ($this->oFormRow->form_send_crm){
            $this->send2Crm();
        }

        return $sRes;
    }


    public function send2Base( $iSectionId = 0 ) {

        $oQuery = orm\Query::InsertInto( 'frm_' . $this->oFormRow->form_name );

        $aFields = $this->getFields();

        foreach ( $aFields as $oFieldRow )
            $oQuery->set( $oFieldRow->param_name, $oFieldRow->param_default );

        $oQuery->set( '__add_date', date('Y-m-d h:i:s') );
        $oQuery->set( '__status', 'new' );
        $oQuery->set( '__section', $iSectionId );

        $iRes = $oQuery->get();

        return $iRes;
    }

    /**
     * Заполнение полей привязанных к товарным позициям
     * @param $iObjectId
     * @return bool
     */
    public function fillGoodsFields( $iObjectId ) {

        $row = catalog\Card::getItemRow( catalog\Card::DEF_BASE_CARD, ['id' => $iObjectId] );
        if ( !$row ) return false;

        $aLinks = orm\Query::SelectFrom( 'forms_links' )
            ->where( 'form_id', $this->getId() )
            ->asArray()->getAll();

        $aFieldList = $this->getFields();

        if ( $aLinks && count( $aLinks ) )
            foreach ( $aLinks as $oLink ) {

                $sFieldName = $oLink['card_field'];
                $sVal = isSet($row->$sFieldName) ? $row->$sFieldName : '';

                /* @var $oFieldRow FieldRow */
                foreach ( $aFieldList as $oFieldRow )
                    if ( $oFieldRow->param_name == $oLink['form_field'] )
                        $oFieldRow->param_default = $sVal;

            }

        return true;
    }

    /**
     * Отправляет данные в CRM
     * @throws \Exception
     */
    private function send2Crm() {

        $sCrm_token = SysVar::get('crm_token');
        $sCrm_email = SysVar::get('crm_email');
        if (!$sCrm_token or !$sCrm_email) return;

        $aText = array();
        foreach ($this->getFieldsParsed() as $oField)
            $aText[] = sprintf(
                '%s: %s',
                $oField->param_title,
                $oField->param_default ?: '---'
            );

        $email = ArrayHelper::getValue($this->aData, 'email', '');
        if (!$email)
            $email = ArrayHelper::getValue($this->aData, 'mail', '');



        $crmSender = new Crm();
        $crmSender->setToken($sCrm_token);
        $crmSender->setEmail($sCrm_email);
        $crmSender->setDomain(\Yii::$app->request->getServerName());
        $crmSender->setDealTitle('Заявка с сайта '.\Yii::$app->request->getServerName().' от '. date('d-m-Y H:i:s'));
        $crmSender->setDealContent(implode("\r\n", $aText));
        $crmSender->setContactClient(ArrayHelper::getValue($this->aData, 'person', ''));
        $crmSender->setContactPhone(ArrayHelper::getValue($this->aData, 'phone', ''));
        $crmSender->setContactEmail($email);
        $crmSender->setContactMobile(ArrayHelper::getValue($this->aData, 'mobile', ''));
        $crmSender->setEventId(ArrayHelper::getValue($this->aData, 'event_id', ''));
        $crmSender->setCanapeuuid(ArrayHelper::getValue($this->aData, '_canapeuuid', ''));

        $crmSender->setItemArticle(ArrayHelper::getValue($this->aData, 'item_index', ''));
        $crmSender->setItemTitle(ArrayHelper::getValue($this->aData, 'item_title', ''));
        $crmSender->setItemCount(ArrayHelper::getValue($this->aData, 'item_count', ''));
        $crmSender->setItemPrice(ArrayHelper::getValue($this->aData, 'item_price', ''));
        $crmSender->setItemUnits(ArrayHelper::getValue($this->aData, 'item_units', ''));

        try {

            $crmSender->sendMail();

        } catch (\Exception $e) {
            Logger::dumpException($e);
        }
    }

    public function getData(){
        return $this->aData;
    }
}