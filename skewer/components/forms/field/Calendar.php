<?php

namespace skewer\components\forms\field;


class Calendar extends Select  {

    public $clsSpec = 'form__date';

    protected $sTpl = 'calendar.twig';

} 