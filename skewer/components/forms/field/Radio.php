<?php

namespace skewer\components\forms\field;


class Radio extends Select {

    public $type = 'radio';
    public $clsSpec = 'form__radio';

    protected $sTpl = 'radio.twig';

} 