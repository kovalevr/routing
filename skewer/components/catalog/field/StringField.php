<?php

namespace skewer\components\catalog\field;


class StringField extends Prototype {

    protected function build( $value, $rowId, $aParams ) {
        return [
            'value' => $value,
            'html' => $value
        ];
    }
}