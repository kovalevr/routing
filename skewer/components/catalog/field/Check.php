<?php

namespace skewer\components\catalog\field;


class Check extends Prototype {

    protected function build( $value, $rowId, $aParams ) {
        return [
            'value' => $value,
            'html' => ( $value == '1' ) ? \Yii::t('page', 'yes') : \Yii::t('page', 'no')
        ];
    }
}