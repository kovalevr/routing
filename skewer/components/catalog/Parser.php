<?php

namespace skewer\components\catalog;

use skewer\base\ft;
use skewer\base\site\Site;
use skewer\components\catalog\field\Prototype;
use yii\helpers\ArrayHelper;
use skewer\base\section\Tree;


/**
 * Парсер. Переводит товар из формата ActiveRecord в ассоциативный массив для вывода
 * Class Parser
 * @package skewer\build\Page\CatalogViewer\Parser
 */
class Parser {

    /** @var field\Prototype[] набор полей для формирования вывода */
    private $fields = [];


    /**
     * Инициализация парсера
     * @param ft\model\Field[] $fields набор полей для вывода
     * @param array $attr набор атрибутов, ограничивающий набор полей
     * @return Parser
     */
    public static function get( $fields, $attr = [] ) {

        $obj = new self();

        foreach ( $fields as $ftField ) {

            $fl = true;

            foreach ( $attr as $name )
                if ( !$ftField->getAttr( $name ) )
                    $fl = false;

            if ( !$fl )
                continue;

            $obj->fields[] = field\Prototype::init( $ftField );
        }

        return $obj;
    }


    /**
     * Парсим кортеж данных как каталожную позицию
     * @param array $aGoodParam значения служебных полей каталожной позиции из таблицы c_goods
     * @param array $aGoodData данные карточек каталожной позиции
     * @param bool $onlyHeader
     * @return array
     */
    public function parseGood( $aGoodParams, $aGoodData, $onlyHeader = false ) {

        // заполняем заголовок - товарные данные
        $out = [
            'id'           => ArrayHelper::getValue($aGoodData, 'id', 0),
            'title'        => $sTitle = ArrayHelper::getValue($aGoodData, 'title', ''),
            'alias'        => ArrayHelper::getValue($aGoodData, 'alias', ''),
            'active'       => (bool)ArrayHelper::getValue($aGoodData, 'active', false),
            'card'         => ArrayHelper::getValue($aGoodParams, 'ext_card_name', ''),
            'base_card_id' => (int)ArrayHelper::getValue($aGoodParams, 'base_card_id', 0),
            'main_obj_id'  => ArrayHelper::getValue($aGoodParams, 'parent', ''),
            'main_secton'  => ArrayHelper::getValue($aGoodParams, 'section', 0),
            'last_modified_date' => ArrayHelper::getValue($aGoodParams, '__upd_date','')
        ];

        $out['url'] = self::buildUrl( $out['main_secton'], $out['id'], $out['alias'] );
        $out['canonical_url'] = Site::httpDomain() . $out['url'];

        // если нужен только заголовое, то дальше не обрабатываем поля
        if ( $onlyHeader ) return $out;


        // список доступных полей для товара
        $out['fields'] = [];
        foreach ( $this->fields as $field ) {
            $value = ArrayHelper::getValue( $aGoodData, $field->getName(), '' );
            $out['fields'][$field->getName()] = $field->parse( $value, $out['id'], ['title' => $sTitle] );
        }



        // список полей по группам todo реализовать функционал когда возникнет необходимость
        $out['groups'] = [];

        return $out;
    }


    /**
     * Парсим кортеж данных как сущность
     * @param array $data данные полей сущности
     * @return array
     */
    public function object( $data ) {

        $out = [
            'id' => ArrayHelper::getValue( $data, 'id', 0 ),
            'title' => ArrayHelper::getValue( $data, 'title', '' ),
            'alias' => ArrayHelper::getValue( $data, 'alias', '' ),
            'card'  => ArrayHelper::getValue( $data, 'card', ''),
            'active' => (bool)ArrayHelper::getValue( $data, 'active', false ),
            'last_modified_date' => ArrayHelper::getValue($data, 'last_modified_date', '')
        ];

        $out['fields'] = [];

        foreach ( $this->fields as $field ) {

            $out['fields'][$field->getName()] = $field->parse( $data[$field->getName()], $out['id'] );
        }

        return $out;
    }

    /**
     * Формирование относительного url для каталожной позиции
     * @param int $iMainSection Id секции
     * @param int $iGoods Id позиции
     * @param string $sGoodsAlias псевдоним позиции
     * @return string
     */
    public static function buildUrl( $iMainSection, $iGoods = 0, $sGoodsAlias = '' ) {

        $sSectionPath = Tree::getSectionAliasPath($iMainSection, true, false, true);

        if ( !$sSectionPath or (!$iGoods and !$sGoodsAlias) )
            return '';

        return ($sGoodsAlias) ?
            "$sSectionPath$sGoodsAlias/" :
            "$sSectionPath?item=$iGoods/";
    }


    /**
     * Вернет массив расспарсенных полей карточки
     * @param $sCard - карточка
     * @param $iGoodId - id товара
     * @param array $aData - данные для подстановки
     * @return array
     */
    public static function parseFieldsByCard($sCard, $iGoodId, $aData = []){
        $aOut = [];
        $aFields = GoodsRow::create($sCard)->getFields();

        foreach ($aFields as $oField) {
            if (isset($aData[$oField->getName()])){
                $oParserField = Prototype::init($oField);
                $aOut[$oField->getName()] = $oParserField->parse($aData[$oField->getName()],$iGoodId);
            }
        }

        return $aOut;
    }

} 