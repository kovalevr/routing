<?php

namespace skewer\components\catalog;

use skewer\base\ft;


class Semantic {

    const TYPE_INCLUDE = 1;
    const TYPE_MODIFICATIONS = 2;
    const TYPE_RELATED = 3;

} 