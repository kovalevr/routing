<div class="b-plushorizontal">
    <div class="plushorizontal__item">
        <div class="plushorizontal__imgbox"><span class="fa fa-retweet"></span></div>

        <div class="plushorizontal__text">
            <p>Lorem ipsum dolor sit amet consectetur</p>
        </div>
    </div>

    <div class="plushorizontal__item">
        <div class="plushorizontal__imgbox"><span class="fa fa-leaf"></span></div>

        <div class="plushorizontal__text">
            <p>Lorem ipsum dolor sit amet consectetur</p>
        </div>
    </div>

    <div class="plushorizontal__item">
        <div class="plushorizontal__imgbox"><span class="fa fa-fire"></span></div>

        <div class="plushorizontal__text">
            <p>Lorem ipsum dolor sit amet consectetur</p>
        </div>
    </div>

    <div class="plushorizontal__item">
        <div class="plushorizontal__imgbox"><span class="fa fa-chain"></span></div>

        <div class="plushorizontal__text">
            <p>Lorem ipsum dolor sit amet consectetur</p>
        </div>
    </div>

    <div class="plushorizontal__item">
        <div class="plushorizontal__imgbox"><span class="fa fa-arrow-circle-down"></span></div>
        <div class="plushorizontal__text">
            <p>Lorem ipsum dolor sit amet consectetur</p>
        </div>
    </div>
</div>