<?php

namespace skewer\components\search;

use skewer\base\orm;
use skewer\base\ft;
use skewer\base\site;

/**
 * Отражение таблицы поискового индекса
 * Class SearchIndex
 * @package skewer\components\search
 */
class SearchIndex extends orm\TablePrototype {

    protected static $sTableName = 'search_index';
    protected static $sKeyField = 'id';

    protected static function initModel() {
        ft\Entity::get( 'search_index' )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'search_title', 'varchar(255)', 'Заголовок' )
            ->addField( 'search_text', 'text', 'Полный текст для поиска' )
            ->addField( 'text', 'text', 'Текст для вывода поиска' )
            ->addField( 'status', 'int(11)', 'Cостояние записи' )
            ->addField( 'href', 'varchar(255)', 'Ссылка' )
            ->addField( 'class_name', 'varchar(255)', 'Имя модуля' )
            ->addField( 'object_id', 'int(11)', 'Идентификатор записи' )
            ->addField( 'language', 'varchar(64)', 'Идентификатор языка' )
            ->addField( 'section_id', 'int(11)', 'ID раздела' )
            ->addField( 'use_in_search', 'int(1)', 'Флаг добавления в поисковый индекс' )
            ->addField( 'priority', 'float', 'Приоритет страницы для Sitemap' )
            ->addField( 'frequency', 'varchar(20)', 'Частота обновления страницы для Sitemap' )
            ->addField( 'use_in_sitemap', 'int(1)', 'Флаг использования в Sitemap' )
            ->addField( 'modify_date', 'datetime', 'Дата обновления записи' )

            ->selectField( 'search_text' )
                ->addIndex( ft\Index::fulltext )
            ->selectFields( array('class_name', 'object_id') )
                ->addIndex()

            ->save()
            //->build()
        ;
    }

    public static function getNewRow($aData = array()) {
        $oRow = new Row();

        $oRow->modify_date = date("Y-m-d H:i:s");

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }

    protected static function getARClass() {
        return '\skewer\components\search\Row';
    }

}