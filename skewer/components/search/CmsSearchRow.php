<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 08.08.2016
 * Time: 15:20
 */

namespace skewer\components\search;


use yii\base\Object;

class CmsSearchRow extends Object{

    /** @var string заголовок найденной записи */
    public $title = '';

    /** @var string url для показа найденной записи */
    public $url = '';

    /** @var string класс объекта найденной записи */
    public $object_class = '';

    /** @var string id объекта найденной записи */
    public $object_id = '';

    /** @var null|string инициализационная строка для открытия записи */
    public $init_param = '';

    /** @var string имя таба в котором нужно применить инициализационный параметр init_param */
    public $init_tab = '';

}