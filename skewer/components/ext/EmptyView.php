<?php

namespace skewer\components\ext;


use skewer\base\ft;

/**
 * Объект для создания новой пусто вкладки
 */
class EmptyView extends ViewPrototype {

    /**
     * Возвращает имя компонента
     * @return string
     */
    function getComponentName() {
        return '';
    }

    /**
     * Отдает интерфейсный массив для атопостроителя интерфейсов
     * @return array
     */
    function getInterfaceArray() {
        return array();
    }

}
