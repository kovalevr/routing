<?php

namespace skewer\components\ext\field;

/**
 * Редактор "дробное число"
 */
class FloatField extends Prototype {

    function getView() {
        return 'float';
    }

}