<?php

namespace skewer\components\auth\models;

use skewer\base\site;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property integer $global_id
 * @property string $login
 * @property string $pass
 * @property integer $group_policy_id
 * @property integer $active
 * @property string $lastlogin
 * @property string $name
 * @property string $email
 * @property string $postcode
 * @property string $address
 * @property string $phone
 * @property string $cache
 * @property integer $version
 * @property integer $del_block
 * @property string $user_info
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public static function defaultLogin()
    {
        return 'default';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ( !site\Type::isShop() ) {
            return [
                [['login', 'group_policy_id'], 'required'],
                [['global_id', 'group_policy_id', 'active', 'version', 'del_block'], 'integer'],
                [['lastlogin'], 'safe'],
                [['cache'], 'string'],
                [['login', 'name', 'email'], 'string', 'max' => 40],
                [['pass'], 'string', 'max' => 32],
            ];
        }
         else {
             return [
                 [['login', 'group_policy_id'], 'required'],
                 [['global_id', 'group_policy_id', 'active', 'version', 'del_block'], 'integer'],
                 [['reg_date', 'lastlogin'], 'safe'],
                 [['cache', 'user_info'], 'string'],
                 [['login', 'name', 'email', 'postcode'], 'string', 'max' => 40],
                 [['pass'], 'string', 'max' => 32],
                 [['address'], 'string', 'max' => 255],
                 [['phone'], 'string', 'max' => 20]
             ];
         }

    }

    public static function updFieldsFilter(){

        return ['id','group_policy_id','name','email','active'];

    }

    public static function insertFieldsFilter(){

        return ['id','login','pass','pass2','group_policy_id','name','email','active'];

    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'global_id' =>  \Yii::t('auth', 'global_id'),
            'login' =>      \Yii::t('auth', 'login'),
            'pass' =>       \Yii::t('auth', 'pass'),
            'group_policy_id' => \Yii::t('auth', 'group_policy_id'),
            'active' =>     \Yii::t('auth', 'active'),
//            'reg_date' =>   \Yii::t('auth', 'reg_date'),
            'lastlogin' =>  \Yii::t('auth', 'lastlogin'),
            'name' =>       \Yii::t('auth', 'name'),
            'email' =>      \Yii::t('auth', 'email'),
            'postcode' =>   \Yii::t('auth', 'postcode'),
            'address' =>    \Yii::t('auth', 'address'),
            'phone' =>      \Yii::t('auth', 'phone'),
            'cache' =>      \Yii::t('auth', 'cache'),
            'version' =>    \Yii::t('auth', 'version'),
            'del_block' =>  \Yii::t('auth', 'del_block'),
            'user_info' =>  \Yii::t('auth', 'user_info'),
        ];
    }

/*
 *         'id' => 'i:hide:auth.id',
        'global_id' => 'i:hide:auth.global_id',
        'login' => 's:str:auth.login',
        'pass' => 's:pass:auth.pass',
        'group_policy_id' => 's:str:auth.group_policy_id',
        'active' => 's:check:auth.active',
        'lastlogin' => 's:str:auth.lastlogin',
        'name' => 's:str:auth.name',
        'email' => 's:str:auth.email',
        'cache' => 's:html:auth.cache',
        'version' => 's:str:auth.version'
 */

}
