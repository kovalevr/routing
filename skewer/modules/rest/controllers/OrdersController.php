<?php

namespace skewer\modules\rest\controllers;

use Yii\helpers\ArrayHelper;
use skewer\components\auth\CurrentUser;
use skewer\build\Adm\Order\model\Status;
use skewer\build\Adm\Order\model\Payments;
use skewer\build\Adm\Order\model\ChangeStatus;
use skewer\build\Adm\Order;
use skewer\components\catalog\GoodsSelector;
use skewer\build\Page\Cart;
use skewer\build\Page\Cart\OrderForm;
use skewer\build\Adm\Order\Service;
use skewer\components\catalog\Card;

/**
 * Работа с заказами через rest
 * Class OrdersController
 * @package skewer\modules\rest\controllers
 */
class OrdersController extends PrototypeController {

    /** Список полей для списка заказов */
    const LIST_ORDER_FIELDS = "id, date, status, type_payment, type_delivery, token";
    /** Список полей детальной информации заказа */
    const DETAIL_ORDER_FIELDS = "id, date, address, person, phone, mail, postcode, status, type_payment, type_delivery, token";
    /** Список полей товаров в заказе */
    const DETAIL_GOODS_FIELDS = "id_goods, title, price, count, total";
    /** Список полей статусов в истории заказа (без пробелов) */
    const DETAIL_GOODS_HISTORY_FIELDS = "change_date,status_old,status_new";

    /** Детальная информация заказа */
    public function actionView($id, $sToken = '') {

        if (!$sToken and !UsersController::isLogged())
            return $this->showError(UsersController::ERR_NOAUTH);

        $oOrderQuery = Order\ar\Order::find()
            ->fields(self::DETAIL_ORDER_FIELDS)
            ->asArray();

        if ($sToken)
            $oOrderQuery->where('token', $sToken);
        else
            $oOrderQuery
                ->where('auth', CurrentUser::getId())
                ->where('id', $id);

        $aOrder = $oOrderQuery->getOne();
        if (!$aOrder) return '';
        $iOrderId = $aOrder['id'];

        $aStatusList   = ArrayHelper::map(Status::find()->all(), 'id', 'title');
        $aPaymentList  = ArrayHelper::map(Payments::find()->asArray()->all(), 'id', 'title');
        $aDeliveryList = ArrayHelper::map(Order\ar\TypeDelivery::find()->asArray()->getAll(), 'id', 'title');
        /** @var array $aHistoryList */
        $aHistoryList  = ChangeStatus::find()->where(['id_order'=> $iOrderId])->asArray()->all();

        // Добавить историю заказа
        if ($aHistoryList) {
            $aAllowedFields = explode(',', self::DETAIL_GOODS_HISTORY_FIELDS);

            foreach ($aHistoryList as $key => &$paItem) {
                $paItem['status_old'] = $aStatusList[$paItem['id_old_status']];
                $paItem['status_new'] = $aStatusList[$paItem['id_new_status']];

                $paItem = array_intersect_key($paItem, array_flip($aAllowedFields));
            }
        }

        $sCurrency = \Yii::t('Order', 'current_currency');

        // Добавление изображение товара
        $aGoodsOrder = Order\ar\Goods::find()->fields(self::DETAIL_GOODS_FIELDS)->where('id_order', $iOrderId)->asArray()->getAll() ?: [];
        foreach($aGoodsOrder as &$paGoodOrder) {

            $aGoodData = GoodsSelector::get($paGoodOrder['id_goods'], Card::DEF_BASE_CARD, true) ?: [];
            $paGoodOrder += [
                'gallery' => ArrayHelper::getValue($aGoodData, "fields.gallery.gallery.images.0.images_data", ''),
            ];
        }

        // Обработать/добавить дополнительные поля в выдачу
        $aOrder = array_merge($aOrder, [
            'status'        => $aStatusList[$aOrder['status']],
            'type_payment'  => $aPaymentList[$aOrder['type_payment']],
            'type_delivery' => $aDeliveryList[$aOrder['type_delivery']],
            'currency'      => "$sCurrency",
            'total_price'   => Order\Api::getOrderSum($iOrderId),
            'goods'         => $aGoodsOrder,
            'history'       => $aHistoryList,
        ]);

        return $aOrder + ['order' => &$aOrder]; // @todo выпилить дублирование подмассива 'order' после обновления мобильного приложения.
    }

    /** Получение заказов по id, токену или состоянию авторизации текущего пользователя */
    public function actionIndex() {
        // (!) При успешной обработке запроса, но при отсутвии позиций, списковые методы должны отдавать пустой массив, а не строку
        // (!) Заголовки постраничника нужно отдавать всегда при успешной обработке запроса

        $iId = \Yii::$app->request->get('id', '');
        $sToken = \Yii::$app->request->get('token', '');

        if ($iId or $sToken)
            return $this->actionView($iId, $sToken);

        if (!UsersController::isLogged())
            return $this->showError(UsersController::ERR_NOAUTH);

        $iPage   = abs((int)\Yii::$app->request->get('page', 0)) ?: 1;
        $iOnPage = abs((int)\Yii::$app->request->get('onpage', 0)) ?: 10;
        ($iOnPage <= 50) or ($iOnPage = 50); // Ограничить до 50 позиций на одной странице

        $iTotalCount = 0;
        /** @var array $aOrders */
        $aOrders = Order\ar\Order::find()
            ->fields(self::LIST_ORDER_FIELDS)
            ->where('auth', CurrentUser::getId())
            ->order('id', 'DESC')
            ->limit($iOnPage, $iOnPage * ($iPage - 1))
            ->setCounterRef($iTotalCount)
            ->asArray()
            ->get();

        // ! Постраничник должен устанавливаться для списков даже при пустой выборке
        $this->setPagination($iTotalCount, ceil($iTotalCount / $iOnPage), $iPage, $iOnPage);

        if (!$aOrders) return [];

        $aStatusList   = ArrayHelper::map(Status::find()->all(), 'id', 'title');
        $aPaymentList  = ArrayHelper::map(Payments::find()->asArray()->all(), 'id', 'title');
        $aDeliveryList = ArrayHelper::map(Order\ar\TypeDelivery::find()->asArray()->getAll(), 'id', 'title');
        $sCurrency     = \Yii::t('Order', 'current_currency');

        // Обработать/добавить дополнительные поля в выдачу
        foreach($aOrders as &$aOrder)
            $aOrder = array_merge($aOrder, [
                'status'                => $aStatusList[$aOrder['status']],
                'type_payment'          => $aPaymentList[$aOrder['type_payment']],
                'type_delivery'         => $aDeliveryList[$aOrder['type_delivery']],
                'currency'              => "$sCurrency",
                'total_price'           => $iTotalPrice = Order\Api::getOrderSum($aOrder['id']),
            ]);

        return $aOrders;
    }

    /** Совершение заказа */
    public function actionCreate() {

        $aData = \Yii::$app->request->post();

        // Если есть заказы
        if ( isset($aData['objid']) and $aData['objid'] and is_array($aData['objid']) ) {

            // Сперва очистить корзину
            Cart\Api::setOrder(new Cart\Order());

            // Добавление в корзину
            foreach ($aData['objid'] as $iKey => &$iItemId)
                Cart\Api::setItem( $iItemId, isset($aData['count'][$iKey]) ? (int)$aData['count'][$iKey] : 1 );

            // Оформление заказа, если есть заказанные позиции
            $oOrder = Cart\Api::getOrder();
            if ( $oOrder->getTotalCount() ) {

                /* @var array Данные пользователя */
                $aUserData = ( isset($aData['user']) and $aData['user'] and is_array($aData['user']) ) ? $aData['user'] : [];

                // Заполнить данные авторизованного пользователя, совместив поля имени пользователя и email таблиц заказов и клиентов
                $aAuthData           = UsersController::isLogged() ? CurrentUser::getProperties() : [];
                $aAuthData['person'] = isset($aAuthData['name']) ? $aAuthData['name'] : 'MobileUser';
                $aAuthData['mail']   = isset($aAuthData['login']) ? $aAuthData['login'] : '';

                $oForm = new OrderForm();
                $oForm->setFast(false);
                $oForm->load($aUserData + $aAuthData + [
                                 'person'  => 'MobileUser',
                                 'mail'    => '',
                                 'address' => '',
                                 'phone'   => '',

                                 'is_mobile' => 1, // Флаг заказа из мобильного приложения
                                 'tp_pay'    => 9, // Тип оплаты: Оплата наличными
                                 'tp_deliv'  => 4, // Тип доставки: Самовывоз
                                 'hash'      => $oForm->getHash(),
                             ]);

                $oService = new Service();
                /** @var Order\ar\OrderRow $oOrder */
                if ( $iOrderId = $oService->saveOrderForm($oForm) and
                     $oOrder = Order\ar\Order::find($iOrderId) )
                    return $this->actionView(0, $oOrder->token);
            }
        }

        return $this->showError(self::ERR_OTHER);
    }
}