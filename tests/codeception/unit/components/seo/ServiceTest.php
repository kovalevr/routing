<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2015
 * Time: 13:25
 */

namespace unit\components\seo;

use skewer\components\search;
use skewer\build\Adm\Tree;
use skewer\base\section;
use skewer\components\seo\Service;

class ServiceTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Service
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Service();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }
    /**
     * @covers skewer\components\seo\Service::rebuildSearchIndex
     */
    public function testRebuildSearchIndex(){

        $this->assertTrue(is_array(search\Selector::create()->searchText('Новый раздел')->find()));//если есть всё норм

        $this->object->rebuildSearchIndex();

        section\Tree::addSection( 70, 'Новый', 7, 'dsfsdfsdf' );

        $aFound = search\Selector::create()->searchText('Новый раздел')->find();
        $this->assertArrayHasKey('count', $aFound);

        $this->assertSame(0, $aFound['count']);

    }
    /**
     * @covers skewer\components\seo\Service::makeSearchIndex
     */
    public function testMakeSearchIndex(){
       /* for($i = 1; $i <= 1000; $i++){
            \Tree::addSection(70,'Новый111111111',7,$i);
        }*/




        $this->object->rebuildSearchIndex();
        $this->object->makeSearchIndex();
        $this->assertTrue(is_array(search\Selector::create()->searchText('Новый раздел')->find()),'fail');
        $this->assertInternalType('integer', $this->object->makeSearchIndex());

    }

    /**
     * @covers skewer\components\seo\Service::updateSearchIndex
     */

    public function testUpdateSearchIndex() {

       // $this->assertInternalType('integer', $this->object->updateSearchIndex());

    }
    /**
     * @covers skewer\components\seo\Service::updateSiteMap
     * @todo   testUpdateSiteMap().
     */
    public function testUpdateSiteMap() {
//        $this->assertInternalType('integer', $this->object->updateSiteMap());
    }

    /**
     * @covers skewer\components\seo\Service::makeSiteMap
     * @todo   testMakeSiteMap().
     */
    public function testMakeSiteMap(){
//        $this->assertInternalType('integer', $this->object->MakeSiteMap());
    }


    /**
     * @covers skewer\components\seo\Service::setNewDomainToSiteMap
     * @todo   testSetNewDomainToSiteMap().
     */
    public function testSetNewDomainToSiteMap(){
//        $this->assertInternalType('boolean', $this->object->SetNewDomainToSiteMap());
    }
    /**
     * @covers skewer\components\seo\Service::updateRobotsTxt
     * @todo   testUpdateRobotsTxt().
     */
    public function testUpdateRobotsTxt(){
//        $this->assertInternalType('boolean', $this->object->UpdateRobotsTxt('adawdwaaw'));
    }
    /**
     * @covers skewer\components\seo\Service::getPageList
     * @todo   testGetPageList().
     */
 /*   public function testGetPageListPrivate(){
        $this->object->rebuildSearchIndex();

        \Tree::addSection(70,'Новый111111111',7,'2342');
        $this->object->makeSearchIndex();
        $this->assertTrue(is_array(Search\Api::executeSearch('Новый раздел')),'fail');

        $this->assertInternalType('integer', $this->object->makeSearchIndex());

        for($i = 1; $i <= 1000; $i++) {
            $this->assertTrue(is_array($this->object->GetPageListPrivate($i)), 'Нифига');
        }
        $this->assertTrue(is_array($this->object->GetPageListPrivate(array('12324','dfsdf','1.4'))), 'Нифига');
    }*/



}
