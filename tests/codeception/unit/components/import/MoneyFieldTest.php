<?php

namespace unit\components\import;


use skewer\components\import\Api;
use skewer\components\import\ar\ImportTemplate;;
use skewer\components\import\field\Money;
use skewer\components\import\Task;
use skewer\base\queue;
use \skewer\components\catalog;

class MoneyFieldTest extends \PHPUnit_Framework_TestCase {

    public function providerMoney() {
        return [
            ['123,456,789',123456789.0],
            ['123.456.789',123456789.0],

            ['123456.789',123456.789],
            ['123456,789',123456.789],


            ['123,456,789.123',123456789.123],
            ['123.456.789,123',123456789.123],

            ['123',123.0],
            ['123руб.',123.0],
            ['123руб',123.0],
            ['123 руб',123.0],
            ['123$',123.0],

            ['123.',123.0],
            ['0.123',0.123],
            ['.123',0.123],

            ['.',0.0],
            ['',0.0],
            [' ',0.0],
            ['      ',0.0],

        ];
    }

    /**
     * @dataProvider providerMoney
     */
    public function testMoney( $sIn, $fOut ){

        $iTpl = ImportTemplate::getNewRow([
            'card' => catalog\Card::DEF_BASE_CARD,
            'type' => Api::Type_File,
            'settings' => json_encode([])
        ])->save();

        $iTask = queue\Api::addTask([
            'class' => '\skewer\components\import\Task',
            'priority' => queue\Task::priorityHigh,
            'resource_use' => Task::weightLow,
            'title' => \Yii::t( 'import', 'task_title', 'test'),
            'parameters' => ['tpl' => (int)$iTpl]
        ]);

        $oTask = queue\Api::getTaskById( $iTask );

        $oTask->init();

        $o = new Money(['val'],'price', $oTask);

        $o->loadData(['val'=>$sIn]);

        $this->assertSame($fOut, $o->getValue());


    }

}