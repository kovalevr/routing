<?php

namespace unit\components\search;
use skewer\components\search\Api;
use skewer\components\search\SearchIndex;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-09-05 at 12:56:35.
 */
class ApiTest extends \PHPUnit_Framework_TestCase {

    /** @var int счетчик для записей */
    public $iCnt = 0;

    const defSection = 123456;

    protected function tearDown() {
        // грохнуть все тестовые записи
        SearchIndex::delete()
            ->where( 'class_name', 'Test' )
            ->get();

        SearchIndex::delete()
            ->where( 'class_name', 'CatalogViewer' )
            ->where( 'section_id', static::defSection )
            ->get();
    }

    /**
     * @covers skewer\components\search\Api::removeFromIndexBySection
     */
    public function testAddToIndex() {

        $this->assertSame( 0, Api::removeFromIndexBySection(self::defSection), 'уже есть данные' );

        // добавление
        $oRow = SearchIndex::getNewRow();
        $oRow->search_title = '1';
        $oRow->status = 1;
        $oRow->use_in_search = 1;
        $oRow->use_in_sitemap = 1;
        $oRow->search_text = '1';
        $oRow->object_id = 1231;
        $oRow->class_name = 'Test';
        $oRow->section_id = self::defSection;
        $oRow->language = \Yii::$app->language;
        $oRow->href = '';
        $oRow->save();

        $oRow = SearchIndex::getNewRow();
        $oRow->search_title = '2';
        $oRow->status = 1;
        $oRow->use_in_search = 1;
        $oRow->use_in_sitemap = 1;
        $oRow->search_text = '2';
        $oRow->object_id = 1232;
        $oRow->class_name = 'Test';
        $oRow->section_id = self::defSection;
        $oRow->language = \Yii::$app->language;
        $oRow->href = '';
        $oRow->save();

        $this->assertSame( 2, Api::removeFromIndexBySection(self::defSection), 'не верный ответ при удалении' );
        $this->assertSame( 0, Api::removeFromIndexBySection(self::defSection),
            'не верный ответ при отсутствии данных к удалению' );

        $this->assertNull( Api::get( 'Test', 1231 ), 'запись не удалена' );

    }

    /**
     * @covers skewer\components\search\Api::getByHref
     */
    public function testGetByHref() {

        $sHref = '/awegdasfhr/jsdkjsgkrtdsk/srktjerjw/';

        $this->assertNull( Api::getByHref($sHref) );

        $oRow = SearchIndex::getNewRow();
        $oRow->href = $sHref;
        $oRow->save();

        $this->assertInstanceOf( 'skewer\components\search\Row', Api::getByHref($sHref) );

        $oRow->delete();

        $this->assertNull( Api::getByHref($sHref) );

    }

}
