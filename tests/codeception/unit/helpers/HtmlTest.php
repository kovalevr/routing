<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 26.05.2016
 * Time: 10:29
 */

namespace unit\helpers;


use skewer\helpers\Html;

class HtmlTest extends \PHPUnit_Framework_TestCase {

    public function providerHasContent() {
        return [
            ['', false],
            ['   ', false],
            ['<img src="HtmlTest.php">', true],
            ['<br>', false],
            ['<br />', false],
            ['<div></div>', false],
            ['<p></p>', false],
            ['<p>asd</p>', true],
            ['​a', true],
            ['a', true],
            ["\xE2\x80\x8B", false],
            ["\xE2\x80\x8Basdf", true],
            ["\r\n", false]
        ];
    }

    /**
     * @dataProvider providerHasContent
     */
    public function testHasContent($sIn, $bRes) {
        $this->assertSame($bRes, Html::hasContent($sIn), "[$sIn] is not [".($bRes?'true':'false')."]");
    }

}