<?php

// add unit testing specific bootstrap code here

use skewer\components\auth\Auth;
use skewer\components\gateway;
use skewer\base\Twig;

define('IS_UNIT_TEST', true);

if ( !INCLUSTER )
    die("Site is not in cluster! Can't create base for test.");


$config = require(RELEASEPATH.'/config/console.php');
new \skewer\app\Application($config);

\skewer\helpers\Files::init(FILEPATH, PRIVATE_FILEPATH);

\skewer\base\log\Logger::init(ROOTPATH.'log/access.log');

// запросить создание тестовой базы у контроллера кластера
try {

    $oClient = gateway\Api::createClient();

    $oBootstrap = new TestBootStrap();

    $oClient->addMethod('HostTools', 'createTestBase', array(), array($oBootstrap, 'initialize') );

    if(!$oClient->doRequest()) throw new gateway\Exception($oClient->getError());

} catch(gateway\Exception $e) {
    die('Test base was not created: '.$e->getMessage()."\r\n\r\n");
}

class TestBootStrap {

    protected $sBaseName = '';

    /** @var array набор доступов */
    public  $aAccess;

    public function initialize( $mResult ) {

        $aDBConf = $mResult;
        $this->aAccess = $aDBConf;
        if ( !$aDBConf )
            throw new gateway\Exception('Gateway answer is empty');

        echo "Тестовая база [".$aDBConf['name']."] создана\n\n";

        \Yii::$app->setComponents([
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host='.$aDBConf['host'].';dbname='.$aDBConf['name'],
                'username' => $aDBConf['user'],
                'password' => $aDBConf['pass'],
                'charset' => 'utf8',
            ]
        ]);

        Auth::init();

        // инициализация событий
        \Yii::$app->register->initEvents();

        require_once( ROOTPATH.'tests/codeception/unit/data/TestHelper.php' );

        Twig::Load(
            array(),
            \Yii::$app->getParam(['cache','rootPath']).'Twig/',
            \Yii::$app->getParam(['debug','parser']) or YII_DEBUG or YII_ENV_DEV
        );

    }

    /**
     * Удаляет тестовую базу после использования
     * @throws gateway\Exception
     */
    public function deleteBase() {

        try {

            $oClient = gateway\Api::createClient();

            $oClient->addMethod('HostTools', 'deleteTestBase', array($this->aAccess), array($this,'onBaseDelete') );

            if(!$oClient->doRequest()) throw new gateway\Exception($oClient->getError());

        } catch(gateway\Exception $e) {
            die('Test base was not deleted: '.$e->getMessage()."\r\n\r\n");
        }

    }

    /**
     * Callback при удалении тестовых доступов SMS'кой
     * @param $mResult
     * @param $mError
     */
    public function onBaseDelete( $mResult, $mError ) {

        if ( !$mResult )
            echo "Test base was not deleted: $mError";
        else
            echo "\nТестовая база [".$this->aAccess['name']."] удалена\n\n";


    }

}

register_shutdown_function( array($oBootstrap,'deleteBase') );

